#pragma  once

#ifndef FEATURE_ATTRIBUTES_H
#define FEATURE_ATTRIBUTES_H
//---------------------------------------------------------------------------------------------------

#include "Engine/Core/RgbaColors.hpp"

#include <string>
#include <vector>
#include "Engine/TinyXml/tinyxml.h"

enum FeatureTypes
{
	FEATURE_TYPE_UNSPECIFIED,
	FEATURE_TYPE_DOOR,
	FEATURE_TYPE_TRAP,
	FEATURE_TYPE_LIGHT_SOURCE,
	FEATURE_TYPE_SWICH,
};

//---------------------------------------------------------------------------------------------------
struct FeatureAttributes
{
public:
	std::string			m_name;
	char				m_onGlyph;
	char				m_offGlyph;
	FeatureTypes		m_itemType;
	bool				m_inOn;
};

#endif