#pragma  once

#ifndef ITEM_ATTRIBUTES_H
#define ITEM_ATTRIBUTES_H
//---------------------------------------------------------------------------------------------------

#include "Engine/Core/RgbaColors.hpp"

#include <string>
#include <vector>
#include "Engine/TinyXml/tinyxml.h"

enum ItemType
{
	IT_WEAPON,
	IT_HEADARMOR,
	IT_HANDARMOR,
	IT_LEGSARMOR,
	IT_CHESTARMOR,
	IT_SHIELD,
	IT_POTION,
	IT_SPECIAL_POTION,
	IT_ITEMTYPES
};


//---------------------------------------------------------------------------------------------------
struct ItemAttributes
{
public:
	std::string			m_name;
	char				m_glyph;
	RgbaColors				m_color;
	float				m_health;
	ItemType			m_itemType;
	bool				m_isInMap;
	int					m_debugLevel;
	float				m_attrib;	
};

#endif