#pragma  once

#ifndef NPC_ATTRIBUTES_H
#define NPC_ATTRIBUTES_H
//---------------------------------------------------------------------------------------------------

#include "Engine/Core/RgbaColors.hpp"

#include <string>
#include <vector>
#include "Engine/TinyXml/tinyxml.h" 

class AIBehaviorInterface;
//---------------------------------------------------------------------------------------------------
struct NPCAttributes
{
public:
	std::string					m_name;
	char						m_glyph;
	RgbaColors					m_color;
	float						m_health;
	float						m_minDamage;
	std::string					m_faction;
	bool						m_isInMap;
	int							m_speed;
	int							m_debugLevel;
	std::vector<std::string>	m_npcBehaviorNames;
	std::vector<std::string>	m_npmBehaviorFilePaths;
	float						m_maxDamage;
	int							m_chanceToHit;
	int							m_wallCost;
	int							m_airCost;
	int							m_waterCost;
	int							m_lavaCost;
};

#endif