#include "Engine/Behaviors/AIBehaviorInterface.hpp"
#include "Engine/Registrations/AIBehaviorRegistration.hpp"

#include <map>

//---------------------------------------------------------------------------------------------------
AIBehaviorInterface::AIBehaviorInterface(TiXmlElement* elem)
	:m_agent(nullptr)
{
	UNUSED(elem);
}

bool AIBehaviorInterface::CompareName(const std::string& nameToCheck)
{
	return (m_behaviorName == nameToCheck);
}

void AIBehaviorInterface::SetAgent(Agent* owningAgent)
{
	m_agent = owningAgent;
}

//---------------------------------------------------------------------------------------------------
AIBehaviorInterface::~AIBehaviorInterface(void)
{

}
