#pragma once

#ifndef AI_BEHAVIOUR_INTERFACE_H
#define AI_BEHAVIOUR_INTERFACE_H
//---------------------------------------------------------------------------------------------------
#include "Engine/TinyXml/tinyxml.h"
#include "Engine/Map/Map.hpp"
#include <string>
//---------------------------------------------------------------------------------------------------

class Agent;

//---------------------------------------------------------------------------------------------------
class AIBehaviorInterface
{
public:
	virtual ~AIBehaviorInterface(void);


	virtual bool Think(Map* mapPointer) = 0;
	virtual float GetChance(Map* mapPointer) = 0;
	virtual void SetAgent(Agent* owningAgent);
	virtual void Initialize(const std::string& name){m_behaviorName = name;}
	virtual void ClearData(){}
	bool CompareName(const std::string& nameToCheck);

protected:
	AIBehaviorInterface(TiXmlElement* elem);
	Agent*		m_agent;
	std::string	m_behaviorName;
};

#endif