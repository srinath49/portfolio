#include "Engine/CombatSystem/CombatManager.hpp"
#include "Engine/Entities/Agent.hpp"
#include "Engine/MessageSystem/MessageSystem.hpp"
#include "Engine/Entities/Item.hpp"
#include "Engine/DebugConsole/ConsolePrintf.hpp"


//---------------------------------------------------------------------------------------------------
void PerformCombat(const AttackInfo& attackInstigatorInfo, Agent* defender)
{
	if (defender)
	{
		DefenceInfo defenderData; 
		defender->FillDefenceInfo(defenderData);

		float damageToApply = 0.0f;
		if (attackInstigatorInfo.m_primaryWeapon)
		{
			damageToApply = attackInstigatorInfo.m_primaryWeapon->m_attrib * attackInstigatorInfo.m_damageChance;
		}
		else
		{
			damageToApply = attackInstigatorInfo.m_damage * attackInstigatorInfo.m_damageChance;
		}

		float defenceAmount = 0.f;

		if (!defenderData.m_armGear && !defenderData.m_headGear && !defenderData.m_legGear && !defenderData.m_legGear && !defenderData.m_shield)
		{
			defenceAmount = defenderData.m_armor * defenderData.m_defenceChance;
		}
		else
		{
			if (defenderData.m_armGear)
			{
				defenceAmount += defenderData.m_armGear->m_attrib * defenderData.m_defenceChance;
			}
			if (defenderData.m_headGear)
			{
				defenceAmount += defenderData.m_headGear->m_attrib * defenderData.m_defenceChance;
			}
			if (defenderData.m_legGear)
			{
				defenceAmount += defenderData.m_legGear->m_attrib * defenderData.m_defenceChance;
			}
			if (defenderData.m_torsoGear)
			{
				defenceAmount += defenderData.m_torsoGear->m_attrib * defenderData.m_defenceChance;
			}
			if (defenderData.m_shield)
			{
				defenceAmount += defenderData.m_shield->m_attrib * defenderData.m_defenceChance;
			}
		}

		damageToApply -= defenceAmount;
		if (attackInstigatorInfo.m_primaryWeapon)
		{
			ClampFloat(damageToApply, 0.0f, attackInstigatorInfo.m_primaryWeapon->m_attrib);
		}
		else
		{
			ClampFloat(damageToApply, 0.0f, attackInstigatorInfo.m_damage);
		}


		bool isDefenderDead = defender->ApplyDamage(damageToApply);
		std::string messageToDisplay = attackInstigatorInfo.m_attackerName;
		messageToDisplay += " attacked the ";
		messageToDisplay += defenderData.m_defenderName;
		messageToDisplay += " and caused %.f damage";
		messageToDisplay = ConsolePrintf(messageToDisplay.c_str(), damageToApply);
		RgbaColors messageColor(1.f, 0.f, 0.f);
		MessageSystem::AddMessageToList(messageToDisplay, messageColor);
		if (isDefenderDead)
		{
			std::string DeathMessage = "The ";
			DeathMessage += defenderData.m_defenderName;
			DeathMessage += " died";
			MessageSystem::AddMessageToList(DeathMessage, RgbaColors(1.f, 1.f, 1.f));
		}
	}
}