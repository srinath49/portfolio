#include "AABB2.hpp"


AABB2::AABB2(const Vector2DF& topLeft, const Vector2DF& bottomLeft, const Vector2DF& bottomRight, const Vector2DF& topRight) : 
	m_topLeft(topLeft),
	m_bottomLeft(bottomLeft),
	m_bottomRight(bottomRight),
	m_topRight(topRight)
{

}


AABB2::~AABB2(void)
{

}

void AABB2::Normalize()
{
	m_topLeft.Normalize();
	m_bottomLeft.Normalize();
	m_bottomRight.Normalize();
	m_topRight.Normalize();
}
