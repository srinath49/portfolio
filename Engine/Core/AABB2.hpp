#pragma once

#ifndef  AABB2_H 
#define AABB2_H

#include "Engine/Math/SriMath.hpp"


class AABB2
{
public:
	AABB2(const Vector2DF& topLeft = Vector2DF(), const Vector2DF& bottomLeft = Vector2DF(), const Vector2DF& bottomRight = Vector2DF(), const Vector2DF& topRight = Vector2DF());
	~AABB2(void);
	void Normalize();
	Vector2DF m_topLeft;
	Vector2DF m_bottomLeft;
	Vector2DF m_bottomRight;
	Vector2DF m_topRight;
};

#endif