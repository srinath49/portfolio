#include "BasicShape.hpp"
#include "Engine/Rendering/Material.hpp"

BasicShape::BasicShape(unsigned int shaderProgramId) : 
	Entity(shaderProgramId)
{
	LoadMaterial();
}


BasicShape::~BasicShape(void)
{

}

void BasicShape::LoadMeshData()
{

}

void BasicShape::LoadMaterial()
{
	std::vector<std::string> textures;
	m_material = new Material(m_shaderProgramId, textures);
}
