#pragma once

#ifndef BASIC_SHAPE_H
#define BASIC_SHAPE_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Core/Entity.hpp"


//---------------------------------------------------------------------------------------------------
class BasicShape : public Entity
{
public:
	BasicShape(unsigned int shaderProgramId);
	virtual ~BasicShape(void);

	virtual void LoadMeshData();
	virtual void LoadMaterial();
};

#endif