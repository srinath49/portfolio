#include "Camera3D.hpp"
#include "Engine\Input\InputSystem.hpp"
#include "Engine\Core\EngineCommons.hpp"

//---------------------------------------------------------------------------------------------------
Camera3D::Camera3D(void): 
	m_orientation(),
	m_position(),
	m_forwardVector(),
	m_movementVector(),
	m_forwardVectorXYZ(),
	m_cameraLeftXYVector(),
	m_cameraUpVector(Vector3DF(0.f, 0.f, 1.f)),
	m_cameraSpeed(15.f)
{

}

//---------------------------------------------------------------------------------------------------
Camera3D::Camera3D(const Camera3D& cameraToCopy) :
	m_orientation(cameraToCopy.m_orientation),
	m_position(cameraToCopy.m_position),
	m_forwardVector(cameraToCopy.m_forwardVector),
	m_movementVector(cameraToCopy.m_movementVector),
	m_forwardVectorXYZ(cameraToCopy.m_forwardVectorXYZ),
	m_cameraLeftXYVector(cameraToCopy.m_cameraLeftXYVector),
	m_cameraUpVector(cameraToCopy.m_cameraUpVector),
	m_cameraSpeed(cameraToCopy.m_cameraSpeed)
{

}

//---------------------------------------------------------------------------------------------------
void Camera3D::UpdateCamera(float deltaSeconds)
{
	UpdatePosition(deltaSeconds);
}

//---------------------------------------------------------------------------------------------------
void Camera3D::ProcessInput(InputSystem* inputSystem)
{
	if(!inputSystem)
	{
		return;
	}
	if (inputSystem->IsKeyDown(KEY_CODE_W))
	{
		m_movementVector.x += m_forwardVector.x;
		m_movementVector.y += m_forwardVector.y;
		m_movementVector.z += m_forwardVector.z;
	}
	if (inputSystem->IsKeyDown(KEY_CODE_S))
	{
		m_movementVector.x -= m_forwardVector.x;
		m_movementVector.y -= m_forwardVector.y;
		m_movementVector.z -= m_forwardVector.z;
	}
	if (inputSystem->IsKeyDown(KEY_CODE_A))
	{
		m_movementVector.x += m_cameraLeftXYVector.x;
		m_movementVector.y += m_cameraLeftXYVector.y;
		m_movementVector.z += m_cameraLeftXYVector.z;
	}
	if (inputSystem->IsKeyDown(KEY_CODE_D))
	{
		m_movementVector.x -= m_cameraLeftXYVector.x;
		m_movementVector.y -= m_cameraLeftXYVector.y;
		m_movementVector.z -= m_cameraLeftXYVector.z;
	}
	if (inputSystem->IsKeyDown(KEY_CODE_SPC))
	{
		m_movementVector.x += m_cameraUpVector.x;
		m_movementVector.y += m_cameraUpVector.y;
		m_movementVector.z += m_cameraUpVector.z;
	}
	if (inputSystem->IsKeyDown(KEY_CODE_C))
	{
		m_movementVector.x -= m_cameraUpVector.x;
		m_movementVector.y -= m_cameraUpVector.y;
		m_movementVector.z -= m_cameraUpVector.z;
	}
	if (inputSystem->IsKeyDown(KEY_CODE_O))
	{
		m_position.x = 0.f;
		m_position.y = 0.f;
		m_position.z = 0.f;
	}

	if (inputSystem->IsKeyDown(KEY_CODE_PLUS))
	{
		m_cameraSpeed += 0.25f;
		ClampFloat(m_cameraSpeed, 0.0f, MAX_CAMERA_SPEED);
	}

	if (inputSystem->IsKeyDown(KEY_CODE_MINUS))
	{
		m_cameraSpeed -= 0.25f;
		ClampFloat(m_cameraSpeed, 0.25f, MAX_CAMERA_SPEED);
	}
}

void Camera3D::UpdatePosition(float deltaSeconds)
{
	//...and so on, for A,S,D moving left, back, right - and for E,C moving up, down.
	Vector3DF cameraPositionModifier = (m_movementVector * deltaSeconds) * m_cameraSpeed;
	m_position.x += cameraPositionModifier.x;
	m_position.y += cameraPositionModifier.y;
	m_position.z += cameraPositionModifier.z;
	m_movementVector.x = 0.f;
	m_movementVector.y = 0.f;
	m_movementVector.z = 0.f;
}
