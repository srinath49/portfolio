#pragma once

#include "Engine\Math\EulerAngles.hpp"
#include "Engine\Math\SriMath.hpp"

class InputSystem;

class Camera3D
{
public:
	Camera3D(void);

	Camera3D(const Camera3D& cameraToCopy);

	~Camera3D(void){}

	void UpdateCamera(float deltaSeconds);
	void ProcessInput(InputSystem* inputSystem);
	void UpdatePosition(float deltaSeconds);
	EulerAngles		m_orientation;
	Vector3DF		m_position;

	Vector3DF		m_forwardVector;
	Vector3DF		m_movementVector;
	Vector3DF		m_forwardVectorXYZ;
	Vector3DF		m_cameraLeftXYVector;
	Vector3DF		m_cameraUpVector;
	float			m_cameraSpeed;
};


