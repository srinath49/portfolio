#include "Engine\Core\EngineCommons.hpp"
#include "Engine\Rendering\SriOGLRenderer.hpp"
#include <io.h>

const int sizeofInt = sizeof(int);
const int sizeofUnsignedInt = sizeof(unsigned int);
const int sizeofFloat = sizeof(float);
const int sizeofDouble = sizeof(double);
const int sizeofShort = sizeof(short);
const int sizeofUnsignedShort = sizeof(unsigned short);
const int sizeofLong = sizeof(long);
const int sizeofUnsignedLong = sizeof(unsigned long);
const int sizeofBool = sizeof(bool);

//---------------------------------------------------------------------------------------------------
const float PI = 3.14159265f;
const float PI_OVER_ONE_EIGHTY = PI / 180.0f;
const float PI_OVER_THREE_SIXTY = PI / 360.0f;
const float ONE_EIGHTY_OVER_PI = 180.f/PI;
const float TWO_PI = PI*2;
const float PI_OVER_TWO = PI/2.f;
const float EPSILON = 0.00001f;

const float MAX_CAMERA_SPEED = 125.f;
const RgbaColors FOG_COLOR = RgbaColors(1.f, 1.f, 1.f, 1.f);
//const RGBA CLEAR_SCREEN_COLOR = FOG_COLOR;
const RgbaColors CLEAR_SCREEN_COLOR = RgbaColors(0.f, 0.f, 0.f, 1.f);
const float ASPECT = (16.f / 9.f);
const float FOVX = 70.f;
const float FOVY = (FOVX / ASPECT);
const float NEAR_CLIPPING_PLANE = 0.1f;
const float FAR_CLIPPING_PLANE = 1500.f;
const float REALLY_FAR = FAR_CLIPPING_PLANE;
const int MAX_LIGHTS = 16;
const float SCREEN_WIDTH = 1600.0f;
const float SCREEN_HEIGHT = 900.0f;

const Vector3DF NDC_TOP_LEFT = Vector3DF(-1.f, 1.f, 0.f);
const Vector3DF NDC_BOTTOM_LEFT = Vector3DF(-1.f, -1.f, 0.f);
const Vector3DF NDC_BOTTOM_RIGHT = Vector3DF(1.f, -1.f, 0.f);
const Vector3DF NDC_TOP_RIGHT = Vector3DF(1.f, 1.f, 0.f);
//---------------------------------------------------------------------------------------------------
bool g_enableGraphicsDebugging = false;
bool g_enableWindowsConsole = false;

const float DELTASECONDS = 1.0f/60.0f;

SriOGLRenderer* g_myRenderer = nullptr;

FileLoadingMode g_loadMode = FLM_FROM_DISK;

//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
float RadiansToDegrees(float radians)
{
	return radians * ONE_EIGHTY_OVER_PI;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
float DegreesToRadians(float degree)
{
	float returnRadians = degree * PI_OVER_ONE_EIGHTY;
	return returnRadians;
}
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
float DegreesToHalfRadians(float degree)
{
	return degree * PI_OVER_THREE_SIXTY;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void ClampInt(int& intToClamp, int min, int max)
{
	if (intToClamp < min)
	{
		intToClamp = min;
	}
	else if (intToClamp > max)
	{
		intToClamp = max;
	}
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void ClampFloat(float& floatToClamp, float min, float max)
{
	if (floatToClamp < min)
	{
		floatToClamp = min;
	}
	else if (floatToClamp > max)
	{
		floatToClamp = max;
	}
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
float GetDistanceSquared(const Vector3DF& position1, const Vector3DF& position2)
{
	float p1 = position2.x - position1.x;
	float p2 = position2.y - position1.y;
	float p3 = position2.z - position1.z;

	p1*=p1;
	p2*=p2;
	p3*=p3;
	return (p1+p2+p3);
}

//---------------------------------------------------------------------------------------------------
float GetDistanceSquared(const Vector2DI& position1, const Vector2DI& position2)
{
	float p1 = (float)(position2.x - position1.x);
	float p2 = (float)(position2.y - position1.y);

	p1*=p1;
	p2*=p2;

	return (p1+p2);
}

//---------------------------------------------------------------------------------------------------
bool IsPointOnLine(const Vector3DF& lineStart, const Vector3DF& lineEnd, const Vector3DF& point)
{
	float a = (lineEnd.y - lineStart.y) / (lineEnd.x - lineStart.x);
	float b = lineStart.y - a * lineStart.x;
	if ( fabs(point.y - (a*point.x+b)) < EPSILON)
	{
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
float Dot(Vector3DF vector1, Vector3DF vector2)
{
	float dot;

	dot = (vector1.x * vector2.x) + (vector1.y * vector2.y) + (vector1.z * vector2.z);

	return dot;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
Vector3DF Normalize(Vector3DF vectorToNormalize)
{
	Vector3DF vectorToReturn;

	float length = vectorToNormalize.GetLength();

	vectorToReturn.x = vectorToNormalize.x / length;
	vectorToReturn.y = vectorToNormalize.y / length;
	vectorToReturn.z = vectorToNormalize.z / length;

	return vectorToReturn;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
Vector2DF Normalize(Vector2DF vectorToNormalize)
{
	Vector2DF vectorToReturn;

	float length = vectorToNormalize.GetLength();

	vectorToReturn.x = vectorToNormalize.x / length;
	vectorToReturn.y = vectorToNormalize.y / length;

	return vectorToReturn;
}

//---------------------------------------------------------------------------------------------------
int GetRandomIntegerInRange(int min, int max)
{
	return min + (rand() % (max - min + 1));
}

//---------------------------------------------------------------------------------------------------
float GetRandomFloatInRange(float min, float max)
{
	float randomFloat = max - min;
	float percent = 1 / (float)RAND_MAX;
	percent *= rand();
	randomFloat *= percent;
	randomFloat += min;
	return randomFloat;
}

//---------------------------------------------------------------------------------------------------
void EnumerateFilesInFolder(std::string& directory, std::vector<std::string>& filesFound, std::vector<std::string>& directoriesFound, const std::string& dirToPushTo /*= "../RawData/Data/"*/, bool shouldRemovePrePath /*= true*/)
{
	int error = 0;
	struct _finddata_t fileInfo;
	intptr_t searchHandle = _findfirst(directory.c_str(), &fileInfo);

	while (searchHandle != -1 && !error)
	{
		bool isADirectory = fileInfo.attrib & _A_SUBDIR ? true : false;
		bool isHidden = fileInfo.attrib & _A_HIDDEN ? true : false;
		if(strcmp(fileInfo.name, ".") != 0 && strcmp(fileInfo.name, "..") != 0)
		{
			if (isADirectory)
			{
				std::string::iterator iter;
				std::string dir = "";
				for (iter = directory.begin(); iter != directory.end(); iter++)
				{
					char* star = "*";
					if(strcmp(&(*iter), star) == 0)
					{
						break;
					}
					dir += *iter;
				}
				dir += fileInfo.name;
				std::string dirToPush = dir;
				if (dirToPush.size() > 11)
				{
					size_t pos = dirToPush.find(dirToPushTo);
					if (shouldRemovePrePath)
					{
						dirToPush.erase(pos, 11);
					}
					
				}
				directoriesFound.push_back(dirToPush);
				//printf_s(dirToPush.c_str());
				//printf_s("\n");
				dir += "/*";
				EnumerateFilesInFolder(dir, filesFound, directoriesFound);
			}
			else if (!isHidden)
			{
				size_t pos = directory.find(dirToPushTo);
				std::string path = directory;
				if (shouldRemovePrePath)
				{
					path.erase(pos, 11);
				}
				
				std::string pathWithoutStar = "";
				std::string::iterator iter;
				for (iter = path.begin(); iter != path.end(); iter++)
				{
					char* star = "*";
					if(strcmp(&(*iter), star) == 0)
					{
						break;
					}
					pathWithoutStar += *iter;
				}
				pathWithoutStar += fileInfo.name;
				filesFound.push_back(pathWithoutStar);
				//printf_s(pathWithoutStar.c_str());
				//printf_s("\n");
			}
		}

		error = _findnext(searchHandle, &fileInfo);
	}
}

//---------------------------------------------------------------------------------------------------
int GetXPercentOfY(int xPercent, int yNumber)
{
	int xPercentOfY = (xPercent * yNumber) / 100;
	return xPercentOfY;
}

//---------------------------------------------------------------------------------------------------
float GetXPercentOfYFloat(float xPercent, float yNumber)
{
	float xPercentOfY = (xPercent * yNumber) / 100;
	return xPercentOfY;
}
