#pragma once

#ifndef ENGINE_COMMONS_H
#define ENGINE_COMMONS_H

#ifndef TIXML_USE_STL
#define TIXML_USE_STL
#endif

#include "Engine/Core/AABB2.hpp"

#include <string>
#include <vector>
#include <functional>
#include <map>

class SriOGLRenderer;
class RgbaColors;
class Vector3DF;
class Vector2DF;


//---------------------------------------------------------------------------------------------------
#define UNUSED(x) (void)(x);

//---------------------------------------------------------------------------------------------------
typedef std::vector<std::string> Strings;
typedef std::function<bool(Strings)> CommandLineFunction;
typedef std::map<std::string, CommandLineFunction> CommandsMap;
//---------------------------------------------------------------------------------------------------
extern SriOGLRenderer* g_myRenderer;
extern bool g_enableGraphicsDebugging;
extern bool g_enableWindowsConsole;

extern const float ASPECT;
extern const float FOVX;
extern const float FOVY;
extern const float NEAR_CLIPPING_PLANE;
extern const float FAR_CLIPPING_PLANE;
extern const float SCREEN_WIDTH;
extern const float SCREEN_HEIGHT;
extern const float PI;
extern const float PI_OVER_ONE_EIGHTY;
extern const float PI_OVER_THREE_SIXTY;
extern const float ONE_EIGHTY_OVER_PI;
extern const float TWO_PI;
extern const float PI_OVER_TWO;
extern const float EPSILON;

extern const RgbaColors FOG_COLOR;
extern const RgbaColors CLEAR_SCREEN_COLOR;
extern const int MAX_LIGHTS;
extern const float REALLY_FAR;

extern const Vector3DF NDC_TOP_LEFT;
extern const Vector3DF NDC_BOTTOM_LEFT;
extern const Vector3DF NDC_BOTTOM_RIGHT;
extern const Vector3DF NDC_TOP_RIGHT;

extern const float DELTASECONDS;

extern const float MAX_CAMERA_SPEED;
extern const int sizeofInt;
extern const int sizeofUnsignedInt;
extern const int sizeofFloat;
extern const int sizeofDouble;
extern const int sizeofShort;
extern const int sizeofUnsignedShort;
extern const int sizeofLong;
extern const int sizeofUnsignedLong;
extern const int sizeofBool;

enum FileLoadingMode
{
	FLM_FROM_DISK,
	FLM_FROM_ZIP,
	FLM_PREFER_DISK,
	FLM_PREFER_ZIP
};

extern FileLoadingMode g_loadMode;
//---------------------------------------------------------------------------------------------------
enum ShaderType
{
	ST_VERTEX_SHADER = 0x8B31,
	ST_FRAGMENT_SHADER = 0x8B30,
	ST_GEOMETRY_SHADER,
	ST_NUM_SHADER_TYPES = 3
};

enum SriShaderAttribLocations
{
	SSAL_VERTEXES = 1,
	SSAL_COLOR = 2,
	SSAL_TEXCOORDS = 3,
	SSAL_TANGENTS = 4,
	SSAL_BITANGENTS = 5,
	SSAL_NORMALS = 6,
};

struct GlyphMetaData
{
public:
	AABB2 m_textureCoords;
	float m_ttfA;
	float m_ttfB;
	float m_ttfC;

	GlyphMetaData(){}
	//char m_glyphSheetIndex;
};

//---------------------------------------------------------------------------------------------------
class Item;
//---------------------------------------------------------------------------------------------------
struct AttackInfo 
{
	float		m_damage;
	float		m_damageChance;
	std::string m_attackerName;
	Item*		m_primaryWeapon;
};

//---------------------------------------------------------------------------------------------------
struct DefenceInfo
{
	float		m_armor;
	float		m_defenceChance;
	std::string m_defenderName;
	Item*		m_headGear;
	Item*		m_torsoGear;
	Item*		m_legGear;
	Item*		m_armGear;
	Item*		m_shield;
};

//---------------------------------------------------------------------------------------------------
float RadiansToDegrees(float radians);

float DegreesToRadians(float degree);

float DegreesToHalfRadians(float degree);

void ClampInt(int& intToClamp, int min, int max);

void ClampFloat(float& floatToClamp, float min, float max);

float GetDistanceSquared(const Vector3DF& position1, const Vector3DF& position2);

float GetDistanceSquared(const Vector2DI& position1, const Vector2DI& position2);

bool IsPointOnLine(const Vector3DF& lineStart, const Vector3DF& lineEnd, const Vector3DF& point);

float Dot(Vector3DF vector1, Vector3DF vector2);

Vector3DF Normalize(Vector3DF vectorToNormalize);

Vector2DF Normalize(Vector2DF vectorToNormalize);

int GetRandomIntegerInRange(int min, int max);

float GetRandomFloatInRange(float min, float max);

void EnumerateFilesInFolder(std::string& directory, std::vector<std::string>& filesFound, std::vector<std::string>& directoriesFound, const std::string& dirToPushTo = "../RawData/", bool shouldRemovePrePath = true);

int GetXPercentOfY(int xPercent, int yNumber);

float GetXPercentOfYFloat(float xPercent, float yNumber);

//---------------------------------------------------------------------------------------------------

#endif