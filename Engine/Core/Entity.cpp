#include "Entity.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Model.hpp"

//---------------------------------------------------------------------------------------------------
Entity::Entity(unsigned int shaderProgramId) :
	m_shaderProgramId(shaderProgramId)
{
	m_mesh		= nullptr;
	m_material	= nullptr;
}

//---------------------------------------------------------------------------------------------------
Entity::~Entity(void)
{
	delete m_material;
	delete m_mesh;
}

//---------------------------------------------------------------------------------------------------
void Entity::Update()
{

}

//---------------------------------------------------------------------------------------------------
void Entity::Render()
{

}

//---------------------------------------------------------------------------------------------------
void Entity::LoadMeshAndMaterial()
{
	LoadMeshData();
}

//---------------------------------------------------------------------------------------------------
void Entity::LoadMeshDataFromOBJFile()
{
	
}

//---------------------------------------------------------------------------------------------------
void Entity::LoadMeshData()
{
	LoadMeshDataFromOBJFile(); // Should not be the default way of loading meshes. Fix Later!
}

//---------------------------------------------------------------------------------------------------
void Entity::LoadMaterial()
{
	if(!m_material)
	{
		// Load Material
	}
}