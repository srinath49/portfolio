#pragma once

#ifndef ENTITY_H
#define ENTITY_H
//---------------------------------------------------------------------------------------------------
#include <vector>
#include "Engine/Math/SriMath.hpp"

class Material;
class MeshVertexData;

//---------------------------------------------------------------------------------------------------
class Entity
{
public:
	Entity(unsigned int shaderProgramId);
	virtual ~Entity(void);

	virtual void Update();
	virtual void Render();

	MeshVertexData* GetMeshData() const { return m_mesh; }
	void SetMeshData(MeshVertexData* newMeshData) { m_mesh = newMeshData; }
	
	Material* GetMaterial() const { return m_material; }
	void SetMaterial(Material* newMaterial) { m_material = newMaterial; }

protected:
	void LoadMeshAndMaterial();
	void LoadMeshDataFromOBJFile();
	
	virtual void LoadMeshData();
	virtual void LoadMaterial();

	MeshVertexData*	m_mesh;
	Material*		m_material;
	int				m_shaderProgramId;
};

#endif