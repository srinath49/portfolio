#include "GameClock.hpp"
#include "Time.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/DebugConsole/ConsolePrintf.hpp"
#include "Engine/DebugConsole/BitmapFont.hpp"
//---------------------------------------------------------------------------------------------------

GameClock* GameClock::s_masterClock = nullptr;
//---------------------------------------------------------------------------------------------------

//************************************
// Method:    GameClock
// FullName:  GameClock::GameClock
// Access:    private 
// Returns:   
// Qualifier:
// Parameter: GameClock * parent
//************************************
GameClock::GameClock(GameClock* parent, double scale, double maxDelta)
	:m_isPaused(false)
	,m_timeScale(scale)
	,m_maxDelta(maxDelta)
	,m_timeAtLastAdvance(0.0)
{
	m_currentTime.QuadPart = 0;
	m_parentClock = parent;
	if (parent)
	{
		parent->m_childClocks.push_back(this);
	}
	m_oneOverTimeScale = 1.0/m_timeScale;
	m_delta = (m_timeScale <= m_maxDelta)?m_oneOverTimeScale:(1.0/m_maxDelta);
	Time::GetAbsoluteTimeSeconds();
}

//************************************
// Method:    ~GameClock
// FullName:  GameClock::~GameClock
// Access:    public 
// Returns:   
// Qualifier:
// Parameter: void
//************************************
GameClock::~GameClock(void)
{

}

//************************************
// Method:    CreateClock
// FullName:  GameClock::CreateClock
// Access:    public static 
// Returns:   GameClock*
// Qualifier:
// Parameter: GameClock * parentClock
//************************************
GameClock* GameClock::CreateClock(GameClock* parentClock, double scale, double maxDelta)
{
	GameClock* clockToReturn = nullptr;

	if (!s_masterClock)
	{
		s_masterClock = new GameClock(nullptr, scale, maxDelta);
		clockToReturn = s_masterClock;
	}
	else
	{
		if (parentClock)
		{
			clockToReturn = new GameClock(parentClock, parentClock->m_timeScale * scale, maxDelta);
		}
		else
		{
			clockToReturn = new GameClock(s_masterClock, s_masterClock->m_timeScale * scale, maxDelta);
		}
	}

	return clockToReturn;
}

//************************************
// Method:    RemoveClock
// FullName:  GameClock::RemoveClock
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void GameClock::RemoveClock()
{
	while (!m_childClocks.empty())
	{
		m_childClocks[0]->m_parentClock = nullptr;
		m_childClocks[0]->RemoveClock();
		m_childClocks[0] = m_childClocks.back();
		m_childClocks.pop_back();
		
	}
	if (m_parentClock)
	{
		m_parentClock->RemoveClockFromChildList(this);
		delete this;
	}
}

//************************************
// Method:    RemoveClockFromChildList
// FullName:  GameClock::RemoveClockFromChildList
// Access:    private 
// Returns:   void
// Qualifier:
// Parameter: GameClock * clockToRemove
//************************************
void GameClock::RemoveClockFromChildList(GameClock* clockToRemove)
{
	if (!m_childClocks.empty() && clockToRemove)
	{
		for (std::vector<GameClock*>::iterator clocksIter = m_childClocks.begin(); clocksIter != m_childClocks.end(); ++clocksIter)
		{
			if (*clocksIter == clockToRemove)
			{
				*clocksIter = m_childClocks.back();
				m_childClocks.pop_back();
				break;
			}
		}
	}
}

//************************************
// Method:    AdvanceTime
// FullName:  GameClock::AdvanceTime
// Access:    public static 
// Returns:   void
// Qualifier:
// Parameter: double deltaSeconds
//************************************
void GameClock::AdvanceTime(double deltaSeconds)
{
	s_masterClock->UpdateTime(deltaSeconds);
}

//************************************
// Method:    UpdateTime
// FullName:  GameClock::UpdateTime
// Access:    private 
// Returns:   void
// Qualifier:
// Parameter: double scaledDeltaTime
//************************************
void GameClock::UpdateTime(double deltaSeconds)
{
	if (m_isPaused)
	{
		return;
	}
	UpdateAlarms();
	double currentAbsoluteTimeSeconds = Time::GetAbsoluteTimeSeconds();
	if (currentAbsoluteTimeSeconds - m_timeAtLastAdvance >= m_delta)
	{
		m_currentTime.QuadPart += 1;
		m_timeAtLastAdvance = currentAbsoluteTimeSeconds;
	}
	for (std::vector<GameClock*>::iterator clocksIter = m_childClocks.begin(); clocksIter != m_childClocks.end(); ++clocksIter) 
	{
		(*clocksIter)->UpdateTime(deltaSeconds);
	}
}

//************************************
// Method:    PauseClock
// FullName:  GameClock::PauseClock
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void GameClock::PauseClock()
{
	m_isPaused = true;
	for (std::vector<GameClock*>::iterator clocksIter = m_childClocks.begin(); clocksIter != m_childClocks.end(); ++clocksIter) 
	{
		(*clocksIter)->PauseClock();
	}
}

//************************************
// Method:    UnPauseClock
// FullName:  GameClock::UnPauseClock
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void GameClock::UnPauseClock()
{
	m_isPaused = false;
	m_timeAtLastAdvance = Time::GetAbsoluteTimeSeconds();
	for (std::vector<GameClock*>::iterator clocksIter = m_childClocks.begin(); clocksIter != m_childClocks.end(); ++clocksIter) 
	{
		(*clocksIter)->UnPauseClock();
	}
}

//************************************
// Method:    TogglePause
// FullName:  GameClock::TogglePause
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void GameClock::TogglePause()
{
	if (m_isPaused)
	{
		UnPauseClock();
	}
	else
	{
		PauseClock();
	}

}

//************************************
// Method:    RenderTime
// FullName:  GameClock::RenderTime
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void GameClock::RenderTime(BitmapFont* bitmapFontRenderer)
{
	char* timeString;;
	timeString = ConsolePrintf("%ld", m_currentTime.QuadPart);
	if (bitmapFontRenderer)
	{
		bitmapFontRenderer->SetUpMyMaterial();
		bitmapFontRenderer->DrawString(timeString, RgbaColors(1.f, 1.f, 0.f), 32, Vector2DF(400.f, 400.f), false);
	}
}

//************************************
// Method:    CreateAlarm
// FullName:  GameClock::CreateAlarm
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void GameClock::CreateAlarm(const std::string& alarmName, double alarmLengthinSeconds)
{
	Alarm alarm;
	alarm.m_secondsToFireAfter = alarmLengthinSeconds * m_oneOverTimeScale;
	alarm.m_alarmName = alarmName;
	alarm.m_timeElapsed = 0.f;
	m_alarms.push_back(alarm);
}

//************************************
// Method:    UpdateAlarms
// FullName:  GameClock::UpdateAlarms
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void GameClock::UpdateAlarms()
{
	if (!m_alarms.empty())
	{
		for (std::vector<Alarm>::iterator alarmsIndex = m_alarms.begin(); alarmsIndex != m_alarms.end(); ++alarmsIndex)
		{
			alarmsIndex->m_timeElapsed += DELTASECONDS;
			if (alarmsIndex->m_timeElapsed >= alarmsIndex->m_secondsToFireAfter)
			{
				printf("%s + : Fired!\n", alarmsIndex->m_alarmName.c_str());
				alarmsIndex->m_alarmFired = true;
			}
		}

		for (int alarmsIndex = 0; alarmsIndex < (int)m_alarms.size(); ++alarmsIndex)
		{
			if (m_alarms[alarmsIndex].m_alarmFired)
			{
				m_alarms[alarmsIndex] = m_alarms.back();
				m_alarms.pop_back();
			}
		}
	}
}

double GameClock::GetAlarmTotalTimeRemaining(const std::string& alarmName)
{
	double timeToReturn = 0.0;
	for (std::vector<Alarm>::iterator alarmsIndex = m_alarms.begin(); alarmsIndex != m_alarms.end(); ++alarmsIndex)
	{
		if (alarmsIndex->m_alarmName == alarmName)
		{
			timeToReturn = alarmsIndex->m_secondsToFireAfter - alarmsIndex->m_timeElapsed;
		}
	}
	return timeToReturn;
}

double GameClock::GetAlarmAverageTimeRemaining(const std::string& alarmName)
{
	double timeToReturn = 0.0;
	for (std::vector<Alarm>::iterator alarmsIndex = m_alarms.begin(); alarmsIndex != m_alarms.end(); ++alarmsIndex)
	{
		if (alarmsIndex->m_alarmName == alarmName)
		{
			double timeRemaining = alarmsIndex->m_secondsToFireAfter - alarmsIndex->m_timeElapsed;
			timeToReturn = GetXPercentOfYFloat((float)timeRemaining, (float)alarmsIndex->m_secondsToFireAfter);
		}
	}
	return timeToReturn;
}
