#pragma once

#ifndef GAME_CLOCK_H
#define GAME_CLOCK_H

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>

#include <vector>

struct Alarm
{
public:
	Alarm()
	:m_secondsToFireAfter(1.f)
	,m_timeElapsed(0.f)
	,m_alarmFired(false)
	{

	}	
	~Alarm(){}

	double		m_secondsToFireAfter;
	double		m_timeElapsed;
	std::string	m_alarmName;
	bool		m_alarmFired;
};

class BitmapFont;

class GameClock
{
public:
	~GameClock(void);

	inline LARGE_INTEGER	GetCurrentTime() const { return m_currentTime; }
	inline void				SetCurrentTime(LARGE_INTEGER newCurrentTime) { m_currentTime = newCurrentTime; }
	inline bool				IsPaused() const { return m_isPaused; }
	void					RemoveClock();
	void					PauseClock();
	void					UnPauseClock();
	void					TogglePause();
	void					RenderTime(BitmapFont* bitmapFontRenderer);
	void					CreateAlarm(const std::string& alarmName, double alarmLengthinSeconds);
	void					UpdateAlarms();
	double					GetAlarmTotalTimeRemaining(const std::string& alarmName);
	double					GetAlarmAverageTimeRemaining(const std::string& alarmName);
	static GameClock*		CreateClock(GameClock* parentClock, double scale = 1.0, double maxDelta = 5.0);
	static void				AdvanceTime(double deltaSeconds);

	static GameClock*		s_masterClock;

private:
	GameClock(GameClock* parent, double scale, double maxDelta);

	void UpdateTime(double deltaSeconds);
	void RemoveClockFromChildList(GameClock* clockToRemove);

	LARGE_INTEGER			m_currentTime;
	bool					m_isPaused;
	std::vector<GameClock*>	m_childClocks;
	double					m_timeScale;
	GameClock*				m_parentClock;
	double					m_delta;
	double					m_maxDelta;
	double					m_timeAtLastAdvance;
	double					m_oneOverTimeScale;
	std::vector<Alarm>		m_alarms;
};

#endif