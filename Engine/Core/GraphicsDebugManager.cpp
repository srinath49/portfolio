#include "Engine\Core\GraphicsDebugManager.hpp"
#include "Engine\Core\EngineCommons.hpp"
#include "Engine\Rendering\SriOGLRenderer.hpp"
#include "Engine\Core\Time.hpp"
#include "Engine\Input\InputSystem.hpp"

//---------------------------------------------------------------------------------------------------

const float DB_OFF_LINE_WIDTH_MODIFIER = 0.25f;
//---------------------------------------------------------------------------------------------------
GraphicsDebugManager::GraphicsDebugManager(void)
{
	m_currentTime  = Time::GetAbsoluteTimeSeconds();
	m_previousTime = m_currentTime;
}

//---------------------------------------------------------------------------------------------------
GraphicsDebugManager::~GraphicsDebugManager(void)
{
}

//---------------------------------------------------------------------------------------------------
void GraphicsDebugManager::Update()
{
	if(!m_renderList.empty())
	{
		for (int index = 0; index < (int)(m_renderList.size()); index++)
		{
			m_renderList[index]->Update();
			if(m_renderList[index]->m_isDead)
			{
				m_renderList[index] = m_renderList[m_renderList.size() -1];
				m_renderList.pop_back();
			}
		}
	}
	//Input::UpdateKeyStates();
}

//---------------------------------------------------------------------------------------------------
void GraphicsDebugManager::Render()
{
	if(!m_renderList.empty())
	{
		for (int index = 0; index < (int)(m_renderList.size()); index++)
		{
			m_renderList[index]->Render();
		}
	}

// 	if(InputSystem::KeysState[KEY_CODE_7])
// 	{
// 		DrawDebugAABB3(Vector3DF(0.f, 0.f, 0.f), Vector3DF(1.f, 3.f, 5.f), RGBA(1.f, 0.f, 1.f), 6.f, RGBA(0.f, 1.f, 0.5f), DRAW_MODE_SOLID, DRAW_MODE_SOLID);
// 	}																		
// 	if(InputSystem::KeysState[KEY_CODE_8])										
// 	{																		
// 		DrawDebugAABB3(Vector3DF(-1.f, -3.f, 0.f), Vector3DF(3.f, 0.f, 4.f), RGBA(1.f, 0.f, 0.25f), 6.f, RGBA(0.f, 1.f, 0.f), DRAW_MODE_SOLID, DRAW_MODE_DUAL);
// 	}																		
// 	if(InputSystem::KeysState[KEY_CODE_9])										
// 	{																		
// 		DrawDebugAABB3(Vector3DF(0.f, 0.f, 0.f), Vector3DF(2.f, 3.f, 5.f), RGBA(1.f, 0.f, 1.f), 6.f, RGBA(0.f, 1.f, 0.5f), DRAW_MODE_DUAL, DRAW_MODE_DUAL);
// 	}
// 
// 	if(InputSystem::KeysState[KEY_CODE_1])
// 	{
// 		DrawDebugLineSegment(Vector3DF(0.f, 0.f, 0.f), Vector3DF(6.f, 5.f, 10.f), RGBA(1.f, 0.f, 1.f), RGBA(0.f, 1.f, 0.5f), 8.5f, DRAW_MODE_DUAL);
// 	}
// 	if(InputSystem::KeysState[KEY_CODE_2])
// 	{
// 		DrawDebugLineSegment(Vector3DF(0.f, 0.f, 0.f), Vector3DF(6.f, 5.f, 10.f), RGBA(1.f, 0.f, 1.f), RGBA(0.f, 1.f, 0.5f), 8.5f, DRAW_MODE_SOLID);
// 	}
// 	if(InputSystem::KeysState[KEY_CODE_3])
// 	{
// 		DrawDebugLineSegment(Vector3DF(0.f, 0.f, 0.f), Vector3DF(6.f, 5.f, 10.f), RGBA(1.f, 0.f, 1.f), RGBA(0.f, 1.f, 0.5f), 8.5f, DRAW_MODE_SEE_THROUGH);
// 	}
// 
// 	if(InputSystem::KeysState[KEY_CODE_4])
// 	{
// 		DrawDebugArrow(Vector3DF(0.f, 0.f, 0.f), Vector3DF(6.f, 5.f, 10.f), 0.25f, RGBA(1.f, 0.f, 1.f), 6.f, RGBA(0.f, 1.f, 0.5f), DRAW_MODE_DUAL, DRAW_MODE_SOLID);
// 	}																		
// 	if(InputSystem::KeysState[KEY_CODE_5])										
// 	{																		
// 		DrawDebugArrow(Vector3DF(0.f, 0.f, 0.f), Vector3DF(6.f, 5.f, 10.f), 0.5f, RGBA(1.f, 0.f, 1.f), 6.f, RGBA(0.f, 1.f, 0.5f), DRAW_MODE_SOLID, DRAW_MODE_SEE_THROUGH);
// 	}																		
// 	if(InputSystem::KeysState[KEY_CODE_6])										
// 	{																		
// 		DrawDebugArrow(Vector3DF(0.f, 0.f, 0.f), Vector3DF(6.f, 5.f, 10.f), 0.75f, RGBA(1.f, 0.f, 1.f), 6.f, RGBA(0.f, 1.f, 0.5f), DRAW_MODE_SEE_THROUGH, DRAW_MODE_DUAL);
// 	}
// 
// 	if (InputSystem::KeysState[KEY_CODE_0])
// 	{
// 		DrawDebugSphere(Vector3DF(1.f, -3.f, 0.f), 0.25f, RGBA(0.75f, 0.25f, 0.4f), DRAW_MODE_DUAL);
// 	}
// 	if (InputSystem::KeysState[KEY_CODE_P])
// 	{
// 		DrawDebugSphere(Vector3DF(1.f, -3.f, 0.f), 0.25f, RGBA(0.75f, 0.25f, 0.4f), DRAW_MODE_SEE_THROUGH);
// 	}
// 	if (InputSystem::KeysState[KEY_CODE_O])
// 	{
// 		DrawDebugSphere(Vector3DF(1.f, -3.f, 0.f), 0.25f, RGBA(0.75f, 0.25f, 0.4f), DRAW_MODE_SOLID);
// 	}

}

//---------------------------------------------------------------------------------------------------
void GraphicsDebugManager::DrawDebugLineSegment(const Vector3DF& startPosition, const Vector3DF& endPosition, const RgbaColors& lineStartColor, const RgbaColors& lineEndColor, float lineWidth, DrawMode mode)
{
	switch (mode)
	{
		case DRAW_MODE_SEE_THROUGH:
			g_myRenderer->DisableDepthBuffer();
			g_myRenderer->DrawLineSegment(startPosition, endPosition, lineStartColor, lineEndColor, (lineWidth * DB_OFF_LINE_WIDTH_MODIFIER) );
			break;
		case DRAW_MODE_DUAL:
			g_myRenderer->EnableDepthBuffer();
			g_myRenderer->DrawLineSegment(startPosition, endPosition, lineStartColor, lineEndColor, lineWidth);

			g_myRenderer->DisableDepthBuffer();
			g_myRenderer->DrawLineSegment(startPosition, endPosition, lineStartColor, lineEndColor, (lineWidth * DB_OFF_LINE_WIDTH_MODIFIER) );
			
			break;
		case DRAW_MODE_SOLID:
			g_myRenderer->EnableDepthBuffer();
			g_myRenderer->DrawLineSegment(startPosition, endPosition, lineStartColor, lineEndColor, lineWidth);
			break;
	}
	g_myRenderer->EnableDepthBuffer();
}

void GraphicsDebugManager::DrawDebugPosition(const Vector3DF& position, float size, const RgbaColors& lineColor, DrawMode mode)
{
	Vector3DF worldMin;
	Vector3DF worldMax;

	worldMin = (position - (size * 0.5f));
	worldMax = (position + (size * 0.5f));
	
	float lineWidth = 3.f;

	std::vector<Vector3DF> vertexes;
	vertexes.push_back(worldMin);
	vertexes.push_back(worldMax);
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMin.x, worldMin.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMax.y, worldMin.z));
	vertexes.push_back(Vector3DF(position.x, position.y, worldMin.z));
	vertexes.push_back(Vector3DF(position.x, position.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMin.x, position.y, position.z));
	vertexes.push_back(Vector3DF(worldMax.x, position.y, position.z));

	switch (mode)
	{
	case DRAW_MODE_SEE_THROUGH:
		g_myRenderer->DisableDepthBuffer();
		for (int index = 0; index < (int)(vertexes.size()); index +=2)
		{
			g_myRenderer->DrawLine(vertexes[index], vertexes[index+1], lineColor, (lineWidth * DB_OFF_LINE_WIDTH_MODIFIER) );
		}
		

		break;
	case DRAW_MODE_DUAL:
		g_myRenderer->EnableDepthBuffer();
		for (int index = 0; index < (int)(vertexes.size()); index +=2)
		{
			g_myRenderer->DrawLine(vertexes[index], vertexes[index+1], lineColor, lineWidth);
		}

		g_myRenderer->DisableDepthBuffer();
		for (int index = 0; index < (int)(vertexes.size()); index +=2)
		{
			g_myRenderer->DrawLine(vertexes[index], vertexes[index+1], lineColor, (lineWidth * DB_OFF_LINE_WIDTH_MODIFIER) );
		}

		break;
	case DRAW_MODE_SOLID:
		g_myRenderer->EnableDepthBuffer();
		for (int index = 0; index < (int)(vertexes.size()); index +=2)
		{
			g_myRenderer->DrawLine(vertexes[index], vertexes[index+1], lineColor, lineWidth);
		}
		break;
	}
	g_myRenderer->EnableDepthBuffer();
}

void GraphicsDebugManager::DrawDebugAABB3(const Vector3DF& worldMin, const Vector3DF& worldMax, const RgbaColors& lineColor, float lineWidth, const RgbaColors& faceColor, DrawMode faceMode, DrawMode lineMode)
{
	std::vector<const Vector3DF> vertexes;

	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMin.z));
	vertexes.push_back(worldMin);
	vertexes.push_back(Vector3DF(worldMin.x, worldMin.y, worldMax.z));

	vertexes.push_back(Vector3DF(worldMin.x, worldMin.y, worldMax.z));
	vertexes.push_back(worldMin);
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMax.z));

	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMax.y, worldMin.z));
	vertexes.push_back(worldMax);

	vertexes.push_back(worldMax);
	vertexes.push_back(Vector3DF(worldMax.x, worldMax.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMax.z));

	vertexes.push_back(worldMax);
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMin.x, worldMin.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMax.z));

	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMax.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMin.z));
	vertexes.push_back(worldMin);
	
	RgbaColors dullFaceColor = faceColor;
	RgbaColors dullLineColor = lineColor;

	dullFaceColor *= DB_OFF_LINE_WIDTH_MODIFIER;
	dullLineColor *= DB_OFF_LINE_WIDTH_MODIFIER;

	switch (faceMode)
	{
		case DRAW_MODE_SEE_THROUGH:
			g_myRenderer->DisableDepthBuffer();

			for (int index = 0; index < (int)(vertexes.size()); index +=4)
			{
				g_myRenderer->DrawTexturedQuad(vertexes[index], vertexes[index+1], vertexes[index+2], vertexes[index+3], dullFaceColor);
			}
			break;
		case DRAW_MODE_DUAL:
			g_myRenderer->EnableDepthBuffer();
			for (int index = 0; index < (int)(vertexes.size()); index +=4)
			{
				g_myRenderer->DrawTexturedQuad(vertexes[index], vertexes[index+1], vertexes[index+2], vertexes[index+3], faceColor);
			}

			g_myRenderer->DisableDepthBuffer();
			for (int index = 0; index < (int)(vertexes.size()); index +=4)
			{
				g_myRenderer->DrawTexturedQuad(vertexes[index], vertexes[index+1], vertexes[index+2], vertexes[index+3], dullFaceColor);
			}

			break;
		case DRAW_MODE_SOLID:
			g_myRenderer->EnableDepthBuffer();
			for (int index = 0; index < (int)(vertexes.size()); index +=4)
			{
				g_myRenderer->DrawTexturedQuad(vertexes[index], vertexes[index+1], vertexes[index+2], vertexes[index+3], faceColor);
			}
			break;
	}
	g_myRenderer->EnableDepthBuffer();
	switch (lineMode)
	{
		case DRAW_MODE_SEE_THROUGH:
			g_myRenderer->DisableDepthBuffer();
			for (int index = 0; index < (int)(vertexes.size()); index +=4)
			{
				g_myRenderer->DrawSquareOutline(vertexes[index], vertexes[index+1], vertexes[index+2], vertexes[index+3], dullLineColor, (lineWidth * DB_OFF_LINE_WIDTH_MODIFIER));
			}
			break;
		case DRAW_MODE_DUAL:
			g_myRenderer->EnableDepthBuffer();
			for (int index = 0; index < (int)(vertexes.size()); index +=4)
			{
				g_myRenderer->DrawSquareOutline(vertexes[index], vertexes[index+1], vertexes[index+2], vertexes[index+3], lineColor, lineWidth);
			}

			g_myRenderer->DisableDepthBuffer();
			for (int index = 0; index < (int)(vertexes.size()); index +=4)
			{
				g_myRenderer->DrawSquareOutline(vertexes[index], vertexes[index+1], vertexes[index+2], vertexes[index+3], dullLineColor, (lineWidth * DB_OFF_LINE_WIDTH_MODIFIER));
			}

			break;
		case DRAW_MODE_SOLID:
			g_myRenderer->EnableDepthBuffer();
			for (int index = 0; index < (int)(vertexes.size()); index +=4)
			{
				g_myRenderer->DrawSquareOutline(vertexes[index], vertexes[index+1], vertexes[index+2], vertexes[index+3], lineColor, lineWidth);
			}
			break;
	}
	g_myRenderer->EnableDepthBuffer();
}

void GraphicsDebugManager::DrawDebugArrow(const Vector3DF& startPosition, const Vector3DF& endPosition, float headSize, const RgbaColors& lineColor, float lineWidth, const RgbaColors& headColor, DrawMode lineMode, DrawMode headMode)
{
	switch (lineMode)
	{
		case DRAW_MODE_SEE_THROUGH:
			g_myRenderer->DisableDepthBuffer();		
			g_myRenderer->DrawLine(startPosition, endPosition, lineColor, (lineWidth * DB_OFF_LINE_WIDTH_MODIFIER) );

			break;
		case DRAW_MODE_DUAL:
			g_myRenderer->EnableDepthBuffer();
			g_myRenderer->DrawLine(startPosition, endPosition, lineColor, lineWidth);

			g_myRenderer->DisableDepthBuffer();
			g_myRenderer->DrawLine(startPosition, endPosition, lineColor, (lineWidth * DB_OFF_LINE_WIDTH_MODIFIER) );

			break;
		case DRAW_MODE_SOLID:
			g_myRenderer->EnableDepthBuffer();
			g_myRenderer->DrawLine(startPosition, endPosition, lineColor, lineWidth);

			break;
	}

	DrawDebugPosition(endPosition, headSize, headColor, headMode);
	g_myRenderer->EnableDepthBuffer();
}


/*
 * Compute lookup table of cos and sin values forming a cirle
 *
 * Notes:
 *    It is the responsibility of the caller to free these tables
 *    The size of the table is (n+1) to form a connected loop
 *    The last entry is exactly the same as the first
 *    The sign of n can be flipped to get the reverse loop
 */
// The Following Code was taken from the functions provided by Professor Anton Ephanov at Guildhall
void GraphicsDebugManager::CircleTable(double **sint,double **cost,const int n)
{
	int i;

	/* Table size, the sign of n flips the circle direction */

	const int size = abs(n);

	/* Determine the angle between samples */

	const double angle = TWO_PI/(double)n;

	/* Allocate memory for n samples, plus duplicate of first entry at the end */

	*sint = (double *) calloc(sizeof(double), size+1);
	*cost = (double *) calloc(sizeof(double), size+1);

	/* Bail out if memory allocation fails, fgError never returns */

	if (!(*sint) || !(*cost))
	{
		free(*sint);
		free(*cost);
		//fgError("Failed to allocate memory in circleTable");
	}

	/* Compute cos and sin around the circle */

	for (i=0; i<size; i++)
	{
		(*sint)[i] = sin(angle*i);
		(*cost)[i] = cos(angle*i);
	}

	/* Last sample is duplicate of the first */

	(*sint)[size] = (*sint)[0];
	(*cost)[size] = (*cost)[0];
}

// The Following Code was taken from the functions provided by Professor Anton Ephanov at Guildhall
void GraphicsDebugManager::DrawSphere(float radius, int slices, int stacks)
{
	g_myRenderer->DrawSolidSphere(radius, slices, stacks, Vector3DF(0.f, 0.f, 0.f));
}

void GraphicsDebugManager::DrawDebugSphere(const Vector3DF& center, float radius, const RgbaColors& lineColor, DrawMode mode)
{
	//g_myRenderer->Translate(center.x, center.y, center.z);
	UNUSED(center);
	RgbaColors dullSphereColor = lineColor;
	dullSphereColor *= 0.25f;
	
	switch(mode)
	{
		case DRAW_MODE_SEE_THROUGH:
			g_myRenderer->SetLineWidth(4.f);
			g_myRenderer->DisableDepthBuffer();
			g_myRenderer->SetColor(dullSphereColor);
			DrawSphere(radius, 16, 32);
			break;
		case DRAW_MODE_SOLID:
			g_myRenderer->EnableDepthBuffer();
			g_myRenderer->SetLineWidth(8.f);
			g_myRenderer->SetColor(lineColor);
			DrawSphere(radius, 16, 32);
			break;
		case DRAW_MODE_DUAL:
			g_myRenderer->EnableDepthBuffer();
			g_myRenderer->SetLineWidth(8.f);
			g_myRenderer->SetColor(lineColor);
			DrawSphere(radius, 16, 32);

			g_myRenderer->DisableDepthBuffer();
			g_myRenderer->SetLineWidth(4.f);
			g_myRenderer->SetColor(dullSphereColor);
			DrawSphere(radius, 16, 32);
			break;
	}
	g_myRenderer->EnableDepthBuffer();
}


GraphicsDebugManager::DebugObject::DebugObject(double lifeSpan /*= 3.f*/, DrawMode mode /*= DRAW_MODE_DUAL*/, float lineWidth /*= 4.f*/, const RgbaColors& lineColor /*= RGBA(1.f, 1.f, 1.f, 1.f)*/) : 
	m_lifeSpan(lifeSpan),
	m_mode(mode),
	m_lineWidth(lineWidth),
	m_lineColor(lineColor)
{
	m_spawnTime = Time::GetAbsoluteTimeSeconds();
	m_timePassed = 0.f;
	m_isDead = false;
	m_myManager = new GraphicsDebugManager();
}