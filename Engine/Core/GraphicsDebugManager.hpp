#pragma once

#ifndef GRAPHICS_DEBUG_MANAGER_H
#define GRAPHICS_DEBUG_MANAGER_H

#include "Engine\Math\SriMath.hpp"
#include "Engine\Core\RgbaColors.hpp"
#include "Engine\Core\Time.hpp"
#include <vector>

class GraphicsDebugManager;

//---------------------------------------------------------------------------------------------------
class GraphicsDebugManager
{
public:
	enum DrawMode
	{
		DRAW_MODE_SOLID,
		DRAW_MODE_SEE_THROUGH,
		DRAW_MODE_DUAL,
		DRAW_MODE_NUM_DRAW_MODES
	};

	struct DebugObject
	{
	public:
		DebugObject(double lifeSpan = 3.f, DrawMode mode = DRAW_MODE_DUAL, float lineWidth = 4.f, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f));

		virtual ~DebugObject()
		{
			delete m_myManager;
		}

		void Update()
		{
			if(Time::GetAbsoluteTimeSeconds() - m_spawnTime >= m_lifeSpan)
			{
				m_isDead = true;
			}
		}

		virtual void Render() = 0;

		bool	 m_isDead;

	protected:
		double	 m_lifeSpan;
		DrawMode m_mode;
		float	 m_lineWidth;
		RgbaColors	 m_lineColor;
		double   m_spawnTime;
		double   m_timePassed;
		GraphicsDebugManager* m_myManager;
	};


	struct DebugLineSegment : public DebugObject
	{
	public:
		DebugLineSegment(const Vector3DF& startPos, const Vector3DF endPos, const RgbaColors& endColor = RgbaColors(1.f, 1.f, 1.f), double lifeSpan = 3.f, DrawMode mode = DRAW_MODE_DUAL, float lineWidth = 4.f, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f)) 
		: DebugObject(lifeSpan, mode, lineWidth, lineColor),
		  startPosition(startPos),
		  endPosition(endPos),
		  lineEndColor(endColor)
		{

		}

		~DebugLineSegment()
		{

		}

		void Render()
		{
			m_myManager->DrawDebugLineSegment(startPosition, endPosition, m_lineColor, lineEndColor, m_lineWidth, m_mode);
		}

		Vector3DF startPosition;
		Vector3DF endPosition;
		RgbaColors lineEndColor;
	};

	struct DebugPosition : public DebugObject
	{
	public:
		DebugPosition(const Vector3DF& position, float size, double lifeSpan = 3.f, DrawMode mode = DRAW_MODE_DUAL, float lineWidth = 4.f, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f)) 
		: DebugObject(lifeSpan, mode, lineWidth, lineColor),
		  m_position(position),
		  m_size(size)
		{

		}

		~DebugPosition()
		{

		}

		void Render()
		{
			m_myManager->DrawDebugPosition(m_position, m_size, m_lineColor, m_mode);
		}

		Vector3DF m_position; 
		float m_size;
	};


	struct DebugSphere : public DebugObject
	{
	public:
		DebugSphere(const Vector3DF& center, float radius, double lifeSpan = 3.f, DrawMode mode = DRAW_MODE_DUAL, float lineWidth = 4.f, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f))
		: DebugObject(lifeSpan, mode, lineWidth, lineColor),
		  m_center(center),
	 	  m_radius(radius)
		{

		}
		
		~DebugSphere()
		{
			delete m_myManager;
		}

		void Render()
		{
			m_myManager->DrawDebugSphere(m_center, m_radius, m_lineColor, m_mode);
		}

		Vector3DF m_center;
		float m_radius;

	};

	struct DebugAABB3 : public DebugObject
	{
	public:
		DebugAABB3(const Vector3DF& worldMin, const Vector3DF& worldMax, const RgbaColors& faceColor, DrawMode faceMode, double lifeSpan = 3.f, DrawMode mode = DRAW_MODE_DUAL, float lineWidth = 4.f, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f))
		: DebugObject(lifeSpan, mode, lineWidth, lineColor),
		  m_worldMin(worldMin),
		  m_worldMax(worldMax),
		  m_faceColor(faceColor),
		  m_faceMode(faceMode)
		{

		}

		~DebugAABB3()
		{
			
		}

		void Render()
		{
			m_myManager->DrawDebugAABB3(m_worldMin, m_worldMax, m_lineColor, m_lineWidth, m_faceColor, m_faceMode, m_mode);
		}

		Vector3DF m_worldMin; 
		Vector3DF m_worldMax;
		RgbaColors	  m_faceColor;
		DrawMode  m_faceMode;

	};

	struct DebugArrow : public DebugObject
	{
	public:
		DebugArrow(const Vector3DF& startPos, const Vector3DF& endPos, float headSize, const RgbaColors& headColor, DrawMode headMode, double lifeSpan = 3.f, DrawMode mode = DRAW_MODE_DUAL, float lineWidth = 4.f, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f))
		: DebugObject(lifeSpan, mode, lineWidth, lineColor),
		  m_startPosition(startPos),
		  m_endPosition(endPos),
		  m_headSize(headSize),
		  m_headColor(headColor),
		  m_headMode(headMode)
		{

		}

		~DebugArrow()
		{

		}

		void Render()
		{
			m_myManager->DrawDebugArrow(m_startPosition, m_endPosition, m_headSize, m_lineColor, m_lineWidth, m_headColor, m_mode, m_headMode);
		}

		Vector3DF m_startPosition;
		Vector3DF m_endPosition;
		float     m_headSize;
		RgbaColors	  m_headColor;
		DrawMode  m_headMode;
	};

public:
	GraphicsDebugManager(void);
	~GraphicsDebugManager(void);
	
	void Update();
	void Render();

	void DrawDebugLineSegment(const Vector3DF& startPosition, const Vector3DF& endPosition, const RgbaColors& lineStartColor, const RgbaColors& lineEndColor, float lineWidth, DrawMode mode);
	void DrawDebugPosition(const Vector3DF& position, float size, const RgbaColors& lineColor, DrawMode mode);
	void DrawDebugAABB3(const Vector3DF& worldMin, const Vector3DF& worldMax, const RgbaColors& lineColor, float lineWidth, const RgbaColors& faceColor, DrawMode faceMode, DrawMode lineMode);
	void DrawDebugArrow(const Vector3DF& startPosition, const Vector3DF& endPosition, float headSize, const RgbaColors& lineColor, float lineWidth, const RgbaColors& headColor, DrawMode lineMode, DrawMode headMode); 
	void DrawDebugSphere(const Vector3DF& center, float radius, const RgbaColors& lineColor, DrawMode mode);

public:
	std::vector<DebugObject*> m_renderList;
private:
	void DrawSphere(float radius, int slices, int stacks);		// Taken from functions provided by Professor Anton Ephanov
	void CircleTable(double **sint,double **cost,const int n);	// Taken from functions provided by Professor Anton Ephanov

	double m_previousTime;
	double m_currentTime;
};

#endif