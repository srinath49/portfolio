#include "Engine/Core/ModelVertex.hpp"


bool ModelVertex::operator==(const ModelVertex& rhs) const
{
	bool isEqual = false;
	isEqual = (m_position == rhs.m_position) && (m_color == rhs.m_color) && (m_texCoords == rhs.m_texCoords) && (m_normal == rhs.m_normal) && (m_tangent == rhs.m_tangent) && (m_bitangent == rhs.m_bitangent);
	return isEqual;
}

bool ModelVertex::operator<(const ModelVertex& rhs) const
{
	bool isEqual = false;
	bool isLessThan = false;	
	isEqual = (*this == rhs);
	isEqual = (m_position == rhs.m_position);
	if (isEqual)
	{
		isLessThan = (m_normal < rhs.m_normal);
	}
	else
	{
		isLessThan = (m_position < rhs.m_position);
	}

	return isLessThan;
}