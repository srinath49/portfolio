#pragma once

#ifndef MODEL_VERTEX_H
#define MODEL_VERTEX_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Math/SriMath.hpp"
#include "Engine/Core/RgbaColors.hpp"
#include <functional>

template<class T>
class  VertexHash;

//---------------------------------------------------------------------------------------------------
struct ModelVertex
{
	ModelVertex()
	{
		m_normal = Vector3DF(0.0f, 0.0f, 1.0f);
		m_color.Initialize(255, 255, 255, 255);
	}

	bool operator==(const ModelVertex& rhs) const;
	bool operator<(const ModelVertex& rhs) const;

	Vector3DF	m_position;
	RgbaColors		m_color;
	Vector2DF	m_texCoords;
	Vector3DF	m_normal;
	Vector3DF	m_tangent;
	Vector3DF	m_bitangent;
};

//---------------------------------------------------------------------------------------------------
template<>
class  VertexHash<ModelVertex>
{
public:	
	size_t operator()(const ModelVertex& rhsVertex) const
	{
		size_t positionHash		= (	(std::hash<float>()(rhsVertex.m_position.x)			
								^	(std::hash<float>()(rhsVertex.m_position.y) << 1)) >> 1)	
								^	(std::hash<float>()(rhsVertex.m_position.z) << 1);

		size_t texCoordsHash	= (	(std::hash<float>()(rhsVertex.m_texCoords.x)			
								^	(std::hash<float>()(rhsVertex.m_texCoords.y) << 1)) >> 1);

		size_t rgbaHash			= (	(std::hash<float>()(rhsVertex.m_color.r)			
								^ ( (std::hash<float>()(rhsVertex.m_color.g) << 1)) >> 1)	
								^	(std::hash<float>()(rhsVertex.m_color.b) << 1)
								^	(std::hash<float>()(rhsVertex.m_color.a) >> 1) << 1 );

		size_t normalHash		= (	(std::hash<float>()(rhsVertex.m_normal.x)			
								^	(std::hash<float>()(rhsVertex.m_normal.y) << 1)) >> 1)	
								^	(std::hash<float>()(rhsVertex.m_normal.z) << 1);

		size_t tangentHash		= (	(std::hash<float>()(rhsVertex.m_tangent.x)			
								^	(std::hash<float>()(rhsVertex.m_tangent.y) << 1)) >> 1)	
								^	(std::hash<float>()(rhsVertex.m_tangent.z) << 1);

		size_t bitangentHash	= (	(std::hash<float>()(rhsVertex.m_bitangent.x)			
								^	(std::hash<float>()(rhsVertex.m_bitangent.y) << 1)) >> 1)	
								^	(std::hash<float>()(rhsVertex.m_bitangent.z) << 1);
		
		size_t returnHash = (positionHash ^ ( (texCoordsHash << 1) ^ (rgbaHash >> 1)) ^ ( normalHash ^ ((tangentHash >> 1) ^ (bitangentHash << 1)) )) >> 1;

		return returnHash;
	}

	
};

#endif