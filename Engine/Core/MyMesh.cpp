#include "Engine/Core/MyMesh.hpp"
#include "Engine/FileIO/OBJLoader.hpp"
#include "Engine/FileIO/FileLoader.hpp"
#include "Engine/Math/SriMath.hpp"
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
MyMesh::MyMesh() :
	m_meshLoader(nullptr)
{
	
}

//---------------------------------------------------------------------------------------------------
MyMesh::~MyMesh()
{
	delete m_meshLoader;
}

//---------------------------------------------------------------------------------------------------
void MyMesh::SetMeshVertexes(std::vector<ModelVertex>& meshVertexes)
{
	m_meshVertexes = meshVertexes;
}

//---------------------------------------------------------------------------------------------------
void MyMesh::SetMeshIndexes(std::vector<unsigned int>& meshIndexes)
{
	m_meshIndexes = meshIndexes;
}
//---------------------------------------------------------------------------------------------------
void MyMesh::LoadMesh()
{
	if (!m_meshLoader)
	{
		m_meshLoader = new OBJLoader();
	}

	m_meshLoader->LoadFile(m_filePath);
}

//---------------------------------------------------------------------------------------------------
void MyMesh::SetMeshLoader(FileLoader* newLoader)
{
	m_meshLoader = newLoader;
}

//---------------------------------------------------------------------------------------------------
void MyMesh::ParseOBJBuffer()
{
	//static_cast<OBJLoader*>(m_meshLoader)->ParseVertexDataFromOBJFile(this);
}

//---------------------------------------------------------------------------------------------------
void MyMesh::SetMeshFilePath(const std::string& meshPath)
{
	m_filePath = meshPath;
}

