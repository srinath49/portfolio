#pragma once

#ifndef MESH_H
#define MESH_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Core/ModelVertex.hpp"

#include <vector>
#include <string>
//---------------------------------------------------------------------------------------------------
class FileLoader;

//---------------------------------------------------------------------------------------------------
class MyMesh
{
public:
	MyMesh();
	~MyMesh();

	void SetMeshVertexes(std::vector<ModelVertex>& meshVertexes);
	void SetMeshIndexes(std::vector<unsigned int>& meshIndexes);
	void LoadMesh();
	void SetMeshLoader(FileLoader* newLoader);
	void ParseOBJBuffer();
	void SetMeshFilePath(const std::string& meshPath);

public:
	std::vector<ModelVertex>	m_meshVertexes;
	std::vector<unsigned int>	m_meshIndexes;
	FileLoader*					m_meshLoader;
	std::string					m_filePath;
};
#endif