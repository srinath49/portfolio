#include "Profiler.hpp"
#include "Time.hpp"
#include "..\Input\InputSystem.hpp"
#include "..\DebugConsole\ConsolePrintf.hpp"
#include "..\DebugConsole\BitmapFont.hpp"
//---------------------------------------------------------------------------------------------------

std::vector<TimeCounter> Profiler::counters;
RenderMode Profiler::mode = RM_TOTAL_TIME;
//---------------------------------------------------------------------------------------------------

void Profiler::StartProfiler(const char* profilerName)
{
	for (std::vector<TimeCounter>::iterator countersIter = counters.begin(); countersIter != counters.end(); ++countersIter)
	{
		if (std::strcmp(countersIter->counterName, profilerName) == 0)
		{
			countersIter->startTimes.push_back(Time::GetAbsoluteTimeSeconds());
			return;
		}
	}
	TimeCounter newCounter;
	newCounter.counterName = (char*)profilerName;
	newCounter.startTimes.push_back(Time::GetAbsoluteTimeSeconds());
	counters.push_back(newCounter);
}

void Profiler::StopProfiler(const char* profilerName)
{
	
	for (std::vector<TimeCounter>::iterator countersIter = counters.begin(); countersIter != counters.end(); ++countersIter)
	{
		if (std::strcmp(countersIter->counterName, profilerName) == 0)
		{
			TimeCounter& counter = *countersIter;
			counter.endTimes.push_back(Time::GetAbsoluteTimeSeconds());
			double executionTime = counter.endTimes.back() - counter.startTimes.back();
			counter.executionTimes.push_back(executionTime);
			double avgTime = 0.0;
			if (!counter.executionTimes.empty())
			{
				int execTimesSize = (int)counter.executionTimes.size();
				for (int index = 0; index < execTimesSize; ++ index)
				{
					avgTime += counter.executionTimes[index];
				}
				avgTime /= execTimesSize;
			}
			counter.averageTime = avgTime;
			break;
		}
	}
}

void Profiler::ProcessInput(InputSystem* inputSystem)
{
	if (inputSystem)
	{
		if (inputSystem->HasKeyJustBeenReleased(KEY_CODE_T))
		{
			ToggleRenderMode();
		}
	}
}

void Profiler::RenderProfiler(const char* profilerToRender, BitmapFont* fontRenderer)
{	
	if (!fontRenderer)
	{
		return;
	}

	TimeCounter counterToRender;
	for (std::vector<TimeCounter>::iterator countersIter = counters.begin(); countersIter != counters.end(); ++countersIter)
	{
		if (std::strcmp(countersIter->counterName, profilerToRender) == 0)
		{
			counterToRender.counterName = countersIter->counterName;
			counterToRender.executionTimes = countersIter->executionTimes;
			counterToRender.averageTime = countersIter->averageTime;
			break;
		}
	}

	char* timeStringLine1 = "";
	char* timeStringLine2 = " ";
	char* timeStringLine3 = "  ";
	char* modeString = "   ";
	if (counterToRender.executionTimes.empty())
	{
		return;
	}
	fontRenderer->SetUpMyMaterial();
	if(mode == RM_TOTAL_TIME)
	{
		modeString = "Total Time This Frame";	
		timeStringLine1 = ConsolePrintf("Profiler Name : %s\n", counterToRender.counterName);
		fontRenderer->DrawString(timeStringLine1, RgbaColors(0.f, 0.25f,0.75f), 16, Vector2DF(100.f, 800.f), false);
		timeStringLine2 = ConsolePrintf("Profiler Mode : %s\n", modeString);
		fontRenderer->DrawString(timeStringLine2, RgbaColors(0.f, 0.0f,0.75f), 16, Vector2DF(100.f, 750.f), false);
		timeStringLine3 = ConsolePrintf("Time Taken To Execute : %lf", counterToRender.executionTimes.back());
		fontRenderer->DrawString(timeStringLine3, RgbaColors(08.f, 0.25f,0.f), 16, Vector2DF(100.f, 700.f), false);
	}
	else
	{
		modeString = "Average Time Per Frame";
		timeStringLine1 = ConsolePrintf("Profiler Name : %s", counterToRender.counterName);
		fontRenderer->DrawString(timeStringLine1, RgbaColors(0.f, 0.25f,0.75f), 16, Vector2DF(100.f, 800.f), false);
		timeStringLine2 = ConsolePrintf("Profiler Mode : %s", modeString);
		fontRenderer->DrawString(timeStringLine2, RgbaColors(0.f, 0.0f,0.75f), 16, Vector2DF(100.f, 750.f), false);
		timeStringLine3 = ConsolePrintf("Time Taken To Execute : %lf", counterToRender.averageTime);
		fontRenderer->DrawString(timeStringLine3, RgbaColors(08.f, 0.25f,0.f), 16, Vector2DF(100.f, 700.f), false);
	}
}

void Profiler::ToggleRenderMode()
{
	if(mode == RM_TOTAL_TIME)
	{
		mode = RM_AVERAGE_TIME;
	}
	else
	{
		mode = RM_TOTAL_TIME;
	}
}
