#pragma once

#ifndef PROFILER_H
#define PROFILER_H
//---------------------------------------------------------------------------------------------------
#include <vector>
//---------------------------------------------------------------------------------------------------

class InputSystem;
class BitmapFont;

struct TimeCounter
{
public:
	char*  counterName;
	std::vector<double> startTimes;
	std::vector<double> endTimes;
	std::vector<double> executionTimes;
	double				averageTime;
};

enum RenderMode
{
	RM_TOTAL_TIME,
	RM_AVERAGE_TIME
};

class Profiler
{
public:
	static void StartProfiler(const char* profilerName);
	static void StopProfiler(const char* profilerName);
	
	static void RenderProfiler(const char* profilerToRender, BitmapFont* fontRenderer);

	static void ProcessInput(InputSystem* inputSystem);
	static void ToggleRenderMode();

private:
	static std::vector<TimeCounter> counters;
	static RenderMode mode;
};
#endif