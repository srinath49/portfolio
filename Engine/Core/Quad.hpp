#pragma once

#ifndef QUAD_H
#define QUAD_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Core/BasicShape.hpp"


//---------------------------------------------------------------------------------------------------
class Quad : public BasicShape
{	
public:
	Quad(unsigned int shaderProgramId, const std::vector<Vector3DF>& vertexes);
	~Quad(void);
};

#endif