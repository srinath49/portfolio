#include "RgbaColors.hpp"


//-----------------------------------------------------------------------------------------------
#define UNUSED(x) (void)(x);


//---------------------------------------------------------------------------------------------------
RgbaColors::RgbaColors(float red, float green, float blue, float alpha)
{
	FloatsToUnsignedBytes(red, green, blue, alpha);
}

//---------------------------------------------------------------------------------------------------
RgbaColors::~RgbaColors(void)
{

}

void RgbaColors::FloatsToUnsignedBytes(float red, float green, float blue, float alpha)
{
	unsigned char tempr = (unsigned char)(red * 255);
	unsigned char tempg = (unsigned char)(green * 255);
	unsigned char tempb = (unsigned char)(blue * 255);
	unsigned char tempa = (unsigned char)(alpha * 255);

	Initialize(tempr, tempg, tempb, tempa);
}

//---------------------------------------------------------------------------------------------------
void RgbaColors::FloatsToUnsignedBytes(float red, float green, float blue)
{
	unsigned char tempr = (unsigned char)(red * 255);
	unsigned char tempg = (unsigned char)(green * 255);
	unsigned char tempb = (unsigned char)(blue * 255);

	Initialize(tempr, tempg, tempb);
}

void RgbaColors::FloatsToUnsignedBytes(RgbaColors& outColors, float red, float green, float blue, float alpha)
{
	unsigned char tempr = (unsigned char)(red * 255);
	unsigned char tempg = (unsigned char)(green * 255);
	unsigned char tempb = (unsigned char)(blue * 255);
	unsigned char tempa = (unsigned char)(alpha * 255);

	outColors.r = tempr;
	outColors.g = tempg;
	outColors.b = tempb;
	outColors.a = tempa;
}

void RgbaColors::FloatsToUnsignedBytes(RgbaColors& outColors, float red, float green, float blue)
{
	unsigned char tempr = (unsigned char)(red * 255);
	unsigned char tempg = (unsigned char)(green * 255);
	unsigned char tempb = (unsigned char)(blue * 255);

	outColors.r = tempr;
	outColors.g = tempg;
	outColors.b = tempb;
}

//---------------------------------------------------------------------------------------------------
void RgbaColors::operator=(const Vector3DF& copyRgba)
{
	FloatsToUnsignedBytes(copyRgba.x, copyRgba.y, copyRgba.z);
}

//---------------------------------------------------------------------------------------------------
void RgbaColors::operator*=(float scaleF)
{
	UNUSED(scaleF);
/*
	r *= scaleF;
	g *= scaleF;
	b *= scaleF;
	a *= scaleF;*/
}

//---------------------------------------------------------------------------------------------------
void RgbaColors::Initialize(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha)
{
	r = red;
	g = green;
	b = blue;
	a = alpha;
}

//---------------------------------------------------------------------------------------------------
void RgbaColors::Initialize(unsigned char red, unsigned char green, unsigned char blue)
{
	r = red;
	g = green;
	b = blue;
}

//---------------------------------------------------------------------------------------------------
bool RgbaColors::operator==(const RgbaColors& rhs) const
{
	bool isEqual = false;
	isEqual = (r == rhs.r) && (g == rhs.g) && (b == rhs.b) && (a == rhs.a);
	return isEqual;
}


//---------------------------------------------------------------------------------------------------
bool RgbaColors::operator<(const RgbaColors& rhs) const
{
	bool isEqual = false;
	
	int rHash = rhs.r*2 + rhs.g*3 + rhs.b*4 * rhs.a*5;
	int lHash = r*2 + g*3 + b*4 * a*5;
	if (rHash == lHash)
	{
		isEqual = false;// Fix This
	}
	else
	{
		if(rHash < lHash)
		{
			isEqual = true;
		}
	}
	
	return isEqual;
}