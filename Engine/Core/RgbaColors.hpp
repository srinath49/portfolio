#pragma once

#ifndef RGBA_H
#define RGBA_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Math/SriMath.hpp"
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
class RgbaColors
{
public:
	RgbaColors(float red = 1.f, float green = 1.f, float blue = 1.f, float alpha = 1.f);
	~RgbaColors(void);

	void operator*=(float scaleF);
	void operator=(const Vector3DF& copyRgba);
	bool operator==(const RgbaColors& rhs) const;
	bool operator<(const RgbaColors& rhs) const;

	void Initialize(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha );
	void Initialize(unsigned char red, unsigned char green, unsigned char blue);
	void FloatsToUnsignedBytes(float red, float green, float blue, float alpha);
	void FloatsToUnsignedBytes(float red, float green, float blue);
	static void FloatsToUnsignedBytes(RgbaColors& outColors, float red, float green, float blue, float alpha);
	static void FloatsToUnsignedBytes(RgbaColors& outColors, float red, float green, float blue);

public:
	unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char a;

};

#endif