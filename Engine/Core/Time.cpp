#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "Engine/Core/Time.hpp"
//---------------------------------------------------------------------------------------------------

bool			Time::s_isInitialized	= false;
LARGE_INTEGER	g_initialTime;
double			Time::s_secondsPerCount = 0.0;

//---------------------------------------------------------------------------------------------------


//************************************
// Method:    GetCurrentTimeSeconds
// FullName:  GetCurrentTimeSeconds
// Access:    public 
// Returns:   double
// Qualifier:
//************************************
double GetCurrentTimeSeconds()
{
	return Time::GetAbsoluteTimeSeconds();
}


//************************************
// Method:    Time
// FullName:  Time::Time
// Access:    public 
// Returns:   
// Qualifier:
//************************************
Time::Time()
{

}


//************************************
// Method:    ~Time
// FullName:  Time::~Time
// Access:    public 
// Returns:   
// Qualifier:
//************************************
Time::~Time()
{

}

//************************************
// Method:    InitializeTimeSystem
// FullName:  Time::InitializeTimeSystem
// Access:    public static 
// Returns:   void
// Qualifier:
//************************************
bool Time::InitializeTimeSystem()
{
	if (!s_isInitialized)
	{
		LARGE_INTEGER countsPerSecond;
		QueryPerformanceFrequency( &countsPerSecond );
		QueryPerformanceCounter( &g_initialTime );
		s_secondsPerCount = 1.0 / static_cast< double >( countsPerSecond.QuadPart );
		s_isInitialized = true;
	}
	 return  s_isInitialized;

}

//************************************
// Method:    GetAbsoluteTimeSeconds
// FullName:  Time::GetAbsoluteTimeSeconds
// Access:    public static 
// Returns:   double
// Qualifier:
//************************************
double Time::GetAbsoluteTimeSeconds()
{
	static bool initialized = s_isInitialized?true:InitializeTimeSystem();
	
	LARGE_INTEGER currentCount;
	QueryPerformanceCounter( &currentCount );
	LONGLONG elapsedCountsSinceInitialTime = currentCount.QuadPart - g_initialTime.QuadPart;

	double currentSeconds = static_cast< double >( elapsedCountsSinceInitialTime ) * s_secondsPerCount;
	return currentSeconds;
}