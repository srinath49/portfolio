#pragma once

#ifndef included_Time
#define included_Time
//---------------------------------------------------------------------------------------------------

double GetCurrentTimeSeconds();

//---------------------------------------------------------------------------------------------------
class Time
{
public:
	Time();
	~Time();

	static bool		InitializeTimeSystem();
	static double	GetAbsoluteTimeSeconds();

private:
	static bool				s_isInitialized;
	static double			s_secondsPerCount;
};

#endif // included_Time
