#pragma once

#ifndef VERTEX_DATA_H
#define VERTEX_DATA_H

#include "Engine\Math\SriMath.hpp"
#include "Engine\Core\RgbaColors.hpp"

struct VertexData
{
	Vector3DF	m_position;
	RgbaColors		m_color;
	Vector2DF	m_texCoords;
	Vector3DF	m_tangent;
	Vector3DF	m_bitangent;
	Vector3DF	m_normal;
};

struct TBN
{
public:
	Vector3DF	m_tangent;
	Vector3DF	m_bitangent;
	Vector3DF	m_normal;
};

#endif