#include "Engine/DebugConsole/BitmapFont.hpp"
#include <iostream>
#include <string>
#include "Engine/Core/AABB2.hpp"
#include "Engine/Math/SriMath.hpp"
#include "Engine/Rendering/SriOGLRenderer.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/SpriteSheet.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/TinyXml/tinyxml.h"
#include "Engine/FileIO/FileSystem.hpp"
//#include "MemoryManager/Memory/MemoryOverride.hpp"

//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
const float g_actualHeight = 63.f;
const float g_oneOverActualHeight = 1.f/g_actualHeight;

BitmapFont::BitmapFont(const std::string& fontFileName, const std::string& fontFNTFilePath)
{
	if(fontFileName == "")
	{
		m_fontSheet = nullptr;
	}
	else
	{
		m_fontSheet = new SpriteSheet(fontFileName);
		m_texWidth = 1024.f;
		m_texHeight = 1024.f;
	}
	m_fontPath = fontFNTFilePath;
}

//---------------------------------------------------------------------------------------------------
BitmapFont::~BitmapFont(void)
{

}

//---------------------------------------------------------------------------------------------------
void BitmapFont::AddNewGlyph(const char* glyphId, const char* xVal, const char* yVal, const char* width, const char* height, const char* xOffset, const char* yOffset, const char* xAdvance, const char* pageId)
{
	int idNum = atoi(glyphId);
	float x = (float)(atoi(xVal));
	float y = (float)(atoi(yVal));
	float w = (float)(atoi(width));
	float h = (float)(atoi(height));
	float xOff = (float)(atoi(xOffset));
	float yOff = (float)(atoi(yOffset));
	float xAdv = (float)(atoi(xAdvance));
	int pId = atoi(pageId);
	
	UNUSED(yOff);
	UNUSED(pId);

	float ttfA, ttfB, ttfC;
	ttfA = xOff;
	ttfB = w;
	ttfC = (xAdv - w - xOff);
	GlyphMetaData* newData = new GlyphMetaData();

	newData->m_textureCoords = GetTextureCoords(x, y, w, h);
	newData->m_ttfA = ttfA;
	newData->m_ttfB = ttfB;
	newData->m_ttfC = ttfC;

	m_glyphData[idNum] = *newData;
	//DrawAlphabet(newData->m_textureCoords.m_topLeft, newData->m_textureCoords.m_bottomRight);
	//std::cout<<idNum<<std::endl;
}

//---------------------------------------------------------------------------------------------------
AABB2 BitmapFont::GetTextureCoords(float x, float y, float w, float h)
{
	Vector2DF min, max;
	min.x = x/m_texWidth;
	min.y = y/m_texHeight;

	max.x = min.x + w/m_texWidth;
	max.y = min.y + h/m_texHeight;

	//min.Normalize();
	//max.Normalize();

	AABB2 returnAABB2;
	returnAABB2.m_topLeft = min;
	returnAABB2.m_bottomLeft = Vector2DF(min.x, min.y+max.y);
	returnAABB2.m_bottomRight = max;
	returnAABB2.m_topRight = Vector2DF(min.x+max.x, min.y);

	//returnAABB2.Normalize();

	return returnAABB2;
}

//---------------------------------------------------------------------------------------------------
void BitmapFont::DrawAlphabet(const Vector2DF& pos, float width, float height, const RgbaColors& colorToDraw)
{
	//g_myRenderer->DrawTexturedQuad(Vector3DF(0.f, 1.f, 1.f), Vector3DF(0.f, 1.f, 0.f), Vector3DF(0.f, 0.f, 0.f), Vector3DF(0.f, 0.f, 1.f), RGBA(1.f, 1.f, 1.f), g_textureID );
	g_myRenderer->RenderCharacter(pos, width, height, m_minTexCoords, m_maxTexCoords, m_fontSheet, colorToDraw );
}

//---------------------------------------------------------------------------------------------------
void BitmapFont::DrawString(const std::vector<char>& stringToDraw, const RgbaColors& colorToDraw /*= RGBA(1.f, 1.f, 1.f)*/, float cellHeight /*= 32.f*/ , const Vector2DF& positionToDraw /*= Vector2DF()*/, bool renderShadow /*= true*/)
{
	int strLen = (int)stringToDraw.size();

	Vector2DF actualPos = positionToDraw;
	float drawRatio =  cellHeight * g_oneOverActualHeight;
	float height = g_actualHeight * drawRatio;

	if(renderShadow)
	{
		Vector2DF shadowPos = positionToDraw;
		shadowPos.x += 1.5f;
		shadowPos.y -= 1.f;
		RgbaColors shadowColor(0.25f, 0.25f, 0.25f);
	
		for (int shadowTextIndex = 0; shadowTextIndex < strLen; shadowTextIndex++)
		{
			int id = stringToDraw[shadowTextIndex];
			m_minTexCoords = m_glyphData[id].m_textureCoords.m_topLeft;
			m_maxTexCoords = m_glyphData[id].m_textureCoords.m_bottomRight;

			shadowPos.x += m_glyphData[id].m_ttfA;
			float width = m_glyphData[id].m_ttfB * drawRatio;
			DrawAlphabet(shadowPos, width , height, shadowColor);

			shadowPos.x += (m_glyphData[id].m_ttfB + m_glyphData[id].m_ttfC) * drawRatio;
		}
	}

	for (int actualTextIndex = 0; actualTextIndex < strLen; actualTextIndex++)
	{
		int id = stringToDraw[actualTextIndex];
		m_minTexCoords = m_glyphData[id].m_textureCoords.m_topLeft;
		m_maxTexCoords = m_glyphData[id].m_textureCoords.m_bottomRight;

		actualPos.x += m_glyphData[id].m_ttfA;
		float width = m_glyphData[id].m_ttfB * drawRatio;
		DrawAlphabet(actualPos, width , height, colorToDraw);

		actualPos.x += (m_glyphData[id].m_ttfB + m_glyphData[id].m_ttfC) * drawRatio;
	}

}

//---------------------------------------------------------------------------------------------------
void BitmapFont::DrawString(const std::string& stringToDraw, const RgbaColors& colorToDraw /*= RGBA(1.f, 1.f, 1.f)*/, float cellHeight /*= 32.f*/, const Vector2DF& positionToDraw /*= Vector2DF()*/, bool renderShadow /*= true*/)
{
	std::vector<char> stringToDrawAsCharArray;
	int index = 0;
	while (index < (int)stringToDraw.length())
	{
		stringToDrawAsCharArray.push_back(stringToDraw[index]);
		index++;
	}
	DrawString(stringToDrawAsCharArray, colorToDraw, cellHeight, positionToDraw, renderShadow);
}

//---------------------------------------------------------------------------------------------------
float BitmapFont::CalculateStringWidth(const std::vector<char>& stringToDraw , float cellHeight)
{
	int strLen = (int)stringToDraw.size();

	Vector2DF startPos   = Vector2DF();
	Vector2DF currentPos = Vector2DF();

	float drawRatio =  cellHeight * g_oneOverActualHeight;
	float height = g_actualHeight * drawRatio;

	UNUSED(height);

	for (int index = 0; index < strLen; index++)
	{
		int id = stringToDraw[index];
		m_minTexCoords = m_glyphData[id].m_textureCoords.m_topLeft;
		m_maxTexCoords = m_glyphData[id].m_textureCoords.m_bottomRight;

		currentPos.x += m_glyphData[id].m_ttfA;
		float width =  (m_glyphData[id].m_ttfB+ m_glyphData[id].m_ttfC) * drawRatio;
		currentPos.x += width;
	}

	float totalStringWidth = (currentPos.x - startPos.x);
	return totalStringWidth;
}

float BitmapFont::CalculateStringWidth(const std::string& stringToDraw, float cellHeight)
{
	std::vector<char> str;
	for (int index = 0; index < stringToDraw.size(); ++index)
	{
		str.push_back(stringToDraw[index]);
	}
	return CalculateStringWidth(str, cellHeight);
}

//---------------------------------------------------------------------------------------------------
float BitmapFont::CalculateCharTTFA(char charToDraw , float cellHeight)
{
	float drawRatio = cellHeight * g_oneOverActualHeight;
	return m_glyphData[charToDraw].m_ttfA * drawRatio;
}

//---------------------------------------------------------------------------------------------------
void BitmapFont::SetUpMyMaterial(const Vector3DF& cameraPos)
{
	if(m_fontMaterial)
	{
		m_fontMaterial->SetupMaterial(cameraPos);
	}
}

//---------------------------------------------------------------------------------------------------
void BitmapFont::SetUpMyMaterial()
{
	if(m_fontMaterial)
	{
		m_fontMaterial->SetupMaterial();
	}
}

//---------------------------------------------------------------------------------------------------
bool BitmapFont::InitializeFonts(int shaderProgramId, const std::vector<std::string>& textureNames)
{
	m_fontMaterial = new Material(shaderProgramId, textureNames);

	TiXmlElement* root = nullptr;
	root = LoadXMLFileToTinyXMLDocument(m_fontMetaDataXML, m_fontPath);
	
	if (!m_fontMetaDataXML)
	{
		return false;
	}
	
	if(!root)
	{
		//std::cout << "Failed to load file: No root element." << std::endl;
		m_fontMetaDataXML->Clear();
		return false;
	}

	//m_bitmapFont = BitmapFont("Fonts/Arial_0.png");

	TiXmlElement* node = nullptr;
	node = root->FirstChildElement("font");
	if(node)
	{
		node = node->FirstChildElement("chars");
		TiXmlElement* elem = node->FirstChildElement("char");

		for(; elem != NULL; elem = elem->NextSiblingElement())
		{
			AddNewGlyph(elem->Attribute("id"), elem->Attribute("x"), elem->Attribute("y"), elem->Attribute("width"), elem->Attribute("height"), elem->Attribute("xoffset"), elem->Attribute("yoffset"), elem->Attribute("xadvance"), elem->Attribute("page"));
		}
	}
	return true;
}