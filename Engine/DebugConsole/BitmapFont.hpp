#pragma once

#ifndef BITMAP_FONT_H
#define BITMAP_FONT_H

#include <map>
#include <vector>
#include <string>
#include "Engine/Core/RgbaColors.hpp"
#include "Engine/Core/AABB2.hpp"
#include "Engine/Core/EngineCommons.hpp"
//---------------------------------------------------------------------------------------------------

class Texture;
class SpriteSheet;
class TiXmlDocument;
class Material;

//---------------------------------------------------------------------------------------------------
class BitmapFont
{
public:
	BitmapFont(const std::string& fontFileName = "", const std::string& fontFNTFilePath = "");
	~BitmapFont(void);

	bool InitializeFonts(int shaderProgramId, const std::vector<std::string>& textureNames);
	void AddNewGlyph(const char* glyphId, const char* xVal, const char* yVal, const char* width, const char* height, const char* xOffset, const char* yOffset, const char* xAdvance, const char* pageId);
	AABB2 GetTextureCoords(float x, float y, float texWidth, float texHeight);
	void DrawAlphabet(const Vector2DF& pos, float width, float height, const RgbaColors& colorToDraw);
	void DrawString(const std::vector<char>& stringToDraw, const RgbaColors& colorToDraw = RgbaColors(1.f, 1.f, 1.f), float cellHeight = 32.f, const Vector2DF& positionToDraw = Vector2DF(), bool renderShadow = true);
	void DrawString(const std::string& stringToDraw, const RgbaColors& colorToDraw = RgbaColors(1.f, 1.f, 1.f), float cellHeight = 32.f, const Vector2DF& positionToDraw = Vector2DF(), bool renderShadow = true);
	float CalculateStringWidth(const std::vector<char>& stringToDraw , float cellHeight);
	float CalculateStringWidth(const std::string& stringToDraw, float cellHeight);
	float CalculateCharTTFA(char charToDraw, float cellHeight);
	void SetUpMyMaterial(const Vector3DF& cameraPos);
	void SetUpMyMaterial();


	Texture* m_MyGlyphSheet;
	std::map<int, GlyphMetaData> m_glyphData;

	Vector2DF m_minTexCoords;
	Vector2DF m_maxTexCoords;

	SpriteSheet* m_fontSheet;

	float m_texWidth;
	float m_texHeight;
	TiXmlDocument* m_fontMetaDataXML;
	std::string m_fontPath;
	Material*	m_fontMaterial;
};

#endif