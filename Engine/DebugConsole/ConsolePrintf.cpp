#include "ConsolePrintf.hpp"
#include <stdarg.h>
#include <stdio.h>
#include <string>

void PrintToConsoleVariable(const char*, va_list, char*&);

//---------------------------------------------------------------------------------------------------
char* ConsolePrintf(const char* stringToConvert, ...)
{
	va_list args;
	char* returnChars;
	va_start(args, stringToConvert);
		PrintToConsoleVariable(stringToConvert, args, returnChars);
	va_end(args);
	return returnChars;
}

//---------------------------------------------------------------------------------------------------
void PrintToConsoleVariable(const char* stringToConvert, va_list args, char*& outString)
{
	const int buffer_size = 2048;
	char buffer[buffer_size];
	_vsnprintf_s(buffer, buffer_size, buffer_size -1, stringToConvert, args);
	outString = buffer;
}