#pragma once

#ifndef CONSOLE_PRINT_F
#define CONSOLE_PRINT_F

//---------------------------------------------------------------------------------------------------
char* ConsolePrintf(const char*, ...);
#endif