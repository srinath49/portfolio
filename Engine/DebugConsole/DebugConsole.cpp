#include "Engine/DebugConsole/DebugConsole.hpp"
// #include "Engine/TinyXml/tinyxml.h"
// #include "Engine/DebugConsole/BitmapFont.hpp"
#include "Engine/Rendering/SriOGLRenderer.hpp"
#include "Engine/Core/Time.hpp"


//---------------------------------------------------------------------------------------------------
const float g_yOffset = 5.f;
const float g_maxScreenHeight = 1600.f;
const float g_maxScreenWidth = 900.f;
const float g_minConsoleHeight = 64.f;
const float g_maxConsoleHeight = g_maxScreenHeight;
const float g_minConsoleWidth = 8.f;
const float g_maxConsoleWidth = 800.f;
const float g_minCommandPromptWidth = 0.f;
const float g_maxCommandPromptWidth = g_maxConsoleWidth;
const float g_minCommandPromptHeight = 0.f;
const float g_maxCommandPromptHeight = g_minConsoleHeight - g_yOffset;

//---------------------------------------------------------------------------------------------------
void ConsoleCommand_Help(const ConsoleCommandArgs& args  , DebugConsole& consoleReference);
void ConsoleCommand_Clear(const ConsoleCommandArgs& args , DebugConsole& consoleReference);
void ConsoleCommand_Quit(const ConsoleCommandArgs& args  , DebugConsole& consoleReference);
void ConsoleCommand_Close(const ConsoleCommandArgs& args , DebugConsole& consoleReference);

//---------------------------------------------------------------------------------------------------
DebugConsole::DebugConsole(void)
{
	InitializeConsole();
	InitializeFonts();
	
	ConsoleString string;
	ConsoleLine	line;
	string.m_string.push_back('H');
	string.m_string.push_back('e');
	string.m_string.push_back('l');
	string.m_string.push_back('l');
	string.m_string.push_back('o');
	string.m_cellHeight = 16.f;
	string.m_stringColor = RgbaColors(1.f, 1.f, 0.f);

	line.m_stringsInLine.push_back(string);
	string.m_string.clear();
	m_consoleLines.push_back(line);
	line.m_stringsInLine.clear();

	string.m_string.push_back('S');
	string.m_string.push_back('r');
	string.m_string.push_back('i');
	string.m_string.push_back('n');
	string.m_string.push_back('a');
	string.m_string.push_back('t');
	string.m_string.push_back('h');
	string.m_cellHeight = 64.f;
	string.m_stringColor = RgbaColors(1.f, 0.f, 0.f);
	line.m_stringsInLine.push_back(string);
	string.m_string.clear();
	string.m_string.push_back(' ');
	line.m_stringsInLine.push_back(string);
	string.m_string.clear();
	string.m_string.push_back('U');
	string.m_string.push_back('p');
	string.m_string.push_back('a');
	string.m_string.push_back('d');
	string.m_string.push_back('h');
	string.m_string.push_back('y');
	string.m_string.push_back('a');
	string.m_string.push_back('y');
	string.m_string.push_back('u');
	string.m_string.push_back('l');
	string.m_string.push_back('a');
	string.m_cellHeight = 64.f;
	string.m_stringColor = RgbaColors(0.f, 1.f, 0.f);
	line.m_stringsInLine.push_back(string);
	string.m_string.clear();

	m_consoleLines.push_back(line);
	line.m_stringsInLine.clear();

}

//---------------------------------------------------------------------------------------------------
DebugConsole::~DebugConsole(void)
{
	delete m_fontMetaDataXML;

// 	for (int index = 0; index < (int)m_commandRegistry.size(); index++)
// 	{
// 		delete m_commandRegistry.at();
// 	}
// 	while
}


//---------------------------------------------------------------------------------------------------
void DebugConsole::InitializeConsole()
{
	m_consoleLinesStartPosition = Vector2DF(8.f, 64.f);
	m_debugConsoleOn = false;
	m_debugCommandPromptOn = false;
	m_renderShadow = true;
	m_cursorIndex = 0;
	m_renderCursor = true;

	m_secsToShowCursor = 0.8;
	m_secsToHideCursor = 0.2;

	m_timeCursorRenderBegan = Time::GetAbsoluteTimeSeconds();


	ConsoleString commandPromptDefaultString;
	commandPromptDefaultString.m_string.push_back('>');
	commandPromptDefaultString.m_string.push_back(' ');
	commandPromptDefaultString.m_string.push_back(' ');
	commandPromptDefaultString.m_stringColor = RgbaColors(1.f, 1.f, 1.f);
	commandPromptDefaultString.m_cellHeight = 32.f;
	m_commandPromptLine.m_stringsInLine.push_back(commandPromptDefaultString);
	m_commandPromptCurrentString.m_stringColor = RgbaColors(1.f, 1.f, 1.f);
	m_commandPromptCurrentString.m_cellHeight = 32.f;
	m_commandPromptLine.m_stringsInLine.push_back(m_commandPromptCurrentString);
	
}

bool DebugConsole::InitializeFonts()
{
	const char* fontPath = "Data/Fonts/Arial.fnt";
	m_fontMetaDataXML = new TiXmlDocument(fontPath);

	m_fontMetaDataXML->LoadFile();

	TiXmlElement* root = nullptr;
	root = m_fontMetaDataXML->RootElement();
	if(!root)
	{
		std::cout << "Failed to load file: No root element." << std::endl;
		m_fontMetaDataXML->Clear();
		return false;
	}

	m_bitmapFont = BitmapFont("Fonts/Arial_0.png");

	TiXmlElement* node = nullptr;
	node = root->FirstChildElement("font");
	if(node)
	{
		node = node->FirstChildElement("chars");
		TiXmlElement* elem = node->FirstChildElement("char");
		
		//m_bitmapFont.AddNewGlyph(elem->Attribute("id"), elem->Attribute("x"), elem->Attribute("y"), elem->Attribute("width"), elem->Attribute("height"), elem->Attribute("xoffset"), elem->Attribute("yoffset"), elem->Attribute("xadvance"), elem->Attribute("page"));

		for(; elem != NULL; elem = elem->NextSiblingElement())
		{
			m_bitmapFont.AddNewGlyph(elem->Attribute("id"), elem->Attribute("x"), elem->Attribute("y"), elem->Attribute("width"), elem->Attribute("height"), elem->Attribute("xoffset"), elem->Attribute("yoffset"), elem->Attribute("xadvance"), elem->Attribute("page"));
		}
	}
	
	CommandFunction* f = ConsoleCommand_Clear;
	RegisterCommand("clear", f, "Clears the Log Screes");

	CommandFunction* f2 = ConsoleCommand_Help;
	RegisterCommand("help", f2, "Lists the Available Console Commands");

	CommandFunction* f3 = ConsoleCommand_Quit;
	RegisterCommand("quit", f3, "Quits the Game");

	CommandFunction* f4 = ConsoleCommand_Close;
	RegisterCommand("close", f4, "Clears and closes the Console");

	return true;
}

void DebugConsole::Update()
{

}

void DebugConsole::Render()
{
	
	if(m_debugCommandPromptOn)
	{
		//g_myRenderer->PushMatrix();
		DrawCommandPrompt();
		//g_myRenderer->PopMatrix();
	}
	//g_myRenderer->UnloadOrtho();
	//g_myRenderer->LoadOrtho(0.0, 1600.0, 0.0, 900.0, 0.f, 1.f);
	if(m_debugConsoleOn)
	{
		m_debugCommandPromptOn = true;
		//g_myRenderer->PushMatrix();
		DrawConsoleLayout();
		//g_myRenderer->PopMatrix();
		if(!m_consoleLines.empty())
		{
			DrawLog();
		}
	}	
}

void DebugConsole::DrawLog()
{
	//float cellHeight = 32.f;
	Vector2DF drawPosition = m_consoleLinesStartPosition;
	for (int lineIndex = (int)m_consoleLines.size() - 1; lineIndex > -1 ; lineIndex--)
	{
		ConsoleString string;
		for (int stringIndex = 0; stringIndex < (int)m_consoleLines[lineIndex].m_stringsInLine.size(); stringIndex++)
		{
			string = m_consoleLines[lineIndex].m_stringsInLine[stringIndex];
			m_bitmapFont.DrawString(string.m_string, string.m_stringColor, string.m_cellHeight, drawPosition, m_renderShadow);
			drawPosition.x += m_bitmapFont.CalculateStringWidth(string.m_string, string.m_cellHeight);
		}
		drawPosition.x = m_consoleLinesStartPosition.x;
		drawPosition.y += (string.m_cellHeight + g_yOffset);
	}
}

void DebugConsole::DrawCommandPrompt()
{
	g_myRenderer->DrawTexturedRect2D(Vector2DF(g_minCommandPromptWidth, g_maxCommandPromptHeight), Vector2DF(g_minCommandPromptWidth, g_minCommandPromptHeight), Vector2DF(g_maxCommandPromptWidth, g_minCommandPromptHeight), Vector2DF(g_maxCommandPromptWidth, g_maxCommandPromptHeight), nullptr, 0, RgbaColors(0.f, 0.f, 0.f));
	//return;
	Vector2DF drawPosition;
	Vector2DF commandLineStartPosition;
	commandLineStartPosition.x = g_minCommandPromptWidth + 4.f;
	commandLineStartPosition.y = g_minCommandPromptHeight + 1.f;
	drawPosition = commandLineStartPosition;
	
	ConsoleString string;

	for (int stringIndex = 0; stringIndex < (int)m_commandPromptLine.m_stringsInLine.size(); stringIndex++)
	{
		string = m_commandPromptLine.m_stringsInLine[stringIndex];
		m_bitmapFont.DrawString(string.m_string, string.m_stringColor, string.m_cellHeight, drawPosition, m_renderShadow);
		drawPosition.x += m_bitmapFont.CalculateStringWidth(string.m_string, string.m_cellHeight);
	}

	string.m_string.clear();
	DrawCursor();
}

void DebugConsole::DrawConsoleLayout()
{
	g_myRenderer->DrawTexturedRect2D(Vector2DF(g_minConsoleWidth, g_maxConsoleHeight), Vector2DF(g_minConsoleWidth, g_minConsoleHeight), Vector2DF(g_maxConsoleWidth, g_minConsoleHeight), Vector2DF(g_maxConsoleWidth, g_maxConsoleHeight), nullptr, 0, RgbaColors(0.3f, 0.3f, 0.3f, 0.25f));
}

void DebugConsole::DrawCursor()
{
	double currentAbsoluteTime = Time::GetAbsoluteTimeSeconds();
	if (m_renderCursor)
	{
		if (currentAbsoluteTime - m_timeCursorRenderBegan >= m_secsToShowCursor)
		{
			m_renderCursor = false;
			m_timeCursorRenderEnded = currentAbsoluteTime;
			return;
		}
		else
		{
			Vector2DF cursorPosition;
			Vector2DF commandLineStartPosition;
			commandLineStartPosition.x = g_minCommandPromptWidth + 4.f;
			commandLineStartPosition.y = g_minCommandPromptHeight + 1.f;
			cursorPosition = commandLineStartPosition;
			ConsoleString textString;
			float cursorXOffset = 0.f;
			for (int index = 0; index < m_cursorIndex; index++)
			{
				textString.m_string.push_back(m_commandPromptLine.m_stringsInLine[1].m_string[index]);
				if (index == 0)
				{
					cursorXOffset = m_bitmapFont.CalculateCharTTFA(m_commandPromptLine.m_stringsInLine[1].m_string[0], m_commandPromptLine.m_stringsInLine[1].m_cellHeight);
				}
			}
			textString.m_cellHeight = m_commandPromptLine.m_stringsInLine[1].m_cellHeight;
			cursorPosition.x = m_bitmapFont.CalculateStringWidth(m_commandPromptLine.m_stringsInLine[0].m_string, 32.f);
			cursorPosition.x += m_bitmapFont.CalculateStringWidth(textString.m_string, 32.f);
			cursorPosition.x += cursorXOffset + 1.f;
			cursorPosition.y += g_yOffset;
			float len = 32.f * (32.f/63.f);
			g_myRenderer->DrawLine(Vector3DF(cursorPosition), Vector3DF(cursorPosition.x, cursorPosition.y + len, 0.f),  m_commandPromptLine.m_stringsInLine[1].m_stringColor, 2.f);
		}
	}
	else
	{
		if (currentAbsoluteTime - m_timeCursorRenderEnded >= m_secsToHideCursor)
		{
			m_renderCursor = true;
			m_timeCursorRenderBegan = currentAbsoluteTime;
			return;
		}
	}
}


void DebugConsole::PrintToLog(const ConsoleLine& lineToAdd)
{
	UNUSED(lineToAdd);
}

// void DebugConsole::ProcessInput(unsigned char inputKey)
// {
// 	
// // 	if (!m_debugCommandPromptOn)
// // 	{
// // 		if (inputKey == '~')
// // 		{
// // 			m_debugCommandPromptOn = true;
// // 			return;
// // 		}
// // 	}
// // 	if(!m_debugConsoleOn)
// // 	{
// // 		if (inputKey == '`')
// // 		{
// // 			m_debugConsoleOn = true;
// // 			m_debugCommandPromptOn = true;
// // 			return;
// // 		}
// // 	}
// // 
// // 	if(m_debugCommandPromptOn || m_debugConsoleOn)
// // 	{
// // 		if (inputKey == VK_ESCAPE)
// // 		{
// // 			m_cursorIndex = 0;
// // 			if(m_commandPromptLine.m_stringsInLine[1].m_string.empty())
// // 			{
// // 				m_debugConsoleOn = false;
// // 				m_debugCommandPromptOn = false;
// // 				return;
// // 			}
// // 			else
// // 			{
// // 				m_commandPromptLine.m_stringsInLine[1].m_string.clear();
// // 				return;
// // 			}
// // 		}
// // 		if (inputKey == VK_RETURN)
// // 		{
// // 			m_cursorIndex = 0;
// // 			if(m_commandPromptLine.m_stringsInLine[1].m_string.empty())
// // 			{
// // 				m_debugConsoleOn = false;
// // 				m_debugCommandPromptOn = false;
// // 				return;
// // 			}
// // 			ConsoleLine lineToAdd;
// // 			lineToAdd.m_stringsInLine.push_back(m_commandPromptLine.m_stringsInLine[1]);
// // 			m_consoleLines.push_back(lineToAdd);
// // 			ClearCurrentString();
// // 			char* commandName;
// // 			bool goodCommand = ExecuteCommandString(GetCommandNameFromLine(lineToAdd, commandName));
// // 			UNUSED(goodCommand); // Would Come in Handy later when displaying error messages
// // 			return;
// // 		}
// // 		if (inputKey == VK_TAB)
// // 		{
// // 			ConsoleString stringToAppend;
// // 			stringToAppend.m_string.push_back(VK_SPACE);
// // 			stringToAppend.m_string.push_back(VK_SPACE);
// // 			stringToAppend.m_string.push_back(VK_SPACE);
// // 			stringToAppend.m_string.push_back(VK_SPACE);
// // 			AppendString(stringToAppend);
// // 			return;
// // 		}
// // 		if (inputKey == VK_RIGHT)
// // 		{
// // 			return;
// // 		}
// // 		if (inputKey == VK_LEFT)
// // 		{
// // 			return;
// // 		}
// // 		if (inputKey == VK_INSERT)
// // 		{
// // 			return;
// // 		}
// // 		if (inputKey == VK_BACK)
// // 		{
// // 			return;
// // 		}
// // 		if (inputKey == VK_DELETE)
// // 		{
// // 			return;
// // 		}
// // 		else
// // 		{
// // 			ConsoleString stringToAppend;
// // 			stringToAppend.m_string.push_back(inputKey);
// // 			AppendString(stringToAppend);
// // 			return;
// // 		}
// // 	}
// }

// void DebugConsole::ProcessOtherInput(unsigned char inputKey)
// {
// // 	if (inputKey == VK_RIGHT)
// // 	{
// // 		if(m_cursorIndex < (int)m_commandPromptLine.m_stringsInLine[1].m_string.size())
// // 		{
// // 			m_cursorIndex++;
// // 		}
// // 		return;
// // 	}
// // 	if (inputKey == VK_LEFT)
// // 	{
// // 		if(m_cursorIndex > 0)
// // 		{
// // 			m_cursorIndex--;
// // 		}
// // 		return;
// // 	}
// // 	if (inputKey == VK_END)
// // 	{
// // 		m_cursorIndex = m_commandPromptLine.m_stringsInLine[1].m_string.size();
// // 		return;
// // 	}
// // 	if (inputKey == VK_HOME)
// // 	{
// // 		m_cursorIndex = 0;
// // 		return;
// // 	}
// // 	if (inputKey == VK_INSERT)
// // 	{
// // 		m_renderShadow = !m_renderShadow;
// // 		return;
// // 	}
// // 	if (inputKey == VK_BACK)
// // 	{
// // 		DeleteLeft();
// // 		return;
// // 	}
// // 	if (inputKey == VK_DELETE)
// // 	{
// // 		DeleteRight();
// // 		return;
// // 	}
// }

void DebugConsole::DeleteLeft()
{	if (m_cursorIndex > 0)
	{
		for (int deleteIndex = m_cursorIndex-1; deleteIndex < (int)m_commandPromptLine.m_stringsInLine[1].m_string.size() - 1; deleteIndex++)
		{
			m_commandPromptLine.m_stringsInLine[1].m_string[deleteIndex] = m_commandPromptLine.m_stringsInLine[1].m_string[deleteIndex+1];
		}
		m_commandPromptLine.m_stringsInLine[1].m_string.pop_back();
		m_cursorIndex--;
	}
}

void DebugConsole::DeleteRight()
{
	if (m_cursorIndex < (int)m_commandPromptLine.m_stringsInLine[1].m_string.size())
	{
		for (int deleteIndex = m_cursorIndex; deleteIndex < (int)m_commandPromptLine.m_stringsInLine[1].m_string.size() - 1; deleteIndex++)
		{
			m_commandPromptLine.m_stringsInLine[1].m_string[deleteIndex] = m_commandPromptLine.m_stringsInLine[1].m_string[deleteIndex+1];
		}
		m_commandPromptLine.m_stringsInLine[1].m_string.pop_back();
	}
}

void DebugConsole::AppendString(const ConsoleString& stringToAppend)
{
	ConsoleString newString;
	newString.m_cellHeight = m_commandPromptLine.m_stringsInLine[1].m_cellHeight;
	newString.m_stringColor = m_commandPromptLine.m_stringsInLine[1].m_stringColor;
	int oldCursorIndex;
	for(int index = 0; index < m_cursorIndex; index++)
	{
		if(m_commandPromptLine.m_stringsInLine[1].m_string.empty())
		{
			break;
		}
		newString.m_string.push_back(m_commandPromptLine.m_stringsInLine[1].m_string[index]);
	}
	oldCursorIndex = m_cursorIndex;
	for(int index = 0; index < (int)stringToAppend.m_string.size(); index++)
	{
		newString.m_string.push_back(stringToAppend.m_string[index]);
		m_cursorIndex++;
	}
	for(int index = oldCursorIndex; index < (int)m_commandPromptLine.m_stringsInLine[1].m_string.size(); index++)
	{
		newString.m_string.push_back(m_commandPromptLine.m_stringsInLine[1].m_string[index]);
	}

	m_commandPromptLine.m_stringsInLine[1] = newString;
	m_renderCursor = true;
	m_timeCursorRenderBegan = Time::GetAbsoluteTimeSeconds();
}

//---------------------------------------------------------------------------------------------------
void DebugConsole::RegisterCommand(const std::string& commandName, CommandFunction* functionPointer, const std::string& functionDescription)
{
	FunctionCommand* command = new FunctionCommand();
	command->m_functionPointer = functionPointer;
	command->m_functionDescription = functionDescription;
	m_commandRegistry[commandName]  = command;
}

//---------------------------------------------------------------------------------------------------
void DebugConsole::ExecuteCommand(const std::string& commandName, const ConsoleCommandArgs& args)
{
	m_commandRegistry[commandName]->m_functionPointer(args, *this);
}

//---------------------------------------------------------------------------------------------------
bool DebugConsole::ExecuteCommandString(const char* commandStringWithArgs)
{
	FunctionCommand* command = nullptr;
	static std::map< std::string, FunctionCommand* >::const_iterator commandInRegistry;
	commandInRegistry = m_commandRegistry.find(commandStringWithArgs);
	if(commandInRegistry != m_commandRegistry.end())
	{
		command = commandInRegistry->second;
	}
	ConsoleCommandArgs fakeArgs;
	if (command)
	{
		ExecuteCommand(commandStringWithArgs, fakeArgs);
		return true;
	}

	return false;
}

//---------------------------------------------------------------------------------------------------
char* DebugConsole::GetCommandNameFromLine(ConsoleLine lineToAdd, char*& commandName)
{
	int size = 0;
	size += (int)(lineToAdd.m_stringsInLine[0].m_string.size());

	//size++;
	commandName = new char[size];
	int index;
	for (index = 0; index < size; index++)
	{
		if(lineToAdd.m_stringsInLine[0].m_string[index] == ' ')
		{
			break;
		}
		commandName[index] = lineToAdd.m_stringsInLine[0].m_string[index];
	}
	commandName[index] = '\0';
	return commandName;
}

void DebugConsole::ClearCurrentString()
{
	std::vector<char> emptyVec;
	m_commandPromptLine.m_stringsInLine[1].m_string.clear();
	m_commandPromptLine.m_stringsInLine[1].m_string.swap(emptyVec);
	m_commandPromptLine.m_stringsInLine[1].m_stringColor = RgbaColors(1.f, 1.f, 1.f);
	m_commandPromptLine.m_stringsInLine[1].m_cellHeight = 32.f;
	m_cursorIndex = 0;
}

//---------------------------------------------------------------------------------------------------
void ConsoleCommand_Help(const ConsoleCommandArgs& args, DebugConsole& consoleReference)
{
	UNUSED(args);
	UNUSED(consoleReference);
}

//---------------------------------------------------------------------------------------------------
void ConsoleCommand_Clear(const ConsoleCommandArgs& args, DebugConsole& consoleReference)
{
	UNUSED(args);
	while (!consoleReference.m_consoleLines.empty())
	{
		consoleReference.m_consoleLines.pop_back();
	}
	
}

//---------------------------------------------------------------------------------------------------
void ConsoleCommand_Quit(const ConsoleCommandArgs& args, DebugConsole& consoleReference)
{
	UNUSED(args);
	UNUSED(consoleReference);
	exit(0);
}

//---------------------------------------------------------------------------------------------------
void ConsoleCommand_Close(const ConsoleCommandArgs& args, DebugConsole& consoleReference)
{
	UNUSED(args);
	consoleReference.m_commandPromptLine.m_stringsInLine[1].m_string.clear();
	consoleReference.m_debugCommandPromptOn = false;
	consoleReference.m_debugConsoleOn = false;
}
