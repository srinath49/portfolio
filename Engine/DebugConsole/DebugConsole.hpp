#pragma once

#ifndef DEBUG_CONSOLE_H
#define	DEBUG_CONSOLE_H

#include "Engine/Core/AABB2.hpp"
#include <vector>
#include <string>
#include "Engine/Core/RgbaColors.hpp"
#include <map>
#include "Engine/DebugConsole/BitmapFont.hpp"
#include "Engine/TinyXml/tinyxml.h"

//---------------------------------------------------------------------------------------------------
struct ConsoleCommandArgs
{
	std::vector<std::string> argsList;
};

class DebugConsole;

typedef void (CommandFunction)(const ConsoleCommandArgs& args, DebugConsole& consoleRef);

struct FunctionCommand
{
	CommandFunction*	m_functionPointer;
	std::string			m_functionDescription;
};

struct  ConsoleString
{
public:
	std::vector<char> m_string;
	RgbaColors	m_stringColor;
	float   m_cellHeight;
};

struct  ConsoleLine
{
public:
	std::vector<ConsoleString> m_stringsInLine;
	//char* GetCharArray();
};

//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
class DebugConsole
{
public:
	

	DebugConsole(void);
	~DebugConsole(void);

	bool InitializeFonts();

	void Update();
	void Render();
	void DrawLog();
	void DrawConsoleLayout();
	void PrintToLog(const ConsoleLine& lineToAdd);
	void ProcessInput(unsigned char inputKey);
	void ProcessOtherInput(unsigned char inputKey);
	void InitializeConsole();
	void DrawCommandPrompt();
	void DrawCursor();
	void DeleteLeft();
	void DeleteRight();
	void AppendString(const ConsoleString& stringToAppend);

	void RegisterCommand(const std::string& commandName, CommandFunction* functionPointer, const std::string& functionDescription);
	void ExecuteCommand(const std::string& commandName, const ConsoleCommandArgs& args);
	bool ExecuteCommandString(const char* commandStringWithArgs);
	char* GetCommandNameFromLine(ConsoleLine lineToAdd, char*& commandName);
	void ClearCurrentString();
	//---------------------------------------------------------------------------------------------------
	TiXmlDocument*	m_fontMetaDataXML;
	BitmapFont		m_bitmapFont;

	std::vector<ConsoleLine> m_consoleLines;
	Vector2DF m_consoleLinesStartPosition;
	bool m_debugConsoleOn;
	bool m_debugCommandPromptOn;

	ConsoleLine m_commandPromptLine;
	ConsoleString m_commandPromptCurrentString;
	bool m_renderShadow;

	bool m_renderCursor;

	double m_timeCursorRenderBegan;
	double m_timeCursorRenderEnded;
	double m_secsToShowCursor;
	double m_secsToHideCursor;
	int m_cursorIndex;

	std::map<std::string, FunctionCommand*> m_commandRegistry;
};

#endif