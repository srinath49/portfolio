#include "Engine/Entities/Agent.hpp"
#include "Engine/Entities/Item.hpp"
#include "Engine/Map/Map.hpp"
#include "Engine/Map/Cell.hpp"
#include "Engine/Core/EngineCommons.hpp"

//************************************
// Method:    Agent
// FullName:  Agent::Agent
// Access:    public 
// Returns:   
// Qualifier: : GameEntity(position, name, glyph, color, health, debugLevel), m_faction(faction), m_isInMap(isInMap), m_speed(speed), m_visibilityRange(4.f), m_stepSize(30.f), m_headGear(nullptr), m_armGear(nullptr), m_torsoGear(nullptr), m_legGear(nullptr), m_shield(nullptr), m_primaryWeapon(nullptr)
// Parameter: const Vector2DI & position
// Parameter: const std::string & name
// Parameter: char glyph
// Parameter: const RgbaColors & color
// Parameter: float health
// Parameter: const std::string & faction
// Parameter: bool isInMap
// Parameter: int speed
// Parameter: int debugLevel
//************************************
Agent::Agent(const Vector2DI& position, const std::string& name, char glyph, const RgbaColors& color, float health, const std::string& faction, bool isInMap, int speed /*= 1*/, int debugLevel /*= 1*/) :
	GameEntity(position, name, glyph, color, health, debugLevel),
	m_faction(faction),
	m_isInMap(isInMap),
	m_speed(speed),
	visibilityRange(4.f),
	stepSize(30.f),
	headGear(nullptr),
	armGear(nullptr),
	torsoGear(nullptr),
	legGear(nullptr),
	shield(nullptr),
	primaryWeapon(nullptr)
{	

}

//************************************
// Method:    ~Agent
// FullName:  Agent::~Agent
// Access:    virtual public 
// Returns:   
// Qualifier:
// Parameter: void
//************************************
Agent::~Agent(void)
{

}

//************************************
// Method:    CanAgentMoveToCell
// FullName:  Agent::CanAgentMoveToCell
// Access:    virtual public 
// Returns:   bool
// Qualifier:
// Parameter: const Vector2DI & cellPositionToCheck
//************************************
bool Agent::CanAgentMoveToCell(const Vector2DI& cellPositionToCheck)
{
	bool canMove = false;
	
	Cell* cellToCheck = m_map->GetCell(cellPositionToCheck);
	canMove = CanAgentMoveToCell(cellToCheck);	
	return canMove;
}

//************************************
// Method:    CanAgentMoveToCell
// FullName:  Agent::CanAgentMoveToCell
// Access:    virtual public 
// Returns:   bool
// Qualifier:
// Parameter: Cell * cellToCheck
//************************************
bool Agent::CanAgentMoveToCell(Cell* cellToCheck)
{
	bool canMove = false;

	if (cellToCheck)
	{
		if(cellToCheck->GetCellType() != CT_WALL && !cellToCheck->IsCellOccupied())
		{
			canMove = true;
		}
	}

	return canMove;
}

//************************************
// Method:    MoveAgentToCell
// FullName:  Agent::MoveAgentToCell
// Access:    virtual public 
// Returns:   void
// Qualifier:
// Parameter: const Vector2DI & currentCellPosition
// Parameter: const Vector2DI & newCellPosition
//************************************
void Agent::MoveAgentToCell(const Vector2DI& currentCellPosition, const Vector2DI& newCellPosition)
{
	Cell* prevCell = m_map->GetCell(currentCellPosition);
	Cell* newCell = m_map->GetCell(newCellPosition);
	if (prevCell && newCell)
	{
		prevCell->ClearOccupyingAgent();
		newCell->SetOccupyingAgent(this);
		SetCellPosition(newCellPosition);
		OnMove(newCell);
	}
}

//************************************
// Method:    OnMove
// FullName:  Agent::OnMove
// Access:    virtual public 
// Returns:   void
// Qualifier:
// Parameter: Cell * newCell
//************************************
void Agent::OnMove(Cell* newCell)
{
	newCell->OnMove();
}

//************************************
// Method:    Raycast
// FullName:  Agent::Raycast
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: float angleDegrees
// Parameter: std::set<Cell * > & visibleCells
//************************************
void Agent::Raycast(float angleDegrees, std::set<Cell*>& visibleCells)
{
	if(!m_map)
	{
		return;
	}
	//float yaw	= DegreesToRadians(angleDegrees);

	Vector2DF forwardVector = Vector2DF( cos(angleDegrees), -sin(angleDegrees));

	Vector2DI startPosition = m_mapPosition;
	Vector2DI currentPosition = startPosition;

	Vector2DF m_rayBegin = startPosition;
 
	Cell* initialCell = m_map->GetCell(startPosition);
	if (!initialCell)
	{
		return;
	}
	Vector2DI startCellCoords = initialCell->GetCellPositionOnMap();
	Vector2DI currentCellCoords = startCellCoords;

	visibleCells.insert(initialCell);
	
	float infinityF = FLT_MAX;
 
	const float tDeltaX = forwardVector.x != 0.f ? abs(1/forwardVector.x) * 0.5f : infinityF;
	const float tDeltaY = forwardVector.y != 0.f ? abs(1/forwardVector.y) * 0.5f : infinityF;
 
	int X = startPosition.x;
	int Y = startPosition.y;
 
	float maxSqrDist =  visibilityRange * visibilityRange;

	int stepX = 1;
	int stepY = 1;

	if(forwardVector.x > 0.f)
	{
		stepX = 1;
	}
	else if(forwardVector.x < 0.f)
	{
		stepX = -1;
	}

	if(forwardVector.y > 0.f)
	{
		stepY = 1;
	}
	else if(forwardVector.y < 0.f)
	{
		stepY = -1;
	}

	float tMaxX = 0.f;
	float tMaxY = 0.f;

	if(forwardVector.x == 0.f)
	{
		tMaxX = infinityF;
	}
	else
	{
		tMaxX = stepX/forwardVector.x;
		if(tMaxX < 0.f)
		{
			tMaxX *= -1.f;
		}
	}
 
	if(forwardVector.y == 0.f)
	{
		tMaxY = infinityF;
	}
	else
	{
		tMaxY = stepY/forwardVector.y;
		if(tMaxY < 0.f)
		{
			tMaxY *= -1.f;
		}
	} 
	bool doLoop = true;
	do 
	{
		if(tMaxX < tMaxY)
		{
			X += stepX;
			tMaxX += tDeltaX;
		} 
		else
		{
			Y += stepY;
			tMaxY += tDeltaY;	
		}
 
 
		currentPosition = Vector2DI(X, Y);
 
		Vector2DI hitBlockCoords;
		if( currentPosition != startPosition)
		{
			Cell* cell = m_map->GetCell(currentPosition);
			if(cell)
			{
				currentCellCoords = cell->GetCellPositionOnMap();
			}

			hitBlockCoords = currentCellCoords;
			int blockIndex = m_map->GetCellIndexFromPosition(hitBlockCoords);
 
			float distSqr = GetDistanceSquared(currentPosition, startPosition);
 
			//unsigned char blockType = 0;
 
			std::set<Cell*>::iterator cellsIter;
			if (blockIndex != -1 && distSqr < maxSqrDist)
			{
				Cell* currentCell = m_map->GetCellAtIndex(blockIndex);
				if (!currentCell)
				{
					continue;
				}
				visibleCells.insert(currentCell);
				if (currentCell->GetCellType() == CT_WALL)
				{
					break;
				}
			}
			else
			{
				break;
			}
		}
	} while (doLoop);
}

//---------------------------------------------------------------------------------------------------
void Agent::UpdateVisibility()
{
	visibleCells.clear();
	for (float degrees = 0.0f; degrees <= 360.0f; degrees += stepSize)
	{
		Raycast(degrees, visibleCells);
	}
}

//---------------------------------------------------------------------------------------------------
void Agent::PickUpItem(Item* itemToPickup)
{
	if (!itemToPickup)
	{
		return;
	}
	bool putInInventory = false;
	switch (itemToPickup->m_itemType)
	{
		case IT_WEAPON:
			if(!primaryWeapon)
			{
				primaryWeapon = itemToPickup;
			}
			else if (itemToPickup->m_attrib > primaryWeapon->m_attrib)
			{
				inventory.push_back(primaryWeapon);
				primaryWeapon = itemToPickup;
			}
			else
			{
				putInInventory = true;
			}
		break;
		case IT_CHESTARMOR:
			if (!torsoGear)
			{
				torsoGear = itemToPickup;
			}
			else if(itemToPickup->m_attrib > torsoGear->m_attrib)
			{
				inventory.push_back(torsoGear);
				torsoGear = itemToPickup;
			}
			else
			{
				putInInventory = true;
			}
			break;
		case IT_HANDARMOR:
			if (!armGear)
			{
				armGear = itemToPickup;
			}
			else if(itemToPickup->m_attrib > armGear->m_attrib)
			{
				inventory.push_back(armGear);
				armGear = itemToPickup;
			}
			else
			{
				putInInventory = true;
			}
			break;
		case IT_HEADARMOR:
			if (!headGear)
			{
				headGear = itemToPickup;
			}
			else if(itemToPickup->m_attrib > headGear->m_attrib)
			{
				inventory.push_back(headGear);
				headGear = itemToPickup;
			}
			else
			{
				putInInventory = true;
			}
			break;
		case IT_LEGSARMOR:
			if (!legGear)
			{
				legGear = itemToPickup;
			}
			else if(itemToPickup->m_attrib > legGear->m_attrib)
			{
				inventory.push_back(legGear);
				legGear = itemToPickup;
			}
			else
			{
				putInInventory = true;
			}
			break;
		case IT_SHIELD:
			if (!shield)
			{
				shield = itemToPickup;
			}
			else if(itemToPickup->m_attrib > shield->m_attrib)
			{
				inventory.push_back(shield);
				shield = itemToPickup;
			}
			else
			{
				putInInventory = true;
			}
			break;
		case IT_POTION:
			putInInventory = true;
			break;
	}
	if (putInInventory)
	{
		inventory.push_back(itemToPickup);
	}
}

//---------------------------------------------------------------------------------------------------
void Agent::FillDefenceInfo(DefenceInfo& defenceInfo_out)
{
	defenceInfo_out.m_armor			= m_armor;
	defenceInfo_out.m_defenderName  = m_name;
	defenceInfo_out.m_headGear 		= headGear ;
	defenceInfo_out.m_torsoGear		= torsoGear;
	defenceInfo_out.m_armGear  		= armGear  ;
	defenceInfo_out.m_legGear  		= legGear  ;
	defenceInfo_out.m_shield   		= shield   ;
	if(headGear || torsoGear || armGear || legGear || shield)
	{
		defenceInfo_out.m_defenceChance	= GetRandomFloatInRange(0.2f, 0.6f);
	}
	else
	{
		defenceInfo_out.m_defenceChance	= GetRandomFloatInRange(0.0f, 0.3f);
	}
}

//---------------------------------------------------------------------------------------------------
void Agent::FillAttackInfo(AttackInfo& attackInfo_out)
{
	attackInfo_out.m_damage			= m_minDamage;
	attackInfo_out.m_attackerName   = m_name;
	attackInfo_out.m_primaryWeapon	= primaryWeapon;
	if (primaryWeapon)
	{
		attackInfo_out.m_damageChance	= GetRandomFloatInRange(0.60f, 1.0f);
	}
	else
	{
		attackInfo_out.m_damageChance	= GetRandomFloatInRange(0.40f, 0.80f);
	}
}
