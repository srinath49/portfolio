#pragma once

#ifndef AGENT_H
#define AGENT_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Entities/GameEntity.hpp"
//#include "GameCode/Game/GameCommons.hpp"
#include <set>
#include <map>
//---------------------------------------------------------------------------------------------------



class Cell;
class Item;
class AIBehaviorInterface;

typedef std::map<std::string, float> MyFactionRelations;
typedef std::map<std::string, MyFactionRelations> FactionRelations;

//---------------------------------------------------------------------------------------------------
class Agent : public GameEntity
{
public:
	Agent(const Vector2DI& position, const std::string& name, char glyph, const RgbaColors& color, float health, const std::string& faction, bool isInMap, int speed = 1, int debugLevel = 1);
	virtual ~Agent(void);
	
	virtual void				Update(float deltaSeconds) {UNUSED(deltaSeconds);}
	virtual void				Render(BitmapFont* fontRendererReference) {UNUSED(fontRendererReference);}

	virtual bool				CanAgentMoveToCell(const Vector2DI& cellPositionToCheck);
	virtual bool				CanAgentMoveToCell(Cell* cellToCheck);
	virtual void				MoveAgentToCell(const Vector2DI& currentCellPosition, const Vector2DI& newCellPosition);
	virtual void				OnMove(Cell* newCell);

	inline std::string			GetFaction() const { return m_faction; }
	inline bool					GetInMap() const { return m_isInMap; }
	inline int					GetSpeed() const { return m_speed; }
	
	inline void					SetFaction(const std::string& newFaction) { m_faction = newFaction; }
	inline void					SetInMap(bool newInMap) { m_isInMap = newInMap; }
	inline void					SetSpeed(int newSpeed) { m_speed = newSpeed; }
	void						Raycast(float angleDegrees, std::set<Cell*>& visibleCells);
	void						UpdateVisibility();
	void						PickUpItem(Item* itemToPickup);

	virtual void				FillDefenceInfo(DefenceInfo& defenceInfo_out);
	virtual void				FillAttackInfo(AttackInfo& attackInfo_out);
	inline MyFactionRelations	GetFactionRelations() const { return m_factionRelations; }
	inline void					SetFactionRelations(const MyFactionRelations& newFactionRelations) { m_factionRelations = newFactionRelations; }


	std::set<Cell*>						visibleCells;
	float								visibilityRange;
	float								stepSize;
	Item*								headGear;
	Item*								torsoGear;
	Item*								legGear;
	Item*								armGear;
	Item*								shield;
	Item*								primaryWeapon;
	std::vector<Item*>					inventory;
	std::vector<AIBehaviorInterface*>	behaviors;

protected:
	std::string							m_faction;
	bool								m_isInMap;
	int									m_speed;
	std::set<Cell*>::iterator			m_visibleCellsIter;
	MyFactionRelations					m_factionRelations;
};

#endif