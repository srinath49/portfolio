#include "Engine/Entities/GameEntity.hpp"
#include "Engine/Map/MapData.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/MessageSystem/MessageSystem.hpp"
//---------------------------------------------------------------------------------------------------

int GameEntity::s_nextGameEntityID = 1;

//---------------------------------------------------------------------------------------------------
GameEntity::GameEntity(const Vector2DI& position, const std::string& name, char glyph, const RgbaColors& color, float health, int debugLevel /*= 1*/ ) :
	m_Id(s_nextGameEntityID++),
	m_name(name),
	m_glyph(glyph),
	m_defaultColor(color),
	m_currentColor(color),
	m_health(health),
	m_maxHealth(health),
	m_debugLevel(debugLevel),
	m_map(nullptr),
	m_isDead(false)
{
	SetPosition(position);
}

//---------------------------------------------------------------------------------------------------
GameEntity::~GameEntity(void)
{

}

//---------------------------------------------------------------------------------------------------
void GameEntity::SetPosition(const Vector2DI& position)
{
	m_truePosition	= position;
	m_mapPosition.x	= (int)(position.x - LEFT_OFFSET)/ONE_CELL_UNIT;
	m_mapPosition.y	= (int)(position.y - BOTTOM_OFFSET)/ONE_CELL_UNIT;
	//m_mapPosition.x /= NUMBER_OF_CELL_COLUMNS;
	//m_mapPosition.y /= NUMBER_OF_CELL_ROWS;
}

void GameEntity::SetCellPosition(const Vector2DI& position)
{
	m_mapPosition		= position;
	m_truePosition.x	= (position.x * ONE_CELL_UNIT) + (int)LEFT_OFFSET;
	m_truePosition.y	= (position.y * ONE_CELL_UNIT) + (int)BOTTOM_OFFSET;
}

//---------------------------------------------------------------------------------------------------
void GameEntity::SetMapReference(Map* map)
{
	if (map)
	{
		m_map = map;
	}
}

//---------------------------------------------------------------------------------------------------
bool GameEntity::ApplyDamage(float damageAmount)
{
	m_health -= damageAmount;
	ClampFloat(m_health, 0.0f, m_maxHealth);
	if (m_health == 0.0f)
	{
		m_isDead = true;
	}
	return m_isDead;
}