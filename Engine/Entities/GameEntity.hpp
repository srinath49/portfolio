#pragma once

#ifndef GAME_ENTITY_H
#define GAME_ENTITY_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Math/SriMath.hpp"
#include "Engine/Core/RgbaColors.hpp"
#include "Engine/Core/EngineCommons.hpp"

#include <string>
//---------------------------------------------------------------------------------------------------

class BitmapFont;
class Map;

//-----------------------------------------------------------------------------------------------
#define UNUSED(x) (void)(x);

enum CellDirection
{
	CELL_CURRENT,
	CELL_NORTH,
	CELL_SOUTH,
	CELL_EAST,
	CELL_WEST,
	CELL_NORTHEAST,
	CELL_SOUTHEAST,
	CELL_NORTHWEST,
	CELL_SOUTHWEST
};

//---------------------------------------------------------------------------------------------------
class GameEntity
{
public:
	GameEntity(const Vector2DI& position, const std::string& name, char glyph, const RgbaColors& color, float health, int debugLevel = 1);
	virtual ~GameEntity(void);

	virtual void		Update(float deltaSeconds) {UNUSED(deltaSeconds);}
	virtual void		Render(BitmapFont* fontRendererReference) {UNUSED(fontRendererReference);}
	virtual void		SetMapReference(Map* map);
	virtual bool		ApplyDamage(float damageAmount);
	virtual void		FillDefenceInfo(DefenceInfo&){}
	virtual void		FillAttackInfo(AttackInfo&){}

	inline Vector2DI	GetPosition() const{ return m_truePosition;};
	inline std::string	GetName() const { return m_name;}
	inline int			GetID() const { return m_Id;}
	inline char			GetGlyph() const { return m_glyph;}
	inline RgbaColors	GetDefaultColor() const { return m_defaultColor;}
	inline RgbaColors	GetCurrentColor() const { return m_currentColor;}
	inline float		GetMaxHealth() const { return m_maxHealth;}
	inline float		GetHealth() const { return m_health;}
	inline int			GetDebugLevel() const { return m_debugLevel;}
	inline void			SetName(const std::string& name){ m_name = name; }
	inline void			SetGlyph(const char& glyph){ m_glyph = glyph; }
	inline void			SetColor(const RgbaColors& color){ m_currentColor = color; }
	inline void			ResetColor(){ m_currentColor = m_defaultColor; }
	inline void			SetHealth(const float health){ m_health = health; }
	inline void			ResetHealth(){ m_health = m_maxHealth; }
	inline void			SetDebugLevel(int debugLevel){ m_debugLevel = debugLevel; }
	inline Vector2DI	GetCellPosition() const {return m_mapPosition;}

	void				SetPosition(const Vector2DI& position);
	void				SetCellPosition(const Vector2DI& position);

	bool				m_isDead;

protected:
	Vector2DI			m_truePosition;
	Vector2DI			m_mapPosition;
	RgbaColors			m_defaultColor;
	RgbaColors			m_currentColor;
	float				m_health;
	float				m_maxHealth;
	float				m_armor;
	float				m_minDamage;
	float				m_maxDamage;
	int					m_chanceToHit;
	int					m_debugLevel;
	int					m_Id;
	Map*				m_map;
	char				m_glyph;
	std::string			m_name;


private:
	static int			s_nextGameEntityID;
	
};

#endif