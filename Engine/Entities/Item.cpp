#include "Engine/Entities/Item.hpp"
#include "Engine/Entities/Agent.hpp"
#include "Engine/Map/Map.hpp"
#include "Engine/Map/Cell.hpp"
#include "Engine/MessageSystem/MessageSystem.hpp"
#include "Engine/DebugConsole/BitmapFont.hpp"

#include <vector>
//---------------------------------------------------------------------------------------------------
//************************************
// Method:    Item
// FullName:  Item::Item
// Access:    public 
// Returns:   
// Qualifier: : GameEntity(position, attribs.m_name, attribs.m_glyph, attribs.m_color, attribs.m_health, attribs.m_debugLevel)
// Parameter: ItemAttributes & attribs
// Parameter: const Vector2DI & position
//************************************
Item::Item(ItemAttributes& attribs, const Vector2DI& position) :
	GameEntity(position, attribs.m_name, attribs.m_glyph, attribs.m_color, attribs.m_health, attribs.m_debugLevel)
	,m_attrib(attribs.m_attrib)
	,m_itemType(attribs.m_itemType)
	,m_currentCell(nullptr)
	,m_isPickedUp(false)
	,m_pickedAgent(nullptr)
{
	
}

//************************************
// Method:    ~Item
// FullName:  Item::~Item
// Access:    virtual public 
// Returns:   
// Qualifier:
// Parameter: void
//************************************
Item::~Item(void)
{

}

//************************************
// Method:    Update
// FullName:  Item::Update
// Access:    virtual public 
// Returns:   void
// Qualifier:
// Parameter: float deltaSeconds
//************************************
void Item::Update(float deltaSeconds)
{
	UNUSED(deltaSeconds);
	if (m_itemType == IT_SPECIAL_POTION && m_isPickedUp && m_pickedAgent)
	{
		m_pickedAgent->ApplyDamage(-1.0f * m_attrib);
	}
}

//************************************
// Method:    Render
// FullName:  Item::Render
// Access:    virtual public 
// Returns:   void
// Qualifier:
// Parameter: BitmapFont * fontRendererReference
//************************************
void Item::Render(BitmapFont* fontRendererReference)
{
	if ( (!m_map || m_isPickedUp) || (!m_currentCell || (m_currentCell->GetCellVisibility() != CV_LIT && !m_map->ShouldShowAllCells()) ))
	{
		return;
	}
	if (fontRendererReference)
	{
		std::vector<char> glyphToDisplay;
		glyphToDisplay.push_back(m_glyph);
		fontRendererReference->SetUpMyMaterial();
		bool renderShadow = false;
		if (m_itemType == IT_SPECIAL_POTION)
		{
			renderShadow = true;
		}
		fontRendererReference->DrawString(glyphToDisplay, m_currentColor, 16.f, Vector2DF(m_truePosition), renderShadow);
		glyphToDisplay.pop_back();
	}
}


//************************************
// Method:    OnPickedUp
// FullName:  Item::OnPickedUp
// Access:    virtual public 
// Returns:   void
// Qualifier:
// Parameter: Agent * agentPickingUp
//************************************
void Item::OnPickedUp(Agent* agentPickingUp)
{
	std::string message = "The ";
	message += agentPickingUp->GetName();
	message += " has picked up a ";
	message += m_name;
	message += "!";
	MessageSystem::AddMessageToList(message, RgbaColors(0.75f, 0.20f, 0.75f));
	m_currentCell->RemoveItem(this);
	m_currentCell = nullptr;
	m_isPickedUp = true;
	m_pickedAgent = agentPickingUp;
}