#pragma once

#ifndef ITEM_H
#define ITEM_H
//---------------------------------------------------------------------------------------------------

#include "Engine/Entities/GameEntity.hpp"
#include "Engine/Attributes/ItemAttributes.hpp"
//---------------------------------------------------------------------------------------------------

class Agent;
class Cell;

//---------------------------------------------------------------------------------------------------
class Item : public GameEntity
{
public:
	Item(ItemAttributes& attribs, const Vector2DI& position);
	virtual ~Item(void);

	virtual void Update(float deltaSeconds);
	virtual void Render(BitmapFont* fontRendererReference);
	virtual void OnPickedUp(Agent* agentPickingUp);

	float		m_attrib;
	ItemType	m_itemType;
	Cell*		m_currentCell;
	bool		m_isPickedUp;
	Agent*		m_pickedAgent;
};

#endif