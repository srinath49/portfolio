#include "Engine/Entities/NPC.hpp"
#include "Engine/Registrations/AIBehaviorRegistration.hpp"
#include "Engine/Map/Cell.hpp"
#include "Engine/DebugConsole/BitmapFont.hpp"

#include <vector>

//---------------------------------------------------------------------------------------------------
NPC::NPC(NPCAttributes& attribs, const Vector2DI& position) :
	Agent(position, attribs.m_name, attribs.m_glyph, attribs.m_color, attribs.m_health, attribs.m_faction, attribs.m_isInMap, attribs.m_speed, attribs.m_debugLevel)
{
	visibilityRange	= 26.5f; 
	stepSize			= 20.0f;
	m_minDamage			= attribs.m_minDamage;
	m_maxDamage			= attribs.m_maxDamage;
	m_chanceToHit		= attribs.m_chanceToHit;
	m_wallMovementCost	= attribs.m_wallCost;
	m_airMovementCost	= attribs.m_airCost;
	m_waterMovementCost	= attribs.m_waterCost;
	m_lavaMovementCost	= attribs.m_lavaCost;
	InitializeBehaviours(attribs);
}

//---------------------------------------------------------------------------------------------------
NPC::~NPC(void)
{

}

//---------------------------------------------------------------------------------------------------
void NPC::Update(float deltaSeconds)
{
	UNUSED(deltaSeconds);
	int indexWithHighestChance = 0;
	float currentHighestChance = 0.0f;
	for (unsigned int chanceIndex = 0; chanceIndex < (unsigned int)behaviors.size(); ++chanceIndex)
	{
		float chance = behaviors[chanceIndex]->GetChance(m_map);
		if (chance > currentHighestChance)
		{
			currentHighestChance = chance;
			indexWithHighestChance = chanceIndex;
		}
		else if (chance == currentHighestChance)
		{
			int chancePicker = GetRandomIntegerInRange(0,1);
			if (chancePicker == 0)
			{
				indexWithHighestChance = chanceIndex;
			}
		}
	}
	for (int behaviorIndex = 0; behaviorIndex < (int)behaviors.size(); ++behaviorIndex)
	{
		if (behaviorIndex == indexWithHighestChance)
		{
			continue;
		}
		behaviors[behaviorIndex]->ClearData();
	}
	behaviors[indexWithHighestChance]->Think(m_map);

	UpdateVisibility();
}

//---------------------------------------------------------------------------------------------------
void NPC::Render(BitmapFont* fontRendererReference)
{
	if (!m_map || (m_map->GetCell(m_mapPosition)->GetCellVisibility() != CV_LIT && !m_map->ShouldShowAllCells()))
	{
		return;
	}
	if (fontRendererReference)
	{
		std::vector<char> glyphToDisplay;
		glyphToDisplay.push_back(m_glyph);
		fontRendererReference->SetUpMyMaterial();
		fontRendererReference->DrawString(glyphToDisplay, m_currentColor, 16.f, Vector2DF(m_truePosition), false);
		glyphToDisplay.pop_back();
	}
}

//---------------------------------------------------------------------------------------------------
void NPC::InitializeBehaviours(NPCAttributes& attribs)
{
	if (!attribs.m_npcBehaviorNames.empty() && ! attribs.m_npmBehaviorFilePaths.empty())
	{
		for(int behaviorIndex = 0; behaviorIndex < (int)attribs.m_npcBehaviorNames.size(); ++behaviorIndex)
		{
			AIBehaviorInterface* behavior = AIBehaviorRegistration::CreateAndGetAI(attribs.m_npcBehaviorNames[behaviorIndex], attribs.m_npmBehaviorFilePaths[behaviorIndex]);
			behavior->SetAgent(this);
			behavior->Initialize(attribs.m_npcBehaviorNames[behaviorIndex]);
			behaviors.push_back(behavior);
		}
	}
}
