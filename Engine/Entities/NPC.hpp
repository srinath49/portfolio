#pragma once

#ifndef NPC_H
#define NPC_H
//---------------------------------------------------------------------------------------------------

#include "Engine/Entities/Agent.hpp"
#include "Engine/Attributes/NPCAttributes.hpp"
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
class NPC : public Agent
{
public:
	NPC(NPCAttributes& attribs, const Vector2DI& position);
	virtual ~NPC(void);

	virtual void Update(float deltaSeconds);
	virtual void Render(BitmapFont* fontRendererReference);
	float Evaluate();
	void Think();
	void InitializeBehaviours(NPCAttributes& attribs);

protected:
	int	m_wallMovementCost;
	int	m_airMovementCost;
	int	m_waterMovementCost;
	int	m_lavaMovementCost;
};

#endif