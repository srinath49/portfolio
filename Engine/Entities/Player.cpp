#include "Engine/Entities/Player.hpp"
#include "Engine/Entities/Item.hpp"
//#include "Engine/Game/GameCommons.hpp"
#include "Engine/Map/Map.hpp"
#include "Engine/Map/Cell.hpp"
#include "Engine/CombatSystem/CombatManager.hpp"
#include "Engine/MessageSystem/MessageSystem.hpp"
#include "Engine/DebugConsole/BitmapFont.hpp"
#include "Engine/Input/InputSystem.hpp"

//---------------------------------------------------------------------------------------------------
Player::Player(const Vector2DI& position, const std::string& name, const RgbaColors& color, float health, const std::string& faction, bool isInMap, const std::string& nickName, float playerSize /*= 32.f*/, int speed /*= 1*/, int debugLevel /*= 1*/) :
	Agent(position, name, '@', color, health, faction, isInMap, speed, debugLevel),
	m_nickName(nickName),
	m_playerSize(playerSize),
	m_hasActed(false)
{

}

//---------------------------------------------------------------------------------------------------
Player::~Player(void)
{

}

//---------------------------------------------------------------------------------------------------
void Player::InitializePlayer()
{
	visibilityRange = 10.0;
	stepSize = 3.0;
	m_faction = "Humans";
	UpdatePlayerVisibility();
}

//---------------------------------------------------------------------------------------------------
void Player::Update(float deltaSeconds)
{
	UNUSED(deltaSeconds);
}

//---------------------------------------------------------------------------------------------------
void Player::Render(BitmapFont* fontRendererReference)
{
	if (fontRendererReference)
	{
		std::vector<char> glyphToDisplay;
		glyphToDisplay.push_back(m_glyph);
		fontRendererReference->SetUpMyMaterial();
		fontRendererReference->DrawString(glyphToDisplay, m_currentColor, m_playerSize/2, Vector2DF(m_truePosition), false);
		glyphToDisplay.pop_back();
	}
}

//---------------------------------------------------------------------------------------------------
void Player::ProcessInput(InputSystem* inputSystem)
{
	if (inputSystem)
	{
		Vector2DI destination(-1,-1);
		
		if ( inputSystem->HasKeyJustBeenReleased(KEY_CODE_PLUS))
		{
			m_health += 25.0f;
		}
		if ( inputSystem->HasKeyJustBeenReleased(KEY_CODE_H) || inputSystem->HasKeyJustBeenReleased(KEY_CODE_A) )
		{
			destination = Vector2DI(m_mapPosition.x - 1, m_mapPosition.y);	
		}
		if ( inputSystem->HasKeyJustBeenReleased(KEY_CODE_J) || inputSystem->HasKeyJustBeenReleased(KEY_CODE_S))
		{
			destination = Vector2DI(m_mapPosition.x, m_mapPosition.y - 1);
		}
		if ( inputSystem->HasKeyJustBeenReleased(KEY_CODE_K) || inputSystem->HasKeyJustBeenReleased(KEY_CODE_W))
		{
			destination = Vector2DI(m_mapPosition.x, m_mapPosition.y + 1);
		}
		if ( inputSystem->HasKeyJustBeenReleased(KEY_CODE_L) || inputSystem->HasKeyJustBeenReleased(KEY_CODE_D))
		{
			destination = Vector2DI(m_mapPosition.x + 1, m_mapPosition.y);
		}
		if ( inputSystem->HasKeyJustBeenReleased(KEY_CODE_Y) || inputSystem->HasKeyJustBeenReleased(KEY_CODE_Q))
		{
			destination = Vector2DI(m_mapPosition.x - 1, m_mapPosition.y + 1);	
		}
		if ( inputSystem->HasKeyJustBeenReleased(KEY_CODE_U) || inputSystem->HasKeyJustBeenReleased(KEY_CODE_E))
		{
			destination = Vector2DI(m_mapPosition.x + 1, m_mapPosition.y + 1);
		}
		if ( inputSystem->HasKeyJustBeenReleased(KEY_CODE_B) || inputSystem->HasKeyJustBeenReleased(KEY_CODE_Z))
		{
			destination = Vector2DI(m_mapPosition.x - 1, m_mapPosition.y - 1);
		}
		if ( inputSystem->HasKeyJustBeenReleased(KEY_CODE_N) || inputSystem->HasKeyJustBeenReleased(KEY_CODE_C))
		{
			destination = Vector2DI(m_mapPosition.x + 1, m_mapPosition.y - 1);
		}
		if ( inputSystem->HasKeyJustBeenReleased(KEY_CODE_M) )
		{
			UpdatePlayerVisibility();
		}
		if (inputSystem->HasKeyJustBeenReleased(KEY_CODE_O))
		{
			std::vector<Cell*> adjacentCells;
			m_map->GetAdjacentCells(m_mapPosition, adjacentCells);
			for (std::vector<Cell*>::iterator cellIter = adjacentCells.begin(); cellIter != adjacentCells.end(); ++cellIter)
			{
				if((*cellIter)->IsCellOccupied())
				{
					Feature* feature = (*cellIter)->GetOccupyingFeature();
					if (feature)
					{
						feature->SetState(false);
						(*cellIter)->ClearOccupyingFeature();
						break;
					}
				}
			}
		}
		if (inputSystem->HasKeyJustBeenReleased(KEY_CODE_COMMA))
		{
			Cell* cell = m_map->GetCell(m_mapPosition);
			if (cell)
			{
				if (!cell->items.empty())
				{
					PickUpItem(cell->items[0]);
					cell->items[0]->OnPickedUp(this);
				}
			}
		}
		if (CanAgentMoveToCell(destination) )
		{
			MoveAgentToCell(m_mapPosition, destination);
			m_hasActed = true;
		}
		else
		{
			Cell* destinationCell = m_map->GetCell(destination);
			if(destinationCell && destinationCell->IsCellOccupied())
			{
				Agent* occupier = destinationCell->GetOccupyingAgent();
				if (!occupier)
				{
					Feature* feature = destinationCell->GetOccupyingFeature();
					if (feature)
					{
						MessageSystem::AddMessageToList(std::string("There is a door on that cell. Press 'O' key to interact"), RgbaColors(0.15f, 0.25f, 0.8f));
					}
					return;
				}
				std::string occupierFaction = occupier->GetFaction();
				float relation = m_factionRelations[occupierFaction];
				if(relation > 1.0f)
				{
					AttackInfo myInfo;
					FillAttackInfo(myInfo);
					PerformCombat(myInfo, occupier);
					m_hasActed = true;
				}
				else
				{
					//Cell* playerCell = m_map->GetCell(m_mapPosition);
					//playerCell->ClearOccupyingAgent();
					//destinationCell->ClearOccupyingAgent();
					occupier->MoveAgentToCell(occupier->GetCellPosition(), m_mapPosition);
					MoveAgentToCell(m_mapPosition, destination);
				}
			}
		}
	}
}

//---------------------------------------------------------------------------------------------------
void Player::UpdatePlayerVisibility()
{
	UpdateVisibility();
	if (!m_map || m_map->ShouldShowAllCells())
	{
		return;
	}
	m_map->ClearAllVisibility();
	m_map->SetVisibility(visibleCells);
// 	for (m_visibleCellsIter = m_visibleCells.begin(); m_visibleCellsIter != m_visibleCells.end(); ++m_visibleCellsIter)
// 	{
// 		(*m_visibleCellsIter)->SetCellVisibility(CV_LIT);
// 	}
}
