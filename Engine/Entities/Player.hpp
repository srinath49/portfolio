#pragma once

#ifndef PLAYER_H
#define PLAYER_H
//---------------------------------------------------------------------------------------------------

#include "Agent.hpp"
//---------------------------------------------------------------------------------------------------

class InputSystem;


//---------------------------------------------------------------------------------------------------
class Player : public Agent
{
public:
	Player(const Vector2DI& position, const std::string& name, const RgbaColors& color, float health, const std::string& faction, bool isInMap, const std::string& nickName, float playerSize = 32.f, int speed = 1, int debugLevel = 1);
	virtual ~Player(void);

	void InitializePlayer();

	virtual void Update(float deltaSeconds);
	virtual void Render(BitmapFont* fontRendererReference);
	void ProcessInput(InputSystem* inputSystem);

	std::string GetNickName() const { return m_nickName; }
	void SetNickName(const std::string& newNickName) { m_nickName = newNickName; }
	void UpdatePlayerVisibility();
	bool m_hasActed;

protected:
	std::string m_nickName;
	float m_playerSize;
	
};

#endif