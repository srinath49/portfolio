#include "Event.hpp"



std::vector<EventHandlerFunc> Event::m_handlers;


Event::Event()
{

}

Event::~Event()
{

}

void Event::AddHandler(EventHandlerFunc handler)
{
	if (!handler)
	{
		return;
	}

	for (std::vector<EventHandlerFunc>::iterator handlersIter = m_handlers.begin(); handlersIter != m_handlers.end(); ++handlersIter)
	{
		if (handler == *handlersIter)
		{
			return;
		}
	}

	m_handlers.push_back(handler);
}

void Event::RemoveHandler(EventHandlerFunc handler)
{
	if (!handler)
	{
		return;
	}

	for (int handlersIndex = 0; handlersIndex < (int)m_handlers.size(); ++handlersIndex)
	{
		if (handler == m_handlers[handlersIndex])
		{
			m_handlers[handlersIndex] = m_handlers.back();
			m_handlers.pop_back();
		}
	}
}

void Event::NotifyHandlers()
{
	for each(EventHandlerFunc handler in m_handlers)
	{
		handler(nullptr);
	}
}

void Event::FireEvent() 
{
	NotifyHandlers();
}
