#pragma once

#ifndef	EVENT_H
#define EVENT_H

#include <vector>

typedef void (*EventHandlerFunc)(void*);

class Event
{
public:
	Event();
	virtual ~Event();

	static void AddHandler(EventHandlerFunc handler);
	static void RemoveHandler(EventHandlerFunc handler);

	static void FireEvent();

private:
	static std::vector<EventHandlerFunc> m_handlers;

	static void NotifyHandlers();

};

#endif