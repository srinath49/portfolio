#include "EventHandler.hpp"



EventHandler::EventHandler(const Event& eventToHandle, EventHandlerFunc handlerFunctionPtr)
	:m_eventToHandle(eventToHandle)
	 ,m_handlerFunctionPtr(handlerFunctionPtr)
{
	m_eventToHandle.AddHandler(m_handlerFunctionPtr);
}

EventHandler::~EventHandler()
{
	m_eventToHandle.RemoveHandler(m_handlerFunctionPtr);
}