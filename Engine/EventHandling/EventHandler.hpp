#pragma once

#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include "Event.hpp"

class EventHandler
{
public:
	EventHandler(const Event& eventToHandle, EventHandlerFunc handlerFunctionPtr);
	~EventHandler();

private:
	Event				m_eventToHandle;
	EventHandlerFunc	m_handlerFunctionPtr;

};

#endif