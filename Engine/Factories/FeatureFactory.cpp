#include "Engine/Factories/FeatureFactory.hpp"
#include "Engine/Features/Feature.hpp"
#include "Engine/TinyXml/tinyxml.h"


//---------------------------------------------------------------------------------------------------
FeatureFactoriesMap	FeatureFactory::s_featureFactories;
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
FeatureFactory::FeatureFactory(const FeatureAttributes& FeatureAttribs) :
	m_featureAttributes(FeatureAttribs)
{
	
}

//---------------------------------------------------------------------------------------------------
FeatureFactory::~FeatureFactory(void)
{
	delete m_featureTemplate;
}

//---------------------------------------------------------------------------------------------------
void FeatureFactory::Clone(Feature*& newFeature, const Vector2DI& positionToCloneAt)
{
	newFeature = new Feature(m_featureAttributes, positionToCloneAt);
}

//---------------------------------------------------------------------------------------------------
bool FeatureFactory::CreateFeatureFactoriesFromXML()
{
	bool hasLoaded = false;
	std::string docPath("Data/Features/Features.xml");
	TiXmlDocument featuresXML(docPath.c_str());

	featuresXML.LoadFile();

	TiXmlElement* root = nullptr;
	root = featuresXML.RootElement();
	if(!root)
	{
		featuresXML.Clear();
	}
	else
	{
		hasLoaded = true;
	}

	if (hasLoaded)
	{
		TiXmlElement* node = nullptr;
		node = root->FirstChildElement("Feature");
		while(node)
		{
			const char* featureName			= node->Attribute("Name");
			const char* featureTypeString	= node->Attribute("Type");
			const char* featureStateString	= node->Attribute("DefaultState");
			const char* onGlyph				= node->Attribute("OnGlyph");
			const char* offGlyph			= node->Attribute("OffGlyph");
			bool on = false;
			if (std::strcmp(featureStateString, "On") == 0)
			{
				on = true;
			}
			FeatureTypes type = FEATURE_TYPE_UNSPECIFIED;
			if(std::strcmp(featureTypeString, "Door") == 0)
			{
				type = FEATURE_TYPE_DOOR;
			}
			FeatureAttributes attribute;
			attribute.m_inOn = on;
			attribute.m_itemType = type;
			attribute.m_onGlyph = onGlyph[0];
			attribute.m_offGlyph = offGlyph[0];
			attribute.m_name = featureName;

			FeatureFactory*	factory = new FeatureFactory(attribute);
			s_featureFactories[attribute.m_name] = factory;
			node = node->NextSiblingElement();
		}
	}
	return hasLoaded;
}



