#pragma once

#ifndef FEATURE_FACTORY_H
#define FEATURE_FACTORY_H

//---------------------------------------------------------------------------------------------------
#include "Engine/Attributes/FeatureAttributes.hpp"
#include "Engine/Core/RgbaColors.hpp"

#include <map>
#include <string>
//---------------------------------------------------------------------------------------------------
class FeatureFactory;
class Feature;
//---------------------------------------------------------------------------------------------------

typedef std::map<std::string, FeatureFactory*>	FeatureFactoriesMap;

//---------------------------------------------------------------------------------------------------
class FeatureFactory
{
public:
	FeatureFactory(const FeatureAttributes& FeatureAttribs);
	~FeatureFactory(void);

	void Clone(Feature*& newFeature, const Vector2DI& positionToCloneAt);

	static bool CreateFeatureFactoriesFromXML();

	FeatureAttributes m_featureAttributes;
	Feature* m_featureTemplate;

	static FeatureFactoriesMap	s_featureFactories;
};

#endif