#include "Engine/Factories/ItemFactory.hpp"
#include "Engine/Entities/Item.hpp"
#include "Engine/TinyXml/tinyxml.h"


//---------------------------------------------------------------------------------------------------
ItemFactoriesMap	ItemFactory::s_itemFactories;
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
ItemFactory::ItemFactory(const ItemAttributes& ItemAttribs) :
	m_itemAttributes(ItemAttribs)
{

}

//---------------------------------------------------------------------------------------------------
ItemFactory::~ItemFactory(void)
{
	delete m_itemTemplate;
}

//---------------------------------------------------------------------------------------------------
void ItemFactory::Clone(Item*& newItem, const Vector2DI& positionToCloneAt)
{
	newItem = new Item(m_itemAttributes, Vector2DI(-100,-100));
	newItem->SetPosition(positionToCloneAt);
}

//---------------------------------------------------------------------------------------------------
bool ItemFactory::CreateItemFactoriesFromXML()
{
	bool returnBool = false;
	char* itemsXMLPath = "Data/Factories/ItemFactories.xml";
	TiXmlDocument m_ItemFactoriesXML(itemsXMLPath);

	m_ItemFactoriesXML.LoadFile();


	TiXmlElement* root = nullptr;
	root = m_ItemFactoriesXML.RootElement();
	if(!root)
	{
		m_ItemFactoriesXML.Clear();
	}
	else
	{
		returnBool = true;
	}

	if (returnBool)
	{
		TiXmlElement* node = nullptr;
		node = root->FirstChildElement("ItemBlueprints");
		node = node->FirstChildElement("ItemBlueprint");

		while(node)
		{
			ItemAttributes ItemAttribs;
			TiXmlElement* elem = node->FirstChildElement("ItemAttribute");
			ItemAttribs.m_name = elem->Attribute("Name");
			ItemAttribs.m_glyph = (elem->Attribute("Glyph"))[0];
			const char* colorR = elem->Attribute("ColorR");
			const char* colorG = elem->Attribute("ColorG");
			const char* colorB = elem->Attribute("ColorB");
			std::string itemType = elem->Attribute("Type");
			if (itemType == "Weapon")
			{
				ItemAttribs.m_itemType = IT_WEAPON;
			}
			else if (itemType == "HeadArmor")
			{
				ItemAttribs.m_itemType = IT_HEADARMOR;
			}
			else if (itemType == "ArmsArmor")
			{
				ItemAttribs.m_itemType = IT_HANDARMOR;
			}
			else if (itemType == "LegsArmor")
			{
				ItemAttribs.m_itemType = IT_LEGSARMOR;
			}
			else if (itemType == "ChestArmor")
			{
				ItemAttribs.m_itemType = IT_CHESTARMOR;
			}
			else if (itemType == "Potion")
			{
				ItemAttribs.m_itemType = IT_POTION;
			}
			else if (itemType == "ContinousHealing")
			{
				ItemAttribs.m_itemType = IT_SPECIAL_POTION;
			}
			ItemAttribs.m_color.Initialize((unsigned char)atoi(colorR), (unsigned char)atoi(colorG), (unsigned char)atoi(colorB));
			const char* health = elem->Attribute("Life");
			ItemAttribs.m_health = (float)atof(health);
			ItemAttribs.m_isInMap = true;
			ItemAttribs.m_debugLevel = 1;
			const char* attribVal = elem->Attribute("Attrib");
			ItemAttribs.m_attrib = (float)atof(attribVal);

			elem = elem->FirstChildElement("ItemBehavior");
			if (elem)
			{
				while(elem)
				{
					elem = elem->NextSiblingElement("ItemBehavior");
				}
			}
	
			ItemFactory* newFactory = new ItemFactory(ItemAttribs);
			s_itemFactories[ItemAttribs.m_name] = newFactory;
			node = node->NextSiblingElement();
		}
	}
	return returnBool;
}


