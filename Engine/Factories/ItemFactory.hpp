#pragma once

#ifndef ITEM_FACTORY_H
#define ITEM_FACTORY_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Attributes/ItemAttributes.hpp"
#include "Engine/Core/RgbaColors.hpp"

#include <map>
#include <string>
//---------------------------------------------------------------------------------------------------
class ItemFactory;
class Item;
//---------------------------------------------------------------------------------------------------

typedef std::map<std::string, ItemFactory*>	ItemFactoriesMap;

//---------------------------------------------------------------------------------------------------
class ItemFactory
{
public:
	ItemFactory(const ItemAttributes& npcAttribs);
	~ItemFactory(void);

	void Clone(Item*& newItem, const Vector2DI& positionToCloneAt);

	static bool CreateItemFactoriesFromXML();

	ItemAttributes m_itemAttributes;
	Item* m_itemTemplate;

	static ItemFactoriesMap	s_itemFactories;
};

#endif