#include "Engine/Factories/NPCFactory.hpp"
#include "Engine/Entities/NPC.hpp"
#include "Engine/Behaviors/AIBehaviorInterface.hpp"
#include "Engine/Registrations/AIBehaviorRegistration.hpp"
#include "Engine/TinyXml/tinyxml.h"

//---------------------------------------------------------------------------------------------------
NPCFactoriesMap	NPCFactory::s_factories;
std::string NPCFactory::s_npcFactoriesPath = "Data/Factories/NPCFactories.xml";
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
NPCFactory::NPCFactory(const NPCAttributes& npcAttribs) :
	m_npcAttributes(npcAttribs)
{
	//m_npcTemplate = new NPC(m_npcAttributes, Vector2DI(-100,-100));
}

//---------------------------------------------------------------------------------------------------
NPCFactory::~NPCFactory(void)
{
	delete m_npcTemplate;
}

//---------------------------------------------------------------------------------------------------
void NPCFactory::Clone(NPC*& newNpc, const Vector2DI& positionToCloneAt)
{
	newNpc = new NPC(m_npcAttributes, positionToCloneAt);
	newNpc->SetPosition(positionToCloneAt);
}

//---------------------------------------------------------------------------------------------------
bool NPCFactory::CreateNPCFactoriesFromXML()
{
	bool returnBool = false;
	TiXmlDocument m_npcFactoriesXML(s_npcFactoriesPath.c_str());

	m_npcFactoriesXML.LoadFile();


	TiXmlElement* root = nullptr;
	root = m_npcFactoriesXML.RootElement();
	if(!root)
	{
		m_npcFactoriesXML.Clear();
	}
	else
	{
		returnBool = true;
	}

	if (returnBool)
	{
		TiXmlElement* node = nullptr;
		node = root->FirstChildElement("NPCBlueprints");
		node = node->FirstChildElement("NPCBlueprint");
		
		while(node)
		{
			NPCAttributes npcAttribs;
			TiXmlElement* elem = node->FirstChildElement("NPCAttribute");
			npcAttribs.m_name = elem->Attribute("Name");
			npcAttribs.m_glyph = (elem->Attribute("Glyph"))[0];
			const char* colorR = elem->Attribute("ColorR");
			const char* colorG = elem->Attribute("ColorG");
			const char* colorB = elem->Attribute("ColorB");
			npcAttribs.m_color.Initialize((unsigned char)atoi(colorR), (unsigned char)atoi(colorG), (unsigned char)atoi(colorB));
			const char* healthString = elem->Attribute("Health");
			npcAttribs.m_health = (float)atof(healthString);
			const char* minDamageString = elem->Attribute("MinDamage");
			npcAttribs.m_minDamage = (float)atof(minDamageString);
			const char* maxDamageString = elem->Attribute("MaxDamage");
			npcAttribs.m_maxDamage = (float)atof(maxDamageString);
			const char* chanceToHit = elem->Attribute("ChanceToHit");
			npcAttribs.m_chanceToHit = atoi(chanceToHit);
			npcAttribs.m_faction = elem->Attribute("Faction");
			npcAttribs.m_isInMap = true;
			npcAttribs.m_speed	= 1;
			npcAttribs.m_debugLevel = 1;
			const char* wallCost = elem->Attribute("WallCost");
			const char* airCost = elem->Attribute("AirCost");
			const char* waterCost = elem->Attribute("WaterCost");
			const char* lavaCost = elem->Attribute("LavaCost");
			npcAttribs.m_wallCost = atoi(wallCost);
			npcAttribs.m_airCost = atoi(airCost);
			npcAttribs.m_waterCost = atoi(waterCost);
			npcAttribs.m_lavaCost = atoi(lavaCost);
			elem = elem->FirstChildElement("NPCBehavior");
			if (elem)
			{
				while(elem)
				{
					npcAttribs.m_npcBehaviorNames.push_back(elem->Attribute("Name"));
					elem = elem->NextSiblingElement("NPCBehavior");
				}
			}

			if (!npcAttribs.m_npcBehaviorNames.empty())
			{
				std::vector<std::string>::iterator namesIter;
				for (namesIter = npcAttribs.m_npcBehaviorNames.begin(); namesIter != npcAttribs.m_npcBehaviorNames.end(); ++namesIter)
				{
					AIBehavioursMap* s_map = AIBehaviorRegistration::GetAIBehaviorMap();
					npcAttribs.m_npmBehaviorFilePaths.push_back((*s_map)[*namesIter]->m_npcBehaviorFilePath);
				}
			}

			NPCFactory* newFactory = new NPCFactory(npcAttribs);
			s_factories[npcAttribs.m_name] = newFactory;
			node = node->NextSiblingElement();
		}
	}
	return returnBool;
}


