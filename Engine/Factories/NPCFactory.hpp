#pragma once

#ifndef NPC_FACTORY_H
#define NPC_FACTORY_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Attributes/NPCAttributes.hpp"
#include "Engine/Core/RgbaColors.hpp"

#include <map>
#include <string>
//---------------------------------------------------------------------------------------------------
class NPCFactory;
class NPC;
//---------------------------------------------------------------------------------------------------

typedef std::map<std::string, NPCFactory*>	NPCFactoriesMap;

//---------------------------------------------------------------------------------------------------
class NPCFactory
{
public:
	NPCFactory(const NPCAttributes& npcAttribs);
	~NPCFactory(void);

	void Clone(NPC*& newNpc, const Vector2DI& positionToCloneAt);

	static bool CreateNPCFactoriesFromXML();
	
	NPCAttributes m_npcAttributes;
	NPC* m_npcTemplate;

	static NPCFactoriesMap	s_factories;
	static std::string s_npcFactoriesPath;
};

#endif