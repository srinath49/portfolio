#include "Engine/Features/Feature.hpp"
#include "Engine/Map/Map.hpp"
#include "Engine/Map/Cell.hpp"
#include "Engine/DebugConsole/BitmapFont.hpp"

//************************************
// Method:    Feature
// FullName:  Feature::Feature
// Access:    public 
// Returns:   
// Qualifier: : GameEntity(Vector2DI(), name, onGlyph, RgbaColors(1.0f, 1.0f, 1.0f), 100.f)
// Parameter: std::string & name
// Parameter: char onGlyph
// Parameter: char offGlyph
// Parameter: FeatureTypes type
// Parameter: bool isOn
//************************************
Feature::Feature(const FeatureAttributes& attribs, const Vector2DI& position) :
	GameEntity(Vector2DI(), attribs.m_name, attribs.m_onGlyph, RgbaColors(1.0f, 1.0f, 1.0f), 100.f),
	m_currentCell(nullptr),
	m_onGlyph(attribs.m_onGlyph),
	m_offGlyph(attribs.m_offGlyph)
{
	SetState(attribs.m_inOn);
	m_myType = attribs.m_itemType;
	SetCellPosition(position);
}

//************************************
// Method:    ~Feature
// FullName:  Feature::~Feature
// Access:    public 
// Returns:   
// Qualifier:
// Parameter: void
//************************************
Feature::~Feature(void)
{

}

//************************************
// Method:    SetState
// FullName:  Feature::SetState
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: bool
//************************************
void Feature::SetState(bool newState)
{
	m_isOn = newState;
	SetGlyph();
}

//************************************
// Method:    SetGlyph
// FullName:  Feature::SetGlyph
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Feature::SetGlyph()
{
	if (m_isOn)
	{
		m_glyph = m_onGlyph;
	}
	else
	{
		m_glyph = m_offGlyph;
	}
}

//************************************
// Method:    Update
// FullName:  Feature::Update
// Access:    virtual public 
// Returns:   void
// Qualifier:
// Parameter: float deltaseconds
//************************************
void Feature::Update(float deltaseconds)
{
	UNUSED(deltaseconds);
}

//************************************
// Method:    Render
// FullName:  Feature::Render
// Access:    virtual public 
// Returns:   void
// Qualifier:
// Parameter: BitmapFont * fontRendererReference
//************************************
void Feature::Render(BitmapFont* fontRendererReference)
{
	if ( (!m_map) || (!m_currentCell || (m_map->GetCell(m_currentCell->GetCellPositionOnMap())->GetCellVisibility() != CV_LIT && !m_map->ShouldShowAllCells()) ))
	{
		return;
	}
	if (fontRendererReference)
	{
		std::vector<char> glyphToDisplay;
		glyphToDisplay.push_back(m_glyph);
		fontRendererReference->SetUpMyMaterial();
		fontRendererReference->DrawString(glyphToDisplay, m_currentColor, 16.f, Vector2DF(m_truePosition), false);
		glyphToDisplay.pop_back();
	}
}


//************************************
// Method:    IsOn
// FullName:  Feature::IsOn
// Access:    public 
// Returns:   bool
// Qualifier:
//************************************
bool Feature::IsOn()
{
	return m_isOn;
}

