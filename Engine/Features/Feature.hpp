#pragma once

#ifndef FEATURE_H
#define FEATURE_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Entities/GameEntity.hpp"
#include "Engine/Attributes/FeatureAttributes.hpp"


class Cell;
class BitmapFont;

//---------------------------------------------------------------------------------------------------
class Feature : public GameEntity
{
public:
	Feature(const FeatureAttributes&, const Vector2DI&);
	~Feature(void);

	void SetState(bool);
	void SetGlyph();
	virtual void Update(float deltaseconds);
	virtual void Render(BitmapFont* fontRendererReference);
	bool IsOn();

	Cell*			m_currentCell;

private:
	FeatureTypes	m_myType;
	std::string		m_name;
	bool			m_isOn;
	char			m_offGlyph;
	char			m_onGlyph;
};

#endif
