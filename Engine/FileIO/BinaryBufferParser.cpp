#include "Engine/FileIO/BinaryBufferParser.hpp"
#include "Engine/FileIO/FileLoader.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "../Core/MyMesh.hpp"


//---------------------------------------------------------------------------------------------------
BinaryBufferParser::BinaryBufferParser() :
	m_subType(1),
	m_version(1),
	m_bufferSize(0),
	m_fileLoaded(false)
{

}

//---------------------------------------------------------------------------------------------------
BinaryBufferParser::~BinaryBufferParser()
{
	while(!m_buffer.empty())
	{
		m_buffer.pop_back();
	}
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::LoadBufferFromBinaryFile(const std::string& filePath)
{
	//GetBinaryExtentionFromOBJ(filePath, m_fileName);

	if (filePath == "" || filePath == " ")
	{
		m_fileLoaded = false;
	}
	m_fileName = filePath;
	FILE *fileToLoad;
	fopen_s ( &fileToLoad , m_fileName.c_str() , "rb" );

	if (fileToLoad == NULL)
	{
		m_fileLoaded = false;
		return;
	}

	fseek(fileToLoad, 0, SEEK_END);
	size_t fsize = ftell(fileToLoad);
	rewind(fileToLoad);

	m_bufferSize = fsize;

	unsigned char* fileBuffer = new unsigned char[m_bufferSize];
	m_buffer.clear();

	if (m_buffer.capacity() < m_bufferSize)
	{
		m_buffer.reserve(m_bufferSize);
	}
	
	fread(fileBuffer, sizeof(unsigned char), fsize, fileToLoad);
	fclose(fileToLoad);

	unsigned int index = 0;
	while (index < m_bufferSize)
	{
		m_buffer.push_back(fileBuffer[index]);
		index++;
	}
	delete[] fileBuffer;
	m_fileLoaded = true;
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::SetBufferFromOtherBuffer(unsigned char*& buffer, size_t bufferSize, const std::string& filePath)
{
	m_bufferSize = bufferSize;
	
	GetBinaryExtentionFromOBJ(filePath, m_fileName);

	size_t index = 0;
	while (index < m_bufferSize)
	{
		m_buffer.push_back(buffer[index]);
		index++;
	}
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::ReadFloat(int& currentIndex, float& floatToRead)
{
	char floatArray[sizeof(float)];
	int sizeIndex = 0;
	while(sizeIndex < sizeofFloat)
	{
		floatArray[sizeIndex] = m_buffer[currentIndex];
		currentIndex++;
		sizeIndex++;
	}
	floatToRead = *((float*)floatArray);
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::ReadString(int& currentIndex, unsigned char* stringToRead)
{
	int sizeIndex = 0;

	while (m_buffer[currentIndex] != '\0') 
	{
		stringToRead[sizeIndex] = m_buffer[currentIndex];
		sizeIndex++;
		currentIndex++;
	}
	stringToRead[sizeIndex] = m_buffer[currentIndex];
	currentIndex++;
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::ReadUnsignedChar(int& currentIndex, unsigned char& charToRead)
{
	charToRead = (unsigned char)m_buffer[currentIndex];
	currentIndex++;
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::ReadUnsignedInt(int& currentIndex, unsigned int& intToRead)
{
	char* intChar = new char[4];
	intChar[0] = m_buffer[currentIndex];
	currentIndex++;
	intChar[1] = m_buffer[currentIndex];
	currentIndex++;
	intChar[2] = m_buffer[currentIndex];
	currentIndex++;
	intChar[3] = m_buffer[currentIndex];
	currentIndex++;

	int testInt = *((int*)(intChar));
	intToRead = testInt;
	delete[] intChar;
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::ReadBool(int& currentIndex, bool& boolToRead)
{
	 boolToRead = *((bool*)m_buffer[currentIndex]);
	 currentIndex++;
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::WriteFloat(float floatToWrite)
{
	int sizeIndex = 0;

	while (sizeIndex < sizeof(float))
	{
		WriteUnsignedChar(((unsigned char*)&floatToWrite)[sizeIndex]);
		sizeIndex++;
	}	
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::WriteDouble(double doubleToWrite)
{
	int sizeIndex = 0;

	while (sizeIndex < sizeof(double))
	{
		WriteUnsignedChar(((unsigned char*)&doubleToWrite)[sizeIndex]);
		sizeIndex++;
	}	
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::WriteString(const std::string& stringToWrite)
{
	unsigned char* charArray = (unsigned char*)stringToWrite.c_str();
	
	for (int stringIndex = 0; stringIndex < (int)stringToWrite.size(); stringIndex++)
	{
		WriteUnsignedChar(charArray[stringIndex]);
	}
	WriteUnsignedChar('\0');
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::WriteUnsignedChar(unsigned char charToWrite)
{
	m_buffer.push_back(charToWrite);
}


//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::WriteUnsignedInt(unsigned int intToWrite)
{
	int sizeIndex = 0;

	while (sizeIndex < sizeof(unsigned int))
	{
		WriteUnsignedChar( ( (unsigned char*)&intToWrite)[sizeIndex] );
		sizeIndex++;
	}	
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::WriteBool(bool boolToWrite)
{
	int sizeIndex = 0;

	while (sizeIndex < sizeofBool)
	{
		WriteUnsignedChar(((unsigned char*)&boolToWrite)[sizeIndex]);
		sizeIndex++;
	}	
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::ReadWordUptoCharacter(char* stringToReadFrom, int currentIndex, char charToStopAt, char* wordToFill)
{

	int charIndex = 0;
	int bufferIndex = currentIndex;

	while( stringToReadFrom[bufferIndex] != charToStopAt)
	{
		wordToFill[charIndex] = stringToReadFrom[bufferIndex];
		charIndex++;
		bufferIndex++;
	}
	wordToFill[charIndex] = '\0';
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::WriteVector2DF(Vector2DF& vector2DF)
{
	WriteFloat(vector2DF.x);
	WriteFloat(vector2DF.y);
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::WriteVector3DF(Vector3DF& vector3DF)
{
	WriteFloat(vector3DF.x);
	WriteFloat(vector3DF.y);
	WriteFloat(vector3DF.z);
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::WriteRGBA(RgbaColors& rgba)
{
	WriteUnsignedChar(rgba.r);
	WriteUnsignedChar(rgba.g);
	WriteUnsignedChar(rgba.b);
	WriteUnsignedChar(rgba.a);
}

//---------------------------------------------------------------------------------------------------
bool BinaryBufferParser::WriteBufferToFile()
{
	FILE *file;

	fopen_s(&file , m_fileName.c_str() , "wb" );
	if (!file)
	{
		return false;
	}
	fwrite (m_buffer.data(), sizeof(char), m_buffer.size(), file);
	fclose (file);

	return true;
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::GetBinaryExtentionFromOBJ(const std::string& filePath, std::string& m_fileName)
{
	char nameWithoutExtention[100];
	ReadWordUptoCharacter((char*)filePath.c_str(), 0, '.', nameWithoutExtention);
	m_fileName = nameWithoutExtention;
	m_fileName += ".C23";
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::BuildBinaryBufferFromVertexData(MyMesh* mesh, const std::string& filePath, bool isBinaryPath /*= false*/)
{
	if (!mesh)
	{
		return;
	}
	if (!isBinaryPath)
	{
		GetBinaryExtentionFromOBJ(filePath, m_fileName);
	}
	else
	{
		m_fileName = filePath;
	}
	

	unsigned int vertexesSize = (unsigned int)mesh->m_meshVertexes.size();
	if (mesh->m_meshIndexes.empty())
	{
		for (unsigned int index = 0; index < (unsigned int)mesh->m_meshVertexes.size(); index++)
		{
			mesh->m_meshIndexes.push_back(index);
		}
	}
	unsigned int indexesSize = (unsigned int)mesh->m_meshIndexes.size();

	m_buffer.reserve(vertexesSize*sizeof(ModelVertex) + indexesSize*sizeof(unsigned int) + 1024);

	WriteUnsignedChar('G');
	WriteUnsignedChar('C');
	WriteUnsignedChar('2');
	WriteUnsignedChar('3');
	WriteUnsignedChar(2);
	WriteUnsignedChar(1);
	WriteString("Test Comment");

	WriteUnsignedInt(indexesSize);
	unsigned int indexIndex = 0;
	while (indexIndex < indexesSize)
	{
		WriteUnsignedInt(mesh->m_meshIndexes[indexIndex]);
		indexIndex++;
	}

	WriteUnsignedInt(vertexesSize);
	

	unsigned int vertexesIndex = 0;
	while (vertexesIndex < vertexesSize)
	{
		WriteVector3DF(mesh->m_meshVertexes[vertexesIndex].m_position);
		WriteRGBA(mesh->m_meshVertexes[vertexesIndex].m_color);
		WriteVector2DF(mesh->m_meshVertexes[vertexesIndex].m_texCoords);
		WriteVector3DF(mesh->m_meshVertexes[vertexesIndex].m_normal);
		WriteVector3DF(mesh->m_meshVertexes[vertexesIndex].m_tangent);
		WriteVector3DF(mesh->m_meshVertexes[vertexesIndex].m_bitangent);
		vertexesIndex++;
	}

	WriteBufferToFile();
}

//************************************
// Method:    BuildBinaryBufferFromExportedVertexData
// FullName:  BinaryBufferParser::BuildBinaryBufferFromExportedVertexData
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: MyMesh * mesh
// Parameter: const std::string & filePath
// Parameter: bool isBinaryPath
//************************************
void BinaryBufferParser::BuildBinaryBufferFromExportedVertexData(MyMesh* mesh, const std::string& filePath, bool isBinaryPath /*= false*/)
{
	UNUSED(isBinaryPath);
	m_fileName = filePath;
	unsigned int vertexesSize = (unsigned int)mesh->m_meshVertexes.size();
	if (mesh->m_meshIndexes.empty())
	{
		for (unsigned int index = 0; index < (unsigned int)mesh->m_meshVertexes.size(); index++)
		{
			mesh->m_meshIndexes.push_back(index);
		}
	}
	unsigned int indexesSize = (unsigned int)mesh->m_meshIndexes.size();

	m_buffer.reserve(vertexesSize*sizeof(ModelVertex) + indexesSize*sizeof(unsigned int) + 1024);

	WriteUnsignedChar('G');
	WriteUnsignedChar('C');
	WriteUnsignedChar('2');
	WriteUnsignedChar('3');
	WriteUnsignedChar(2);
	WriteUnsignedChar(1);
	WriteString("Test Comment");

	WriteUnsignedInt(indexesSize);
	unsigned int indexIndex = 0;
	while (indexIndex < indexesSize)
	{
		WriteUnsignedInt(mesh->m_meshIndexes[indexIndex]);
		indexIndex++;
	}

	WriteUnsignedInt(vertexesSize);


	unsigned int vertexesIndex = 0;
	while (vertexesIndex < vertexesSize)
	{
		WriteVector3DF(mesh->m_meshVertexes[vertexesIndex].m_position);
		WriteRGBA(mesh->m_meshVertexes[vertexesIndex].m_color);
		WriteVector2DF(mesh->m_meshVertexes[vertexesIndex].m_texCoords);
		WriteVector3DF(mesh->m_meshVertexes[vertexesIndex].m_normal);
		WriteVector3DF(mesh->m_meshVertexes[vertexesIndex].m_tangent);
		WriteVector3DF(mesh->m_meshVertexes[vertexesIndex].m_bitangent);
		vertexesIndex++;
	}

	WriteBufferToFile();
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::ParseBinaryBufferToVertexData(MyMesh* meshToLoad)
{
	
	if (!meshToLoad)
	{
		return;
	}

	int bufferIndex = 0;

	unsigned char FourCC[4];
	unsigned char subType, version;
	unsigned char commentString[1024];

	ReadUnsignedChar(bufferIndex, FourCC[0]);
	ReadUnsignedChar(bufferIndex, FourCC[1]);
	ReadUnsignedChar(bufferIndex, FourCC[2]);
	ReadUnsignedChar(bufferIndex, FourCC[3]);
	ReadUnsignedChar(bufferIndex, subType);
	ReadUnsignedChar(bufferIndex, version);

	ReadString(bufferIndex, commentString);

	if (subType == 1)
	{
		ReadSubTypeOne(bufferIndex, meshToLoad);
	}
	else if (subType == 2)
	{
		ReadSubTypeTwo(bufferIndex, meshToLoad);
	}
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::ReadVector3DF(int& bufferIndex, Vector3DF& vector3D)
{
	ReadFloat(bufferIndex, vector3D.x);
	ReadFloat(bufferIndex, vector3D.y);
	ReadFloat(bufferIndex, vector3D.z);
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::ReadVector2DF(int& bufferIndex, Vector2DF& vector2D)
{
	ReadFloat(bufferIndex, vector2D.x);
	ReadFloat(bufferIndex, vector2D.y);
}

//---------------------------------------------------------------------------------------------------
void BinaryBufferParser::ReadRGBA(int& bufferIndex, RgbaColors& rgba)
{
	ReadUnsignedChar(bufferIndex, rgba.r);
	ReadUnsignedChar(bufferIndex, rgba.g);
	ReadUnsignedChar(bufferIndex, rgba.b);
	ReadUnsignedChar(bufferIndex, rgba.a);
}

void BinaryBufferParser::ReadSubTypeOne(int& bufferIndex, MyMesh* meshToLoad)
{
	if (!meshToLoad)
	{
		return;
	}
	meshToLoad->m_meshVertexes.clear();
	meshToLoad->m_meshIndexes.clear();
	int vertexIndex = 0;
	unsigned int vertexesSize = 0;
	ModelVertex	vertex;
	
	ReadUnsignedInt(bufferIndex, vertexesSize);

	while (vertexIndex < (int)vertexesSize)
	{
		ReadVector3DF(bufferIndex, vertex.m_position);
		ReadRGBA(bufferIndex, vertex.m_color);
		ReadVector2DF(bufferIndex, vertex.m_texCoords);
		ReadVector3DF(bufferIndex, vertex.m_normal);
		ReadVector3DF(bufferIndex, vertex.m_tangent);
		ReadVector3DF(bufferIndex, vertex.m_bitangent);
		meshToLoad->m_meshVertexes.push_back(vertex);
		meshToLoad->m_meshIndexes.push_back(vertexIndex);
		vertexIndex++;
	}
}

void BinaryBufferParser::ReadSubTypeTwo(int& bufferIndex, MyMesh* meshToLoad)
{
	if (!meshToLoad)
	{
		return;
	}
	meshToLoad->m_meshVertexes.clear();
	meshToLoad->m_meshIndexes.clear();
	//Read IndexesFirst
	int indexIndex = 0;
	unsigned int indexesSize = 0;
	ReadUnsignedInt(bufferIndex, indexesSize);
	while (indexIndex < (int)indexesSize)
	{
		unsigned int currentIndex;
		ReadUnsignedInt(bufferIndex, currentIndex);
		meshToLoad->m_meshIndexes.push_back(currentIndex);
		indexIndex++;
	}


	int vertexIndex = 0;
	unsigned int vertexesSize = 0;
	ModelVertex	vertex;
	
	ReadUnsignedInt(bufferIndex, vertexesSize);

	while (vertexIndex < (int)vertexesSize)
	{
		ReadVector3DF(bufferIndex, vertex.m_position);
		ReadRGBA(bufferIndex, vertex.m_color);
		ReadVector2DF(bufferIndex, vertex.m_texCoords);
		ReadVector3DF(bufferIndex, vertex.m_normal);
		ReadVector3DF(bufferIndex, vertex.m_tangent);
		ReadVector3DF(bufferIndex, vertex.m_bitangent);
		meshToLoad->m_meshVertexes.push_back(vertex);
		vertexIndex++;
	}
}
