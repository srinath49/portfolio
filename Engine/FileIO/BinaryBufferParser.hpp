#pragma  once

#ifndef BINARY_BUFFER_PARSER_H
#define BINARY_BUFFER_PARSER_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Core/ModelVertex.hpp"

#include <string>
#include <vector>

class MyMesh;

//---------------------------------------------------------------------------------------------------
class BinaryBufferParser
{
public:
	BinaryBufferParser();
	~BinaryBufferParser();

	void LoadBufferFromBinaryFile(const std::string& filePath);
	void SetBufferFromOtherBuffer(unsigned char*& buffer, size_t bufferSize, const std::string& filePath);
	void BuildBinaryBufferFromVertexData(MyMesh* mesh, const std::string& filePath, bool isBinaryPath = false);
	void BuildBinaryBufferFromExportedVertexData(MyMesh* mesh, const std::string& filePath, bool isBinaryPath = false);
	void ParseBinaryBufferToVertexData(MyMesh* meshToLoad);

private:
	void ReadWordUptoCharacter(char* stringToReadFrom, int currentIndex, char charToStopAt, char* wordToFill);
	void ReadFloat(int& currentIndex, float& floatToRead);
	void ReadDouble(int& currentIndex, double& doubleToRead);
	void ReadString(int& currentIndex, unsigned char* stringToRead);
	void ReadChar(int& currentIndex, char& charToRead);
	void ReadUnsignedChar(int& currentIndex, unsigned char& charToRead);
	void ReadInt(int& currentIndex, int& intToRead);
	void ReadUnsignedInt(int& currentIndex, unsigned int& intToRead);
	void ReadShort(int& currentIndex);
	void ReadUnsignedShort(int& currentIndex);
	void ReadLong(int& currentIndex);
	void ReadUnsignedLong(int& currentIndex);
	void ReadBool(int& currentIndex, bool& boolToRead);

	void WriteFloat(float floatToWrite);
	void WriteDouble(double doubleToWrite);
	void WriteString(const std::string& stringToWrite);
	void WriteChar(char charToWrite);
	void WriteUnsignedChar(unsigned char charToWrite);
	void WriteInt(int intToWrite);
	void WriteUnsignedInt(unsigned int intToWrite);
	void WriteShort(short shortToWrite);
	void WriteUnsignedShort(unsigned short shortToWrite);
	void WriteLong(long longToWrite);
	void WriteUnsignedLong(unsigned long longToWrite);
	void WriteBool(bool boolToWrite);
	void WriteVector3DF(Vector3DF& vector3DF);
	void WriteVector2DF(Vector2DF& vector2DF);
	void WriteRGBA(RgbaColors& rgba);
	bool WriteBufferToFile();
	void GetBinaryExtentionFromOBJ(const std::string& filePath, std::string& m_fileName);
	void ReadVector3DF(int& bufferIndex, Vector3DF& vector3D);
	void ReadVector2DF(int& bufferIndex, Vector2DF& vector2D);
	void ReadRGBA(int& bufferIndex, RgbaColors& rgba);
	void ReadSubTypeOne(int& bufferIndex, MyMesh* meshToLoad);
	void ReadSubTypeTwo(int& bufferIndex, MyMesh* meshToLoad);
public:
	bool						m_fileLoaded;

private:
	std::vector<unsigned char>	m_buffer;
	std::string					m_fileName;
	size_t						m_bufferSize;
	int							m_subType;
	int							m_version;
};
#endif