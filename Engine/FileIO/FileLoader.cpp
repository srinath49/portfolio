#include "Engine/FileIO/FileLoader.hpp"
#include <stdio.h>
#include <locale>

//---------------------------------------------------------------------------------------------------
FileLoader::FileLoader()
	: m_fileLoaded(false)
	, m_fileBuffer(nullptr)
{
	m_bufferSize = (size_t)-1;
	m_locale = std::locale();
}

//---------------------------------------------------------------------------------------------------
FileLoader::~FileLoader()
{
	delete[] m_fileBuffer;
}

//---------------------------------------------------------------------------------------------------
void FileLoader::LoadFile(std::string& fileToReadPath)
{
	if (fileToReadPath == "" || fileToReadPath == " ")
	{
		m_fileLoaded = false;
	}

	FILE *fileToRead;
	fopen_s ( &fileToRead , fileToReadPath.c_str() , "rb" );

	if (fileToRead == NULL)
	{
		m_fileLoaded = false;
		return;
	}

	fseek(fileToRead, 0, SEEK_END);
	size_t fsize = ftell(fileToRead);
	rewind(fileToRead);

	m_bufferSize = fsize;
	m_fileBuffer = new unsigned char[m_bufferSize];
	fread(m_fileBuffer, sizeof(unsigned char), m_bufferSize, fileToRead);
	fclose(fileToRead);

	m_fileLoaded = true;
}

//---------------------------------------------------------------------------------------------------
unsigned char FileLoader::GetCharacterAtIndex(int index)
{
	if(!m_fileLoaded || index >= (int)m_bufferSize)
	{
		return '\0';
	}

	return m_fileBuffer[index];
}

//---------------------------------------------------------------------------------------------------
void FileLoader::AdvanceIndexToNextWhiteSpace(int& currentIndex)
{
	if(!m_fileLoaded || currentIndex >= (int)m_bufferSize)
	{
		currentIndex = -1;
	}
	
	currentIndex++;

	while (currentIndex < (int)m_bufferSize)
	{
		if(std::isspace(m_fileBuffer[currentIndex], m_locale))
		{
			break;
		}
		currentIndex++;
	}
}

//---------------------------------------------------------------------------------------------------
void FileLoader::AdvanceIndexToNextNonWhiteSpace(int& currentIndex)
{
	if(!m_fileLoaded || currentIndex >= (int)m_bufferSize)
	{
		currentIndex = -1;
	}

	currentIndex++;

	while (currentIndex < (int)m_bufferSize)
	{
		if(!std::isspace(m_fileBuffer[currentIndex], m_locale))
		{
			break;
		}
		currentIndex++;
	}
}

//---------------------------------------------------------------------------------------------------
void FileLoader::AdvanceIndexToNextLine(int& currentIndex)
{
	if(!m_fileLoaded || currentIndex >= (int)m_bufferSize)
	{
		currentIndex = -1;
	}

	while (currentIndex < (int)m_bufferSize)
	{
		if(m_fileBuffer[currentIndex] == '\n')
		{
			currentIndex++;
			break;
		}
		currentIndex++;
	}
}

//---------------------------------------------------------------------------------------------------
void FileLoader::AdvanceIndexToCharacter(int& currentIndex, char characterToAdvanceTo, bool isCaseSensitive /*= false*/)
{
	currentIndex++;
	if (isCaseSensitive)
	{
		while( (currentIndex <= (int)m_bufferSize) && (m_fileBuffer[currentIndex] != characterToAdvanceTo) )
		{
			currentIndex++;
		}
	}
	else
	{
		char lowerChar =  (char)tolower(m_fileBuffer[currentIndex]);
		char upperChar =  (char)toupper(m_fileBuffer[currentIndex]);
		while( (currentIndex <= (int)m_bufferSize) &&  (lowerChar != tolower(characterToAdvanceTo) ) && (upperChar != toupper(characterToAdvanceTo) ) )
		{
			currentIndex++;
			if((currentIndex <= (int)m_bufferSize))
			{
				lowerChar = (char)tolower(m_fileBuffer[currentIndex]);
				upperChar = (char)toupper(m_fileBuffer[currentIndex]);
			}
		}
	}
}

//---------------------------------------------------------------------------------------------------
float FileLoader::GetFloatFromNextWord(int& currentIndex)
{
	if (!m_fileLoaded || currentIndex == -1 || currentIndex >= (int)m_bufferSize)
	{
		return NULL; // Replace with assertion or exception and quit application later.
	}
	
	char floatChar[50];
	int charIndex = 0;

	while(!std::isspace( m_fileBuffer[currentIndex], m_locale ) )
	{
		floatChar[charIndex] = m_fileBuffer[currentIndex];
		charIndex++;
		currentIndex++;
	}
	floatChar[charIndex] = '\0';
	float returnValue = (float)std::atof(floatChar);

	return returnValue;
}

//---------------------------------------------------------------------------------------------------
int FileLoader::GetIntFromNextWord(int& currentIndex)
{
	if (!m_fileLoaded || currentIndex == -1 || currentIndex >= (int)m_bufferSize)
	{
		return NULL; // Replace with assertion or exception and quit application later.
	}

	char intChar[50];
	int charIndex = 0;

	while(!std::isspace( m_fileBuffer[currentIndex], m_locale ) )
	{
		intChar[charIndex] = m_fileBuffer[currentIndex];
		++charIndex;
		++currentIndex;
	}
	intChar[charIndex] = '\0';
	int returnValue = std::atoi(intChar);

	return returnValue;
}

//---------------------------------------------------------------------------------------------------
int FileLoader::GetIntFromWord(const std::string& word)
{
	char intChar[50];
	int charIndex = 0;
	int currentIndex = 0;
	int size = (int)word.size();
	unsigned char* buffer = (unsigned char*)word.c_str();
	while (currentIndex < size &&!std::isspace(word[currentIndex], m_locale))
	{
		intChar[charIndex] = buffer[currentIndex];
		++charIndex;
		++currentIndex;
	}
	intChar[charIndex] = '\0';
	int returnValue = std::atoi(intChar);

	return returnValue;
}

//---------------------------------------------------------------------------------------------------
void FileLoader::ReadWord(int currentIndex, char* wordToFill)
{
	int charIndex = 0;
	int bufferIndex = currentIndex;

	while( (bufferIndex <= (int)m_bufferSize) && !isspace(m_fileBuffer[bufferIndex]) )
	{
		wordToFill[charIndex] = m_fileBuffer[bufferIndex];
		charIndex++;
		bufferIndex++;
	}
	wordToFill[charIndex] = '\0';
}

//---------------------------------------------------------------------------------------------------
void FileLoader::ReadWordUptoCharacter(int currentIndex, char charToStopAt, char* wordToFill)
{
	int charIndex = 0;
	int bufferIndex = currentIndex;

	while( (bufferIndex <= (int)m_bufferSize) && m_fileBuffer[bufferIndex] != charToStopAt)
	{
		wordToFill[charIndex] = m_fileBuffer[bufferIndex];
		charIndex++;
		bufferIndex++;
	}
	wordToFill[charIndex] = '\0';
}

//---------------------------------------------------------------------------------------------------
bool FileLoader::DoesWordHaveCharacter(char* wordToCheck, char charToTestAgainst)
{
	int index = 0;
	while(wordToCheck[index] != '\0')
	{
		if(wordToCheck[index] == charToTestAgainst)
		{
			return true;
		}
		index++;
	}
	return false;
}

//---------------------------------------------------------------------------------------------------
void FileLoader::ReadWordAsLowerCase(char* word)
{
	int i = 0;
	
	while(word[i])
	{
		word[i] = (char)tolower(word[i]);
		i++;
	}
}

//---------------------------------------------------------------------------------------------------
void FileLoader::ReadLine(int currentIndex, char* line)
{
	int charIndex = 0;
	int bufferIndex = currentIndex;

	while( (bufferIndex <= (int)m_bufferSize) && m_fileBuffer[bufferIndex] != '\n' )
	{
		line[charIndex] = m_fileBuffer[bufferIndex];
		charIndex++;
		bufferIndex++;
	}
	line[charIndex] = '\0';
}
