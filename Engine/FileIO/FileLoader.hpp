#pragma once

#ifndef FILE_LOADER_H
#define FILE_LOADER_H
//---------------------------------------------------------------------------------------------------

#include <string>
#include <locale>

//---------------------------------------------------------------------------------------------------
class FileLoader
{
public:
	FileLoader();
	virtual ~FileLoader();

	void LoadFile(std::string& fileToReadPath);

	
	void AdvanceIndexToNextWhiteSpace(int& currentIndex);
	void AdvanceIndexToNextNonWhiteSpace(int& currentIndex);
	void AdvanceIndexToNextLine(int& currentIndex);
	void AdvanceIndexToCharacter(int& currentIndex, char characterToAdvanceTo, bool isCaseSensitive = false);

	unsigned char	GetCharacterAtIndex(int index);
	float			GetFloatFromNextWord(int& currentIndex);
	int				GetIntFromNextWord(int& currentIndex);
	int				GetIntFromWord(const std::string& word);

	void ReadWord(int currentIndex, char* wordToFill);
	void ReadWordUptoCharacter(int currentIndex, char charToStopAt, char* wordToFill);
	void ReadWordAsLowerCase(char* word);
	void ReadLine(int currentIndex, char* line);
	bool DoesWordHaveCharacter(char* wordToCheck, char charToTestAgainst);


	unsigned char*	m_fileBuffer;
	bool			m_fileLoaded;
	size_t			m_bufferSize;
	std::locale		m_locale;
};

#endif