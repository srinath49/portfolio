#include "Engine/FileIO/FileSystem.hpp"
//#include "Engine/ZipUtils/ZipUtils.hpp"
#include <fstream>

unsigned char* LoadSTBIFileToBuffer(const std::string& fileName, unsigned int& x, unsigned int&y, int& numComponents, int& numReqComponents)
{
	unsigned char* bufferToLoadTo = nullptr;
//	unsigned char* zipBuffer = nullptr;
	//unsigned int zipBufferLength = 0;
	switch (g_loadMode)
	{
	case FLM_FROM_DISK:
		bufferToLoadTo = stbi_load(fileName.c_str(), (int*)&x, (int*)&y, &numComponents, numReqComponents);
		break;
	case FLM_FROM_ZIP:
		//LoadFileFromZipToBuffer(zipBuffer, fileName, zipBufferLength);
		//bufferToLoadTo = stbi_load_from_memory(zipBuffer, zipBufferLength, (int*)&x, (int*)&y, &numComponents, numReqComponents);
		break;
	case FLM_PREFER_DISK:
		bufferToLoadTo = stbi_load(fileName.c_str(), (int*)&x, (int*)&y, &numComponents, numReqComponents);
		if (!bufferToLoadTo)
		{
			//LoadFileFromZipToBuffer(zipBuffer, fileName, zipBufferLength);
			//bufferToLoadTo = stbi_load_from_memory(zipBuffer, zipBufferLength, (int*)&x, (int*)&y, &numComponents, numReqComponents);
		}
		break;
	case FLM_PREFER_ZIP:
		//LoadFileFromZipToBuffer(zipBuffer, fileName, zipBufferLength);
		//bufferToLoadTo = stbi_load_from_memory(zipBuffer, zipBufferLength, (int*)&x, (int*)&y, &numComponents, numReqComponents);
		//if (!bufferToLoadTo)
		//{
			bufferToLoadTo = stbi_load(fileName.c_str(), (int*)&x, (int*)&y, &numComponents, numReqComponents);
		//}
		break;
	}
	return bufferToLoadTo;
}

void LoadShaderFileToBuffer(const char* shaderFilePath, char*& shaderBuffer, int& numChars, FileLoadingMode& loadedMode)
{
	shaderBuffer = nullptr;
//	unsigned char* zipBuffer = nullptr;
//	unsigned int zipBufferLength = 0;

	switch (g_loadMode)
	{
	case FLM_FROM_DISK:
		LoadShaderFromDisk(shaderFilePath, shaderBuffer, numChars);
		loadedMode = FLM_FROM_DISK;
		break;
	case FLM_FROM_ZIP:
		//LoadFileFromZipToBuffer(zipBuffer, shaderFilePath, zipBufferLength);
		//numChars = zipBufferLength + 1;
		//shaderBuffer = new char[numChars];
		//shaderBuffer = (char*)(zipBuffer);
		//if (shaderBuffer)
		//{
		//	shaderBuffer[zipBufferLength] = '\0';
		//	loadedMode = FLM_FROM_ZIP;
		//}
		break;
	case FLM_PREFER_DISK:
		LoadShaderFromDisk(shaderFilePath, shaderBuffer, numChars);
		loadedMode = FLM_FROM_DISK;
		if (!shaderBuffer)
		{
			//LoadFileFromZipToBuffer(zipBuffer, shaderFilePath, zipBufferLength);
			//numChars = zipBufferLength + 1;
			//shaderBuffer = new char[numChars];
			//shaderBuffer = (char*)(zipBuffer);
			//if (shaderBuffer)
			//{
			//	shaderBuffer[zipBufferLength] = '\0';
			//	loadedMode = FLM_FROM_ZIP;
			//}
		}
		break;
	case FLM_PREFER_ZIP:
		//LoadFileFromZipToBuffer(zipBuffer, shaderFilePath, zipBufferLength);
		//numChars = zipBufferLength + 1;
		//shaderBuffer = new char[numChars];
		//shaderBuffer = (char*)(zipBuffer);
		//if (shaderBuffer)
		//{
		//	shaderBuffer[zipBufferLength] = '\0';
		//	loadedMode = FLM_FROM_ZIP;
		//}
		//else
		//{
			LoadShaderFromDisk(shaderFilePath, shaderBuffer, numChars);
			loadedMode = FLM_FROM_DISK;
		//}
		break;
	}
}

int GetNumCharsInFile(std::ifstream& file)
{
	if (!file.good())
	{
		return 0;
	}

	unsigned long seekPosition;
	unsigned long numChars;

	seekPosition = (unsigned long)file.tellg();
	file.seekg(0, std::ios::end);

	numChars = (unsigned long)file.tellg();
	file.seekg(std::ios::beg);

	return numChars;
}

void LoadShaderFromDisk(const char* shaderFilePath, char*& shaderBuffer, int& numChars)
{
	std::ifstream file;
	file.open(shaderFilePath, std::ios::in); // opens as ASCII!
	if (!file)
	{
		numChars = 1;
		return;
	}

	numChars = GetNumCharsInFile(file);


	shaderBuffer = new char[numChars + 1];


	// numChars isn't always strlen cause some characters are stripped in ascii read...
	// it is important to 0-terminate the real length later, len is just max possible value... 
	shaderBuffer[numChars] = 0;

	unsigned int i = 0;
	while (file.good())
	{
		shaderBuffer[i] = (unsigned char)file.get();       // get character from file.
		if (!file.eof())
		{
			i++;
		}
	}

	shaderBuffer[i] = 0;  // 0-terminate it at the correct position

	file.close();
}

TiXmlElement* LoadXMLFileToTinyXMLDocument(TiXmlDocument*& document, const std::string& fileName)
{
	switch (g_loadMode)	
	{
	case FLM_FROM_DISK:
		LoadXMLFromDisk(document, fileName);
		break;
	case FLM_FROM_ZIP:
		LoadXMLFromZip(document, fileName);
		break;
	case FLM_PREFER_DISK:
		LoadXMLFromDisk(document, fileName);
		if (!document->RootElement())
		{
			LoadXMLFromZip(document, fileName);
		}
		break;
	case FLM_PREFER_ZIP:
		LoadXMLFromZip(document, fileName);
		if (!document->RootElement())
		{
			LoadXMLFromDisk(document, fileName);
		}
		break;
	}
	return document->RootElement();
}

void LoadXMLFromDisk(TiXmlDocument*& document, const std::string& fileName)
{
	document = new TiXmlDocument(fileName);

	document->LoadFile();
}

void LoadXMLFromZip(TiXmlDocument*& document, const std::string& fileName)
{
	UNUSED(fileName);
	document = new TiXmlDocument();

	unsigned char* buffer = nullptr;
//	unsigned int bufferLength = 0;
	//LoadFileFromZipToBuffer(buffer, fileName, bufferLength);
	if (buffer)
	{
		document->Parse((char*)buffer, 0, TIXML_ENCODING_UTF8);
	}
}

