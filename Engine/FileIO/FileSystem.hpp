#pragma once#

#ifndef FILES_SYSTED_H
#define FILES_SYSTED_H

#include "Engine/Core/EngineCommons.hpp"
#include "Engine/Core/std_image.h"
#include "Engine/TinyXml/tinyxml.h"
//---------------------------------------------------------------------------------------------------
unsigned char* LoadSTBIFileToBuffer(const std::string& fileName, unsigned int& x, unsigned int&y, int& numComponents, int& numReqComponents);
void LoadShaderFileToBuffer(const char* shaderFilePath, char*& shaderBuffer, int& numChars, FileLoadingMode& loadedMode);
TiXmlElement* LoadXMLFileToTinyXMLDocument(TiXmlDocument*& document, const std::string& fileName);
void LoadShaderFromDisk(const char* shaderFilePath, char*& shaderBuffer, int& numChars);
void LoadShaderFromZip(const char* shaderFilePath, char*& shaderBuffer, int& numChars);
void LoadXMLFromDisk(TiXmlDocument*& document, const std::string& fileName);
void LoadXMLFromZip(TiXmlDocument*& document, const std::string& fileName);
int GetNumCharsInFile(std::ifstream& file);

#endif