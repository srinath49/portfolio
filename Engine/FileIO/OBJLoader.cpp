#include "Engine/FileIO/OBJLoader.hpp"
#include "Engine/Core/MyMesh.hpp"

#include <algorithm>
//---------------------------------------------------------------------------------------------------

void ComputeSurfaceTangentsAtVertex(Vector3DF&,	Vector3DF&, const Vector3DF&, const Vector3DF&, 
									const Vector2DF&, const Vector3DF&, const Vector2DF&,
									const Vector3DF&, const Vector2DF&);

//-----------------------------------------------------------------------------------------------
#define UNUSED(x) (void)(x);

//---------------------------------------------------------------------------------------------------
OBJLoader::OBJLoader() : 
	FileLoader()
{

}

//---------------------------------------------------------------------------------------------------
OBJLoader::~OBJLoader()
{

}

//---------------------------------------------------------------------------------------------------
bool OBJLoader::ParseVertexDataFromOBJFile(MyMesh*& mesh)
{
	if (!m_fileLoaded)
	{
		return false;
	}

	int dataIndex = 0;
	unsigned char firstCharacterInLine;
	unsigned char vertexType;
	unsigned char character;
	unsigned char commentType;
	char* word = new char;

	mesh->m_meshVertexes.clear();

	while (dataIndex != -1 && dataIndex < (int)m_bufferSize)
	{
		firstCharacterInLine = '\0';
		vertexType  = '\0';
		character   = '\0';
		commentType = '\0';

		firstCharacterInLine = GetCharacterAtIndex(dataIndex);
		ModelVertex vertex;
		switch(firstCharacterInLine)
		{
			case '#':
				dataIndex++;
				commentType = GetCharacterAtIndex(dataIndex);
				switch(commentType)
				{
					case 'B':
					case 'b':
						ReadWord(dataIndex, word);
						ReadWordAsLowerCase(word);
						
						if(strcmp(word, "beginmeta") == 0)
						{
							AdvanceIndexToNextLine(dataIndex);
							GetModelOrientation(dataIndex);
							AdvanceIndexToNextLine(dataIndex);
							GetModelScale(dataIndex);
							AdvanceIndexToNextLine(dataIndex);
						}
						//do stuff
						break;
					default:
						AdvanceIndexToNextLine(dataIndex);
						break;
				}
				//dataIndex++;
				break;
			case 'u':
			case 'U':
			case 's':
			case 'S':
			case 'g': 
			case 'G': 
				AdvanceIndexToNextLine(dataIndex);
				break;
			case 'v':
			case 'V':
				dataIndex++;
				vertexType = GetCharacterAtIndex(dataIndex);
				switch(vertexType)
				{
					case 't':
					case 'T':
						ReadTextureCoords(dataIndex);
						break;
					case 'n': 
					case 'N': 
						ReadNormals(dataIndex);
						break;
					case ' ': 
						ReadVertexesInCurrentLineAndAdvanceIndex(dataIndex);
						bool shouldReadColors = IsThereACharBeforeNextLine(dataIndex);
						if (shouldReadColors)
						{
							ReadVertexColorsInCurrentLineAndAdvanceIndex(dataIndex);
						}
						AdvanceIndexToNextLine(dataIndex);
						
						vertex.m_position = Vector3DF(m_vertexesInThisLine[0], m_vertexesInThisLine[1], m_vertexesInThisLine[2]);
						if (shouldReadColors)
						{
							vertex.m_color = RgbaColors(m_vertexColorsInThisLine[0], m_vertexColorsInThisLine[1], m_vertexColorsInThisLine[2]);
						}
						m_vertexesReadFromFile.push_back(vertex);
						break;
				}
				
				break;
			case 'f':
			case 'F':
				ReadFaceData(dataIndex);
				AdvanceIndexToNextLine(dataIndex);
				break;
			default:
				dataIndex++;
				break;
		}
	} 

	mesh->m_meshVertexes.clear();
	if (mesh->m_meshVertexes.capacity() < m_vertexesReadFromFile.size())
	{
		mesh->m_meshVertexes.reserve(m_vertexesReadFromFile.size());
	}
	
	UnrollDataIntoNonIndexedVBO(mesh->m_meshVertexes);

	ComputeTextureCoords(mesh);
	ComputeTBN(mesh);
	CorrectOrientationAndScale(mesh->m_meshVertexes);
	
	for (unsigned int index = 0; index < (unsigned int)mesh->m_meshVertexes.size(); index++)
	{
		mesh->m_meshIndexes.push_back(index);
	}
// 	for (unsigned int index = 0; index < (unsigned int)m_vertexIndexes.size(); index++)
// 	{
// 		unsigned int indexToPush = m_vertexIndexes[index]-1;
// 		mesh->m_meshIndexes.push_back(indexToPush);
// 	}
// 	for (unsigned int index = 0; index < (unsigned int)m_normalIndexes.size(); index++)
// 	{
// 		unsigned int indexOfNormalToPush = m_normalIndexes[index] - 1;
// 		modelNormals.push_back(indexOfNormalToPush);
// 	}

	return true;
}

//---------------------------------------------------------------------------------------------------
void OBJLoader::GetModelOrientation(int& dataIndex)
{
	int charIndex = 0;
	char orientation[1024];

	AdvanceIndexToCharacter(dataIndex, 'o');
	AdvanceIndexToNextWhiteSpace(dataIndex);
	
	// Read X Orientation
	AdvanceIndexToCharacter(dataIndex, 'x');
	AdvanceIndexToCharacter(dataIndex, '=');
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	ReadWord(dataIndex, orientation);
	AdvanceIndexToNextWhiteSpace(dataIndex);
	while(orientation[charIndex])
	{
		orientation[charIndex] = (char)tolower(orientation[charIndex]);
		charIndex++;
	}
	charIndex = 0;
	switch(orientation[0])
	{
		case 'f':
			m_xOrientation = FORWARD;
			break;
		case 'b':
			m_xOrientation = BACKWARD;
			break;
		case 'r':
			m_xOrientation = RIGHT;
			break;
		case 'l':
			m_xOrientation = LEFT;
			break;
		case 'u':
			m_xOrientation = UP;
			break;
		case 'd':
			m_xOrientation = DOWN;
			break;
	}
	orientation[0] = '\0';

	//---------------------------------------------------------------------------------------------------
	// Read Y Orientation
	AdvanceIndexToCharacter(dataIndex, 'y');
	AdvanceIndexToCharacter(dataIndex, '=');
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	ReadWord(dataIndex, orientation);
	AdvanceIndexToNextWhiteSpace(dataIndex);
	while(orientation[charIndex])
	{
		orientation[charIndex] = (char)tolower(orientation[charIndex]);
		charIndex++;
	}
	charIndex = 0;
	switch(orientation[0])
	{
	case 'f':
		m_yOrientation = FORWARD;
		break;
	case 'b':
		m_yOrientation = BACKWARD;
		break;
	case 'r':
		m_yOrientation = RIGHT;
		break;
	case 'l':
		m_yOrientation = LEFT;
		break;
	case 'u':
		m_yOrientation = UP;
		break;
	case 'd':
		m_yOrientation = DOWN;
		break;
	}
	orientation[0] = '\0';


	// Read Z Orientation
	AdvanceIndexToCharacter(dataIndex, 'z');
	AdvanceIndexToCharacter(dataIndex, '=');
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	ReadWord(dataIndex, orientation);
	AdvanceIndexToNextWhiteSpace(dataIndex);
	while(orientation[charIndex])
	{
		orientation[charIndex] = (char)tolower(orientation[charIndex]);
		charIndex++;
	}
	charIndex = 0;
	switch(orientation[0])
	{
	case 'f':
		m_zOrientation = FORWARD;
		break;
	case 'b':
		m_zOrientation = BACKWARD;
		break;
	case 'r':
		m_zOrientation = RIGHT;
		break;
	case 'l':
		m_zOrientation = LEFT;
		break;
	case 'u':
		m_zOrientation = UP;
		break;
	case 'd':
		m_zOrientation = DOWN;
		break;
	}
	orientation[0] = '\0';

}

//---------------------------------------------------------------------------------------------------
void OBJLoader::GetModelScale(int& dataIndex)
{
	float scale = 0.0f;

	AdvanceIndexToCharacter(dataIndex, 's');
	AdvanceIndexToNextWhiteSpace(dataIndex);
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	AdvanceIndexToCharacter(dataIndex, '=');
	AdvanceIndexToNextNonWhiteSpace(dataIndex);

	scale = GetFloatFromNextWord(dataIndex);

	m_modelScale = scale;
}

void OBJLoader::CorrectOrientationAndScale(std::vector<ModelVertex>& modelVertexes)
{
	Matrix4x4F scaleAndOrientationCorrectionMatrix;

	Vector3DF xOrientationVector = GetVector3DFromOrientation(m_xOrientation);
	Vector3DF yOrientationVector = GetVector3DFromOrientation(m_yOrientation);
	Vector3DF zOrientationVector = GetVector3DFromOrientation(m_zOrientation);

	xOrientationVector /= m_modelScale;
	yOrientationVector /= m_modelScale;
	zOrientationVector /= m_modelScale;

	scaleAndOrientationCorrectionMatrix.m_matrixElements[0]  = xOrientationVector.x;
	scaleAndOrientationCorrectionMatrix.m_matrixElements[1]  = xOrientationVector.y;
	scaleAndOrientationCorrectionMatrix.m_matrixElements[2]  = xOrientationVector.z;
		  
	scaleAndOrientationCorrectionMatrix.m_matrixElements[4]  = yOrientationVector.x;
	scaleAndOrientationCorrectionMatrix.m_matrixElements[5]  = yOrientationVector.y;
	scaleAndOrientationCorrectionMatrix.m_matrixElements[6]  = yOrientationVector.z;
		  
	scaleAndOrientationCorrectionMatrix.m_matrixElements[8]  = zOrientationVector.x;
	scaleAndOrientationCorrectionMatrix.m_matrixElements[9]  = zOrientationVector.y;
	scaleAndOrientationCorrectionMatrix.m_matrixElements[10] = zOrientationVector.z;

	
	for (int vertexIndex = 0; vertexIndex < (int)modelVertexes.size(); vertexIndex++)
	{
		modelVertexes[vertexIndex].m_position	= MatrixVector3DMultiplication(scaleAndOrientationCorrectionMatrix, modelVertexes[vertexIndex].m_position);
		modelVertexes[vertexIndex].m_normal		= MatrixVector3DMultiplication(scaleAndOrientationCorrectionMatrix, modelVertexes[vertexIndex].m_normal);
		modelVertexes[vertexIndex].m_tangent	= MatrixVector3DMultiplication(scaleAndOrientationCorrectionMatrix, modelVertexes[vertexIndex].m_tangent);
		modelVertexes[vertexIndex].m_bitangent	= MatrixVector3DMultiplication(scaleAndOrientationCorrectionMatrix, modelVertexes[vertexIndex].m_bitangent);
	}

}

//---------------------------------------------------------------------------------------------------
Vector3DF OBJLoader::GetVector3DFromOrientation(ModelOrientation orientation)
{
	Vector3DF orientationVector;
	switch(orientation)
	{
		case FORWARD:
			orientationVector = Vector3DF(1.0f, 0.0f, 0.0f);
			break;
		case BACKWARD:
			orientationVector = Vector3DF(-1.0f, 0.0f, 0.0f);
			break;
		case RIGHT:
			orientationVector = Vector3DF(0.0f, -1.0f, 0.0f);
			break;
		case LEFT:
			orientationVector = Vector3DF(0.0f, 1.0f, 0.0f);
			break;
		case UP:
			orientationVector = Vector3DF(0.0f, 0.0f, 1.0f);
			break;
		case DOWN:
			orientationVector = Vector3DF(0.0f, 0.0f, -1.0f);
			break;
	}
	return orientationVector;
}

//---------------------------------------------------------------------------------------------------
Vector3DF OBJLoader::MatrixVector3DMultiplication(const Matrix4x4F& matrix, const Vector3DF& vector)
{
	Vector3DF returnVector;

	returnVector.x = matrix.m_matrixElements[0] * vector.x + matrix.m_matrixElements[4] * vector.y + matrix.m_matrixElements[8]  * vector.z; 
	returnVector.y = matrix.m_matrixElements[1] * vector.x + matrix.m_matrixElements[5] * vector.y + matrix.m_matrixElements[9]  * vector.z; 
	returnVector.z = matrix.m_matrixElements[2] * vector.x + matrix.m_matrixElements[6] * vector.y + matrix.m_matrixElements[10] * vector.z; 

	return returnVector;
}

//---------------------------------------------------------------------------------------------------
void OBJLoader::ReadFaceData(int& dataIndex)
{
	char faceDataLine[1024];
	ReadLine(dataIndex, faceDataLine);
	bool hasSlashes = DoesWordHaveCharacter(faceDataLine, '/');
	FaceData firstData;
	FaceData prevLastData;
	ReadFirstTriangleData(dataIndex, hasSlashes, firstData, prevLastData);
	AdvanceIndexToNextWhiteSpace(dataIndex);
	while (IsThereACharBeforeNextLine(dataIndex))
	{
		ReadNextTriangleData(dataIndex, hasSlashes, firstData, prevLastData);
		AdvanceIndexToNextWhiteSpace(dataIndex);
	}
}

//---------------------------------------------------------------------------------------------------
void OBJLoader::ReadFirstTriangleData(int& dataIndex, bool hasSlashes, FaceData& first, FaceData& last)
{
	int index = 0;
	char indexWord[100];

	if(!hasSlashes)
	{
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		index = GetIntFromNextWord(dataIndex);
		m_vertexIndexes.push_back(index);
		first.m_vertexIndex = index;

		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		index = GetIntFromNextWord(dataIndex);
		m_vertexIndexes.push_back(index);

		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		index = GetIntFromNextWord(dataIndex);
		m_vertexIndexes.push_back(index);
		last.m_vertexIndex = index;
	}
	else
	{
		bool hasTexCoordsIndex = false;

		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWordUptoCharacter(dataIndex, '/', indexWord);
		index = atoi(indexWord);
		m_vertexIndexes.push_back(index);
		first.m_vertexIndex = index;
		indexWord[0] = '\0';

		AdvanceIndexToCharacter(dataIndex, '/');
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWordUptoCharacter(dataIndex, '/', indexWord);
		hasTexCoordsIndex = (indexWord[0] != '\0');
		if (hasTexCoordsIndex)
		{
			index = atoi(indexWord);
			m_textureCoordsIndexes.push_back(index);
			first.m_texCoordsIndex = index;
		}
		indexWord[0] = '\0';

		if(hasTexCoordsIndex)
		{
			AdvanceIndexToCharacter(dataIndex, '/');
		}
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWord(dataIndex, indexWord);
		if (indexWord[0] != '\0')
		{
			index = atoi(indexWord);
			m_normalIndexes.push_back(index);
			first.m_normalIndex = index;
		}
		indexWord[0] = '\0';
		//---------------------------------------------------------------------------------------------------

		AdvanceIndexToNextWhiteSpace(dataIndex);
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWordUptoCharacter(dataIndex, '/', indexWord);
		index = atoi(indexWord);
		m_vertexIndexes.push_back(index);
		indexWord[0] = '\0';

		AdvanceIndexToCharacter(dataIndex, '/');
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWordUptoCharacter(dataIndex, '/', indexWord);
		hasTexCoordsIndex = (indexWord[0] != '\0');
		if (hasTexCoordsIndex)
		{
			index = atoi(indexWord);
			m_textureCoordsIndexes.push_back(index);
		}
		indexWord[0] = '\0';

		if(hasTexCoordsIndex)
		{
			AdvanceIndexToCharacter(dataIndex, '/');
		}
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWord(dataIndex, indexWord);
		if (indexWord[0] != '\0')
		{
			index = atoi(indexWord);
			m_normalIndexes.push_back(index);
		}
		indexWord[0] = '\0';
		//---------------------------------------------------------------------------------------------------

		AdvanceIndexToNextWhiteSpace(dataIndex);
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWordUptoCharacter(dataIndex, '/', indexWord);
		index = atoi(indexWord);
		m_vertexIndexes.push_back(index);
		last.m_vertexIndex = index;
		indexWord[0] = '\0';

		AdvanceIndexToCharacter(dataIndex, '/');
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWordUptoCharacter(dataIndex, '/', indexWord);
		hasTexCoordsIndex = (indexWord[0] != '\0');
		if (hasTexCoordsIndex)
		{
			index = atoi(indexWord);
			m_textureCoordsIndexes.push_back(index);
			last.m_texCoordsIndex = index;
		}
		indexWord[0] = '\0';

		if(hasTexCoordsIndex)
		{
			AdvanceIndexToCharacter(dataIndex, '/');
		}
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWord(dataIndex, indexWord);
		if (indexWord[0] != '\0')
		{
			index = atoi(indexWord);
			m_normalIndexes.push_back(index);
			last.m_normalIndex = index;
		}
		indexWord[0] = '\0';
	}
}

//---------------------------------------------------------------------------------------------------
void OBJLoader::ReadNormals(int& dataIndex)
{
	float xNormal;
	float yNormal;
	float zNormal;

	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	xNormal = GetFloatFromNextWord(dataIndex);
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	yNormal = GetFloatFromNextWord(dataIndex);
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	zNormal = GetFloatFromNextWord(dataIndex);

	Vector3DF normalValue = Vector3DF(xNormal, yNormal, zNormal);
	m_normals.push_back(normalValue);

}

//---------------------------------------------------------------------------------------------------
void OBJLoader::ReadTextureCoords(int& dataIndex)
{
	float xCoord;
	float yCoord;
	float wValue = 0.0f;

	bool hasWValue = false;

	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	xCoord = GetFloatFromNextWord(dataIndex);
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	yCoord = GetFloatFromNextWord(dataIndex);
	
	hasWValue = IsThereACharBeforeNextLine(dataIndex);
	if (hasWValue)
	{
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		wValue = GetFloatFromNextWord(dataIndex);

		// Do something to x and y?
	}

	Vector2DF texCoords = Vector2DF(xCoord, 1.0f - yCoord);

	m_textureCoords.push_back(texCoords);
}

//---------------------------------------------------------------------------------------------------
bool OBJLoader::IsThereACharBeforeNextLine(int dataIndex)
{
	bool hasCharBeforeNewline = false;

	while(m_fileBuffer[dataIndex] != '\n')
	{
		if (!isspace(m_fileBuffer[dataIndex]))
		{
			hasCharBeforeNewline = true;
			break;
		}
		dataIndex++;
	}
	return hasCharBeforeNewline;
}

//---------------------------------------------------------------------------------------------------
void OBJLoader::ReadVertexesInCurrentLineAndAdvanceIndex(int& dataIndex)
{
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	m_vertexesInThisLine[0] = GetFloatFromNextWord(dataIndex);
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	m_vertexesInThisLine[1] = GetFloatFromNextWord(dataIndex);
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	m_vertexesInThisLine[2] = GetFloatFromNextWord(dataIndex);
}

//---------------------------------------------------------------------------------------------------
void OBJLoader::ReadVertexColorsInCurrentLineAndAdvanceIndex(int& dataIndex)
{
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	m_vertexColorsInThisLine[0] = GetFloatFromNextWord(dataIndex);
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	m_vertexColorsInThisLine[1] = GetFloatFromNextWord(dataIndex);
	AdvanceIndexToNextNonWhiteSpace(dataIndex);
	m_vertexColorsInThisLine[2] = GetFloatFromNextWord(dataIndex);
}

//---------------------------------------------------------------------------------------------------
void OBJLoader::UnrollDataIntoNonIndexedVBO(std::vector<ModelVertex>& modelVertexes)
{
	if (m_vertexIndexes.empty())
	{
		modelVertexes = m_vertexesReadFromFile;
		if (!m_normals.empty())
		{
			for(int index = 0; index < (int)m_normals.size(); index)
			{
				modelVertexes[index].m_normal = m_normals[index];
			}
		}
	}
	else
	{
		std::vector<int> testVector;
		for (int index = 0; index < (int)m_vertexIndexes.size(); index++)
		{
			int indexOfVertexToPush = m_vertexIndexes[index] - 1;
			
			if (indexOfVertexToPush < 0)
			{
				indexOfVertexToPush = (int)m_vertexesReadFromFile.size() + indexOfVertexToPush + index;
				testVector.push_back(indexOfVertexToPush);
			}
			modelVertexes.push_back(m_vertexesReadFromFile[indexOfVertexToPush]);
			
		}
		for (int index = 0; index < (int)m_normalIndexes.size(); index++)
		{
			int normalIndex = m_normalIndexes[index] - 1;
			if (normalIndex < 0)
			{
				normalIndex = (int)m_normals.size() + normalIndex;
				continue;
			}
			modelVertexes[index].m_normal = m_normals[normalIndex];

		}
		for (int index = 0; index < (int)m_textureCoordsIndexes.size(); index++)
		{
			int textureCoordsIndex = m_textureCoordsIndexes[index] - 1;
			if (textureCoordsIndex < 0)
			{
				textureCoordsIndex = (int)m_textureCoords.size() + textureCoordsIndex;
				continue;
			}
			modelVertexes[index].m_texCoords = m_textureCoords[textureCoordsIndex];
		}
		
	}
}

//---------------------------------------------------------------------------------------------------
void OBJLoader::ReadNextTriangleData(int& dataIndex, bool hasSlashes, const FaceData& firstData, FaceData& prevLastData)
{
	int index = 0;
	char indexWord[100];

	if(!hasSlashes)
	{
		m_vertexIndexes.push_back(firstData.m_vertexIndex);

		m_vertexIndexes.push_back(prevLastData.m_vertexIndex);

		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		index = GetIntFromNextWord(dataIndex);
		m_vertexIndexes.push_back(index);
		prevLastData.m_vertexIndex = index;
	}
	else
	{
		bool hasTexCoordsIndex = false;

		m_vertexIndexes.push_back(firstData.m_vertexIndex);

		m_textureCoordsIndexes.push_back(firstData.m_texCoordsIndex);

		m_normalIndexes.push_back(firstData.m_normalIndex);

		indexWord[0] = '\0';
		//---------------------------------------------------------------------------------------------------

		m_vertexIndexes.push_back(prevLastData.m_vertexIndex);

		m_textureCoordsIndexes.push_back(prevLastData.m_texCoordsIndex);

		m_normalIndexes.push_back(prevLastData.m_normalIndex);

		indexWord[0] = '\0';
		//---------------------------------------------------------------------------------------------------

		//AdvanceIndexToNextWhiteSpace(dataIndex);
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWordUptoCharacter(dataIndex, '/', indexWord);
		index = atoi(indexWord);
		m_vertexIndexes.push_back(index);
		prevLastData.m_vertexIndex = index;
		indexWord[0] = '\0';

		AdvanceIndexToCharacter(dataIndex, '/');
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWordUptoCharacter(dataIndex, '/', indexWord);
		hasTexCoordsIndex = (indexWord[0] != '\0');
		if (hasTexCoordsIndex)
		{
			index = atoi(indexWord);
			m_textureCoordsIndexes.push_back(index);
			prevLastData.m_texCoordsIndex = index;
		}
		indexWord[0] = '\0';

		if(hasTexCoordsIndex)
		{
			AdvanceIndexToCharacter(dataIndex, '/');
		}
		AdvanceIndexToNextNonWhiteSpace(dataIndex);
		ReadWord(dataIndex, indexWord);
		if (indexWord[0] != '\0')
		{
			index = atoi(indexWord);
			m_normalIndexes.push_back(index);
			prevLastData.m_normalIndex = index;
		}
		indexWord[0] = '\0';
	}
}

//---------------------------------------------------------------------------------------------------
void OBJLoader::UnrollDataIntoIndexedBuffer(std::unordered_map<ModelVertex, int, VertexHash<ModelVertex>>& vertexes, MyMesh*& mesh)
{
	if (m_vertexIndexes.empty())
	{
		mesh->m_meshVertexes = m_vertexesReadFromFile;
		if (!m_normals.empty())
		{
			for(int index = 0; index < (int)m_normals.size(); index)
			{
				mesh->m_meshVertexes[index].m_normal = m_normals[index];
			}
		}
	}
	else
	{
		//unsigned int mapIndex = 0;
		ModelVertex currentVertex;
		std::vector<ModelVertex> tempVertexes;
		std::vector<int> testVector;
		for (unsigned int index = 0; index < (unsigned int)m_vertexIndexes.size(); index++)
		{
			int indexOfVertexToPush = (int)m_vertexIndexes[index] - 1;

			if (indexOfVertexToPush < 0)
			{
				indexOfVertexToPush = (int)m_vertexesReadFromFile.size() + indexOfVertexToPush + index;
				testVector.push_back(indexOfVertexToPush);
			}
			currentVertex = m_vertexesReadFromFile[indexOfVertexToPush];
			//mesh->m_meshVertexes.push_back(m_vertexesReadFromFile[indexOfVertexToPush]);
// 		}
// 
// 		for (int index = 0; index < (int)m_normalIndexes.size(); index++)
// 		{
			if(!m_normalIndexes.empty())
			{
				int normalIndex = m_normalIndexes[index] - 1;
				if (normalIndex < 0)
				{
					normalIndex = (int)m_normals.size() + normalIndex;
					continue;
				}
				currentVertex.m_normal = m_normals[normalIndex];
				//mesh->m_meshVertexes[index].m_normal = m_normals[normalIndex];
			}
// 		}
// 		for (int index = 0; index < (int)m_textureCoordsIndexes.size(); index++)
// 		{
			if(!m_textureCoordsIndexes.empty())
			{
				int textureCoordsIndex = m_textureCoordsIndexes[index] - 1;
				if (textureCoordsIndex < 0)
				{
					textureCoordsIndex = (int)m_textureCoords.size() + textureCoordsIndex;
					continue;
				}
				currentVertex.m_texCoords = m_textureCoords[textureCoordsIndex];
				//mesh->m_meshVertexes[index].m_texCoords = m_textureCoords[textureCoordsIndex];
			}
			//unsigned int mapIndex = (unsigned int)vertexes.size();
			tempVertexes.push_back(currentVertex);
		}
		for (int index = 0; index < (int)tempVertexes.size(); index++)
		{
			vertexes[tempVertexes[index]] = (int)m_vertexesReadFromFile.size();
		}
		
	}

}

//---------------------------------------------------------------------------------------------------
void OBJLoader::ComputeTBN(MyMesh* mesh)
{
	if (!mesh)
	{
		return;
	}

	if (m_normals.empty())
	{
		for (std::vector<ModelVertex>::iterator vertexIter = mesh->m_meshVertexes.begin(); vertexIter != mesh->m_meshVertexes.end(); vertexIter+=3)
		{
			std::vector<ModelVertex>::iterator vert1 = vertexIter;
			std::vector<ModelVertex>::iterator vert2;
			if (vert1+1 != mesh->m_meshVertexes.end())
			{
				vert2 = vert1 + 1;
			}
			else
			{
				vert2 = mesh->m_meshVertexes.begin();
			}
			std::vector<ModelVertex>::iterator vert3;
			if (vert2+1 != mesh->m_meshVertexes.end())
			{
				vert3 = vert2 + 1;
			}
			else
			{
				vert3 = mesh->m_meshVertexes.begin();
			}

			Vector3DF u = vert2->m_position - vert1->m_position;
			Vector3DF v = vert3->m_position - vert1->m_position;

			Vector3DF normal;
			normal.x = (u.y * v.z) - (u.z * v.y);
			normal.y = (u.z * v.x) - (u.x * v.z);
			normal.z = (u.x * v.y) - (u.y * v.x);
			normal.Normalize();
			vert1->m_normal = normal;
			vert2->m_normal = normal;
			vert3->m_normal = normal;
			ComputeSurfaceTangentsAtVertex(vert2->m_tangent, vert2->m_bitangent, vert2->m_normal, vert2->m_position, vert2->m_texCoords, vert1->m_position, vert1->m_texCoords, vert3->m_position, vert3->m_texCoords);
			
			vert1->m_tangent = vert2->m_tangent;
			vert3->m_tangent = vert2->m_tangent;
			vert1->m_bitangent = vert2->m_bitangent;
			vert3->m_bitangent = vert2->m_bitangent;
		}
	}
	else
	{
		for(std::vector<ModelVertex>::iterator vertexIter = mesh->m_meshVertexes.begin(); vertexIter != mesh->m_meshVertexes.end(); vertexIter+=3)
		{
			std::vector<ModelVertex>::iterator vert1 = vertexIter;
			std::vector<ModelVertex>::iterator vert2;
			if (vert1+1 != mesh->m_meshVertexes.end())
			{
				vert2 = vert1 + 1;
			}
			else
			{
				vert2 = mesh->m_meshVertexes.begin();
			}
			std::vector<ModelVertex>::iterator vert3;
			if (vert2+1 != mesh->m_meshVertexes.end())
			{
				vert3 = vert2 + 1;
			}
			else
			{
				vert3 = mesh->m_meshVertexes.begin();
			}
			ComputeSurfaceTangentsAtVertex(vert2->m_tangent, vert2->m_bitangent, vert2->m_normal, vert2->m_position, vert2->m_texCoords, vert1->m_position, vert1->m_texCoords, vert3->m_position, vert3->m_texCoords);
			vert1->m_tangent = vert2->m_tangent;
			vert3->m_tangent = vert2->m_tangent;
			vert1->m_bitangent = vert2->m_bitangent;
			vert3->m_bitangent = vert2->m_bitangent;
		}
	}
}


void OBJLoader::ComputeTextureCoords(MyMesh* mesh)
{
	if (!mesh)
	{
		return;
	}
	if (m_textureCoords.empty())
	{
		for (std::vector<ModelVertex>::iterator vertexIter = mesh->m_meshVertexes.begin(); vertexIter != mesh->m_meshVertexes.end(); vertexIter+=3)
		{
			std::vector<ModelVertex>::iterator vert1 = vertexIter;
			std::vector<ModelVertex>::iterator vert2;
			if (vert1+1 != mesh->m_meshVertexes.end())
			{
				vert2 = vert1 + 1;
			}
			else
			{
				vert2 = mesh->m_meshVertexes.begin();
			}
			std::vector<ModelVertex>::iterator vert3;
			if (vert2+1 != mesh->m_meshVertexes.end())
			{
				vert3 = vert2 + 1;
			}
			else
			{
				vert3 = mesh->m_meshVertexes.begin();
			}

			vert1->m_texCoords = Vector2DF(0.0f, 0.0f);
			vert2->m_texCoords = Vector2DF(0.0f, 1.0f);
			vert3->m_texCoords = Vector2DF(1.0f, 0.0f);
		}
	}
}

//---------------------------------------------------------------------------------------------------
// The following code is based on the function provided by professor Eiserloh
// in class at The Guildhall at SMU.
// This function computes Tangents and Bitangent of the surface for a given vertex based on 
// the texture coordinates, Vertex position and normal of the vertex.
//---------------------------------------------------------------------------------------------------
void ComputeSurfaceTangentsAtVertex(
	Vector3DF& surfaceTangentAtVertex_out,
	Vector3DF& surfaceBitangentAtVertex_out,
	const Vector3DF& normalAtThisVertex,
	const Vector3DF& positionOfThisVertex,
	const Vector2DF& texCoordsOfThisVertex,
	const Vector3DF& positionOfPreviousAdjacentVertex,
	const Vector2DF& texCoordsOfPreviousAdjacentVertex,
	const Vector3DF& positionOfNextAdjacentVertex,
	const Vector2DF& texCoordsOfNextAdjacentVertex )
{
	UNUSED(normalAtThisVertex);
	Vector3DF vecToPrevious	= positionOfPreviousAdjacentVertex - positionOfThisVertex;
	Vector3DF vecToNext		= positionOfNextAdjacentVertex - positionOfThisVertex;

	Vector2DF texToPrevious	= Vector2DF(texCoordsOfPreviousAdjacentVertex.x - texCoordsOfThisVertex.x, texCoordsOfPreviousAdjacentVertex.y - texCoordsOfThisVertex.y);
	Vector2DF texToNext		= Vector2DF(texCoordsOfNextAdjacentVertex.x - texCoordsOfThisVertex.x, texCoordsOfNextAdjacentVertex.y - texCoordsOfThisVertex.y);

	float determinant = ((texToPrevious.x * texToNext.y) - (texToNext.x * texToPrevious.y));

	Vector3DF uDirectionInWorldSpace(	
		(texToNext.y * vecToPrevious.x - texToPrevious.y * vecToNext.x),
		(texToNext.y * vecToPrevious.y - texToPrevious.y * vecToNext.y),
		(texToNext.y * vecToPrevious.z - texToPrevious.y * vecToNext.z)
		);

	Vector3DF vDirectionInWorldSpace(
		(texToPrevious.x * vecToNext.x - texToNext.x * vecToPrevious.x),
		(texToPrevious.x * vecToNext.y - texToNext.x * vecToPrevious.y),
		(texToPrevious.x * vecToNext.z - texToNext.x * vecToPrevious.z)
		);

	float invDeterminant = 1.0f / determinant;
	uDirectionInWorldSpace *= invDeterminant;
	vDirectionInWorldSpace *= invDeterminant;
	//vDirectionInWorldSpace *= -1.0f;	// NOTE: You should remove this minus sign if your V texture coordinates are using the opposite convention of mine!
	surfaceTangentAtVertex_out = uDirectionInWorldSpace; 
	surfaceBitangentAtVertex_out = vDirectionInWorldSpace; 
	surfaceTangentAtVertex_out.Normalize();
	surfaceBitangentAtVertex_out.Normalize();
}
