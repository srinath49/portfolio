#pragma once

#ifndef OBJ_LOADER_H
#define OBJ_LOADER_H

//---------------------------------------------------------------------------------------------------
#include "Engine/FileIO/FileLoader.hpp"
#include "Engine/Core/ModelVertex.hpp"
#include "Engine/Math/SriMath.hpp"

#include <vector>
#include <unordered_map>

enum ModelOrientation
{
	FORWARD,
	BACKWARD,
	RIGHT,
	LEFT,
	UP,
	DOWN
};

struct FaceData
{
public:
	FaceData():
		m_vertexIndex(0),
		m_texCoordsIndex(0),
		m_normalIndex(0)
	{

	}

	~FaceData() {}

	int m_vertexIndex;
	int m_texCoordsIndex;
	int m_normalIndex;
};

class MyMesh;

//---------------------------------------------------------------------------------------------------
class OBJLoader : public FileLoader
{
public:
	OBJLoader();
	~OBJLoader();

	bool ParseVertexDataFromOBJFile(MyMesh*& mesh);
	void GetModelOrientation(int& dataIndex);
	void GetModelScale(int& dataIndex);
	void CorrectOrientationAndScale(std::vector<ModelVertex>& modelVertexes);
	Vector3DF GetVector3DFromOrientation(ModelOrientation orientation);
	Vector3DF MatrixVector3DMultiplication(const Matrix4x4F& matrix, const Vector3DF& vector);
	void ReadFaceData(int& dataIndex);
	void ReadFirstTriangleData(int& dataIndex, bool hasSlashes, FaceData& first, FaceData& last);
	void ReadNormals(int& dataIndex);
	void ReadTextureCoords(int& dataIndex);
	bool IsThereACharBeforeNextLine(int dataIndex);
	void ReadVertexesInCurrentLineAndAdvanceIndex(int& dataIndex);
	void ReadVertexColorsInCurrentLineAndAdvanceIndex(int& dataIndex);
	void UnrollDataIntoNonIndexedVBO(std::vector<ModelVertex>& modelVertexes);
	void ReadNextTriangleData(int& dataIndex, bool hasSlashes, const FaceData& firstData, FaceData& prevLastData);
	void UnrollDataIntoIndexedBuffer(std::unordered_map<ModelVertex, int, VertexHash<ModelVertex>>& vertexes, MyMesh*& mesh);
	void ComputeTBN(MyMesh* mesh);
	void ComputeTextureCoords(MyMesh* mesh);



	ModelOrientation			m_xOrientation;
	ModelOrientation			m_yOrientation;
	ModelOrientation			m_zOrientation;
	std::vector<unsigned int>	m_vertexIndexes;
	std::vector<unsigned int>	m_normalIndexes;
	std::vector<unsigned int>	m_textureCoordsIndexes;
	std::vector<ModelVertex>	m_vertexesReadFromFile;
	std::vector<Vector3DF>		m_normals;
	std::vector<Vector2DF>		m_textureCoords;
	float						m_modelScale;
	float						m_vertexesInThisLine[3];
	float						m_vertexColorsInThisLine[3];
};

#endif