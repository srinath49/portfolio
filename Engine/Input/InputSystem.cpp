#include "InputSystem.hpp"
#include "..\Core\EngineCommons.hpp"

//---------------------------------------------------------------------------------------------------
KeyState InputSystem::KeysState[NUM_OF_KEYS];
bool InputSystem::KeysPressed[NUM_OF_KEYS];
bool InputSystem::KeysReleased[NUM_OF_KEYS];
//---------------------------------------------------------------------------------------------------

InputSystem* _inputSystem = nullptr;

//-----------------------------------------------------------------------------------------------
#define UNUSED(x) (void)(x);

unsigned char lMouseKey = 0x1;
unsigned char rMouseKey = 0x2;

//---------------------------------------------------------------------------------------------------
LRESULT CALLBACK InputSystem_MessageHandler( HWND windowHandle, UINT wmMessageCode, WPARAM wParam, LPARAM lParam )
{
	unsigned char asKey = (unsigned char) wParam;
	DWORD value;

	switch( wmMessageCode )
	{
	case WM_CLOSE:
	case WM_DESTROY:

	case WM_CHAR:

		break;
	case WM_KEYDOWN:
		_inputSystem->OnKeyDown(asKey);
		break;
	case WM_KEYUP:
		_inputSystem->OnKeyUp(asKey);
		break;

	case WM_MOUSEWHEEL: 
		value= HIWORD(lParam);
		break;

	case WM_LBUTTONDOWN:
		_inputSystem->xMousePos = LOWORD(lParam);
		_inputSystem->yMousePos = SCREEN_HEIGHT - HIWORD(lParam);
		_inputSystem->OnKeyDown(lMouseKey);
		break;

	case WM_LBUTTONUP:
		_inputSystem->xMousePos = LOWORD(lParam);
		_inputSystem->yMousePos = SCREEN_HEIGHT - HIWORD(lParam);
		_inputSystem->OnKeyUp(lMouseKey);
		break;

	case WM_RBUTTONDOWN:
		_inputSystem->xMousePos = LOWORD(lParam);
		_inputSystem->yMousePos = SCREEN_HEIGHT - HIWORD(lParam);
		_inputSystem->OnKeyDown(rMouseKey);
		break;

	case WM_RBUTTONUP:
		_inputSystem->xMousePos = LOWORD(lParam);
		_inputSystem->yMousePos = SCREEN_HEIGHT - HIWORD(lParam);
		_inputSystem->OnKeyUp(rMouseKey);
		break;

	case WM_QUIT:
		return 0;
	}

	// pass this message to other program.
	return DefWindowProc( windowHandle, wmMessageCode, wParam, lParam );
}

//---------------------------------------------------------------------------------------------------
void InputSystem::RunMessagePump()
{
	MSG queuedMessage;
	for( ;; )
	{
		const BOOL wasMessagePresent = PeekMessage( &queuedMessage, NULL, 0, 0, PM_REMOVE );
		if( !wasMessagePresent )
		{
			break;
		}

		TranslateMessage( &queuedMessage );
		DispatchMessage( &queuedMessage );
	}
}
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
InputSystem::InputSystem(void* platformHandle)
	: m_platformHandle(platformHandle)
{
	SetWindowLongPtr((HWND)m_platformHandle, GWLP_WNDPROC, (LONG)InputSystem_MessageHandler);
	_inputSystem = this;
	ClearAllKeys();
}

//---------------------------------------------------------------------------------------------------
InputSystem::~InputSystem(void)
{
	
}

//---------------------------------------------------------------------------------------------------
void InputSystem::UpdateKeysStates(unsigned char keyToUpdate, bool newState)
{
	if (KeysState[keyToUpdate].m_isPressed && !newState)
	{
		KeysState[keyToUpdate].m_hasJustBeenReleased = true;
	}
	KeysState[keyToUpdate].m_isPressed = newState;
}

//---------------------------------------------------------------------------------------------------
void InputSystem::OnKeyDown(unsigned char keyDown)
{
	KeysPressed[keyDown] = true;
	UpdateKeysStates(keyDown, true);
}

//---------------------------------------------------------------------------------------------------
void InputSystem::OnKeyUp(unsigned char keyUp)
{
	KeysReleased[keyUp] = true;
	UpdateKeysStates(keyUp, false);
}

//---------------------------------------------------------------------------------------------------
void InputSystem::Update(float deltaSeconds)
{
	UNUSED(deltaSeconds);
	ClearKeysDownAndUp();
	ClearHasJustBeenPressed();
	RunMessagePump();
}

//---------------------------------------------------------------------------------------------------
void InputSystem::ClearKeysDownAndUp()
{
	for (int index = 0; index < NUM_OF_KEYS; index++)
	{
		KeysPressed[index] = false;
		KeysReleased[index] = false;
	}
}

//---------------------------------------------------------------------------------------------------
void InputSystem::ClearAllKeys()
{
	for (int index = 0; index < NUM_OF_KEYS; index++)
	{
		KeysPressed[index] = false;
		KeysReleased[index] = false;
		KeysState[index].m_isPressed = false;
		KeysState[index].m_hasJustBeenReleased = false;
		
	}
}

//---------------------------------------------------------------------------------------------------
bool InputSystem::IsKeyDown(KeyCodes key)
{
	return KeysState[key].m_isPressed;
}

void InputSystem::ClearHasJustBeenPressed()
{
	for (int index = 0; index < NUM_OF_KEYS; index++)
	{
		KeysState[index].m_hasJustBeenReleased = false;
	}
}

bool InputSystem::HasKeyJustBeenReleased(KeyCodes key)
{
	return KeysState[key].m_hasJustBeenReleased;
}

bool InputSystem::AnyKeyPressed()
{
	bool returnVal = false;
	for (int keyStateIndex = 0;  keyStateIndex < NUM_OF_KEYS; ++keyStateIndex)
	{
		if (InputSystem::KeysPressed[keyStateIndex])
		{
			returnVal = InputSystem::KeysPressed[keyStateIndex];
			break;
		}
	}
	return returnVal;
}
