#pragma once

#ifndef INPUT_H
#define INPUT_H

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>

enum KeyCodes
{
	KEY_CODE_0				= '0',
	KEY_CODE_1				= '1',
	KEY_CODE_2				= '2',
	KEY_CODE_3				= '3',
	KEY_CODE_4				= '4',
	KEY_CODE_5				= '5',
	KEY_CODE_6				= '6',
	KEY_CODE_7				= '7',
	KEY_CODE_8				= '8',
	KEY_CODE_9				= '9',
	KEY_CODE_A				= 'A',
	KEY_CODE_B				= 'B',
	KEY_CODE_C				= 'C',
	KEY_CODE_D				= 'D',
	KEY_CODE_E				= 'E',
	KEY_CODE_F				= 'F',
	KEY_CODE_G				= 'G',
	KEY_CODE_H				= 'H',
	KEY_CODE_I				= 'I',
	KEY_CODE_J				= 'J',
	KEY_CODE_K				= 'K',
	KEY_CODE_L				= 'L',
	KEY_CODE_M				= 'M',
	KEY_CODE_N				= 'N',
	KEY_CODE_O				= 'O',
	KEY_CODE_P				= 'P',
	KEY_CODE_Q				= 'Q',
	KEY_CODE_R				= 'R',
	KEY_CODE_S				= 'S',
	KEY_CODE_T				= 'T',
	KEY_CODE_U				= 'U',
	KEY_CODE_V				= 'V',
	KEY_CODE_W				= 'W',
	KEY_CODE_X				= 'X',
	KEY_CODE_Y				= 'Y',
	KEY_CODE_Z				= 'Z',
	KEY_CODE_TILDA			= '~',
	KEY_CODE_UP				= VK_UP,
	KEY_CODE_DOWN			= VK_DOWN,
	KEY_CODE_LEFT			= VK_LEFT,
	KEY_CODE_RIGHT			= VK_RIGHT,
	KEY_CODE_COMMA			= VK_OEM_COMMA,
	KEY_CODE_LMOUSEB		= VK_LBUTTON,
	KEY_CODE_RMOUSEB		= VK_RBUTTON,
	KEY_CODE_MMOUSEB		= VK_MBUTTON,
	KEY_CODE_MOUSESCROLL	= VK_SCROLL,
	KEY_CODE_SPC			= VK_SPACE,
	KEY_CODE_TAB			= VK_TAB,
	KEY_CODE_ENTER			= VK_RETURN,
	KEY_CODE_LSHT			= VK_LSHIFT,
	KEY_CODE_RSHT			= VK_RSHIFT,
	KEY_CODE_LCTRL			= VK_LCONTROL,
	KEY_CODE_RCTRL			= VK_RCONTROL,
	KEY_CODE_ALT			= VK_MENU,
	KEY_CODE_CAPSLOCK		= VK_CAPITAL,
	KEY_CODE_BKSPC			= VK_BACK,
	KEY_CODE_ESCAPE			= VK_ESCAPE,
	KEY_CODE_PLUS			= VK_ADD,
	KEY_CODE_MINUS			= VK_SUBTRACT,
	KEY_CODE_STAR			= VK_MULTIPLY,
	KEY_CODE_SLASH			= VK_DIVIDE,
	KEY_CODE_F1				= VK_F1,
	KEY_CODE_F2				= VK_F2,
	KEY_CODE_F3				= VK_F3,
	KEY_CODE_F4				= VK_F4,
	KEY_CODE_F5				= VK_F5,
	KEY_CODE_F6				= VK_F6,
	KEY_CODE_F7				= VK_F7,
	KEY_CODE_F8				= VK_F8,
	KEY_CODE_F9				= VK_F9,
	KEY_CODE_F10			= VK_F10,			
	KEY_CODE_F11			= VK_F11,			
	KEY_CODE_F12			= VK_F12,			
	NUM_OF_KEYS				= 256
};


struct KeyState
{
	bool m_isPressed;
	bool m_hasJustBeenReleased;
};

class InputSystem
{
public:
	//static const int NUM_OF_KEYS = 100;
	InputSystem(void* platformHandle);
	~InputSystem(void);

	static KeyState KeysState[NUM_OF_KEYS];
	static bool KeysPressed[NUM_OF_KEYS];
	static bool KeysReleased[NUM_OF_KEYS];

	void*	m_platformHandle;
	static void UpdateKeysStates(unsigned char keyToUpdate, bool newState);
	static void OnKeyDown(unsigned char keyDown);
	static void OnKeyUp(unsigned char keyUp);
	void RunMessagePump();
	void Update(float deltaSeconds = 0.1f);
	void ClearKeysDownAndUp();
	void ClearAllKeys();
	bool IsKeyDown(KeyCodes key);
	void ClearHasJustBeenPressed();
	bool HasKeyJustBeenReleased(KeyCodes key);
	bool AnyKeyPressed();

public:
	DWORD xMousePos;
	DWORD yMousePos;
};

#endif