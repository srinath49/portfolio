#include "Engine/Map/Cell.hpp"
#include "Engine/Map/Map.hpp"
#include "Engine/MessageSystem/MessageSystem.hpp"
#include "Engine/Entities/Item.hpp"
#include "Engine/Entities/Player.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/Map/MapData.hpp"
#include "Engine/Rendering/SriOGLRenderer.hpp"
//---------------------------------------------------------------------------------------------------


//************************************
// Method:    Cell
// FullName:  Cell::Cell
// Access:    public 
// Returns:   
// Qualifier: : m_cellUnexploredColor(RgbaColors(0.f, 0.f, 0.f)) ,m_cellSize(ONE_CELL_UNIT_IN_FLOAT) ,m_cellSymbol(' ') ,m_cellType(CT_AIR) ,m_cellVisibility(CV_UNLIT) ,m_cellPosition() ,m_cellPositionOnMap() ,m_occupyigAgent(nullptr) ,m_isOccupied(false) ,m_occupyingFeature(nullptr) ,m_mapPointer(nullptr)
//************************************
Cell::Cell(): 
	 bottomLeft()
	,bottomRight()
	,topRight()
	,topLeft()
	,mapPointer(nullptr)
	,m_cellPosition()
	,m_cellPositionOnMap()
	,m_cellType(CT_AIR)
	,m_cellVisibility(CV_UNLIT)
	,m_cellCurrentColor(RgbaColors())
	,m_cellVisibleColor(RgbaColors())
	,m_cellFOWColor(RgbaColors())
	,m_cellUnexploredColor(RgbaColors(0.f, 0.f, 0.f))
	,m_cellRestoreColor(RgbaColors())
	,m_occupyigAgent(nullptr)
	,m_occupyingFeature(nullptr)
	,m_cellSize(ONE_CELL_UNIT_IN_FLOAT)
	,m_cellIndex(-1)
	,m_cellSymbol(' ')
	,m_isOccupied(false)
	 
{
	m_cellCurrentColor = m_cellUnexploredColor;
}

//************************************
// Method:    ~Cell
// FullName:  Cell::~Cell
// Access:    public 
// Returns:   
// Qualifier:
//************************************
Cell::~Cell()
{

}


//************************************
// Method:    Update
// FullName:  Cell::Update
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: float deltaSeconds
//************************************
void Cell::Update()
{
	if (m_occupyigAgent && m_cellType == CT_LAVA)
	{
		m_occupyigAgent->ApplyDamage(10.f);
	}
}

//************************************
// Method:    Render
// FullName:  Cell::Render
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Cell::Render()
{
	g_myRenderer->DrawQuad(topLeft, bottomLeft, bottomRight, topRight, m_cellCurrentColor);
}

//************************************
// Method:    SetCellPosition
// FullName:  Cell::SetCellPosition
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: const Vector3DF & newCellPosition
//************************************
void Cell::SetCellPosition(const Vector3DF& newCellPosition)
{ 
	m_cellPosition	= newCellPosition; 
	bottomLeft		= newCellPosition;
	bottomRight		= Vector3DF(newCellPosition.x + ONE_CELL_UNIT, newCellPosition.y, 0.f);
	topRight		= Vector3DF(newCellPosition.x + ONE_CELL_UNIT, newCellPosition.y + ONE_CELL_UNIT, 0.f);
	topLeft			= Vector3DF(newCellPosition.x, newCellPosition.y + ONE_CELL_UNIT, 0.f);
}

//************************************
// Method:    SetCellType
// FullName:  Cell::SetCellType
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: int newCellType
//************************************
void Cell::SetCellType(int newCellType)
{
	m_cellType = (CellType)newCellType;
	switch (m_cellType)
	{
	case CT_WALL:
		SetCellVisibleColor(RgbaColors (0.5f, 0.25f, 0.1f));
		SetCellSymbol('#');
		m_gCost = 1000.0f;
		break;
	case CT_AIR:
		SetCellSymbol('.');
		SetCellVisibleColor(RgbaColors(0.1f, 0.1f, 0.1f));
		m_gCost = 10.0f;
		break;
	case CT_WATER:
		SetCellSymbol('~');
		SetCellVisibleColor(RgbaColors(0.0f, 0.3f, 0.8f));
		m_gCost = 175.0f;
		break;
	case CT_LAVA:
		SetCellSymbol('`');
		SetCellVisibleColor(RgbaColors(0.9f, 0.3f, 0.1f));
		m_gCost = 500.0f;
		break;
	}
	UpdateCurrentColor();
}

//************************************
// Method:    SetCellVisibleColor
// FullName:  Cell::SetCellVisibleColor
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: const RgbaColors & newCellVisibleColor
//************************************
void Cell::SetCellVisibleColor(const RgbaColors& newCellVisibleColor) 
{ 
	m_cellVisibleColor = newCellVisibleColor; 
	m_cellFOWColor.r = newCellVisibleColor.r / 2;
	m_cellFOWColor.g = newCellVisibleColor.g / 2;
	m_cellFOWColor.b = newCellVisibleColor.b / 2;
}

//************************************
// Method:    ClearCellVisibility
// FullName:  Cell::ClearCellVisibility
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Cell::ClearCellVisibility()
{
	if (m_cellVisibility == CV_LIT)
	{
		SetCellVisibility(CV_FOG_OF_WAR);
	}
}

//************************************
// Method:    ShowCellAsLit
// FullName:  Cell::ShowCellAsLit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Cell::ShowCellAsLit()
{
	StoreColor();
	m_cellCurrentColor = m_cellVisibleColor;
}

//************************************
// Method:    SetCellVisibility
// FullName:  Cell::SetCellVisibility
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CellVisibility newCellVisibility
//************************************
void Cell::SetCellVisibility(CellVisibility newCellVisibility)
{ 
	if (newCellVisibility != m_cellVisibility)
	{
		m_cellVisibility = newCellVisibility; 
		switch(newCellVisibility)
		{
		case CV_LIT: 
			m_cellCurrentColor = m_cellVisibleColor;
			break;
		case CV_UNLIT:
			m_cellCurrentColor = m_cellUnexploredColor;
			break;
		case CV_FOG_OF_WAR:
			m_cellCurrentColor = m_cellFOWColor;
			break;
		}
		if(mapPointer)
		{
			mapPointer->SetVBODirty(true);
		}
	}
}

//************************************
// Method:    GetCellWorldPositionAsVector2DI
// FullName:  Cell::GetCellWorldPositionAsVector2DI
// Access:    public 
// Returns:   Vector2DI
// Qualifier:
//************************************
Vector2DI Cell::GetCellWorldPositionAsVector2DI()
{
	Vector2DI returnPosition;
	returnPosition.x = (int)m_cellPosition.x;
	returnPosition.y = (int)m_cellPosition.y;
	return returnPosition;
}

//************************************
// Method:    SetOccupyingAgent
// FullName:  Cell::SetOccupyingAgent
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: Agent * occupyingAgent
//************************************
void Cell::SetOccupyingAgent(Agent* occupyingAgent)
{
	m_occupyigAgent = occupyingAgent;
	m_isOccupied = true;
}

//************************************
// Method:    ClearOccupyingAgent
// FullName:  Cell::ClearOccupyingAgent
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Cell::ClearOccupyingAgent()
{
	m_occupyigAgent = nullptr;
	m_isOccupied = false;
}

//************************************
// Method:    ClearOccupyingFeature
// FullName:  Cell::ClearOccupyingFeature
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Cell::ClearOccupyingFeature()
{
	m_isOccupied = false;
}

//************************************
// Method:    SetMapReference
// FullName:  Cell::SetMapReference
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: const Map * mapPointer
//************************************
void Cell::SetMapReference(Map& map)
{
	mapPointer = &map;
}

//************************************
// Method:    UpdateCurrentColor
// FullName:  Cell::UpdateCurrentColor
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Cell::UpdateCurrentColor()
{
	switch(m_cellVisibility)
	{
	case CV_LIT: 
		m_cellCurrentColor = m_cellVisibleColor;
		break;
	case CV_UNLIT:
		m_cellCurrentColor = m_cellUnexploredColor;
		break;
	case CV_FOG_OF_WAR:
		m_cellCurrentColor = m_cellFOWColor;
		break;
	}
	if(mapPointer)
	{
		mapPointer->SetVBODirty(true);
	}
}

//************************************
// Method:    SetOccupyingFeature
// FullName:  Cell::SetOccupyingFeature
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: Feature * occupyingFeature
//************************************
void Cell::SetOccupyingFeature(Feature* occupyingFeature)
{
	if (!occupyingFeature)
	{
		return;
	}
	if (occupyingFeature->IsOn())
	{
		m_occupyingFeature = occupyingFeature;
	}
	m_isOccupied = true;
}

//************************************
// Method:    AddNewItem
// FullName:  Cell::AddNewItem
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: Item * newItem
//************************************
void Cell::AddNewItem(Item* newItem)
{
	if (newItem)
	{
		items.push_back(newItem);
	}
}

//************************************
// Method:    RemoveItem
// FullName:  Cell::RemoveItem
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: Item * itemToRemove
//************************************
void Cell::RemoveItem(Item* itemToRemove)
{
	if (!itemToRemove)
	{
		return;
	}
	for (int itemsIndex = 0; itemsIndex < (int)items.size(); ++itemsIndex)
	{
		if (items[itemsIndex] == itemToRemove)
		{
			items[itemsIndex] = items.back();
			items.pop_back();
			return;
		}
	}
}

//************************************
// Method:    OnMove
// FullName:  Cell::OnMove
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Cell::OnMove()
{
	Agent* player = dynamic_cast<Player*> (m_occupyigAgent);
	if (!player)
	{
		return;
	}
	for (std::vector<Item*>::iterator itemIter = items.begin(); itemIter != items.end(); itemIter++)
	{
		std::string newMessage = "There is an item : '";
		newMessage += (*itemIter)->GetName();
		newMessage += "' on this cell. Press , to pick it up";
		MessageSystem::AddMessageToList(newMessage, RgbaColors(0.75f,0.75f, 0.10f));
	}
}

