#pragma once

#ifndef CELL_H
#define CELL_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Math/SriMath.hpp"
#include "Engine/Core/RgbaColors.hpp"
#include <vector>

//---------------------------------------------------------------------------------------------------
class BitmapFont;
class Agent;
class Item;
class Map;
class Feature;

enum CellVisibility;
enum CellType;

//---------------------------------------------------------------------------------------------------
class Cell
{
public:
	Cell();
	~Cell();

	 void					Update();
	 void					Render();
	 void					RenderCellIndex(BitmapFont* fontRenderer, int index);	
	 void					SetCellPosition(const Vector3DF& newCellPosition);
	 void					SetCellType(int newCellType);
	 void					SetCellVisibleColor(const RgbaColors& newCellVisibleColor);
	 void					ClearCellVisibility();
	 void					ShowCellAsLit();
	 void					SetCellVisibility(CellVisibility newCellVisibility);
	 void					SetOccupyingAgent(Agent* occupyingAgent);
	 void					ClearOccupyingAgent();
	 void					ClearOccupyingFeature();
	 void					SetMapReference(Map& map);
	 void					UpdateCurrentColor();
	 void					SetOccupyingFeature(Feature* occupyingFeature);
	 void					AddNewItem(Item* newItem);
	 void					RemoveItem(Item* itemToRemove);
	 void					OnMove();
	 Vector2DI				GetCellWorldPositionAsVector2DI();

	 inline CellType		GetCellType(){ return m_cellType;}
	 inline CellVisibility	GetCellVisibility() const { return m_cellVisibility; }
	 inline Vector3DF		GetCellPosition(){return m_cellPosition;}
	 inline Vector2DI		GetCellPositionOnMap() const { return m_cellPositionOnMap; }
	 inline void			SetCellPositionOnMap(const Vector2DI& newCellPositionOnMap) { m_cellPositionOnMap = newCellPositionOnMap; }
	 inline bool			IsCellOccupied(){return m_isOccupied;}
	 inline Agent*			GetOccupyingAgent() const { return m_occupyigAgent; }
	 inline RgbaColors		GetCellCurrentColor() const { return m_cellCurrentColor; }
	 inline void			StoreColor(){m_cellRestoreColor = m_cellCurrentColor;}
	 inline void			RestoreColor(){m_cellCurrentColor = m_cellRestoreColor;}
	 inline Feature*		GetOccupyingFeature() { return m_occupyingFeature; }
	 inline void			SetIndex(int cellIndex){m_cellIndex = cellIndex;}
	 inline	char			GetCellSymbol() const { return m_cellSymbol; }
	 inline	void			SetCellSymbol(char newCellSymbol) { m_cellSymbol = newCellSymbol; }
	 inline float			GetGCost()const {return m_gCost;}

	 Vector3DF			bottomLeft;
	 Vector3DF			bottomRight;
	 Vector3DF			topRight;
	 Vector3DF			topLeft;
	 Map*				mapPointer;
	 std::vector<Item*>	items;

private:
	Vector3DF			m_cellPosition;
	Vector2DI			m_cellPositionOnMap;
	CellType			m_cellType;
	CellVisibility		m_cellVisibility;
	RgbaColors			m_cellCurrentColor;
	RgbaColors			m_cellVisibleColor;
	RgbaColors			m_cellFOWColor;
	RgbaColors			m_cellUnexploredColor;
	RgbaColors			m_cellRestoreColor;
	Agent*				m_occupyigAgent;
	Feature*			m_occupyingFeature;
	float				m_cellSize;
	int					m_cellIndex;
	char				m_cellSymbol;
	bool				m_isOccupied;
	float				m_gCost;
};

#endif