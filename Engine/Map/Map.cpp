#include "Engine/Map/Map.hpp"
#include "Engine/Map/Cell.hpp"
#include "Engine/Map/MapData.hpp"
#include "Engine/MessageSystem/MessageSystem.hpp"
#include "Engine/Entities/Player.hpp"
#include "Engine/Entities/Item.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/Rendering/OpenGlExtentions.hpp"
#include "Engine/Rendering/SriOGLRenderer.hpp"
#include "Engine/DebugConsole/BitmapFont.hpp"
#include "Engine/Input/InputSystem.hpp"

#include <string>


//************************************
// Method:    Map
// FullName:  Map::Map
// Access:    public 
// Returns:   
// Qualifier: : m_vboID(0), m_iboID(0), m_isVboDirty(true), m_showAll(false)
// Parameter: void
//************************************
Map::Map(void): 
	m_vboID(0),
	m_iboID(0),
	m_isVboDirty(true),
	m_showAll(false)
{
	m_numberOfCells = NUMBER_OF_CELL_ROWS * NUMBER_OF_CELL_COLUMNS;
}

//************************************
// Method:    ~Map
// FullName:  Map::~Map
// Access:    public 
// Returns:   
// Qualifier:
// Parameter: void
//************************************
Map::~Map(void)
{

}

//************************************
// Method:    SetCellType
// FullName:  Map::SetCellType
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CellType cellType
// Parameter: int cellIndex
//************************************
void Map::SetCellType(CellType cellType, int cellIndex)
{
	if(cellIndex >= 0 && cellIndex < m_numberOfCells && cellType != CT_NONE)
	{
		GetCellAtIndex(cellIndex)->SetCellType(cellType);
		if (GetCellAtIndex(cellIndex)->mapPointer)
		{
			GetCellAtIndex(cellIndex)->mapPointer->m_isVboDirty = true;
		}
	}
}

//************************************
// Method:    GetCellType
// FullName:  Map::GetCellType
// Access:    public 
// Returns:   CellType
// Qualifier:
// Parameter: int cellIndex
//************************************
CellType Map::GetCellType(int cellIndex)
{
	if(cellIndex >= 0 && cellIndex < m_numberOfCells)
	{
		return GetCellAtIndex(cellIndex)->GetCellType();
	}
	return CT_NONE;
}

//************************************
// Method:    GetCellType
// FullName:  Map::GetCellType
// Access:    public 
// Returns:   CellType
// Qualifier:
// Parameter: const Vector2DI cellPosition
//************************************
CellType Map::GetCellType(const Vector2DI cellPosition)
{
	int cellIndex = GetCellIndexFromPosition(cellPosition);
	return GetCellType(cellIndex);
}

//************************************
// Method:    Update
// FullName:  Map::Update
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: float deltaSeconds
//************************************
void Map::Update(float deltaSeconds)
{
	UNUSED(deltaSeconds);
	if (m_isVboDirty)
	{
		BuildVBO();
	}
}

//************************************
// Method:    UpdateCells
// FullName:  Map::UpdateCells
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Map::UpdateCells()
{
	int cellSize = (int)m_cells.size(); 
	for (int cellIndex = 0; cellIndex < cellSize; ++cellIndex)
	{
		GetCellAtIndex(cellIndex)->Update();
	}
}

//************************************
// Method:    Render
// FullName:  Map::Render
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BitmapFont * fontRenderer
//************************************
void Map::Render(BitmapFont* fontRenderer)
{
	UNUSED(fontRenderer);
	if (!m_isVboDirty && m_doneGeneration && m_vboID > 0)
	{
		g_myRenderer->DrawVBO(m_vboID, m_iboID, m_vboSize, GL_QUADS);
	}
	else
	{
		int cellSize = (int)m_cells.size(); 
		for (int cellIndex = 0; cellIndex < cellSize; ++cellIndex)
		{
			GetCellAtIndex(cellIndex)->Render();
		}
	}
}

//************************************
// Method:    ProcessInput
// FullName:  Map::ProcessInput
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: InputSystem * inputSystem
//************************************
void Map::ProcessInput(InputSystem* inputSystem)
{
	if(inputSystem)
	{
		if (inputSystem->HasKeyJustBeenReleased(KEY_CODE_M))
		{
			ToggleMapVisibility();
		}
	}
}

//************************************
// Method:    BuildVBO
// FullName:  Map::BuildVBO
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Map::BuildVBO()
{
	if (m_vboID != 0)
	{
		g_myRenderer->DeleteBuffers(&m_vboID);
		m_vboID = 0;
	}
	unsigned int cellsSize = (unsigned int)m_cells.size();
	std::vector<ModelVertex> vertexes;
	std::vector<unsigned int> indexes;
	int index = 0;
	ModelVertex vertex;
	for (unsigned int cellIndex = 0; cellIndex < cellsSize; ++cellIndex)
	{
		vertex.m_color = GetCellAtIndex(cellIndex)->GetCellCurrentColor();
		vertex.m_position = GetCellAtIndex(cellIndex)->topLeft;
		vertexes.push_back(vertex);
		indexes.push_back(index);
		++index;
		vertex.m_position = GetCellAtIndex(cellIndex)->bottomLeft;
		vertexes.push_back(vertex);
		indexes.push_back(index);
		++index;
		vertex.m_position = GetCellAtIndex(cellIndex)->bottomRight;
		vertexes.push_back(vertex);
		indexes.push_back(index);
		++index;
		vertex.m_position = GetCellAtIndex(cellIndex)->topRight;
		vertexes.push_back(vertex);
		indexes.push_back(index);
		++index;
	}
	g_myRenderer->GenerateVertexBuffer(vertexes, &m_vboID);
	m_vboSize = g_myRenderer->GenerateIndexBuffer(indexes, &m_iboID);
	m_isVboDirty = false;
}

//************************************
// Method:    FinalizeMap
// FullName:  Map::FinalizeMap
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Map::FinalizeMap()
{
	m_doneGeneration = true;
	for(std::vector<Cell>::iterator cellIter = m_cells.begin(); cellIter != m_cells.end(); ++cellIter)
	{
		cellIter->SetMapReference(*this);
	}
	BuildVBO();
}

//************************************
// Method:    ResetMap
// FullName:  Map::ResetMap
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Map::ResetMap()
{
	g_myRenderer->DeleteBuffers(&m_vboID);
	g_myRenderer->DeleteBuffers(&m_iboID);
	m_doneGeneration = false;
	m_vboID = 0;
	m_vboSize = 0;
	while (!m_cells.empty())
	{
		m_cells.pop_back();
	}
}

//************************************
// Method:    GetCellIndexFromPosition
// FullName:  Map::GetCellIndexFromPosition
// Access:    public 
// Returns:   int
// Qualifier:
// Parameter: const Vector2DI & cellPosition
//************************************
int Map::GetCellIndexFromPosition(const Vector2DI& cellPosition)
{
	int returnIndex = -1;
	if(cellPosition.x >= 0 && cellPosition.x <= (int)m_numColumns && cellPosition.y >= 0 && cellPosition.y < (int)m_numRows)
	{
		returnIndex = cellPosition.y * m_numColumns + cellPosition.x;
	}
	if (returnIndex < 0 || returnIndex >= (int)m_cells.size())
	{
		returnIndex = -1 ;
	}
	return returnIndex;
}

//************************************
// Method:    GetCellPositionFromIndex
// FullName:  Map::GetCellPositionFromIndex
// Access:    public 
// Returns:   Vector2DI
// Qualifier:
// Parameter: int cellIndex
//************************************
Vector2DI Map::GetCellPositionFromIndex(int cellIndex)
{
	int x = cellIndex % m_numColumns;
	int y = cellIndex / m_numColumns;

	return Vector2DI(x, y);
}

//************************************
// Method:    GetPositionOfRandomUnoccupiedCell
// FullName:  Map::GetPositionOfRandomUnoccupiedCell
// Access:    public 
// Returns:   Vector2DI
// Qualifier:
// Parameter: CellType type
//************************************
Vector2DI Map::GetPositionOfRandomUnoccupiedCell(CellType type)
{
	int randomCellIndex = GetRandomIntegerInRange(0, (int)m_cells.size());
	if (type == CT_ALL && !m_cells[randomCellIndex].IsCellOccupied())
	{
		return GetCellPositionFromIndex(randomCellIndex);
	}
	else
	{
		bool shouldLoop = true;
		while(shouldLoop)
		{
			randomCellIndex = GetRandomIntegerInRange(0, (int)m_cells.size());
			shouldLoop = (randomCellIndex >=0 && randomCellIndex < (int)m_cells.size() && (m_cells[randomCellIndex].GetCellType() != type || m_cells[randomCellIndex].IsCellOccupied()) );  
		}
		return GetCellPositionFromIndex(randomCellIndex);
	}
}

//************************************
// Method:    GetRandomUnoccupiedCell
// FullName:  Map::GetRandomUnoccupiedCell
// Access:    public 
// Returns:   Cell*
// Qualifier:
// Parameter: CellType type
//************************************
Cell* Map::GetRandomUnoccupiedCell(CellType type /*= CT_ALL*/)
{
	if (m_cells.empty())
	{
		return nullptr;
	}
	int		randomCellIndex = GetRandomIntegerInRange(0, (int)m_cells.size() - 1);
	Cell*	returnCell = nullptr;
	if (type == CT_ALL && !m_cells[randomCellIndex].IsCellOccupied())
	{
		returnCell =  &m_cells[randomCellIndex];
	}
	else
	{
		bool shouldLoop = true;
		while(shouldLoop)
		{
			randomCellIndex = GetRandomIntegerInRange(0, (int)m_cells.size() - 1);
			shouldLoop = (/*randomCellIndex >=0 && randomCellIndex < (int)m_cells.size() &&*/ (m_cells[randomCellIndex].GetCellType() != type || m_cells[randomCellIndex].IsCellOccupied()) );  
		}
		returnCell = &m_cells[randomCellIndex];
	}

	return returnCell;
}

//************************************
// Method:    GetRandomCell
// FullName:  Map::GetRandomCell
// Access:    public 
// Returns:   Cell*
// Qualifier:
// Parameter: CellType type
//************************************
Cell* Map::GetRandomCell(CellType type /*= CT_ALL*/)
{
	int		randomCellIndex = GetRandomIntegerInRange(0, (int)m_cells.size() - 1);
	Cell*	returnCell = nullptr;
	if (type == CT_ALL)
	{
		returnCell =  &m_cells[randomCellIndex];
	}
	else
	{
		bool shouldLoop = true;
		while(shouldLoop)
		{
			randomCellIndex = GetRandomIntegerInRange(0, (int)m_cells.size() - 1);
			shouldLoop = (/*randomCellIndex >=0 && randomCellIndex < (int)m_cells.size() &&*/ (m_cells[randomCellIndex].GetCellType() != type) );  
		}
		returnCell = &m_cells[randomCellIndex];
	}
	return returnCell;
}

//************************************
// Method:    GetRandomCellInRadius
// FullName:  Map::GetRandomCellInRadius
// Access:    public 
// Returns:   Cell*
// Qualifier:
// Parameter: float
// Parameter: const Vector2DI &
// Parameter: CellType type
//************************************
Cell* Map::GetRandomCellInRadius(float, const Vector2DI&, CellType type /*= CT_ALL */)
{
	int		randomCellIndex = GetRandomIntegerInRange(0, (int)m_cells.size() - 1);
	Cell*	returnCell = nullptr;
	if (type == CT_ALL)
	{
		returnCell =  &m_cells[randomCellIndex];
	}
	else
	{
		bool shouldLoop = true;
		while(shouldLoop)
		{
			randomCellIndex = GetRandomIntegerInRange(0, (int)m_cells.size() - 1);
			shouldLoop = (/*randomCellIndex >=0 && randomCellIndex < (int)m_cells.size() &&*/ (m_cells[randomCellIndex].GetCellType() != type) );  
		}
		returnCell = &m_cells[randomCellIndex];
	}
	return returnCell;
}

//************************************
// Method:    GetAdjacentCells
// FullName:  Map::GetAdjacentCells
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: const Vector2DI & position
// Parameter: std::vector<Cell> & adjacentCells
//************************************
void Map::GetAdjacentCells(const Vector2DI& position, std::vector<Cell>& adjacentCells)
{
	int firstLoop = 0;
	int secondLoop = 0;
	for (int x = -1; x <= 1; ++x)
	{
		++firstLoop;
		for (int y = -1; y <= 1; ++y)
		{
			++secondLoop;
			Vector2DI posToTest = Vector2DI(position.x + x, position.y+y);
			if (posToTest == position)
			{
				continue;
			}
			int cellIndex = GetCellIndexFromPosition(posToTest);
			if (cellIndex >= 0)
			{
				adjacentCells.push_back(m_cells[cellIndex]);
			}
		}
	}
}

//************************************
// Method:    GetAdjacentCells
// FullName:  Map::GetAdjacentCells
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: const Vector2DI & position
// Parameter: std::vector<Cell * > & adjacentCells
//************************************
void Map::GetAdjacentCells(const Vector2DI& position, std::vector<Cell*>& adjacentCells)
{
	int firstLoop = 0;
	int secondLoop = 0;
	for (int x = -1; x <= 1; ++x)
	{
		++firstLoop;
		for (int y = -1; y <= 1; ++y)
		{
			++secondLoop;
			Vector2DI posToTest = Vector2DI(position.x + x, position.y+y);
			if (posToTest == position)
			{
				continue;
			}
			int cellIndex = GetCellIndexFromPosition(posToTest);
			if (cellIndex >= 0)
			{
				adjacentCells.push_back(&m_cells[cellIndex]);
			}
		}
	}
}

//************************************
// Method:    SetCellColor
// FullName:  Map::SetCellColor
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: const Vector2DI & mapPosition
// Parameter: const RgbaColors & colorToSet
//************************************
void Map::SetCellColor(const Vector2DI& mapPosition, const RgbaColors& colorToSet)
{
	int cellIndex = GetCellIndexFromPosition(mapPosition);

	if (cellIndex >= 0)
	{
		GetCellAtIndex(cellIndex)->SetCellVisibleColor(colorToSet);
	}
}

//************************************
// Method:    GetCell
// FullName:  Map::GetCell
// Access:    public 
// Returns:   Cell*
// Qualifier:
// Parameter: const Vector2DI & cellPositionToCheck
//************************************
Cell* Map::GetCell(const Vector2DI& cellPositionToCheck)
{
	Cell* returnCell = nullptr;
	int cellIndex = GetCellIndexFromPosition(cellPositionToCheck);
	if (cellIndex != -1)
	{
		returnCell = &m_cells[cellIndex];
	}
	
	return returnCell;
}

//************************************
// Method:    ClearAllVisibility
// FullName:  Map::ClearAllVisibility
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Map::ClearAllVisibility()
{
	for (int index=0; index < (int)m_cells.size(); ++index)
	{
		m_cells[index].ClearCellVisibility();
	}
}

//************************************
// Method:    ToggleMapVisibility
// FullName:  Map::ToggleMapVisibility
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Map::ToggleMapVisibility()
{
	if (m_showAll)
	{
		for (int index=0; index < (int)m_cells.size(); ++index)
		{
			m_cells[index].RestoreColor();
		}
	}
	else
	{
		for (int index=0; index < (int)m_cells.size(); ++index)
		{
			m_cells[index].StoreColor();
			m_cells[index].ClearCellVisibility();
			m_cells[index].ShowCellAsLit();
		}
	}
	m_showAll = !(m_showAll);
	m_isVboDirty = true;
}

//************************************
// Method:    ShouldShowAllCells
// FullName:  Map::ShouldShowAllCells
// Access:    public 
// Returns:   bool
// Qualifier:
//************************************
bool Map::ShouldShowAllCells()
{
	return m_showAll;
}

//************************************
// Method:    AddCell
// FullName:  Map::AddCell
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: const Cell & newCell
//************************************
void Map::AddCell(const Cell& newCell)
{
	m_cells.push_back(newCell);
}

//************************************
// Method:    GetCellAtIndex
// FullName:  Map::GetCellAtIndex
// Access:    public 
// Returns:   Cell*
// Qualifier:
// Parameter: int index
//************************************
Cell* Map::GetCellAtIndex(int index)
{
	Cell* cellToReturn = nullptr;
	if (index < 0 || index >= (int)m_cells.size())
	{
		cellToReturn = nullptr;
	}
	else
	{
		cellToReturn = &m_cells[index];
	}
	return cellToReturn;
}

//************************************
// Method:    GetNumberOfCells
// FullName:  Map::GetNumberOfCells
// Access:    public 
// Returns:   int
// Qualifier:
//************************************
int Map::GetNumberOfCells()
{
	return (int)m_cells.size();
}

//************************************
// Method:    SetNumberOfCells
// FullName:  Map::SetNumberOfCells
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Map::SetNumberOfCells()
{
	m_numberOfCells = (int)m_cells.size();
}

//************************************
// Method:    SetVBODirty
// FullName:  Map::SetVBODirty
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: bool isDirty
//************************************
void Map::SetVBODirty(bool isDirty)
{
	m_isVboDirty = isDirty;
}

//************************************
// Method:    HasNoCells
// FullName:  Map::HasNoCells
// Access:    public 
// Returns:   bool
// Qualifier:
//************************************
bool Map::HasNoCells()
{
	return m_cells.empty();
}

//************************************
// Method:    ClearCells
// FullName:  Map::ClearCells
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void Map::ClearCells()
{
	m_cells.clear();
}

//************************************
// Method:    SetVisibility
// FullName:  Map::SetVisibility
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: std::set<Cell * > visibleCells
//************************************
void Map::SetVisibility(std::set<Cell*> visibleCells)
{
	if (!visibleCells.empty())
	{
		for (std::set<Cell*>::iterator cellsIter = visibleCells.begin(); cellsIter != visibleCells.end(); ++cellsIter)
		{
			(*cellsIter)->SetCellVisibility(CV_LIT);
		}
	}
}

//************************************
// Method:    SetCellAtIndex
// FullName:  Map::SetCellAtIndex
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: int cellIndex
// Parameter: const Cell & tempCell
//************************************
void Map::SetCellAtIndex(int cellIndex, const Cell& tempCell)
{
	m_cells[cellIndex] = tempCell;
}

