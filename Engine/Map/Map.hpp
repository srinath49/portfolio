#pragma once

#ifndef MAP_H
#define MAP_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Core/RgbaColors.hpp"
#include "Engine/Math/SriMath.hpp"
//#include "GameCode/Game/GameCommons.hpp"
#include "Engine/Entities/Agent.hpp"
#include "Engine/Features/Feature.hpp"

#include <vector>
//---------------------------------------------------------------------------------------------------

class BitmapFont;
class Map;
class InputSystem;
class Cell;

//---------------------------------------------------------------------------------------------------
enum CellType
{
	CT_AIR,
	CT_WALL,
	CT_WATER,
	CT_LAVA,
	CT_NONE,
	CT_ALL
};

enum CellVisibility
{
	CV_UNLIT = 0,
	CV_FOG_OF_WAR = 1,
	CV_LIT = 2
};


//---------------------------------------------------------------------------------------------------
class Map
{
public:
	Map(void);
	~Map(void);

	void		Update(float deltaSeconds);
	void		UpdateCells();
	void		Render(BitmapFont* fontRenderer);
	void		ProcessInput(InputSystem* inputSystem);
	void		SetCellType(CellType cellType, int cellIndex);
	CellType	GetCellType(int cellIndex);
	CellType	GetCellType(const Vector2DI cellPosition);	
	inline int	GetNumRows(){ return m_numRows; }
	inline int	GetNumColumns(){ return m_numColumns; }
	inline void	SetNumRows(int numRows){ m_numRows = numRows; }
	inline void	SetNumColumns(int numColumns){ m_numColumns = numColumns; }
	inline void SetNumberOfCells(int numCells){ m_numberOfCells = numCells;}

	void		BuildVBO();
	void		FinalizeMap();
	void		ResetMap();
	int			GetCellIndexFromPosition(const Vector2DI& cellPosition);
	Vector2DI	GetCellPositionFromIndex(int cellIndex);
	Vector2DI	GetPositionOfRandomUnoccupiedCell(CellType type = CT_ALL);
	Cell*		GetRandomUnoccupiedCell(CellType type = CT_ALL);
	Cell*		GetRandomCell(CellType type = CT_ALL);
	Cell*		GetRandomCellInRadius(float, const Vector2DI&, CellType type = CT_ALL );
	void		GetAdjacentCells(const Vector2DI& position, std::vector<Cell>& adjacentCells);
	void		GetAdjacentCells(const Vector2DI& position, std::vector<Cell*>& adjacentCells);
	void		SetCellColor(const Vector2DI& mapPosition, const RgbaColors& colorToSet);
	Cell*		GetCell(const Vector2DI& cellPositionToCheck);
	void		ClearAllVisibility();
	void		ToggleMapVisibility();
	bool		ShouldShowAllCells();
	void		AddCell(const Cell& newCell);
	Cell*		GetCellAtIndex(int index);
	int			GetNumberOfCells();
	void		SetNumberOfCells();
	void		SetVBODirty(bool isDirty);
	bool		HasNoCells();
	void		ClearCells();
	void		SetVisibility(std::set<Cell*> visibleCells);
	void		SetCellAtIndex(int cellIndex, const Cell& tempCell);
private:
	unsigned int		m_vboID;
	unsigned int		m_iboID;
	unsigned int		m_cellIndexVboID;
	unsigned int		m_vboSize;
	unsigned int		m_numRows;
	unsigned int		m_numColumns;
	int					m_numberOfCells;
	bool				m_isVboDirty;
	bool				m_showAll;
	bool				m_doneGeneration;
	std::vector<Cell>	m_cells;
};

#endif