#include "MapData.hpp"


//---------------------------------------------------------------------------------------------------
const float BOTTOM_OFFSET = 110.f;
const float TOP_OFFSET = 110.f;
const float LEFT_OFFSET = 0.f;
const float RIGHT_OFFSET = 70.f;
const int ONE_CELL_UNIT = 22;
const float ONE_CELL_UNIT_IN_FLOAT = (float)ONE_CELL_UNIT;
const float VERTICAL_GAME_AREA = SCREEN_HEIGHT - (BOTTOM_OFFSET + TOP_OFFSET);
const float HORIZONTAL_GAME_AREA = SCREEN_WIDTH - (BOTTOM_OFFSET + TOP_OFFSET);
const int NUMBER_OF_CELL_COLUMNS = (int)(HORIZONTAL_GAME_AREA / ONE_CELL_UNIT);
const int NUMBER_OF_CELL_ROWS = (int)(VERTICAL_GAME_AREA / ONE_CELL_UNIT);
//---------------------------------------------------------------------------------------------------