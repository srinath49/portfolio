#pragma once


#ifndef MAP_DATA_H
#define MAP_DATA_H
//---------------------------------------------------------------------------------------------------

extern const float SCREEN_HEIGHT;
extern const float SCREEN_WIDTH;
//---------------------------------------------------------------------------------------------------
extern const float BOTTOM_OFFSET;
extern const float TOP_OFFSET;
extern const float LEFT_OFFSET;
extern const float RIGHT_OFFSET;
extern const int ONE_CELL_UNIT;
extern const float ONE_CELL_UNIT_IN_FLOAT;
extern const float VERTICAL_GAME_AREA;
extern const float HORIZONTAL_GAME_AREA;
extern const int NUMBER_OF_CELL_COLUMNS;
extern const int NUMBER_OF_CELL_ROWS;

//---------------------------------------------------------------------------------------------------
enum Directions
{
	D_NONE,
	D_EAST,
	D_WEST,
	D_NORTH,
	D_SOUTH,
	D_NORTH_EAST,
	D_NORTH_WEST,
	D_SOUTH_EAST,
	D_SOUTH_WEST
};


#endif