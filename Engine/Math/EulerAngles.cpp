#include "EulerAngles.hpp"


EulerAngles::EulerAngles(float roll, float pitch, float yaw)
{
	m_rollDegreesAboutX  = roll;
	m_pitchDegreesAboutY = pitch;
	m_yawDegreesAboutZ   = yaw;
}


EulerAngles::~EulerAngles(void)
{

}
