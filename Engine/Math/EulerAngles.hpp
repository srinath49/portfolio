#pragma once

class EulerAngles
{
public:
	EulerAngles(float roll = 0.f, float pitch = 0.f, float yaw = 0.f);
	~EulerAngles(void);

	float m_rollDegreesAboutX;
	float m_pitchDegreesAboutY;
	float m_yawDegreesAboutZ;

};

