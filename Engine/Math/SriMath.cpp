#include "Engine\Math\SriMath.hpp"
#include "Engine\Core\EngineCommons.hpp"
#include <algorithm>

Matrix4x4F Matrix4x4F::IDENTITY;
//---------------------------------------------------------------------------------------------------
//------------------------------------------Vector3DF------------------------------------------------
//---------------------------------------------------------------------------------------------------
Vector3DF::Vector3DF(float x , float y, float z )
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector3DF::Vector3DF(const Vector2DF& vector2D)
{
	this->x = vector2D.x;
	this->y = vector2D.y;
	this->z = 0.f;
}

Vector3DF::~Vector3DF(void)
{
}

Vector3DF Vector3DF::operator+(const Vector3DF& vec3d) const
{
	Vector3DF returnVector;
	returnVector.x = x + vec3d.x;
	returnVector.y = y + vec3d.y;
	returnVector.z = y + vec3d.z;
	return returnVector;
}

Vector3DF Vector3DF::operator+(int scalarInt) const
{
	Vector3DF returnVector;
	returnVector.x = x + scalarInt;
	returnVector.y = y + scalarInt;
	returnVector.z = z + scalarInt;
	return returnVector;
}

Vector3DF Vector3DF::operator+(float scalarFloat) const
{
	Vector3DF returnVector;
	returnVector.x = x + scalarFloat;
	returnVector.y = y + scalarFloat;
	returnVector.z = z + scalarFloat;
	return returnVector;
}

void Vector3DF::operator+=(const Vector3DF& vec3d)
{
	x += vec3d.x;
	y += vec3d.y;
	z += vec3d.z;
}


void Vector3DF::operator+=(const Vector2DF& vector2DToAdd)
{
	x += vector2DToAdd.x;
	y += vector2DToAdd.y;
}


void Vector3DF::operator+=(int scalarInt)
{
	x += scalarInt;
	y += scalarInt;
	z += scalarInt;
}

void Vector3DF::operator+=(float scalarFloat)
{
	x += scalarFloat;
	y += scalarFloat;
	z += scalarFloat;
}


Vector3DF Vector3DF::operator-(const Vector3DF& vec3d) const
{
	Vector3DF returnVector;
	returnVector.x = x - vec3d.x;
	returnVector.y = y - vec3d.y;
	returnVector.z = z - vec3d.z;
	return returnVector;
}

Vector3DF Vector3DF::operator-(int scalarInt) const
{
	Vector3DF returnVector;
	returnVector.x = x - scalarInt;
	returnVector.y = y - scalarInt;
	returnVector.z = z - scalarInt;
	return returnVector;
}

Vector3DF Vector3DF::operator-(float scalarFloat) const
{
	Vector3DF returnVector;
	returnVector.x = x - scalarFloat;
	returnVector.y = y - scalarFloat;
	returnVector.z = z - scalarFloat;
	return returnVector;
}

void Vector3DF::operator-=(const Vector3DF& vec3d)
{
	x -= vec3d.x;
	y -= vec3d.y;
	z -= vec3d.z;
}

void Vector3DF::operator-=(int scalarInt)
{
	x -= scalarInt;
	y -= scalarInt;
	z -= scalarInt;
}

void Vector3DF::operator-=(float scalarFloat)
{
	x -= scalarFloat;
	y -= scalarFloat;
	z -= scalarFloat;
}

void Vector3DF::operator=(const Vector3DF& copyVector)
{
	x = copyVector.x;
	y = copyVector.y;
	z = copyVector.z;
}

bool Vector3DF::operator==(const Vector3DF& compareVector) const
{
	if(x == compareVector.x && y == compareVector.y && z == compareVector.z)
	{
		return true;
	}
	return false;
}

bool Vector3DF::operator<(const Vector3DF& compareVector) const
{
	bool isLessThan = false;
	float lHash = x*2.0f + y*3.0f + z*4.0f;
	float rHash = compareVector.x*2.0f + compareVector.y*3.0f + compareVector.z*4.0f;
	if(lHash != rHash && lHash < rHash)
	{
		isLessThan = true;
	}
	return isLessThan;
}

bool Vector3DF::operator!=(const Vector3DF& compareVector) const
{
	if(x != compareVector.x || y != compareVector.y || z != compareVector.z)
	{
		return true;
	}
	return false;
}

Vector3DF Vector3DF::operator*(float scalarFloat)
{
	Vector3DF returnVector;

	returnVector.x = this->x*scalarFloat;
	returnVector.y = this->y*scalarFloat;
	returnVector.z = this->z*scalarFloat;

	return returnVector;
}

Vector3DF Vector3DF::operator*(int scalarInt)
{
	Vector3DF returnVector;

	returnVector.x = this->x*scalarInt;
	returnVector.y = this->y*scalarInt;
	returnVector.z = this->z*scalarInt;

	return returnVector;
}

void Vector3DF::operator*=(float scalarFloat)
{
	this->x *= scalarFloat;
	this->y *= scalarFloat;
	this->z *= scalarFloat;
}

void Vector3DF::operator*=(int scalarInt)
{
	this->x *= scalarInt;
	this->y *= scalarInt;
	this->z *= scalarInt;
}

void Vector3DF::operator/=(float scalarFloat)
{
	this->x /= scalarFloat;
	this->y /= scalarFloat;
	this->z /= scalarFloat;
}

void Vector3DF::operator/=(int scalarInt)
{
	this->x /= scalarInt;
	this->y /= scalarInt;
	this->z /= scalarInt;
}

Vector3DF Vector3DF::operator*(const Vector3DF& vectorToMultiply) const
{
	Vector3DF returnVector;

	returnVector.x = this->x*vectorToMultiply.x;
	returnVector.y = this->y*vectorToMultiply.y;
	returnVector.z = this->z*vectorToMultiply.z;

	return returnVector;

}

float Vector3DF::Normalize()
{
	float length = GetLength();

	this->x = this->x / length;
	this->y = this->y / length;
	this->z = this->z / length;

	return length;
}

float Vector3DF::GetLength()
{
	float length = sqrtf(this->x * this->x + this->y * this->y + this->z * this->z);
	return length;
}

//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
//------------------------------------------Vector3DI------------------------------------------------
//---------------------------------------------------------------------------------------------------
Vector3DI::Vector3DI(int x , int y, int z )
{
	this->x = x;
	this->y = y;
	this->z = z;
}


Vector3DI::~Vector3DI(void)
{
}

Vector3DI Vector3DI::operator+(const Vector3DI& vec3d)
{
	Vector3DI returnVector;
	returnVector.x = x+vec3d.x;
	returnVector.y = y+vec3d.y;
	returnVector.z = y+vec3d.z;
	return returnVector;
}

Vector3DI Vector3DI::operator+(int scalarInt)
{
	Vector3DI returnVector;
	returnVector.x = x+scalarInt;
	returnVector.y = y+scalarInt;
	returnVector.z = z+scalarInt;
	return returnVector;
}

Vector3DI Vector3DI::operator+(float scalarFloat)
{
	Vector3DI returnVector;
	returnVector.x = x + static_cast<int>(scalarFloat);
	returnVector.y = y + static_cast<int>(scalarFloat);
	returnVector.z = z + static_cast<int>(scalarFloat);
	return returnVector;
}

void Vector3DI::operator+=(const Vector3DI& vectorToAdd)
{
	x += vectorToAdd.x;
	y += vectorToAdd.y;
	z += vectorToAdd.z;
}


void Vector3DI::operator+=(const Vector2DI& vector2DToAdd)
{
	x += vector2DToAdd.x;
	y += vector2DToAdd.y;
}


void Vector3DI::operator+=(int scalarInt)
{
	x += scalarInt;
	y += scalarInt;
	z += scalarInt;
}

void Vector3DI::operator+=(float scalarFloat)
{
	x += static_cast<int>(scalarFloat);
	y += static_cast<int>(scalarFloat);
	z += static_cast<int>(scalarFloat);
}


Vector3DI Vector3DI::operator-(const Vector3DI& vectorToSubtract)
{
	Vector3DI returnVector;
	returnVector.x = x-vectorToSubtract.x;
	returnVector.y = y-vectorToSubtract.y;
	returnVector.z = y-vectorToSubtract.z;
	return returnVector;
}

Vector3DI Vector3DI::operator-(int scalarInt)
{
	Vector3DI returnVector;
	returnVector.x = x-scalarInt;
	returnVector.y = y-scalarInt;
	returnVector.z = z-scalarInt;
	return returnVector;
}

Vector3DI Vector3DI::operator-(float scalarFloat)
{
	Vector3DI returnVector;
	returnVector.x = x - static_cast<int>(scalarFloat);
	returnVector.y = y - static_cast<int>(scalarFloat);
	returnVector.z = z - static_cast<int>(scalarFloat);
	return returnVector;
}

void Vector3DI::operator-=(const Vector3DI& vectorToSubtract)
{
	x -= vectorToSubtract.x;
	y -= vectorToSubtract.y;
	z -= vectorToSubtract.z;
}

void Vector3DI::operator-=(int scalarInt)
{
	x -= scalarInt;
	y -= scalarInt;
	z -= scalarInt;
}

void Vector3DI::operator-=(float scalarFloat)
{
	x -= static_cast<int>(scalarFloat);
	y -= static_cast<int>(scalarFloat);
	z -= static_cast<int>(scalarFloat);
}

void Vector3DI::operator=(const Vector3DI& copyVector)
{
	x = copyVector.x;
	y = copyVector.y;
	z = copyVector.z;
}

bool Vector3DI::operator==(const Vector3DI& compareVector)
{
	if(x == compareVector.x && y == compareVector.y && z == compareVector.z)
	{
		return true;
	}
	return false;
}

bool Vector3DI::operator!=(const Vector3DI& compareVector)
{
	if(x != compareVector.x || y != compareVector.y || z != compareVector.z)
	{
		return true;
	}
	return false;
}

Vector3DI Vector3DI::operator*(float scalarFloat)
{
	Vector3DI returnVector;

	returnVector.x = this->x*static_cast<int>(scalarFloat);
	returnVector.y = this->y*static_cast<int>(scalarFloat);
	returnVector.z = this->z*static_cast<int>(scalarFloat);

	return returnVector;
}

Vector3DI Vector3DI::operator*(int scalarInt)
{
	Vector3DI returnVector;

	returnVector.x = this->x*scalarInt;
	returnVector.y = this->y*scalarInt;
	returnVector.z = this->z*scalarInt;

	return returnVector;
}


int Vector3DI::Normalize()
{
	int length = (int)sqrtf((float)(this->x * this->x + this->y * this->y + this->z * this->z));

	this->x = (this->x / length);
	this->y = (this->y / length);
	this->z = (this->z / length);

	return length;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
//------------------------------------------Vector2DF------------------------------------------------
//---------------------------------------------------------------------------------------------------
Vector2DF::Vector2DF(const Vector2DI& intVector2D)
{
	x = (float)intVector2D.x;
	y = (float)intVector2D.y;
}

Vector2DF::~Vector2DF(void)
{

}

Vector2DF::Vector2DF(float x, float y)
{
	this->x = x;
	this->y = y;
}

Vector2DF Vector2DF::operator+(const Vector2DF& vec2d)
{
	Vector2DF returnVector;
	returnVector.x = x + vec2d.x;
	returnVector.y = y + vec2d.y;
	return returnVector;
}

Vector2DF Vector2DF::operator+(int scalarInt)
{
	Vector2DF returnVector;
	returnVector.x = x + scalarInt;
	returnVector.y = y + scalarInt;
	return returnVector;
}

Vector2DF Vector2DF::operator+(float scalarFloat)
{
	Vector2DF returnVector;
	returnVector.x = x + scalarFloat;
	returnVector.y = y + scalarFloat;
	return returnVector;
}


void Vector2DF::operator+=(const Vector2DF& vectorToAdd)
{
	x += vectorToAdd.x;
	y += vectorToAdd.y;
}


void Vector2DF::operator+=(const Vector3DF& vector3DToAdd)
{
	x += vector3DToAdd.x;
	y += vector3DToAdd.y;
}

void Vector2DF::operator+=(int scalarInt)
{
	x += scalarInt;
	y += scalarInt;
}

void Vector2DF::operator+=(float scalarFloat)
{
	x += scalarFloat;
	y += scalarFloat;
}

Vector2DF Vector2DF::operator-(const Vector2DF& vec2d)
{
	Vector2DF returnVector;
	returnVector.x = x - vec2d.x;
	returnVector.y = y - vec2d.y;
	return returnVector;
}

Vector2DF Vector2DF::operator-(int scalarInt)
{
	Vector2DF returnVector;
	returnVector.x = x - scalarInt;
	returnVector.y = y - scalarInt;
	return returnVector;
}

Vector2DF Vector2DF::operator-(float scalarFloat)
{
	Vector2DF returnVector;
	returnVector.x = x - scalarFloat;
	returnVector.y = y - scalarFloat;
	return returnVector;
}

void Vector2DF::operator-=(const Vector2DF& vec2d)
{
	x -= vec2d.x;
	y -= vec2d.y;
}

void Vector2DF::operator-=(const Vector3DF& vector3DToSubtract)
{
	x -= vector3DToSubtract.x;
	y -= vector3DToSubtract.y;
}

void Vector2DF::operator-=(int scalarInt)
{
	x -= scalarInt;
	y -= scalarInt;
}

void Vector2DF::operator-=(float scalarFloat)
{
	x -= scalarFloat;
	y -= scalarFloat;
}

void Vector2DF::operator=(const Vector2DF& copyVector)
{
	x = copyVector.x;
	y = copyVector.y;
}

bool Vector2DF::operator==(const Vector2DF& compareVector) const
{
	if(x == compareVector.x && y == compareVector.y)
	{
		return true;
	}
	return false;
}

bool Vector2DF::operator!=(const Vector2DF& compareVector) const
{
	if(x != compareVector.x || y != compareVector.y)
	{
		return true;
	}
	return false;
}

Vector2DF Vector2DF::operator*(float scalarFloat)
{
	return Vector2DF(this->x*scalarFloat, this->y*scalarFloat);
}

Vector2DF Vector2DF::operator*(int scalarInt)
{
	return Vector2DF(this->x*scalarInt, this->y*scalarInt);
}

float Vector2DF::Normalize()
{
	float length = sqrtf(this->x * this->x + this->y * this->y);

	this->x = this->x / length;
	this->y = this->y / length;

	return length;
}

float Vector2DF::GetLength()
{
	float length = sqrtf(this->x * this->x + this->y * this->y);
	return length;
}

//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
//------------------------------------------Vector2DI------------------------------------------------
//---------------------------------------------------------------------------------------------------
Vector2DI::~Vector2DI(void)
{

}

Vector2DI::Vector2DI(int x, int y)
{
	this->x = x;
	this->y = y;
}

Vector2DI Vector2DI::operator+(const Vector2DI& vec2d)
{
	Vector2DI returnVector;
	returnVector.x = x+vec2d.x;
	returnVector.y = y+vec2d.y;
	return returnVector;
}

Vector2DI Vector2DI::operator+(int scalarInt)
{
	Vector2DI returnVector;
	returnVector.x = x+scalarInt;
	returnVector.y = y+scalarInt;
	return returnVector;
}

Vector2DI Vector2DI::operator+(float scalarFloat)
{
	Vector2DI returnVector;
	returnVector.x = x+(int)scalarFloat;
	returnVector.y = y+(int)scalarFloat;
	return returnVector;
}

void Vector2DI::operator+=(const Vector2DI& vec2d)
{
	x += vec2d.x;
	y += vec2d.y;
}

void Vector2DI::operator+=(int scalarInt)
{
	x += scalarInt;
	y += scalarInt;
}

void Vector2DI::operator+=(float scalarFloat)
{
	x += (int)scalarFloat;
	y += (int)scalarFloat;
}

Vector2DI Vector2DI::operator-(const Vector2DI& vec2d)
{
	Vector2DI returnVector;
	returnVector.x = x-vec2d.x;
	returnVector.y = y-vec2d.y;
	return returnVector;
}

Vector2DI Vector2DI::operator-(int scalarInt)
{
	Vector2DI returnVector;
	returnVector.x = x-scalarInt;
	returnVector.y = y-scalarInt;
	return returnVector;
}

Vector2DI Vector2DI::operator-(float scalarFloat)
{
	Vector2DI returnVector;
	returnVector.x = x-(int)scalarFloat;
	returnVector.y = y-(int)scalarFloat;
	return returnVector;
}

void Vector2DI::operator-=(const Vector2DI& vec2d)
{
	x -= vec2d.x;
	y -= vec2d.y;
}

void Vector2DI::operator-=(int scalarInt)
{
	x -= scalarInt;
	y -= scalarInt;
}

void Vector2DI::operator-=(float scalarFloat)
{
	x -= (int)scalarFloat;
	y -= (int)scalarFloat;
}

void Vector2DI::operator=(const Vector2DI& copyVector)
{
	x = copyVector.x;
	y = copyVector.y;
}

bool Vector2DI::operator==(const Vector2DI& compareVector)
{
	return (x == compareVector.x && y == compareVector.y);
}

bool Vector2DI::operator!=(const Vector2DI& compareVector)
{
	if(x != compareVector.x || y != compareVector.y)
	{
		return true;
	}
	return false;
}

Vector2DI Vector2DI::operator*(float scalarFloat)
{
	return Vector2DI((int)(x*scalarFloat), (int)(y*scalarFloat));
}

Vector2DI Vector2DI::operator*(int scalarInt)
{
	return Vector2DI(x*scalarInt, y*scalarInt);
}

int Vector2DI::Normalize()
{
	int length = (int)sqrtf((float)(this->x * this->x + this->y * this->y));

	this->x = (this->x / length);
	this->y = (this->y / length);

	return length;
}
//---------------------------------------------------------------------------------------------------

float Vector2DI::GetDistance(const Vector2DI& rhs)
{
	float length = sqrtf((float)(x * rhs.x + y * rhs.y));
	return length;
}

//---------------------------------------------------------------------------------------------------
//---------------------------------------Quaternion--------------------------------------------------
//---------------------------------------------------------------------------------------------------
Quaternion::Quaternion(float fx, float fy, float fz, float fw) :
x(fx),
	y(fy),
	z(fz),
	w(fw)
{
	SetAxis(w, x, y, z);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
Quaternion::Quaternion(float Angle, const Vector3DF& Axis) :
w(Angle),
	x(Axis.x),
	y(Axis.y),
	z(Axis.z)
{
	SetAxis(Angle, Axis.x, Axis.y, Axis.z);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
Quaternion::~Quaternion()
{

}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void Quaternion::SetAxis(float degrees, float fX, float fY, float fZ)
{
	float HalfAngle = DegreesToHalfRadians(degrees); // Get half angle in radians from angle in degrees
	float sinA = (float)sinf(HalfAngle);
	w = (float)cosf(HalfAngle);
	x = fX * sinA;
	y = fY * sinA;
	z = fZ * sinA;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
Quaternion Quaternion::MultiplyQuaternions(const Quaternion &lhs, const Quaternion &rhs) const
{
	Quaternion returnQuaternion;

	// Note that order matters with concatenating Quaternion rotations
	returnQuaternion.w = lhs.w*rhs.w - lhs.x*rhs.x - lhs.y*rhs.y - lhs.z*rhs.z;
	returnQuaternion.x = lhs.w*rhs.x + lhs.x*rhs.w + lhs.y*rhs.z - lhs.z*rhs.y;
	returnQuaternion.y = lhs.w*rhs.y + lhs.y*rhs.w + lhs.z*rhs.x - lhs.x*rhs.z;
	returnQuaternion.z = lhs.w*rhs.z + lhs.z*rhs.w + lhs.x*rhs.y - lhs.y*rhs.x;

	return returnQuaternion;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
Quaternion Quaternion::AddQuaternions(const Quaternion &lhs, const Quaternion &rhs) const
{
	Quaternion returnQuaternion;

	returnQuaternion.w = lhs.x + rhs.x;
	returnQuaternion.x = lhs.y + rhs.y;
	returnQuaternion.y = lhs.z + rhs.z;
	returnQuaternion.z = lhs.w + rhs.w;

	return returnQuaternion;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
float Quaternion::DotProduct(const Quaternion& quaternionToDotWith) const
{
	float dot = this->x * quaternionToDotWith.x + this->y * quaternionToDotWith.y + this->z * quaternionToDotWith.z + this->w * quaternionToDotWith.w; 
	return dot;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void Quaternion::GetQuaternionFromEulerAngles(float rx, float ry, float rz)
{
	Quaternion qx(-rx, Vector3DF(1, 0, 0));
	Quaternion qy(-ry, Vector3DF(0, 1, 0));
	Quaternion qz(-rz, Vector3DF(0, 0, 1));
	qz = MultiplyQuaternions(qy, qz);
	*this = MultiplyQuaternions(qx, qz);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void Quaternion::Normalize()
{
	float lengthSq = x * x + y * y + z * z + w * w;

	if (lengthSq == 0.0) 
	{
		return;
	}
	if (lengthSq != 1.0)
	{
		float scale = (1.0f / sqrtf(lengthSq));
		x *= scale;
		y *= scale;
		z *= scale;
		w *= scale;
	}
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void Quaternion::ToRotationMatrix(float* mf) const
{
	float twoX	  =  2.0f * x;
	float twoY	  =  2.0f * y;
	float twoZ	  =  2.0f * z;
	float twoXY   =  twoX * y;
	float twoXZ   =  twoX * z;
	float twoYSqr =  twoY * y;
	float twoYW   =  twoY * w;
	float twoZW   =  twoZ * w;
	float twoZSqr =  twoZ * z;
	float twoXSqr =  twoX * x;
	float twoXW	  =  twoX * w;
	float twoYZ   =  twoY * z;

	mf[0]  = 1.0f - (twoYSqr + twoZSqr);
	mf[1]  = (twoXY - twoZW);
	mf[2]  = (twoXZ + twoYW);
	mf[3]  = 0.0f;

	mf[4]  = (twoXY + twoZW);
	mf[5]  = 1.0f - (twoXSqr + twoZSqr);
	mf[6]  = (twoYZ - twoXW);
	mf[7]  = 0.0f;

	mf[8]  = (twoXZ - twoYW);
	mf[9]  = (twoYZ + twoXW);
	mf[10] = 1.0f - (twoXSqr + twoYSqr);
	mf[11] = 0.0f;

	mf[12] = 0.0f;
	mf[13] = 0.0f;
	mf[14] = 0.0f;
	mf[15] = 1.0f;
}
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
//------------------------------------------Matrix4x4------------------------------------------------
//---------------------------------------------------------------------------------------------------

Matrix4x4F::Matrix4x4F()
{
	MakeIdentity();
}

Matrix4x4F::Matrix4x4F(const float matrix[16])
{
	for (int index = 0; index < 16; index++)
	{
		m_matrixElements[index] = matrix[index];
	}
}

void Matrix4x4F::SetMatrixElements(const float matrix[16])
{
	for (int index = 0; index < 16; index++)
	{
		m_matrixElements[index] = matrix[index];
	}
}

const float* Matrix4x4F::GetMatrixElements() const
{
	return this->m_matrixElements;
}

void Matrix4x4F::TransposeThisMatrix()
{
	std::swap(m_matrixElements[1],  m_matrixElements[4]);
	std::swap(m_matrixElements[2],  m_matrixElements[8]);
	std::swap(m_matrixElements[3],  m_matrixElements[12]);
	std::swap(m_matrixElements[6],  m_matrixElements[9]);
	std::swap(m_matrixElements[7],  m_matrixElements[13]);
	std::swap(m_matrixElements[11], m_matrixElements[14]);

}

void Matrix4x4F::MakeIdentity()
{
	for (int index = 0; index< 16; index++)
	{
		if (index == 0 || index == 5 || index == 10 || index == 15)
		{
			m_matrixElements[index] = 1.f;
			continue;
		}
		m_matrixElements[index] = 0.f;
	}
}

void Matrix4x4F::MatrixMultiplication(const Matrix4x4F& rhsMatrix)
{
	float lhsElements[16]; 
	float rhsElements[16];

	for (int index = 0; index < 16; index++)
	{
		lhsElements[index] = this->m_matrixElements[index];
		rhsElements[index] = rhsMatrix.m_matrixElements[index];
	}
	
	m_matrixElements[0]  = lhsElements[0] * rhsElements[0]  + lhsElements[4] * rhsElements[1]  + lhsElements[8]  * rhsElements[2]  + lhsElements[12] * rhsElements[3];
	m_matrixElements[1]  = lhsElements[1] * rhsElements[0]  + lhsElements[5] * rhsElements[1]  + lhsElements[9]  * rhsElements[2]  + lhsElements[13] * rhsElements[3];
	m_matrixElements[2]  = lhsElements[2] * rhsElements[0]  + lhsElements[6] * rhsElements[1]  + lhsElements[10] * rhsElements[2]  + lhsElements[14] * rhsElements[3];
	m_matrixElements[3]  = lhsElements[3] * rhsElements[0]  + lhsElements[7] * rhsElements[1]  + lhsElements[11] * rhsElements[2]  + lhsElements[15] * rhsElements[3];
																																   
	m_matrixElements[4]  = lhsElements[0] * rhsElements[4]  + lhsElements[4] * rhsElements[5]  + lhsElements[8]  * rhsElements[6]  + lhsElements[12] * rhsElements[7];
	m_matrixElements[5]  = lhsElements[1] * rhsElements[4]  + lhsElements[5] * rhsElements[5]  + lhsElements[9]  * rhsElements[6]  + lhsElements[13] * rhsElements[7];
	m_matrixElements[6]  = lhsElements[2] * rhsElements[4]  + lhsElements[6] * rhsElements[5]  + lhsElements[10] * rhsElements[6]  + lhsElements[14] * rhsElements[7];
	m_matrixElements[7]  = lhsElements[3] * rhsElements[4]  + lhsElements[7] * rhsElements[5]  + lhsElements[11] * rhsElements[6]  + lhsElements[15] * rhsElements[7];
																							   
	m_matrixElements[8]  = lhsElements[0] * rhsElements[8]  + lhsElements[4] * rhsElements[9]  + lhsElements[8]  * rhsElements[10] + lhsElements[12] * rhsElements[11];
	m_matrixElements[9]  = lhsElements[1] * rhsElements[8]  + lhsElements[5] * rhsElements[9]  + lhsElements[9]  * rhsElements[10] + lhsElements[13] * rhsElements[11];
	m_matrixElements[10] = lhsElements[2] * rhsElements[8]  + lhsElements[6] * rhsElements[9]  + lhsElements[10] * rhsElements[10] + lhsElements[14] * rhsElements[11];
	m_matrixElements[11] = lhsElements[3] * rhsElements[8]  + lhsElements[7] * rhsElements[9]  + lhsElements[11] * rhsElements[10] + lhsElements[15] * rhsElements[11];
	
	m_matrixElements[12] = lhsElements[0] * rhsElements[12] + lhsElements[4] * rhsElements[13] + lhsElements[8]  * rhsElements[14] + lhsElements[12] * rhsElements[15];
	m_matrixElements[13] = lhsElements[1] * rhsElements[12] + lhsElements[5] * rhsElements[13] + lhsElements[9]  * rhsElements[14] + lhsElements[13] * rhsElements[15];
	m_matrixElements[14] = lhsElements[2] * rhsElements[12] + lhsElements[6] * rhsElements[13] + lhsElements[10] * rhsElements[14] + lhsElements[14] * rhsElements[15];
	m_matrixElements[15] = lhsElements[3] * rhsElements[12] + lhsElements[7] * rhsElements[13] + lhsElements[11] * rhsElements[14] + lhsElements[15] * rhsElements[15];
}

void Matrix4x4F::CreateTranslationMatrix(float x, float y, float z, Matrix4x4F& translationMatrix)
{
	translationMatrix.m_matrixElements[12] = x;
	translationMatrix.m_matrixElements[13] = y;
	translationMatrix.m_matrixElements[14] = z;
}

void Matrix4x4F::Translate(const Vector3DF& v)
{
	Translate(v.x, v.y, v.z);
}

void Matrix4x4F::Translate(float x, float y, float z)
{
	Matrix4x4F translationMatrix;
	CreateTranslationMatrix(x, y, z, translationMatrix);

	MatrixMultiplication(translationMatrix);
}

void Matrix4x4F::UniformScale(float s)
{
	Scale(s, s, s);
}

void Matrix4x4F::Scale(float x, float y, float z)
{
	m_matrixElements[0] *= x;   m_matrixElements[4] *= x;   m_matrixElements[8] *= x;   //m_matrixElements[12] *= x;
	m_matrixElements[1] *= y;   m_matrixElements[5] *= y;   m_matrixElements[9] *= y;   //m_matrixElements[13] *= y;
	m_matrixElements[2] *= z;   m_matrixElements[6] *= z;   m_matrixElements[10]*= z;   //m_matrixElements[14] *= z;
}

void Matrix4x4F::rotate(float angle, const Vector3DF& axis)
{
	rotate(angle, axis.x, axis.y, axis.z);
}

void Matrix4x4F::rotate(float angle, float x, float y, float z)
{
	float radians = DegreesToRadians(angle);
	float cosine = cos(radians);
	float sine   = sin(radians);
	float oneMinusCosine = 1.0f - cosine;  
//  	float	m0 = m_matrixElements[0],  m4 = m_matrixElements[4],  m8 = m_matrixElements[8],  m12= m_matrixElements[12],
//  			m1 = m_matrixElements[1],  m5 = m_matrixElements[5],  m9 = m_matrixElements[9],  m13= m_matrixElements[13],
//  			m2 = m_matrixElements[2],  m6 = m_matrixElements[6],  m10= m_matrixElements[10], m14= m_matrixElements[14];

	// build rotation matrix
	Matrix4x4F rotationMatrix;
	rotationMatrix.m_matrixElements[0]  = x * x * oneMinusCosine + cosine;
	rotationMatrix.m_matrixElements[1]  = x * y * oneMinusCosine + z * sine;
	rotationMatrix.m_matrixElements[2]  = x * z * oneMinusCosine - y * sine;
	rotationMatrix.m_matrixElements[3]  = 0.f;
	rotationMatrix.m_matrixElements[4]  = y * x * oneMinusCosine - z * sine;
	rotationMatrix.m_matrixElements[5]  = y * y * oneMinusCosine + cosine;
	rotationMatrix.m_matrixElements[6]  = y * z * oneMinusCosine + x * sine;
	rotationMatrix.m_matrixElements[7]  = 0.f;
	rotationMatrix.m_matrixElements[8]  = z * x * oneMinusCosine + y * sine;
	rotationMatrix.m_matrixElements[9]  = z * y * oneMinusCosine - x * sine;
	rotationMatrix.m_matrixElements[10] = z * z * oneMinusCosine + cosine;
	rotationMatrix.m_matrixElements[11] = 0.f;
	rotationMatrix.m_matrixElements[12] = 0.f;
	rotationMatrix.m_matrixElements[13] = 0.f;
	rotationMatrix.m_matrixElements[14] = 0.f;
	rotationMatrix.m_matrixElements[15] = 1.f;

	this->MatrixMultiplication(rotationMatrix);

	//m_matrixElements[0] = rotationMatrix.m_matrixElements[0] * m0 + rotationMatrix.m_matrixElements[4] * m1 + rotationMatrix.m_matrixElements[8 ] * m2;
	//m_matrixElements[1] = rotationMatrix.m_matrixElements[1] * m0 + rotationMatrix.m_matrixElements[5] * m1 + rotationMatrix.m_matrixElements[9 ] * m2;
	//m_matrixElements[2] = rotationMatrix.m_matrixElements[2] * m0 + rotationMatrix.m_matrixElements[6] * m1 + rotationMatrix.m_matrixElements[10] * m2;
	//m_matrixElements[4] = rotationMatrix.m_matrixElements[0] * m4 + rotationMatrix.m_matrixElements[4] * m5 + rotationMatrix.m_matrixElements[8 ] * m6;
	//m_matrixElements[5] = rotationMatrix.m_matrixElements[1] * m4 + rotationMatrix.m_matrixElements[5] * m5 + rotationMatrix.m_matrixElements[9 ] * m6;
	//m_matrixElements[6] = rotationMatrix.m_matrixElements[2] * m4 + rotationMatrix.m_matrixElements[6] * m5 + rotationMatrix.m_matrixElements[10] * m6;
	//m_matrixElements[8] = rotationMatrix.m_matrixElements[0] * m8 + rotationMatrix.m_matrixElements[4] * m9 + rotationMatrix.m_matrixElements[8 ] * m10;
	//m_matrixElements[9] = rotationMatrix.m_matrixElements[1] * m8 + rotationMatrix.m_matrixElements[5] * m9 + rotationMatrix.m_matrixElements[9 ] * m10;
	//m_matrixElements[10]= rotationMatrix.m_matrixElements[2] * m8 + rotationMatrix.m_matrixElements[6] * m9 + rotationMatrix.m_matrixElements[10] * m10;
	//m_matrixElements[12]= rotationMatrix.m_matrixElements[0] * m12+ rotationMatrix.m_matrixElements[4] * m13+ rotationMatrix.m_matrixElements[8 ] * m14;
	//m_matrixElements[13]= rotationMatrix.m_matrixElements[1] * m12+ rotationMatrix.m_matrixElements[5] * m13+ rotationMatrix.m_matrixElements[9 ] * m14;
	//m_matrixElements[14]= rotationMatrix.m_matrixElements[2] * m12+ rotationMatrix.m_matrixElements[6] * m13+ rotationMatrix.m_matrixElements[10] * m14;

}

void Matrix4x4F::rotateX(float angle)
{
	float radians = DegreesToRadians(angle);
	float c = cos(radians);
	float s = sin(radians);
	float m1 = m_matrixElements[1],  m2 = m_matrixElements[2],
		  m5 = m_matrixElements[5],  m6 = m_matrixElements[6],
		  m9 = m_matrixElements[9],  m10= m_matrixElements[10],
		  m13= m_matrixElements[13], m14= m_matrixElements[14];

	m_matrixElements[1] = m1 * c + m2 *-s;
	m_matrixElements[2] = m1 * s + m2 * c;
	m_matrixElements[5] = m5 * c + m6 *-s;
	m_matrixElements[6] = m5 * s + m6 * c;
	m_matrixElements[9] = m9 * c + m10*-s;
	m_matrixElements[10]= m9 * s + m10* c;
	m_matrixElements[13]= m13* c + m14*-s;
	m_matrixElements[14]= m13* s + m14* c;

}

void Matrix4x4F::rotateY(float angle)
{
	float c = cos(DegreesToRadians(angle));
	float s = sin(DegreesToRadians(angle));
	float m0 = m_matrixElements[0],  m2 = m_matrixElements[2],
		  m4 = m_matrixElements[4],  m6 = m_matrixElements[6],
		  m8 = m_matrixElements[8],  m10= m_matrixElements[10],
		  m12= m_matrixElements[12], m14= m_matrixElements[14];

	m_matrixElements[0] = m0 * c + m2 * s;
	m_matrixElements[2] = m0 *-s + m2 * c;
	m_matrixElements[4] = m4 * c + m6 * s;
	m_matrixElements[6] = m4 *-s + m6 * c;
	m_matrixElements[8] = m8 * c + m10* s;
	m_matrixElements[10]= m8 *-s + m10* c;
	m_matrixElements[12]= m12* c + m14* s;
	m_matrixElements[14]= m12*-s + m14* c;

}

void Matrix4x4F::rotateZ(float angle)
{
	float c = cos(DegreesToRadians(angle));
	float s = sin(DegreesToRadians(angle));
	float m0 = m_matrixElements[0],  m1 = m_matrixElements[1],
		  m4 = m_matrixElements[4],  m5 = m_matrixElements[5],
		  m8 = m_matrixElements[8],  m9 = m_matrixElements[9],
		  m12= m_matrixElements[12], m13= m_matrixElements[13];

	m_matrixElements[0] = m0 * c + m1 *-s;
	m_matrixElements[1] = m0 * s + m1 * c;
	m_matrixElements[4] = m4 * c + m5 *-s;
	m_matrixElements[5] = m4 * s + m5 * c;
	m_matrixElements[8] = m8 * c + m9 *-s;
	m_matrixElements[9] = m8 * s + m9 * c;
	m_matrixElements[12]= m12* c + m13*-s;
	m_matrixElements[13]= m12* s + m13* c;

}
//---------------------------------------------------------------------------------------------------