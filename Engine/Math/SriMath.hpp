#pragma once

#ifndef SRI_MATH_H
#define SRI_MATH_H
//---------------------------------------------------------------------------------------------------
class Vector2DF;
class Vector2DI;
class Vector3DI;
class Vector3DF;
class Matrix4x4F;
//---------------------------------------------------------------------------------------------------
#include <cmath>


//---------------------------------------------------------------------------------------------------
class Vector3DF
{
public:
	Vector3DF(float x = 0.f, float y = 0.f, float z = 0.f);
	Vector3DF(const Vector2DF& vector2D);
	~Vector3DF(void);

	Vector3DF operator+(const Vector3DF& vectorToAdd) const;
	Vector3DF operator+(float scalarFloat) const;
	Vector3DF operator+(int scalarInt) const;
	void operator+=(const Vector3DF& vectorToAdd);
	void operator+=(const Vector2DF& vector2DToAdd);
	void operator+=(float scalarFloat);
	void operator+=(int scalarInt);
	Vector3DF operator-(const Vector3DF& vectorToSubtract) const;
	Vector3DF operator-(float scalarFloat) const;
	Vector3DF operator-(int scalarInt) const;
	void operator-=(const Vector3DF& vectorToSubtract);
	void operator-=(float scalarFloat);
	void operator-=(int scalarInt);
	void operator=(const Vector3DF& copyVector);
	bool operator==(const Vector3DF& compareVector) const;
	bool operator!=(const Vector3DF& compareVector) const;
	bool operator<(const Vector3DF& compareVector) const;
	Vector3DF operator*(const Vector3DF& vectorToMultiply) const;
	Vector3DF operator*(float scalarFloat);
	Vector3DF operator*(int scalarInt);
	void operator*=(float scalarFloat);
	void operator*=(int scalarInt);
	void operator/=(float scalarFloat);
	void operator/=(int scalarInt);

	float Normalize();
	float GetLength();
public:
	float x;
	float y;
	float z;
};
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
class Vector3DI
{
public:
	Vector3DI(int x = 0.f, int y = 0.f, int z = 0.f);
	~Vector3DI(void);

	Vector3DI operator+(const Vector3DI& vectorToAdd);
	Vector3DI operator+(float scalarFloat);
	Vector3DI operator+(int scalarInt);
	void operator+=(const Vector3DI& vectorToAdd);
	void operator+=(const Vector2DI& vector2DToAdd);
	void operator+=(float scalarFloat);
	void operator+=(int scalarInt);
	Vector3DI operator-(const Vector3DI& vectorToSubtract);
	Vector3DI operator-(float scalarFloat);
	Vector3DI operator-(int scalarInt);
	void operator-=(const Vector3DI& vectorToSubtract);
	void operator-=(float scalarFloat);
	void operator-=(int scalarInt);
	void operator=(const Vector3DI& copyVector);
	bool operator==(const Vector3DI& compareVector);
	bool operator!=(const Vector3DI& compareVector);
	Vector3DI operator*(float scalarFloat);
	Vector3DI operator*(int scalarInt);
	int Normalize();
public:
	int x;
	int y;
	int z;
};
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
class Vector2DF
{
public:
	Vector2DF(float x = 0.f, float y = 0.f);
	Vector2DF(const Vector2DI& intVector2D);
	~Vector2DF(void);

	Vector2DF operator+(const Vector2DF& vectorToAdd);
	Vector2DF operator+(float scalarFloat);
	Vector2DF operator+(int scalarInt);
	void operator+=(const Vector2DF& vectorToAdd);
	void operator+=(const Vector3DF& vector3DToAdd);
	void operator+=(float scalarFloat);
	void operator+=(int scalarInt);
	Vector2DF operator-(const Vector2DF& vectorToSubtract);
	Vector2DF operator-(float scalarFloat);
	Vector2DF operator-(int scalarInt);
	void operator-=(const Vector2DF& vectorToSubtract);
	void operator-=(const Vector3DF& vector3DToSubtract);
	void operator-=(float scalarFloat);
	void operator-=(int scalarInt);
	void operator=(const Vector2DF& copyVector);
	bool operator==(const Vector2DF& compareVector) const;
	bool operator!=(const Vector2DF& compareVector) const;
	Vector2DF operator*(float scalarFloat);
	Vector2DF operator*(int scalarInt);
	float Normalize();
	float GetLength();
public:
	float x;
	float y;
};
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
class Vector2DI
{
public:
	Vector2DI(int x = 0, int y = 0);
	~Vector2DI(void);

	Vector2DI operator+(const Vector2DI& vectorToAdd);
	Vector2DI operator+(float scalarFloat);
	Vector2DI operator+(int scalarInt);
	void operator+=(const Vector2DI& vectorToAdd);
	void operator+=(float scalarFloat);
	void operator+=(int scalarInt);
	Vector2DI operator-(const Vector2DI& vectorToSubtract);
	Vector2DI operator-(float scalarFloat);
	Vector2DI operator-(int scalarInt);
	void operator-=(const Vector2DI& vectorToSubtract);
	void operator-=(float scalarFloat);
	void operator-=(int scalarInt);
	void operator=(const Vector2DI& copyVector);
	bool operator==(const Vector2DI& compareVector);
	bool operator!=(const Vector2DI& compareVector);;
	Vector2DI operator*(float scalarFloat);
	Vector2DI operator*(int scalarInt);
	int Normalize();
	float GetDistance(const Vector2DI& rhs);
public:
	int x;
	int y;
};
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
class Quaternion
{
public:

	Quaternion(float fx = 0.f, float fy = 0.f, float fz = 0.f, float fw = 1.f);

	Quaternion(float Angle, const Vector3DF& Axis);

	~Quaternion();

	void SetAxis(float degrees, float fX, float fY, float fZ);

	inline Quaternion MultiplyQuaternions (const Quaternion &lhs, const Quaternion &rhs) const;
	inline Quaternion AddQuaternions (const Quaternion &lhs, const Quaternion &rhs) const;

	int IsIdentity() const;

	float DotProduct(const Quaternion& a) const;	

	void GetQuaternionFromEulerAngles(float rx, float ry, float rz);

	void Normalize();

	void ToRotationMatrix(float* mf) const;


public:
	float x;
	float y;
	float z;
	float w;
};
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
class Matrix4x4F
{
public:
	// constructors
	Matrix4x4F();  // init with identity
	Matrix4x4F(const float matrix[16]);


	void MakeIdentity();
	void SetMatrixElements(const float matrix[16]);
	const float* GetMatrixElements() const;
	const float* GetTransposeFromThisMatrix();                        // return transposed matrix
	void TransposeThisMatrix();       

	// transform matrix
	void CreateTranslationMatrix(float x, float y, float z, Matrix4x4F& translationMatrix);
	void Translate(float x, float y, float z);   // translation by (x,y,z)
	void Translate(const Vector3DF& v);            //
	void rotate(float angle, const Vector3DF& axis); // rotate angle(degree) along the given axix
	void rotate(float angle, float x, float y, float z);
	void rotateX(float angle);                   // rotate on X-axis with degree
	void rotateY(float angle);                   // rotate on Y-axis with degree
	void rotateZ(float angle);                   // rotate on Z-axis with degree
	void UniformScale(float scale);                     // uniform scale
	void Scale(float sx, float sy, float sz);    // scale by (sx, sy, sz) on each axis

	void MatrixMultiplication(const Matrix4x4F& rhs);   
	
public:
	float m_matrixElements[16];
	static Matrix4x4F IDENTITY;
};
//---------------------------------------------------------------------------------------------------
#endif