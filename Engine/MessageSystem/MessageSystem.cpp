#include "Engine/MessageSystem/MessageSystem.hpp"
#include "Engine/Map/MapData.hpp"
#include "Engine/DebugConsole/BitmapFont.hpp"
#include "Engine/Input/InputSystem.hpp"
//---------------------------------------------------------------------------------------------------

MessageSystem MessageSystem::s_messageSystem;


//---------------------------------------------------------------------------------------------------
MessageSystem::MessageSystem()
{

}

//---------------------------------------------------------------------------------------------------
MessageSystem::~MessageSystem()
{

}

//---------------------------------------------------------------------------------------------------
void MessageSystem::DrawMessage(BitmapFont* bitmapFont)
{
	if (bitmapFont && !s_messageSystem.m_messagesList.empty())
	{
		std::string messageToDisplay = s_messageSystem.m_messagesList[0].m_messageToDisplay;
		if (s_messageSystem.m_messagesList.size() > 1)
		{
			messageToDisplay += " ... more...";
		}
		bitmapFont->DrawString(messageToDisplay, s_messageSystem.m_messagesList[0].m_messageColor, 32, Vector2DF(LEFT_OFFSET, 850.f), true);
	}
}


//---------------------------------------------------------------------------------------------------
void MessageSystem::AddMessageToList(const std::string& newMessage, const RgbaColors& messageColor)
{
	DisplayMessage newDisplayMessage;
	newDisplayMessage.m_messageToDisplay = newMessage;
	newDisplayMessage.m_messageColor = messageColor;

	s_messageSystem.m_messagesList.push_back(newDisplayMessage);
}


//---------------------------------------------------------------------------------------------------
void MessageSystem::GotoNextMessage()
{
	if (!s_messageSystem.m_messagesList.empty())
	{
		s_messageSystem.m_messagesList.erase(s_messageSystem.m_messagesList.begin());
	}
}

//---------------------------------------------------------------------------------------------------
bool MessageSystem::HasMultipleMessages()
{
	bool returnVal = false;

	returnVal = s_messageSystem.m_messagesList.size() > 1;

	return returnVal;
}

//---------------------------------------------------------------------------------------------------
void MessageSystem::ClearAllMessages()
{
	s_messageSystem.m_messagesList.clear();
}

//---------------------------------------------------------------------------------------------------
void MessageSystem::ProcessInput(InputSystem* inputSystem)
{
	if (!inputSystem)
	{
		return;
	}
	if (inputSystem->HasKeyJustBeenReleased(KEY_CODE_SPC))
	{
		GotoNextMessage();
	}
}
