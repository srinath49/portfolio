#pragma once

#ifndef MESSAGE_SYSTEM_H
#define MESSAGE_SYSTEM_H
//---------------------------------------------------------------------------------------------------

#include <vector>
#include <string>
#include "Engine\Core\RgbaColors.hpp"
//---------------------------------------------------------------------------------------------------

class BitmapFont;
class InputSystem;

//---------------------------------------------------------------------------------------------------
struct DisplayMessage 
{
	std::string m_messageToDisplay;
	RgbaColors		m_messageColor;
};

//---------------------------------------------------------------------------------------------------
class MessageSystem
{
public:
	MessageSystem();
	~MessageSystem();

	static void DrawMessage(BitmapFont* bitmapFont);
	static void AddMessageToList(const std::string&, const RgbaColors& color);
	static void GotoNextMessage();
	static bool HasMultipleMessages();
	static void ClearAllMessages();
	static void ProcessInput(InputSystem* inputSystem);

	//---------------------------------------------------------------------------------------------------
	std::vector<DisplayMessage> m_messagesList;
	static MessageSystem		s_messageSystem;
};

#endif