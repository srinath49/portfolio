#include "Engine/ParticleSystem/ParticleEmiter.hpp"
#include <iostream>
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/SpriteSheet.hpp"
#include "Engine/Rendering/SriOGLRenderer.hpp"
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

//---------------------------------------------------------------------------------------------------
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <random>

const float g_gravity = -(0.098f + 0.098f);
const float g_deltaT = 0.5f;
const float g_quadSize = 3.f;

//---------------------------------------------------------------------------------------------------
ParticleEmiter::ParticleEmiter()
{
	m_spriteSheet = nullptr;
	m_strategy = nullptr;

	for (int index = 0; index < 150; index++)
	{
		m_particles.push_back(Particle());
	}

}

//---------------------------------------------------------------------------------------------------
ParticleEmiter::~ParticleEmiter(void)
{
	delete m_spriteSheet;
	delete m_strategy;
}

//---------------------------------------------------------------------------------------------------
void ParticleEmiter::Update()
{
	if (m_spriteSheet)
	{
		m_spriteSheet->Update();
	}
	
	// LifeSpan Update
	for (int index = 0; index < (int)m_particles.size(); index++)
	{
		m_particles[index].UpdateLife();
	}

	int particlesSize = (int)m_particles.size();
	int maxDeletePerFrame = 200;
	int deletedThisFrame = 0;
	for (int index = 0; index < particlesSize; index++)
	{
		if(!m_particles[index].m_isAlive)
		{
			m_particles[index] = m_particles[m_particles.size() - 1];
			m_particles.pop_back();
			index--;
			particlesSize--;
			deletedThisFrame++;
			if(deletedThisFrame >= maxDeletePerFrame)
			{
				break;
			}
		}
	}

	// Forward Euler Update
	for (int index = 0; index < (int)m_particles.size(); index++)
	{
		if(m_strategy)
		{
			m_strategy->Update(m_particles, index);
			continue;
		}
		//ForwardEuler(index);
	}
}

//---------------------------------------------------------------------------------------------------
void ParticleEmiter::Render()
{
	std::vector<VertexData> vertexes;
	
	VertexData data1;
	VertexData data2;
	VertexData data3;
	VertexData data4;

	Vector2DF minTextureCoords; 
	Vector2DF maxTextureCoords; 

	if(m_spriteSheet)
	{
		minTextureCoords = m_spriteSheet->m_minTexCoords;
		maxTextureCoords = m_spriteSheet->m_maxTexCoords;
	}

	for(int index = 0; index < (int)m_particles.size(); index++)
	{
		data1.m_color = m_particles[index].m_color;
		data1.m_position = Vector2DF(m_particles[index].m_position.x, m_particles[index].m_position.y + g_quadSize);
		data1.m_texCoords = Vector2DF(minTextureCoords.x, minTextureCoords.y);
		vertexes.push_back(data1);
		
		data2.m_color = m_particles[index].m_color;
		data2.m_position = Vector2DF(m_particles[index].m_position.x, m_particles[index].m_position.y);
		data2.m_texCoords = Vector2DF(minTextureCoords.x, maxTextureCoords.y);
		vertexes.push_back(data2);
		
		data3.m_color = m_particles[index].m_color;
		data3.m_position = Vector2DF(m_particles[index].m_position.x + g_quadSize, m_particles[index].m_position.y);
		data3.m_texCoords = Vector2DF(maxTextureCoords.x, maxTextureCoords.y);
		vertexes.push_back(data3);
		
		data4.m_color = m_particles[index].m_color;
		data4.m_position = Vector2DF(m_particles[index].m_position.x + g_quadSize, m_particles[index].m_position.y + g_quadSize);
		data4.m_texCoords = Vector2DF(maxTextureCoords.x, minTextureCoords.y);
		vertexes.push_back(data4);
	}
	
	DrawVertexArray(vertexes, GL_QUADS);
}

//---------------------------------------------------------------------------------------------------
void ParticleEmiter::DrawVertexArray(std::vector<VertexData> vertexes, unsigned int mode)
{
	if(vertexes.size() == 0)
	{
		return;
	}
	
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_ONE, GL_ONE);
	g_myRenderer->DrawVertexArray(vertexes, mode);
// 	glEnableClientState( GL_VERTEX_ARRAY );
// 	glEnableClientState( GL_COLOR_ARRAY );
// 	glEnableClientState( GL_TEXTURE_COORD_ARRAY );
// 
// 	glVertexPointer(	2, GL_FLOAT, sizeof( VertexData ), &vertexes[0].m_position );	
// 	glColorPointer(		3, GL_FLOAT, sizeof( VertexData ), &vertexes[0].m_color );	
// 	glTexCoordPointer(	2, GL_FLOAT, sizeof( VertexData ), &vertexes[0].m_texCoords );	
// 
// 	glDrawArrays( mode, 0, vertexes.size() );
// 
// 	glDisableClientState( GL_VERTEX_ARRAY );
// 	glDisableClientState( GL_COLOR_ARRAY );
// 	glDisableClientState( GL_TEXTURE_COORD_ARRAY );
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

// Deprecated!
//---------------------------------------------------------------------------------------------------
void ParticleEmiter::ForwardEuler(int index)
{
	Vector2DF prevPos = m_particles[index].m_position;
	Vector2DF prevVel = m_particles[index].m_velocity;
	m_particles[index].m_position = prevPos + (prevVel * g_deltaT);
	Vector2DF force = Vector2DF(0.f, g_gravity);
	Vector2DF acc = Vector2DF(0.f, force.y * m_particles[index].m_oneOverMass);
	m_particles[index].m_velocity = prevVel + (acc*g_deltaT);
}

//---------------------------------------------------------------------------------------------------
void ParticleEmiter::SetAndInitStrategy(UpdateStrategy& strategy, const Vector2DF& spawnPosition, const RgbaColors& color)
{
	m_strategy = &strategy;
	if (m_strategy)
	{
		m_strategy->InitSystem(m_particles, spawnPosition, color);
	}
}

//---------------------------------------------------------------------------------------------------
void Particle::UpdateLife()
{
	if (Time::GetAbsoluteTimeSeconds() - m_bornTime >= m_lifeSpan)
	{
		m_isAlive =	false;
	}

}


// Deprecated!
//---------------------------------------------------------------------------------------------------
// void Particle::InitExplosion()
// {
// 	float a = (float)(rand() & g_mask) * 0.00628f;
// 	float b = (float)(rand() & g_mask) * 0.00628f;
// 	if (a < b)
// 	{
// 		if (b==0)
// 		{
// 			b = 0.1f;
// 		}
// 		std::swap(a, b);
// 	}
// 	float abyB = a/b;
// 	Vector2DF initialVelocity(0.f, 0.f);
// 	m_isAlive = true;
// 	m_position = Vector2DF(800.f, 450.f);
// 	m_lifeSpan = 2.5f + (rand() % 2);
// 	//m_velocity = Vector2DF(b*15.f*cos(TWO_PI*abyB), b*15.f*sin(TWO_PI*abyB));
// 	m_velocity = Vector2DF(/*b*15.f**/cos(TWO_PI*abyB), /*b*15.f**/sin(TWO_PI*abyB)) * 10.f;
// 	m_color = RGBA(0.f, 0.f, 0.f);
// 	m_oneOverMass= 1.f;
// 	m_bornTime = Time::GetAbsoluteTimeSeconds();
// }


//---------------------------------------------------------------------------------------------------
void ExplosionStrategy::InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color)
{
	float a;
	float b;
	for (int index = 0; index < 100; index++)
	{
		Particle particle;
		particle.m_position = Vector2DF(position.x - 800.f, position.y - 450.f);
		particles.push_back(particle);
	}

	for (int index = 0; index < (int)particles.size(); index++)
	{
		a = (float)(rand() & g_mask) * 0.00628f;
		b = (float)(rand() & g_mask) * 0.00628f;
		if (a < b)
		{
			if (b==0)
			{
				b = 0.1f;
			}
			std::swap(a, b);
		}
		float abyB = a/b;
		Vector2DF initialVelocity(0.f, 0.f);
		particles[index].m_isAlive = true;
		particles[index].m_position = Vector2DF(position.x, position.y);
		particles[index].m_lifeSpan = 2.5f + (rand() % 2);
		particles[index].m_velocity = Vector2DF(/*b*15.f**/cos(TWO_PI*abyB), /*b*15.f**/sin(TWO_PI*abyB)) * 10.f;
		particles[index].m_color = RgbaColors(color.r * 10.f, color.g * 10.f, color.b * 10.f);
		particles[index].m_oneOverMass= 1.f;
		particles[index].m_bornTime = Time::GetAbsoluteTimeSeconds();
	}
}	

//---------------------------------------------------------------------------------------------------
void ExplosionStrategy::Update(std::vector<Particle>& particles, int index)
{
	Vector2DF prevPos = particles[index].m_position;
	Vector2DF prevVel = particles[index].m_velocity;
	particles[index].m_position = prevPos + (prevVel * g_deltaT);
	Vector2DF force = Vector2DF(0.f, 0.f);
	Vector2DF acc = Vector2DF(0.f, force.y * particles[index].m_oneOverMass);
	particles[index].m_velocity = prevVel + (acc*g_deltaT);
}


//---------------------------------------------------------------------------------------------------
void FireworksStrategy::InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color)
{
	UNUSED(position);
	UNUSED(color);

	float a;
	float b;

	for (int index = 0; index < 25; index++)
	{
		particles.push_back(Particle());
	}

	for (int index = 0; index < (int)particles.size(); index++)
	{

		a = (float)(rand() & g_mask) * 0.00628f * 2.f - 1.f;
		b = (float)(rand() & g_mask) * 0.00628f + 8.f;

// 		if (a < b)
// 		{
// 			if (b==0)
// 			{
// 				b = 0.1f;
// 			}
// 			std::swap(a, b);
// 		}
		//float abyB = a/b;
		
		Vector2DF initialVelocity(a, b);
		
		particles[index].m_isAlive = true;
		particles[index].m_position = Vector2DF(800.f, 450.f);
		particles[index].m_lifeSpan = 3.5f + (rand() % 7);
		//particles[index].m_velocity = Vector2DF(/*b*15.f**/cos(TWO_PI*abyB), /*b*15.f**/sin(TWO_PI*abyB)) * 10.f;
		particles[index].m_velocity = initialVelocity;
		particles[index].m_color = RgbaColors(0.f, 0.f, 0.f);
		particles[index].m_oneOverMass= 1.f;
		particles[index].m_bornTime = Time::GetAbsoluteTimeSeconds();
	}
}

void FireworksStrategy::Update(std::vector<Particle>& particles, int index)
{
	Vector2DF prevPos = particles[index].m_position;
	Vector2DF prevVel = particles[index].m_velocity;
	particles[index].m_position = prevPos + (prevVel * g_deltaT);
	Vector2DF force = Vector2DF(0.f, g_gravity);
	Vector2DF acc = Vector2DF(0.f, force.y * particles[index].m_oneOverMass);
	particles[index].m_velocity = prevVel + (acc*g_deltaT);
}

void FountainStrategy::InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color)
{
	UNUSED(position);
	UNUSED(color);
	for (int index = 0; index < 10; index++)
	{
		particles.push_back(Particle());
	}

	for (int index = 0; index < (int)particles.size(); index++)
	{

		float a = (float)(rand() & g_mask) * 0.00628f * 2.f - 1.f;
		float b = (float)(rand() & g_mask) * 0.00628f + 7.f;
		Vector2DF initialVelocity(a, b);

		particles[index].m_isAlive = true;
		particles[index].m_position = Vector2DF(800.f, 450.f);
		particles[index].m_lifeSpan = 1.5f + (rand() % 3);
		//particles[index].m_velocity = Vector2DF(/*b*15.f**/cos(TWO_PI*abyB), /*b*15.f**/sin(TWO_PI*abyB)) * 10.f;
		particles[index].m_velocity = initialVelocity;
		particles[index].m_color = RgbaColors(0.f, 0.f, 0.f);
		particles[index].m_oneOverMass= 1.f;
		particles[index].m_bornTime = Time::GetAbsoluteTimeSeconds();
	}
	m_timePassed = Time::GetAbsoluteTimeSeconds();
}

void FountainStrategy::Update(std::vector<Particle>& particles, int index)
{
	Vector2DF prevPos = particles[index].m_position;
	Vector2DF prevVel = particles[index].m_velocity;
	particles[index].m_position = prevPos + (prevVel * g_deltaT);
	Vector2DF force = Vector2DF(0.f, g_gravity);
	Vector2DF acc = Vector2DF(0.f, force.y * particles[index].m_oneOverMass);
	particles[index].m_velocity = prevVel + (acc*g_deltaT);

	if(/*particles.size() < 300 &&*/ Time::GetAbsoluteTimeSeconds() - m_timePassed > 0.025 )
	{
		particles.push_back(Particle());
		float a = (float)(rand() & g_mask) * 0.00628f * 2.2f - 1.1f;
		float b = (float)(rand() & g_mask) * 0.00628f + 7.5f;
		Vector2DF initialVelocity(a, b);

		particles.back().m_isAlive = true;
		particles.back().m_position = Vector2DF(800.f, 450.f);
		particles.back().m_lifeSpan = 3.5f + (rand() % 7);
		particles.back().m_velocity = initialVelocity;
		particles.back().m_color = RgbaColors(0.f, 0.f, 0.f);
		particles.back().m_oneOverMass= 1.f;
		particles.back().m_bornTime = Time::GetAbsoluteTimeSeconds();

		particles.push_back(Particle());
		a = (float)(rand() & g_mask) * 0.00628f * 2.2f - 1.1f;
		b = (float)(rand() & g_mask) * 0.00628f + 7.5f;
		initialVelocity = Vector2DF(a, b);

		particles.back().m_isAlive = true;
		particles.back().m_position = Vector2DF(800.f, 450.f);
		particles.back().m_lifeSpan = 3.5f + (rand() % 7);
		particles.back().m_velocity = initialVelocity;
		particles.back().m_color = RgbaColors(0.f, 0.f, 0.f);
		particles.back().m_oneOverMass= 1.f;
		particles.back().m_bornTime = Time::GetAbsoluteTimeSeconds();

		particles.push_back(Particle());
		a = (float)(rand() & g_mask) * 0.00628f * 2.2f - 1.1f;
		b = (float)(rand() & g_mask) * 0.00628f + 7.5f;
		initialVelocity = Vector2DF(a, b);

		particles.back().m_isAlive = true;
		particles.back().m_position = Vector2DF(800.f, 450.f);
		particles.back().m_lifeSpan = 3.5f + (rand() % 7);
		particles.back().m_velocity = initialVelocity;
		particles.back().m_color = RgbaColors(0.f, 0.f, 0.f);
		particles.back().m_oneOverMass= 1.f;
		m_timePassed = Time::GetAbsoluteTimeSeconds();
		particles.back().m_bornTime = m_timePassed;
	}
}

void DebrisStrategy::InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color)
{
	UNUSED(position);
	UNUSED(color);
	for (int index = 0; index < 5; index++)
	{
		particles.push_back(Particle());
	}

	for (int index = 0; index < (int)particles.size(); index++)
	{

		float a = (float)(rand() & g_mask) * 0.00628f * 2.2f - 1.1f;
		float b = (float)(rand() & g_mask) * 0.00628f + 7.5f;
		Vector2DF initialVelocity(a, b);

		particles[index].m_isAlive = true;
		particles[index].m_position = Vector2DF(800.f, 450.f);
		particles[index].m_lifeSpan = 7.5f + (rand() % 3);
		//particles[index].m_velocity = Vector2DF(/*b*15.f**/cos(TWO_PI*abyB), /*b*15.f**/sin(TWO_PI*abyB)) * 10.f;
		particles[index].m_velocity = initialVelocity;
		particles[index].m_color = RgbaColors(0.f, 0.f, 0.f);
		particles[index].m_oneOverMass= 1.f;
		particles[index].m_bornTime = Time::GetAbsoluteTimeSeconds();
	}
	m_totalSpawned = 0;
	m_timePassed = Time::GetAbsoluteTimeSeconds();
	minYPosition = 400;
}

void DebrisStrategy::Update(std::vector<Particle>& particles, int index)
{
	Vector2DF prevPos = particles[index].m_position;
	Vector2DF prevVel = particles[index].m_velocity;
	particles[index].m_position = prevPos + (prevVel * g_deltaT);
	Vector2DF force = Vector2DF(0.f, g_gravity);
	Vector2DF acc = Vector2DF(0.f, force.y * particles[index].m_oneOverMass);
	particles[index].m_velocity = prevVel + (acc*g_deltaT);

	if(m_totalSpawned < 100 && Time::GetAbsoluteTimeSeconds() - m_timePassed > 0.001)
	{
		particles.push_back(Particle());
		float a = (float)(rand() & g_mask) * 0.00628f * 2.2f - 1.1f;
		float b = (float)(rand() & g_mask) * 0.00628f + 7.5f;
		Vector2DF initialVelocity(a, b);

		particles.back().m_isAlive = true;
		particles.back().m_position = Vector2DF(800.f, 450.f);
		particles.back().m_lifeSpan = 7.5f + (rand() % 3);
		particles.back().m_velocity = initialVelocity;
		particles.back().m_color = RgbaColors(0.f, 0.f, 0.f);
		particles.back().m_oneOverMass= 1.f;
		m_timePassed = Time::GetAbsoluteTimeSeconds();
		particles.back().m_bornTime = m_timePassed;
		m_totalSpawned++;
	}

	if (particles[index].m_position.y <= minYPosition)
	{
		particles[index].m_position = Vector2DF(particles[index].m_position.x, minYPosition+0.01f);
		particles[index].m_velocity.y = particles[index].m_velocity.y * - 0.5f;
	}
}

void SmokeStrategy::InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color)
{
	UNUSED(position);
	UNUSED(color);
	for (int index = 0; index < 5; index++)
	{
		particles.push_back(Particle());
	}

	for (int index = 0; index < (int)particles.size(); index++)
	{

		float a = (float)((rand() & g_mask) * 0.00628f * 2.f - 1.f) * 0.5f;
		float b = (float)(rand() & g_mask) * 0.00628f + 1.f;
		Vector2DF initialVelocity(a, b);

		particles[index].m_isAlive = true;
		particles[index].m_position = Vector2DF(800.f, 200.f);
		particles[index].m_lifeSpan = 5.f + (rand() % 3);
		particles[index].m_velocity = initialVelocity;
		particles[index].m_color = RgbaColors(0.f, 0.f, 0.f);
		particles[index].m_oneOverMass= 1.f;
		particles[index].m_bornTime = Time::GetAbsoluteTimeSeconds();
	}
// 	m_totalSpawned = 0;
	m_timePassed = Time::GetAbsoluteTimeSeconds();
}

void SmokeStrategy::Update(std::vector<Particle>& particles, int index)
{
	Vector2DF prevPos = particles[index].m_position;
	Vector2DF prevVel = particles[index].m_velocity;
	particles[index].m_position = prevPos + (prevVel * g_deltaT);
	Vector2DF force = Vector2DF(0.13f, 0.10f);
	Vector2DF acc = Vector2DF(force.x, force.y * particles[index].m_oneOverMass);
	particles[index].m_velocity = prevVel + (acc*g_deltaT);

	if(Time::GetAbsoluteTimeSeconds() - m_timePassed > 0.01 )
	{
		particles.push_back(Particle());
		float a = (float)((rand() & g_mask) * 0.00628f * 2.f - 1.f) * 0.5f;
		float b = (float)(rand() & g_mask) * 0.00628f + 1.f;
		Vector2DF initialVelocity(a, b);

		particles.back().m_isAlive = true;
		particles.back().m_position = Vector2DF(800.f, 200.f);
		particles.back().m_lifeSpan = 3.5f + (rand() % 7);
		particles.back().m_velocity = initialVelocity;
		particles.back().m_color = RgbaColors(0.f, 0.f, 0.f);
		particles.back().m_oneOverMass= 1.f;
		particles.back().m_bornTime = Time::GetAbsoluteTimeSeconds();

		m_timePassed = Time::GetAbsoluteTimeSeconds();
	}
}

void SriStrategy::InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color)
{
	UNUSED(position);
	UNUSED(color);
	for (int index = 0; index < 15; index++)
	{
		particles.push_back(Particle());
	}

	for (int index = 0; index < (int)particles.size(); index++)
	{
		float a = (float)((rand() & g_mask) * 0.00628f * 2.f - 1.f) * 800.f;
		float b = (float)(rand() & g_mask) * 0.00628f + 1.f;
		Vector2DF initialVelocity(b, 0.f);

		particles[index].m_isAlive = true;
		particles[index].m_position = Vector2DF(a, 900.f);
		m_initialX = a;
		m_minX.push_back(a - (b * 15.f));
		m_maxX.push_back(a + (b * 15.f));
		particles[index].m_lifeSpan = 15.f + (rand() % 10);
		particles[index].m_velocity = initialVelocity;
		particles[index].m_color = RgbaColors(0.f, 0.f, 0.f);
		particles[index].m_oneOverMass= 1.f;
		particles[index].m_bornTime = Time::GetAbsoluteTimeSeconds();
	}
	// 	m_totalSpawned = 0;
	m_timePassed = Time::GetAbsoluteTimeSeconds();
}

void SriStrategy::Update(std::vector<Particle>& particles, int index)
{
	Vector2DF prevPos = particles[index].m_position;
	Vector2DF prevVel = particles[index].m_velocity;
	particles[index].m_position = prevPos + (prevVel * g_deltaT);
	Vector2DF force = Vector2DF(0.f, -0.07f);
	Vector2DF acc = Vector2DF(force.x, force.y * particles[index].m_oneOverMass);
	particles[index].m_velocity = prevVel + (acc*g_deltaT);
	
	if (particles[index].m_position.x  < m_minX[index] || particles[index].m_position.x  >  m_maxX[index])
	{
		particles[index].m_velocity.x = particles[index].m_velocity.x * -1.f;
	}

	if(Time::GetAbsoluteTimeSeconds() - m_timePassed > 0.01 )
	{
		particles.push_back(Particle());
		float a = (float)((rand() & g_mask) * 0.00628f * 2.f - 1.f) * 800.f;
		float b = (float)(rand() & g_mask) * 0.00628f + 1.f;
		Vector2DF initialVelocity(b, 0.f);

		particles.back().m_isAlive = true;
		particles.back().m_position = Vector2DF(a, 900.f);
		m_initialX = a;
		m_minX.push_back(a - (b * 15.f));
		m_maxX.push_back(a + (b * 15.f));
		particles.back().m_lifeSpan = 3.5f + (rand() % 7);
		particles.back().m_velocity = initialVelocity;
		particles.back().m_color = RgbaColors(0.f, 0.f, 0.f);
		particles.back().m_oneOverMass= 1.f;
		particles.back().m_bornTime = Time::GetAbsoluteTimeSeconds();

		m_timePassed = Time::GetAbsoluteTimeSeconds();
	}
}
