#pragma once

#ifndef PARTICLE_EMMITER_H
#define PARTICLE_EMMITER_H

#include <vector>
#include "Engine/Math/SriMath.hpp"
#include "Engine/Core/RgbaColors.hpp"
#include "Engine/Core/Time.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/Core/VertexData.hpp"
extern const unsigned char g_mask;
extern const float g_gravity;

class SpriteSheet;


//---------------------------------------------------------------------------------------------------
struct Particle
{
public:

	Particle(){}

	~Particle(){}

	void UpdateLife();
//	void InitExplosion();
	
	double		m_lifeSpan;
	double		m_bornTime;
	bool		m_isAlive;
	Vector2DF	m_position;
	Vector2DF	m_velocity;
	RgbaColors		m_color;
	float		m_c;
	float		m_oneOverMass;
};

//---------------------------------------------------------------------------------------------------
class ParticleEmiter
{
public:
	struct UpdateStrategy
	{
		UpdateStrategy(){}
		virtual ~UpdateStrategy(){}

		virtual void InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color)
		{
			UNUSED(particles);
			UNUSED(position);
			UNUSED(color);
		}
		virtual void Update(std::vector<Particle>& particles, int index)
		{
			UNUSED(particles);
			UNUSED(index);
		}
	};
	
	ParticleEmiter();
	~ParticleEmiter();

	void Update();
	void Render();
	void ForwardEuler(int index);
	void DrawVertexArray(std::vector<VertexData>, unsigned int mode);
	void SetAndInitStrategy(UpdateStrategy& strategy, const Vector2DF& spawnPosition, const RgbaColors& color);

	std::vector<Particle>	m_particles;
	int m_aliveCount;
	SpriteSheet* m_spriteSheet;
	UpdateStrategy* m_strategy;
};
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
class ExplosionStrategy:public ParticleEmiter::UpdateStrategy
{
public:
	ExplosionStrategy(){}
	~ExplosionStrategy(){}


	void InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color);
	void Update(std::vector<Particle>& particles, int index);
};


//---------------------------------------------------------------------------------------------------
class FireworksStrategy:public ParticleEmiter::UpdateStrategy
{
public:
	FireworksStrategy(){}
	~FireworksStrategy(){}


	void InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color);
	void Update(std::vector<Particle>& particles, int index);
};

class FountainStrategy:public ParticleEmiter::UpdateStrategy
{
public:
	FountainStrategy(){}
	~FountainStrategy(){}


	void InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color);
	void Update(std::vector<Particle>& particles, int index);
	double m_timePassed;
};


class DebrisStrategy:public ParticleEmiter::UpdateStrategy
{
public:
	DebrisStrategy(){}
	~DebrisStrategy(){}

	void InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color);
	void Update(std::vector<Particle>& particles, int index);

	int m_totalSpawned;
	double m_timePassed;
	float minYPosition;
};

//---------------------------------------------------------------------------------------------------
class SmokeStrategy:public ParticleEmiter::UpdateStrategy
{
public:
	SmokeStrategy(){}
	~SmokeStrategy(){}

	void InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color);
	void Update(std::vector<Particle>& particles, int index);

	int m_totalSpawned;
	double m_timePassed;
};



//---------------------------------------------------------------------------------------------------
class SriStrategy:public ParticleEmiter::UpdateStrategy
{
public:
	SriStrategy(){}
	~SriStrategy(){}

	void InitSystem(std::vector<Particle>& particles, const Vector2DF& position, const RgbaColors& color);
	void Update(std::vector<Particle>& particles, int index);

	int m_totalSpawned;
	double m_timePassed;
	float m_initialX;
	std::vector<float> m_minX;
	std::vector<float> m_maxX;

};
#endif