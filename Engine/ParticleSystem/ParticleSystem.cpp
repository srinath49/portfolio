#include "ParticleSystem.hpp"
#include "ParticleEmiter.hpp"


ParticleSystem::ParticleSystem()
{
	m_explosionEmiter	= nullptr;
	m_fireworksEmiter	= nullptr;
	m_debrisEmiter		= nullptr;
	m_fountainEmiter	= nullptr;
	m_smokeEmiter		= nullptr;
	m_sriEmiter			= nullptr;
	//srand(Time::GetAbsoluteTimeSeconds());
	m_explosionEmiter = new ParticleEmiter();
//	ParticleEmiter::UpdateStrategy* strategy = new ExplosionStrategy();
	//
	//m_explosionEmiter->SetAndInitStrategy(*strategy);

	//m_fireworksEmiter = new ParticleEmiter();
	//strategy = new FireworksStrategy();
	//m_fireworksEmiter->SetAndInitStrategy(*strategy);
	//strategy = new FountainStrategy();

	//m_fountainEmiter = new ParticleEmiter();
	//m_fountainEmiter->SetAndInitStrategy(*strategy);
	//m_debrisEmiter = new ParticleEmiter();
	//m_debrisEmiter->SetAndInitStrategy(*strategy);

	//m_smokeEmiter = new ParticleEmiter();
	//m_smokeEmiter->SetAndInitStrategy(*strategy);

	//m_sriEmiter = new ParticleEmiter();
	//m_sriEmiter->SetAndInitStrategy(*strategy);
}


ParticleSystem::~ParticleSystem(void)
{

}

void ParticleSystem::Update()
{
	if(m_fireworksEmiter)
	{
		m_fireworksEmiter->Update();
	}
	if (m_explosionEmiter)
	{
		m_explosionEmiter->Update();
	}
	if (m_fountainEmiter)
	{
		m_fountainEmiter->Update();
	}
	if (m_debrisEmiter)
	{
		m_debrisEmiter->Update();
	}
	if (m_smokeEmiter)
	{
		m_smokeEmiter->Update();
	}
	if (m_sriEmiter)
	{
		m_sriEmiter->Update();
	}
}

void ParticleSystem::Render()
{
	if(m_fireworksEmiter)
	{
		m_fireworksEmiter->Render();
	}
	if (m_explosionEmiter)
	{
		m_explosionEmiter->Render();
	}
	if (m_fountainEmiter)
	{
		m_fountainEmiter->Render();
	}
	if (m_debrisEmiter)
	{
		m_debrisEmiter->Render();
	}
	if (m_smokeEmiter)
	{
		m_smokeEmiter->Render();
	}
	if (m_sriEmiter)
	{
		m_sriEmiter->Render();
	}
}

void ParticleSystem::ProcessInput(unsigned char key)
{
	UNUSED(key);
}
