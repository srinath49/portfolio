#pragma once

#ifndef PARTICLE_SYSTEM_H
#define PARTICLE_SYSTEM_H

class ParticleEmiter;


class ParticleSystem
{
public:
	ParticleSystem(void);
	~ParticleSystem(void);

	ParticleEmiter* m_explosionEmiter;
	ParticleEmiter* m_fireworksEmiter;
	ParticleEmiter* m_debrisEmiter;
	ParticleEmiter* m_fountainEmiter;
	ParticleEmiter* m_smokeEmiter;
	ParticleEmiter* m_sriEmiter;


	void Update();
	void Render();
	void ProcessInput(unsigned char key);
};

#endif