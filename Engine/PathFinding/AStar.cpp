#include "Engine/PathFinding/AStar.hpp"
#include "Engine/Map/Cell.hpp"
#include "Engine/Math/SriMath.hpp"
#include "Engine/Core/EngineCommons.hpp"


//************************************
// Method:    SortPathNodesInDecending
// FullName:  SortPathNodesInDecending
// Access:    public 
// Returns:   bool
// Qualifier:
// Parameter: const PathNode * first
// Parameter: const PathNode * second
//************************************
bool SortPathNodesInDecending(const PathNode* first, const PathNode* second)
{
	bool retvalue = false;
	if (first->m_fCost > second->m_fCost)
	{
		retvalue = true;
	}
	else if (first->m_fCost == second->m_fCost && first->m_hCost > second->m_hCost)
	{
		retvalue = true;
	}
	return retvalue;
}

//---------------------------------------------------------------------------------------------------
AStar::AStar(void)
{

}

//---------------------------------------------------------------------------------------------------
AStar::~AStar(void)
{

}


//---------------------------------------------------------------------------------------------------
void AStar::GetPath(Map& map, std::stack<Cell*>& outStack, const Vector2DI& startPosition, const Vector2DI& endPosition)
{
	PathNode* startNode = new PathNode;
	PathNode* endNode = new PathNode;
	PathNode* activeNode = nullptr;
	startNode->m_position = startPosition;
	endNode->m_position = endPosition;

	//bool reachedEnd = false;
	m_openList.push_back(startNode);
	while (!m_openList.empty())
	{
		m_openList.sort(&SortPathNodesInDecending);
		activeNode = m_openList.back();
		activeNode->m_gCost = map.GetCell( m_openList.back()->m_position)->GetGCost();
		m_openList.pop_back();
		m_closedList.push_back(activeNode);
		
		if (activeNode->m_position == endNode->m_position)
		{
			break;
		}
		std::vector<Cell> neighbours;
		std::vector<Cell>::iterator neighboursIter;
		PathNode* neighbourNode = nullptr;
		map.GetAdjacentCells(activeNode->m_position, neighbours);
		for (neighboursIter = neighbours.begin(); neighboursIter != neighbours.end(); ++neighboursIter)
		{
			neighbourNode = new PathNode;
			neighbourNode->m_position = neighboursIter->GetCellPositionOnMap();
			neighbourNode->m_gCost = neighboursIter->GetGCost();
			
			if (neighboursIter->GetCellType() == CT_WALL || (neighboursIter->GetCellPositionOnMap() != endPosition && neighboursIter->IsCellOccupied()))
			{
				m_closedList.push_back(neighbourNode);
				continue;
			}

			bool isInClosedList = false;
			PathNodes::iterator closedIter;
			for (closedIter = m_closedList.begin(); closedIter != m_closedList.end(); ++closedIter)
			{
				if (neighbourNode->m_position == (*closedIter)->m_position)
				{
					isInClosedList = true;
					break;
				}
			}
			if (isInClosedList)
			{
				continue;
			}
			
			bool isInOpenList = false;
			PathNodes::iterator openIter;
			for (openIter = m_openList.begin(); openIter != m_openList.end(); ++openIter)
			{
				if (neighbourNode->m_position == (*openIter)->m_position )
				{
					isInOpenList = true;
					break;
				}
			}

			//---------------------------------------------------------------------------------------------------
			float newGCostToNeighbour = activeNode->m_gCost + GetDistance(*activeNode, *neighbourNode);
			if (newGCostToNeighbour < neighbourNode->m_gCost || !isInOpenList)
			{
				neighbourNode->m_gCost = newGCostToNeighbour;
				neighbourNode->m_hCost = GetDistance(*neighbourNode, *endNode);
				neighbourNode->UpdateFCost();
				neighbourNode->m_parentNode = activeNode;
			}

			if (!isInOpenList)
			{
				m_openList.push_back(neighbourNode);
			}
		}
	}
	//---------------------------------------------------------------------------------------------------
	PathNode* traversalNode = activeNode;
	while(traversalNode)
	{
		if (traversalNode != startNode && traversalNode != endNode)
		{
			outStack.push(map.GetCell(traversalNode->m_position));
		}
		traversalNode = traversalNode->m_parentNode;
	}
	m_openList.clear();
	m_closedList.clear();
	
}

//************************************
// Method:    GetDistance
// FullName:  AStar::GetDistance
// Access:    public 
// Returns:   float
// Qualifier:
// Parameter: const PathNode & activeNode
// Parameter: const PathNode & node
//************************************ 

float AStar::GetDistance(const PathNode& activeNode, const PathNode& node)
{
	float xDistance		= (float)abs(activeNode.m_position.x - node.m_position.x);
	float yDistance		= (float)abs(activeNode.m_position.y - node.m_position.y);
	float activeGCost	= activeNode.m_gCost;
	float nodeGCost		= node.m_gCost;
	float distance = 0.f;
	
	if (xDistance > yDistance)
	{
		distance = (nodeGCost * yDistance) + (activeGCost * (xDistance - yDistance) );
	}
	else
	{
		distance = (nodeGCost * xDistance) + (activeGCost * (yDistance - xDistance) );
	}

	return distance;
}
