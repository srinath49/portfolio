#pragma once

#ifndef A_STAR_H
#define A_STAR_H
//---------------------------------------------------------------------------------------------------

#include "Engine/PathFinding/PathNode.hpp"
#include "Engine/Map/Map.hpp"
#include <stack>
 
//---------------------------------------------------------------------------------------------------
class AStar
{
public:
	AStar(void);
	~AStar(void);

	void		GetPath(Map& map, std::stack<Cell*>& outStack, const Vector2DI& startPosition, const Vector2DI& endPosition);
	PathNode	GetNodeWithLowestFValue(const PathNodes& m_openList);
	float		GetDistance(const PathNode& activeNode, const PathNode& node);
	PathNodes m_path;

	PathNodes m_closedList;
	PathNodes m_openList;
};

#endif