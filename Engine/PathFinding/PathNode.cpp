#include "PathNode.hpp"

bool PathNode::IsAtSameLocation(const PathNode& rhs)
{
	bool returnValue = false;
	if (m_position == rhs.m_position)
	{
		returnValue = true;
	}
	return returnValue;
}
