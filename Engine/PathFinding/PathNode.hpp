#pragma once

#ifndef PATH_NODE_H
#define PATH_NODE_H

#include <list>
#include "Engine/Math/SriMath.hpp"

struct PathNode;
typedef std::list<PathNode*> PathNodes;

//---------------------------------------------------------------------------------------------------
struct PathNode
{
public:
	PathNode() :
		m_gCost(0.f),
		m_hCost(0.f),
		m_fCost(0.f),
		m_parentNode(nullptr)
	{
	
	}
	~PathNode(){}

	float GetCost() const { return m_fCost; }

	bool IsAtSameLocation(const PathNode& rhs);
	

	inline void UpdateFCost() { m_fCost = m_gCost + m_hCost; }



	Vector2DI	m_position;
	float		m_gCost;
	float		m_hCost;
	float		m_fCost;
	PathNode*	m_parentNode;
};
#endif