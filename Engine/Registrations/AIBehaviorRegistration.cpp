#include "Engine/Registrations/AIBehaviorRegistration.hpp"
#include "Engine/TinyXml/tinyxml.h"


AIBehavioursMap* AIBehaviorRegistration::s_behaviorRegistrationMap = nullptr;
std::string AIBehaviorRegistration::s_pathToFileWithAllPaths = "Data/Behaviors/BehaviorsList.xml";

//---------------------------------------------------------------------------------------------------
AIBehaviorRegistration::AIBehaviorRegistration(const std::string& name, AICreateFunc& aiCreationFunc)
	:	m_name (name)
	,m_aiCreationFunc(aiCreationFunc)
{
	if(!s_behaviorRegistrationMap)
	{
		s_behaviorRegistrationMap = new AIBehavioursMap();
		s_pathToFileWithAllPaths = "Data/Behaviors/BehaviorsList.xml";
	}
	GetBehaviorFilePaths(name, m_name, m_npcBehaviorFilePath);
	TiXmlDocument(m_npcBehaviorFilePath.c_str()); 
	(*s_behaviorRegistrationMap)[name] = this;
}

//---------------------------------------------------------------------------------------------------
AIBehaviorRegistration::~AIBehaviorRegistration(void)
{

}

//---------------------------------------------------------------------------------------------------
AIBehaviorInterface* AIBehaviorRegistration::CreateAndGetAI(const std::string& name, const std::string& path)
{
	AIBehaviorInterface* result = nullptr;
	AIBehavioursMap::iterator iter = s_behaviorRegistrationMap->begin();
	while(iter != s_behaviorRegistrationMap->end())
	{
		if((*iter).first == name)
		{
			bool fileLoaded = false;
			TiXmlDocument file(path.c_str());

			file.LoadFile();
			TiXmlElement* root = nullptr;
			root = file.RootElement();
			if(!root)
			{
				
			}
			else
			{
				fileLoaded = true;
			}
			TiXmlElement* elem;
			if (fileLoaded)
			{
				TiXmlNode* node = root;
				elem = node->FirstChildElement();
			}
			file.Clear();
			result = (*iter).second->CreateAIBehaviour(elem);
		}
		++iter;
	}

	return result;
}

//---------------------------------------------------------------------------------------------------
AIBehaviorInterface* AIBehaviorRegistration::CreateAIBehaviour(TiXmlElement*& xmlElem)
{
	return (*s_behaviorRegistrationMap)[m_name]->m_aiCreationFunc(xmlElem);
}

//---------------------------------------------------------------------------------------------------
void AIBehaviorRegistration::GetBehaviorFilePaths(const std::string& name, std::string& outString, std::string& outFilePath)
{
	bool fileLoaded = false;
	TiXmlDocument m_npcBehaviorsXML(s_pathToFileWithAllPaths.c_str());

	m_npcBehaviorsXML.LoadFile();

	outString = "NoFile";
	TiXmlElement* root = nullptr;
	root = m_npcBehaviorsXML.RootElement();
	if(!root)
	{
		
	}
	else
	{
		fileLoaded = true;
	}

	if (fileLoaded)
	{
		TiXmlElement* node = nullptr;
		node = root;
		TiXmlElement* elem = node->FirstChildElement("Behavior");
		while(elem)
		{
			if(elem->Attribute("Name") == name)
			{
				outString = name;
				outFilePath = elem->Attribute("FullPath");
				break;
			} 
			elem = elem->NextSiblingElement();
		}
	}
	m_npcBehaviorsXML.Clear();
}

AIBehavioursMap* AIBehaviorRegistration::GetAIBehaviorMap()
{
	return s_behaviorRegistrationMap;
}
