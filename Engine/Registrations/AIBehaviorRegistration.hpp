#pragma once

#ifndef AI_BEHAVIOUR_REGISTRATION_H
#define AI_BEHAVIOUR_REGISTRATION_H
//---------------------------------------------------------------------------------------------------

#include <map>
#include <string>
#include <vector>

#include "Engine/Behaviors/AIBehaviorInterface.hpp"
//---------------------------------------------------------------------------------------------------

class AIBehaviorRegistration;
//---------------------------------------------------------------------------------------------------

typedef std::map<std::string, AIBehaviorRegistration*> AIBehavioursMap;
typedef AIBehaviorInterface* (AICreateFunc)(TiXmlElement*& elem);

//---------------------------------------------------------------------------------------------------
class AIBehaviorRegistration
{
public:
	AIBehaviorRegistration(const std::string& name, AICreateFunc& aiCreationFunc);
	~AIBehaviorRegistration(void);

	AIBehaviorInterface* CreateAIBehaviour(TiXmlElement*& xmlElem);

	static AIBehaviorInterface* CreateAndGetAI(const std::string& name, const std::string& path);
	static void GetBehaviorFilePaths(const std::string& name, std::string& outString, std::string& outFilePath);
	static AIBehavioursMap* GetAIBehaviorMap();

	std::string		m_name;
	std::string		m_npcBehaviorFilePath;
	AICreateFunc*	m_aiCreationFunc;

	static AIBehavioursMap* s_behaviorRegistrationMap;
	static std::string s_pathToFileWithAllPaths;
};

#endif