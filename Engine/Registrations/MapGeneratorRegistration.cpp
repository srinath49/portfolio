#include "MapGeneratorRegistration.hpp"

STDMapOfRegisteredMapGenerators* MapGeneratorRegistration::s_registeredMapGenerators = nullptr;

//---------------------------------------------------------------------------------------------------
MapGeneratorRegistration::MapGeneratorRegistration(const std::string& name, RegistrationFunc* registrationFunc):
m_name(name),
	m_registrationFunc(registrationFunc)
{
	if(!s_registeredMapGenerators)
	{
		s_registeredMapGenerators = new STDMapOfRegisteredMapGenerators();
	}
	(*s_registeredMapGenerators)[name] = this;
}

//---------------------------------------------------------------------------------------------------
MapGeneratorRegistration::~MapGeneratorRegistration(void)
{

}

//---------------------------------------------------------------------------------------------------
MapGeneratorInterface* MapGeneratorRegistration::CreateMapGenerator()
{
	return (*m_registrationFunc)(m_name);
}

//---------------------------------------------------------------------------------------------------
std::string MapGeneratorRegistration::GetName() const
{
	return m_name;
}
