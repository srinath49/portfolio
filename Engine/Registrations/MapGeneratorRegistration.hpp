#pragma once

#ifndef MAP_GENERATOR_REGISTRATION
#define MAP_GENERATOR_REGISTRATION
//---------------------------------------------------------------------------------------------------


#include <string>
#include <map>
//---------------------------------------------------------------------------------------------------

class MapGeneratorInterface;
class MapGeneratorRegistration;

typedef MapGeneratorInterface* (RegistrationFunc)(const std::string& name);
typedef std::map<std::string, MapGeneratorRegistration*> STDMapOfRegisteredMapGenerators;

//---------------------------------------------------------------------------------------------------
class MapGeneratorRegistration
{
public:
	MapGeneratorRegistration(const std::string& name, RegistrationFunc* registrationFunc);
	~MapGeneratorRegistration();

	static STDMapOfRegisteredMapGenerators* GetRegistrationsMap(){return s_registeredMapGenerators;}
	MapGeneratorInterface* CreateMapGenerator();
	std::string GetName() const;

protected:
	std::string	m_name;
	RegistrationFunc*	m_registrationFunc;
	static STDMapOfRegisteredMapGenerators* s_registeredMapGenerators;
};

#endif