#include "Engine\Rendering\FrameBuffer.hpp"
#include "Engine\Rendering\OpenGlExtentions.hpp"
#include "Engine\Core\Time.hpp"

//---------------------------------------------------------------------------------------------------
FrameBuffer::FrameBuffer(unsigned int shaderProgramId)
{
	m_shaderProgramID = shaderProgramId;
	m_debugInt = 0;
	m_topLeft = NDC_TOP_LEFT;
	m_bottomLeft = NDC_BOTTOM_LEFT;
	m_topRight = NDC_TOP_RIGHT;
	m_bottomRight = NDC_BOTTOM_RIGHT;

	//---------------------------------------------------------------------------------------------------
	SetFrameBufferTarget(FBT_FRAMEBUFFER);
	GenerateFBO();
	glUseProgram(m_shaderProgramID);
	m_depthTextureUniform = glGetUniformLocation(m_shaderProgramID, "u_depthTexture");
	m_colorTextureUniform = glGetUniformLocation(m_shaderProgramID, "u_colorTexture");
	m_controllerUniform   = glGetUniformLocation(m_shaderProgramID, "u_debugInt");
	m_matrixUniform       = glGetUniformLocation(m_shaderProgramID, "u_objectToClipMatrix");
	m_timeUniform         = glGetUniformLocation(m_shaderProgramID, "u_time");
	glUseProgram(0);
}

//---------------------------------------------------------------------------------------------------
FrameBuffer::~FrameBuffer(void)
{
	DestroyFBO();
}

//---------------------------------------------------------------------------------------------------
void FrameBuffer::GenerateFBO(unsigned int width, unsigned int height)
{
	//unsigned int attachmentIndex = 0;
	glGenFramebuffers(1, &m_fboID);
	glBindFramebuffer(m_fbTarget, m_fboID);

	GenerateColorTexture(width, height);//generate First of the 16 color textures
	GenerateDepthTexture(width, height);//generate empty texture
	
	unsigned int mipmapLevel = 0;
	
	AttachColorTextureToFrameBuffer(m_fbTarget, 0, mipmapLevel);
	AttachDepthTextureToFrameBuffer(m_fbTarget, mipmapLevel);

	glBindFramebuffer(m_fbTarget, 0);
}

//---------------------------------------------------------------------------------------------------
void FrameBuffer::DestroyFBO()
{
	glDeleteFramebuffers(1, &m_fboID);
}

//---------------------------------------------------------------------------------------------------
void FrameBuffer::BindFBO()
{
	glBindFramebuffer(m_fbTarget, m_fboID);
}

//---------------------------------------------------------------------------------------------------
void FrameBuffer::UnBindFBO()
{
	glBindFramebuffer(m_fbTarget, 0);
}

//---------------------------------------------------------------------------------------------------
void FrameBuffer::Render()
{
	//g_myRenderer->Clear(CLEAR_SCREEN_COLOR);	
	//g_myRenderer->DrawQuad(NDC_TOP_LEFT, NDC_BOTTOM_LEFT, NDC_BOTTOM_RIGHT, NDC_TOP_RIGHT);
	//g_myRenderer->DrawQuad(Vector3DF(-1.0, 1.0, 0.0), Vector3DF(-1.0, 0.0, 0.0), Vector3DF(0.0, 0.0, 0.0), Vector3DF(0.0, 1.0, 0.0));
	//g_myRenderer->DrawQuad(Vector3DF(0.0, 1.0, 0.0), Vector3DF(0.0, 0.0, 0.0), Vector3DF(1.0, 0.0, 0.0), Vector3DF(1.0, 1.0, 0.0));
	//g_myRenderer->DrawQuad(Vector3DF(-1.0, 0.0, 0.0), Vector3DF(-1.0, -1.0, 0.0), Vector3DF(0.0, -1.0, 0.0), Vector3DF(0.0, 0.0, 0.0));
	//g_myRenderer->DrawQuad(Vector3DF(0.0,  0.0, 0.0), Vector3DF(0.0, -1.0, 0.0), Vector3DF(1.0, -1.0, 0.0), Vector3DF(1.0, 0.0, 0.0));
	DrawFBOQuad();
}

//---------------------------------------------------------------------------------------------------
void FrameBuffer::GenerateColorTexture(unsigned int width, unsigned int height)
{
	glGenTextures(1, &m_colorTextureIds[0]);
	glBindTexture(GL_TEXTURE_2D, m_colorTextureIds[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
}

//---------------------------------------------------------------------------------------------------
void FrameBuffer::GenerateDepthTexture(unsigned int width, unsigned int height)
{
	glGenTextures(1, &m_depthTextureId);
	glBindTexture(GL_TEXTURE_2D, m_depthTextureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL );
}

//---------------------------------------------------------------------------------------------------
unsigned int FrameBuffer::GetColorTextureID(unsigned int textureIndex)
{
	return m_colorTextureIds[textureIndex];
}

//---------------------------------------------------------------------------------------------------
unsigned int FrameBuffer::GetDepthTextureID()
{
	return m_depthTextureId;
}

//---------------------------------------------------------------------------------------------------
void FrameBuffer::AttachColorTextureToFrameBuffer(const FrameBufferTarget& fbTarget, unsigned int attachmentIndex, int mipmapLevel)
{
	glFramebufferTexture2D(fbTarget, GL_COLOR_ATTACHMENT0 + attachmentIndex, GL_TEXTURE_2D, m_colorTextureIds[attachmentIndex], mipmapLevel);
	m_drawbuffer.push_back(GL_COLOR_ATTACHMENT0 + attachmentIndex);
}

//---------------------------------------------------------------------------------------------------
void FrameBuffer::AttachDepthTextureToFrameBuffer(const FrameBufferTarget& fbTarget, int mipmapLevel)
{
	glFramebufferTexture2D(fbTarget, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depthTextureId, mipmapLevel);
}

//---------------------------------------------------------------------------------------------------
void FrameBuffer::SetFrameBufferTarget(FrameBufferTarget fbt)
{
	m_fbTarget = fbt;
}


void FrameBuffer::ProcessInput(unsigned char key)
{
	UNUSED(key);
}

void FrameBuffer::SetQuadVertexPositions(const Vector3DF& topleft, const Vector3DF& botomleft, const Vector3DF& bottomRight, const Vector3DF& topRight)
{
	m_topLeft = topleft;
	m_bottomLeft = botomleft;
	m_topRight = topRight;
	m_bottomRight = bottomRight;
}

void FrameBuffer::DrawFBOQuad()
{
	//return;
	glUseProgram(m_shaderProgramID);

	glActiveTexture(GL_TEXTURE0 + 5);
	glBindTexture(GL_TEXTURE_2D, m_colorTextureIds[0]);
	glUniform1i(m_colorTextureUniform, 5);

	glActiveTexture(GL_TEXTURE0 + 4);
	glBindTexture(GL_TEXTURE_2D, m_depthTextureId);
	glUniform1i(m_depthTextureUniform, 4);

	glUniform1i(m_controllerUniform, m_debugInt);
	glUniformMatrix4fv(m_matrixUniform, 1, GL_FALSE, g_myRenderer->m_matrixStack.back().m_matrixElements);
	glUniform1f(m_timeUniform, (float)GetCurrentTimeSeconds());

	g_myRenderer->DrawQuad(m_topLeft, m_bottomLeft, m_bottomRight, m_topRight);
}
