#pragma once

#ifndef FRAME_BUFFER_H
#define FRAME_BUFFER_H

#include "SriOGLRenderer.hpp"
#include "Engine/Rendering/glext.h"

enum FrameBufferTarget
{
	FBT_FRAMEBUFFER		 = GL_FRAMEBUFFER,
	FBT_READ_FRAMEBUFFER = GL_READ_FRAMEBUFFER,
	FBT_DRAW_FRAMEBUFFER = GL_DRAW_FRAMEBUFFER
};

//---------------------------------------------------------------------------------------------------
class FrameBuffer
{
public:
	FrameBuffer(unsigned int shaderProgramId);
	~FrameBuffer(void);

	void GenerateFBO(unsigned int width = SCREEN_WIDTH, unsigned int height = SCREEN_HEIGHT);
	void DestroyFBO();

	void BindFBO();
	void UnBindFBO();

	void Render();

	void GenerateColorTexture(unsigned int width, unsigned int height);
	void GenerateDepthTexture(unsigned int width, unsigned int height);
	void AttachColorTextureToFrameBuffer(const FrameBufferTarget& fbTarget, unsigned int attachmentIndex, int mipmapLevel);
	void AttachDepthTextureToFrameBuffer(const FrameBufferTarget& fbTarget, int mipmapLevel);
	unsigned int GetColorTextureID(unsigned int textureIndex);
	unsigned int GetDepthTextureID();

	void SetFrameBufferTarget(FrameBufferTarget fbt);
	void ProcessInput(unsigned char key);
	void SetQuadVertexPositions(const Vector3DF& topleft, const Vector3DF& botomleft, const Vector3DF& bottomRight, const Vector3DF& topRight);
	void DrawFBOQuad();
	unsigned int m_fboID;
	unsigned int m_colorTextureIds[16];
	unsigned int m_depthTextureId;
	FrameBufferTarget m_fbTarget;
	std::vector<unsigned int> m_drawbuffer;
	unsigned int m_shaderProgramID;
	unsigned int m_colorTextureUniform;
	unsigned int m_depthTextureUniform;
	unsigned int m_controllerUniform;
	unsigned int m_matrixUniform;
	unsigned int m_timeUniform;
	int m_debugInt;

	Vector3DF m_topLeft;
	Vector3DF m_bottomLeft;
	Vector3DF m_topRight;
	Vector3DF m_bottomRight;
};

#endif