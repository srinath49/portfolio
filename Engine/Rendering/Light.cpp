#include "Light.hpp"


Light::Light(const Vector3DF& position, const RgbaColors& lightColor, float innerApertureAngleDegrees, float outerApertureAngleDegrees, float innerRadius, float outerRadius, float ambiance)
{
	SetApertureInnerDot(innerApertureAngleDegrees);
	SetApertureOuterDot(outerApertureAngleDegrees);

	m_outerRadius = innerRadius;
	m_innerRadius = outerRadius;

	m_position = position;
	m_originalPosition = position;
	m_lightColor = lightColor;
	m_ambiance = 0.2f;
	UNUSED(ambiance);
}


Light::~Light(void)
{

}

void Light::SetApertureInnerDot(float innerApertureAngleDegrees)
{
	float angleRadians = DegreesToRadians(innerApertureAngleDegrees);
	m_innerPenumbraDot = cosf(angleRadians);
}

void Light::SetApertureOuterDot(float outerApertureAngleDegrees)
{
	float angleRadians = DegreesToRadians(outerApertureAngleDegrees);
	m_outerPenumbraDot = cosf(angleRadians);
}

