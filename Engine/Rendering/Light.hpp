#pragma once
#ifndef LIGHT_H
#define LIGHT_H
//---------------------------------------------------------------------------------------------------

#include "Engine\Core\RgbaColors.hpp"
#include "Engine\Core\EngineCommons.hpp"
#include "Engine\Math\SriMath.hpp"

//---------------------------------------------------------------------------------------------------
class Light
{
public:
	Light(const Vector3DF& position = Vector3DF(), const RgbaColors& lightColor = RgbaColors(), float innerApertureAngleDegrees = 0.f, float outerApertureAngleDegrees = 0.f, float innerRadius = REALLY_FAR, float outerRadius = REALLY_FAR + 1.f, float ambiance = 0.f);
	~Light(void);

	void SetApertureInnerDot(float innerApertureAngleDegrees);
	void SetApertureOuterDot(float outerApertureAngleDegrees);


	float		m_innerPenumbraDot;
	float		m_outerPenumbraDot;

	float		m_outerRadius;
	float		m_innerRadius;
	float		m_ambiance;
	Vector3DF	m_forwardVector;
	RgbaColors		m_lightColor;
	Vector3DF	m_position;
	Vector3DF	m_originalPosition;
};
#endif