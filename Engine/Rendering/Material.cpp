#include "Material.hpp"
#include "Engine\Rendering\OpenGlExtentions.hpp"
#include "Engine\Rendering\SpriteSheet.hpp"
#include "Engine\Rendering\Texture.hpp"
#include "Engine\Rendering\SriOGLRenderer.hpp"
#include "Engine\Core\Time.hpp"
#include "Engine\Core\GraphicsDebugManager.hpp"
#include "Engine\Core\Camera3D.hpp"
#include "Engine\Input\InputSystem.hpp"
#include "..\Math\SriMath.hpp"
#include "..\FileIO\FileSystem.hpp"

//---------------------------------------------------------------------------------------------------
GraphicsDebugManager g_debugManager;
int g_debugInt = 0;
float g_effectStartTime = 0.0f;

Material::Material(unsigned int shaderProgramId, const std::vector<std::string>& textureNames)
{
	m_shaderProgramId = shaderProgramId;

	m_diffuseSprite  = nullptr;
	m_normalSprite   = nullptr;
	m_specularSprite = nullptr;
	m_emissiveSprite = nullptr;

	SetTextures(textureNames);
	
	g_myRenderer->BindAllTextures(m_diffuseSprite, m_normalSprite, m_specularSprite, m_emissiveSprite);
	m_fogColorAndIntensity = RgbaColors(FOG_COLOR);
	m_fogMinDistance = REALLY_FAR;
	m_fogMaxDistance = REALLY_FAR + 125.f;
	ActivateShaderProgram();
}


Material::~Material(void)
{
	delete m_diffuseSprite; 
	delete m_normalSprite;
	delete m_specularSprite;
	delete m_emissiveSprite;
}

//---------------------------------------------------------------------------------------------------
void Material::ActivateShaderProgram()
{
	glUseProgram(m_shaderProgramId);
	GetAttribLocations();
	GetUniformLocations();
}

//---------------------------------------------------------------------------------------------------
void Material::GetAttribLocations()
{
	m_attribLocations.m_vertexLocation	   = glGetAttribLocation(m_shaderProgramId, "a_vertex");
	m_attribLocations.m_colorLocation	   = glGetAttribLocation(m_shaderProgramId, "a_color");
	m_attribLocations.m_normalLocation	   = glGetAttribLocation(m_shaderProgramId, "a_normal");
	m_attribLocations.m_texCoordsLocation  = glGetAttribLocation(m_shaderProgramId, "a_texCoords");
	m_attribLocations.m_tangentsLocation   = glGetAttribLocation(m_shaderProgramId, "a_tangent");
	m_attribLocations.m_bitangentsLocation = glGetAttribLocation(m_shaderProgramId, "a_bitangent");
}

//---------------------------------------------------------------------------------------------------
void Material::GetUniformLocations()
{
	m_uniformLocations.m_diffuseUniform					=	glGetUniformLocation(m_shaderProgramId, "u_diffuseTexture");
	m_uniformLocations.m_normalUniform					=	glGetUniformLocation(m_shaderProgramId, "u_normalTexture");
	m_uniformLocations.m_specularUniform				=	glGetUniformLocation(m_shaderProgramId, "u_specularTexture");
	m_uniformLocations.m_emissiveUniform				=	glGetUniformLocation(m_shaderProgramId, "u_emissiveTexture");
	m_uniformLocations.m_timeUniform					=	glGetUniformLocation(m_shaderProgramId, "u_time");
	m_uniformLocations.m_effectStartTimeUniform			=	glGetUniformLocation(m_shaderProgramId, "u_effectStartTime");
	m_uniformLocations.m_worldToClipMatrixUniform		=	glGetUniformLocation(m_shaderProgramId, "u_worldToClipMatrix");
	m_uniformLocations.m_cameraPositionUniform			=	glGetUniformLocation(m_shaderProgramId, "u_cameraPosition");
	m_uniformLocations.m_fogColorAndIntensityUniform	=	glGetUniformLocation(m_shaderProgramId, "u_fogColorAndIntensity");
	m_uniformLocations.m_fogMinDistanceUniform			=	glGetUniformLocation(m_shaderProgramId, "u_fogMinDistance");
	m_uniformLocations.m_fogMaxDistanceUniform			=	glGetUniformLocation(m_shaderProgramId, "u_fogMaxDistance");

	m_uniformLocations.m_debugIntUniform				=	glGetUniformLocation(m_shaderProgramId, "u_debugInt");
	m_uniformLocations.m_lightWorldPositionsUniform		=	glGetUniformLocation(m_shaderProgramId, "u_lightWorldPositions");
	m_uniformLocations.m_lightColorsUniform				=   glGetUniformLocation(m_shaderProgramId, "u_lightColors");
	m_uniformLocations.m_innerRadiiUniform				=   glGetUniformLocation(m_shaderProgramId, "u_lightInnerRadii");
	m_uniformLocations.m_outerRadiiUniform				=   glGetUniformLocation(m_shaderProgramId, "u_lightOuterRadii");
	m_uniformLocations.m_innerDotsUniform				=   glGetUniformLocation(m_shaderProgramId, "u_lightInnerPenumbraDots");
	m_uniformLocations.m_outerDotsUniform				=   glGetUniformLocation(m_shaderProgramId, "u_lightOuterPenumbraDots");
	m_uniformLocations.m_ambiancesUniform				=   glGetUniformLocation(m_shaderProgramId, "u_lightAmbiences");
	m_uniformLocations.m_forwardVectorsUniform   		=   glGetUniformLocation(m_shaderProgramId, "u_lightForwardVectors");
	m_uniformLocations.m_numLightsToProcessUniform		=   glGetUniformLocation(m_shaderProgramId, "u_numLightsToProcess");

	m_uniformLocations.m_tangentToWorldMatrixUniform	=	glGetUniformLocation(m_shaderProgramId, "u_tangentToWorldMatrix");
}

//---------------------------------------------------------------------------------------------------
void Material::SetupMaterial(const Vector3DF& cameraPos)
{
	glUseProgram(m_shaderProgramId);
	m_cameraPosition = cameraPos;
	SetUniforms();
	g_myRenderer->BindAllTextures(m_diffuseSprite, m_normalSprite, m_specularSprite, m_emissiveSprite);
	g_myRenderer->SetCurrentShaderProgramID(m_shaderProgramId);
}

//---------------------------------------------------------------------------------------------------
void Material::SetupMaterial()
{
	glUseProgram(m_shaderProgramId);
	SetUniforms();
	g_myRenderer->BindAllTextures(m_diffuseSprite, m_normalSprite, m_specularSprite, m_emissiveSprite);
	g_myRenderer->SetCurrentShaderProgramID(m_shaderProgramId);
}

//---------------------------------------------------------------------------------------------------
void Material::SetDiffuseSprite (const std::string& newDiffuseSprite) 
{ 
	//LoadSTBIFileToBuffer(newDiffuseSprite, x, y, numComps, reqComps);
	Texture* texture = nullptr;
	if (newDiffuseSprite.compare("") != 0)
	{
		bool fileExists = true;
		FILE* fileToTest;
		fopen_s(&fileToTest, newDiffuseSprite.c_str(), "rb");
		if (fileToTest == NULL)
		{
			fileExists = false;
		}

		if (fileExists)
		{
			texture = Texture::CreateOrGetTexture(newDiffuseSprite);

			if (texture)
			{
				m_diffuseSprite = new SpriteSheet();
				m_diffuseSprite->m_texture = texture;
			}
			else
			{
				texture = Texture::CreateDefaultDiffuseTexture();
				m_diffuseSprite = new SpriteSheet();
				m_diffuseSprite->m_texture = texture;
			}
		}
		else
		{
			texture = Texture::CreateDefaultDiffuseTexture();
			m_diffuseSprite = new SpriteSheet();
			m_diffuseSprite->m_texture = texture;
		}
		
	}
	else
	{
		texture = Texture::CreateDefaultDiffuseTexture();
		m_diffuseSprite = new SpriteSheet(); 
		m_diffuseSprite->m_texture = texture;
	}
}

//---------------------------------------------------------------------------------------------------
void Material::SetNormalSprite  (const std::string& newNormalSprite) 
{
	bool fileExists = true;
	FILE* fileToTest;
	fopen_s(&fileToTest, newNormalSprite.c_str(), "rb");
	if (fileToTest == NULL)
	{
		fileExists = false;
	}

	if (fileExists)
	{
		fclose(fileToTest);
		m_normalSprite = new SpriteSheet(newNormalSprite); 
	}
	else
	{
		Texture* texture = Texture::CreateDefaultNormalTexture();
		m_normalSprite = new SpriteSheet(); 
		m_normalSprite->m_texture = texture;
	}
}

//---------------------------------------------------------------------------------------------------
void Material::SetSpecularSprite(const std::string& newSpecularSprite) 
{ 
	bool fileExists = true;
	FILE* fileToTest;
	fopen_s(&fileToTest, newSpecularSprite.c_str(), "rb");
	if (fileToTest == NULL)
	{
		fileExists = false;
	}

	if (fileExists)
	{
		fclose(fileToTest);
		m_specularSprite = new SpriteSheet(newSpecularSprite); 
	}
	else
	{
		Texture* texture = Texture::CreateDefaultSpecularTexture();
		m_specularSprite = new SpriteSheet(); 
		m_specularSprite->m_texture = texture;
	}
}

//---------------------------------------------------------------------------------------------------
void Material::SetEmissiveSprite(const std::string& newEmissiveSprite) 
{ 
	bool fileExists = true;
	FILE* fileToTest;
	fopen_s(&fileToTest, newEmissiveSprite.c_str(), "rb");
	if (fileToTest == NULL)
	{
		fileExists = false;
	}

	if (fileExists)
	{
		fclose(fileToTest);
		m_emissiveSprite = new SpriteSheet(newEmissiveSprite);
	}
	else
	{
		Texture* texture = Texture::CreateDefaultSpecularTexture();
		m_emissiveSprite = new SpriteSheet();
		m_emissiveSprite->m_texture = texture;
	}
	//m_emissiveSprite = new SpriteSheet(newEmissiveSprite); 
}

//---------------------------------------------------------------------------------------------------
void Material::SetUniforms()
{
	glUniform1f(m_uniformLocations.m_timeUniform,	(float)Time::GetAbsoluteTimeSeconds());
	glUniform1f(m_uniformLocations.m_effectStartTimeUniform, g_effectStartTime);

	glUniformMatrix4fv(m_uniformLocations.m_worldToClipMatrixUniform, 1, GL_FALSE, g_myRenderer->m_matrixStack.back().m_matrixElements);
	glUniformMatrix4fv(m_uniformLocations.m_tangentToWorldMatrixUniform, 1, GL_FALSE, Matrix4x4F::IDENTITY.m_matrixElements);
	glUniform1i(m_uniformLocations.m_diffuseUniform,  0);
	glUniform1i(m_uniformLocations.m_normalUniform,   1);
	glUniform1i(m_uniformLocations.m_specularUniform, 2);
	glUniform1i(m_uniformLocations.m_emissiveUniform, 3);
	glUniform1i(m_uniformLocations.m_debugIntUniform, g_debugInt);
	glUniform3f(m_uniformLocations.m_cameraPositionUniform, m_cameraPosition.x, m_cameraPosition.y, m_cameraPosition.z);

	glUniform1f(m_uniformLocations.m_fogMinDistanceUniform, m_fogMinDistance);
	glUniform1f(m_uniformLocations.m_fogMaxDistanceUniform, m_fogMaxDistance);
	glUniform4f(m_uniformLocations.m_fogColorAndIntensityUniform, m_fogColorAndIntensity.r, m_fogColorAndIntensity.g, m_fogColorAndIntensity.b, m_fogColorAndIntensity.a);
}

void Material::ProcessInput(InputSystem* inputSystem)
{
	UNUSED(inputSystem);
}

//---------------------------------------------------------------------------------------------------
void Material::SetTextures(const std::vector<std::string>& textures)
{
	if (textures.size() > 0)
	{
		SetDiffuseSprite(textures[0]);
	}
	else
	{
		SetDiffuseSprite("");
	}
	if (textures.size() > 1)
	{
		SetNormalSprite(textures[1]);
	}
	else
	{
		SetNormalSprite("");
	}
	if (textures.size() > 2)
	{
		SetSpecularSprite(textures[2]);
	}
	else
	{
		SetSpecularSprite("");
	}
	if (textures.size() > 3)
	{
		SetEmissiveSprite(textures[3]);
	}
	else
	{
		SetEmissiveSprite("");
	}
}
