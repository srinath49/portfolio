#pragma once

#ifndef MATERIAL_H
#define MATERIAL_H

#include <vector>
#include <string>
#include "Engine\Math\SriMath.hpp"
#include "Engine\Core\RgbaColors.hpp"

class SpriteSheet;
class TheGame;
class InputSystem;

//---------------------------------------------------------------------------------------------------
struct ShaderAttribLocations
{
	unsigned int m_vertexLocation;
	unsigned int m_colorLocation;
	unsigned int m_texCoordsLocation;
	unsigned int m_normalLocation;
	unsigned int m_tangentsLocation;
	unsigned int m_bitangentsLocation;
};

//---------------------------------------------------------------------------------------------------
struct ShaderUniformLocations
{
	unsigned int m_diffuseUniform;
	unsigned int m_normalUniform;
	unsigned int m_specularUniform;
	unsigned int m_emissiveUniform;
	unsigned int m_timeUniform;
	unsigned int m_effectStartTimeUniform;
	unsigned int m_worldToClipMatrixUniform;
	unsigned int m_cameraPositionUniform;
	unsigned int m_fogMinDistanceUniform;
	unsigned int m_fogMaxDistanceUniform;
	unsigned int m_fogColorAndIntensityUniform;
	unsigned int m_debugIntUniform;

	unsigned int m_lightWorldPositionsUniform;
	unsigned int m_lightColorsUniform;
	unsigned int m_innerRadiiUniform;
	unsigned int m_outerRadiiUniform;
	unsigned int m_innerDotsUniform;
	unsigned int m_outerDotsUniform;
	unsigned int m_ambiancesUniform;
	unsigned int m_forwardVectorsUniform;
	unsigned int m_numLightsToProcessUniform;

	unsigned int m_tangentToWorldMatrixUniform;
};

//---------------------------------------------------------------------------------------------------
class Material
{
public:
	Material(unsigned int shaderProgramId, const std::vector<std::string>& textureNames);
	~Material(void);

	void ActivateShaderProgram();
	void GetAttribLocations();
	void GetUniformLocations();
	void SetupMaterial(const Vector3DF& cameraPos);
	void SetupMaterial();

	void SetDiffuseSprite (const std::string& newDiffuseSprite); 
	void SetNormalSprite  (const std::string& newNormalSprite);  
	void SetSpecularSprite(const std::string& newSpecularSprite);
	void SetEmissiveSprite(const std::string& newEmmisiveSprite);
	void SetUniforms();
	void ProcessInput(InputSystem* inputSystem);
	void SetTextures(const std::vector<std::string>& textures);

public:
	SpriteSheet*			m_diffuseSprite;
	SpriteSheet*			m_normalSprite;
	SpriteSheet*			m_specularSprite;
	SpriteSheet*			m_emissiveSprite;
	int						m_shaderProgramId;

	ShaderAttribLocations	m_attribLocations;
	ShaderUniformLocations	m_uniformLocations;
	Vector3DF				m_currentLightPosition;
	Vector3DF				m_cameraPosition;
	RgbaColors					m_fogColorAndIntensity;
	TheGame*				m_theGame;
	float					m_fogMinDistance;
	float					m_fogMaxDistance;

};
#endif