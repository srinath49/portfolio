#pragma once

#ifndef MODEL_VERTEX_H
#define MODEL_VERTEX_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Math/SriMath.hpp"
#include "Engine/Core/RGBA.hpp"

//---------------------------------------------------------------------------------------------------
struct ModelVertex
{
	ModelVertex()
	{

	}

	Vector3DF	m_position;
	RGBA		m_color;
	Vector2DF	m_texCoords;
	Vector3DF	m_tangent;
	Vector3DF	m_bitangent;
	Vector3DF	m_normal;
};

#endif