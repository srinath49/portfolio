#include "MyModel.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/Core/MyMesh.hpp"
#include "Engine/FileIO/OBJLoader.hpp"
#include "Engine/FileIO/BinaryBufferParser.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/Rendering/SriOGLRenderer.hpp"

//---------------------------------------------------------------------------------------------------
MyModel::MyModel() :
	m_mesh(nullptr),
	m_meshLoader(nullptr),
	m_material(nullptr),
	m_bufferSize(0),
	m_vboId(0),
	m_iboId(0)
{
	m_mesh = new MyMesh();
}

//---------------------------------------------------------------------------------------------------
MyModel::~MyModel(void)
{
	delete m_mesh;
	delete m_meshLoader;
}

//---------------------------------------------------------------------------------------------------
void MyModel::SetMaterial(Material* material)
{
	m_material = material;
}

//---------------------------------------------------------------------------------------------------
void MyModel::Render(unsigned int drawMode)
{
	g_myRenderer->DrawVBO(m_vboId, m_iboId, m_bufferSize, drawMode);
}

//---------------------------------------------------------------------------------------------------
void MyModel::SetUpMyMaterial(const Vector3DF& cameraPos)
{
	if (m_material)
	{
		m_material->SetupMaterial(cameraPos);
	}
}

void MyModel::SetMesh(const std::string& meshPath)
{
	m_meshPath = meshPath;
	m_mesh->SetMeshFilePath(m_meshPath);
}

//---------------------------------------------------------------------------------------------------
void MyModel::BuildVBO()
{
	g_myRenderer->GenerateVertexBuffer(m_mesh->m_meshVertexes, &m_vboId);
	m_bufferSize = g_myRenderer->GenerateIndexBuffer(m_mesh->m_meshIndexes, &m_iboId);
	m_mesh->m_meshVertexes.clear();
	m_mesh->m_meshIndexes.clear();
}

//---------------------------------------------------------------------------------------------------
void MyModel::LoadModel(std::string& fileToLoad, bool isBinaryMode /*= true*/)
{
	m_mesh->m_meshVertexes.clear();
	m_mesh->m_meshIndexes.clear();
	if (isBinaryMode)
	{
		BinaryBufferParser bbp;
		bbp.LoadBufferFromBinaryFile(fileToLoad+".C23");
		bbp.ParseBinaryBufferToVertexData(m_mesh);
		std::vector<std::string> textures;
		textures.push_back(fileToLoad + "_Diffuse.png");
		textures.push_back(fileToLoad + "_Normal.png");
		textures.push_back(fileToLoad + "_SpecGlossEmit.png");
		m_material->SetTextures(textures);
		BuildVBO();
	}
	else
	{
		fileToLoad += ".OBJ";
		OBJLoader loader;
		loader.LoadFile(fileToLoad);
		loader.ParseVertexDataFromOBJFile(m_mesh);
		BuildVBO();
	}
}
