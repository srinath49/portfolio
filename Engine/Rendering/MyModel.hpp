#pragma once
#ifndef MESH_VERTEX_DATA_H
#define MESH_VERTEX_DATA_H
//---------------------------------------------------------------------------------------------------
#include "Engine/Core/ModelVertex.hpp"

#include <vector>
//---------------------------------------------------------------------------------------------------

class MyMesh;
class FileLoader;
class Material;

//---------------------------------------------------------------------------------------------------
class MyModel
{
public:
	MyModel();
	~MyModel(void);

	void SetMaterial(Material* material);
	void Render(unsigned int drawMode);
	void SetUpMyMaterial(const Vector3DF& cameraPos);
	void SetMesh(const std::string& meshPath);
	void BuildVBO();
	void LoadModel(std::string& fileToLoad, bool isBinaryMode = true);

	unsigned int m_vboId;
	unsigned int m_iboId;
	unsigned int m_bufferSize;

	MyMesh*		m_mesh;
	Material*	m_material;
	FileLoader*	m_meshLoader;
	std::string m_meshPath;
};

#endif