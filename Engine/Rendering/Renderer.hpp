#pragma once

#ifndef RENDERER_H
#define RENDERER_H

#include <vector>
#include "Engine\Core\std_image.h"
#include "Engine\Math\SriMath.hpp"
#include "Engine\Core\VertexData.hpp"
#include "Engine\Rendering\SpriteSheet.hpp"
#include "Engine\Core\EngineCommons.hpp"
#include "Engine\Core\ModelVertex.hpp"

#define UNUSED(x) (void)(x);
//---------------------------------------------------------------------------------------------------
class Vector2DF;
class Vector2DI;
class Vector3DI;
class Rect;

//---------------------------------------------------------------------------------------------------
class Renderer
{
public:
	Renderer(void){}
	virtual ~Renderer(void) { }

public:
	virtual void Initialize(void* platformHandle) 
	{
		UNUSED(platformHandle);
	}

	virtual void Clear(const RgbaColors& clearColor = RgbaColors(), unsigned int clearDepth = 1.f) 
	{
		UNUSED(clearColor);
		UNUSED(clearDepth);
	}

	virtual void SetUpPerspectiveProjection(float fieldOfViewY, float aspect, float zNearClipping, float zFarClipping) 
	{ 
		UNUSED(fieldOfViewY);
		UNUSED(aspect);
		UNUSED(zNearClipping);
		UNUSED(zFarClipping);
	}

	virtual void PushMatrix() { }
	virtual void PopMatrix() { }
	virtual void SetLineWidth(float width) { UNUSED(width); }
	virtual void Rotate(float angle, float rotationOnX, float rotationOnY, float rotationOnZ) 
	{
		UNUSED(angle);
		UNUSED(rotationOnX);
		UNUSED(rotationOnY);
		UNUSED(rotationOnZ);
	}

	virtual void Translate(float translationOnX, float translationOnY, float translationOnZ) 
	{
		UNUSED(translationOnX);
		UNUSED(translationOnY);
		UNUSED(translationOnZ);
	}

	virtual void SetColor(const RgbaColors& colorToSet)
	{
		UNUSED(colorToSet);
	}

	virtual void LoadIdentityMatrix() 
	{
	
	}

	virtual void GenerateTexture(unsigned int& sizeX, unsigned int& sizeY, unsigned int& textureID, const std::string& imageFilePath) 
	{
		UNUSED(sizeX);
		UNUSED(sizeY);
		UNUSED(textureID);
		UNUSED(imageFilePath);
	}

	void GenerateDefaultDiffuseTexture(unsigned int& sizeX, unsigned int& sizeY, unsigned int& textureID)
	{
		UNUSED(sizeX);
		UNUSED(sizeY);
		UNUSED(textureID);
	}

	virtual void LoadOrtho(float left, float right, float bottom, float top, float nearClippingPlane = 0.0, float farClippingPlane = 1.0) 
	{
		UNUSED(left);
		UNUSED(right);
		UNUSED(bottom);
		UNUSED(top);
		UNUSED(nearClippingPlane);
		UNUSED(farClippingPlane);
	}

	virtual void UnloadOrtho() { }

	virtual void DrawAxis(float xAxisLength = 1.f, float lineWidth = 1.f) 
	{
		UNUSED(xAxisLength);
		UNUSED(lineWidth);
	}

	virtual void DrawPoint(const Vector3DF& point, const RgbaColors& pointColor = RgbaColors(1.f, 1.f, 1.f, 1.f), float pointSize = 1.f) 
	{
		UNUSED(point);
		UNUSED(pointColor);
		UNUSED(pointSize);
	}

	virtual void DrawPoints(const std::vector<Vector3DF>& points, const RgbaColors& pointColor = RgbaColors(1.f, 1.f, 1.f, 1.f), float pointSize = 1.f) 
	{
		UNUSED(points);
		UNUSED(pointColor);
		UNUSED(pointSize);
	}
	 
	virtual void DrawLine(const Vector3DF& startPoint, const Vector3DF& endPoint, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f), float lineWidth = 1.f) 
	{
		UNUSED(startPoint);
		UNUSED(endPoint);
		UNUSED(lineColor);
		UNUSED(lineWidth);
	}

	virtual void DrawLineSegment(const Vector3DF& startPoint, const Vector3DF& endPoint, const RgbaColors& lineStartColor, const RgbaColors& lineEndColor, float lineWidth = 1.f) 
	{
		UNUSED(startPoint);
		UNUSED(endPoint);
		UNUSED(lineStartColor);
		UNUSED(lineEndColor);
		UNUSED(lineWidth);
	}

	virtual void DrawLines(const std::vector<Vector3DF>& positions, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f), float lineWidth = 1.f) 
	{
		UNUSED(positions);
		UNUSED(lineColor);
		UNUSED(lineWidth);
	}

	virtual void DrawSquareOutline(const Vector3DF& topLeft, const Vector3DF& bottomleft, const Vector3DF& bottomRight, const Vector3DF& topRight, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f), float lineWidth = 1.f) 
	{
		UNUSED(topLeft);
		UNUSED(bottomleft);
		UNUSED(bottomRight);
		UNUSED(topRight);
		UNUSED(lineColor);
		UNUSED(lineWidth);
	}

	virtual void DrawTexturedQuad(const Vector3DF& leftTop, const Vector3DF& leftBottom, const Vector3DF& rightBottom, const Vector3DF& rightTop, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f), SpriteSheet* sprite = nullptr, int spriteFrame = 0, const TBN& tbn = TBN()) 
	{
		UNUSED(leftTop);
		UNUSED(leftBottom);
		UNUSED(rightBottom);
		UNUSED(rightTop);
		UNUSED(lineColor);
		UNUSED(sprite);
		UNUSED(spriteFrame);
		UNUSED(tbn);
	}

	virtual void DrawTexturedCube(const Vector3DF& cubeCenter, float lengthOfEachSide, const RgbaColors& color = RgbaColors(1.f, 1.f, 1.f, 1.f), SpriteSheet* diffuseTexture = nullptr, bool drawDebugOutline = false )
	{
		UNUSED(cubeCenter);
		UNUSED(lengthOfEachSide);
		UNUSED(color);
		UNUSED(diffuseTexture);
		UNUSED(drawDebugOutline);
	}

	virtual void DrawTexturedSquare(const Vector3DF& startPoint, float lengthOfEachSide, SpriteSheet* sprite = nullptr, int spriteFrame = 0, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f)) 
	{
		UNUSED(startPoint);
		UNUSED(lengthOfEachSide);
		UNUSED(sprite);
		UNUSED(spriteFrame);
		UNUSED(lineColor);	
	}

	virtual void DrawTexturedRect2D(const Vector2DF& leftTop, const Vector2DF& leftBottom, const Vector2DF& rightBottom, const Vector2DF& rightTop, SpriteSheet* sprite = nullptr, int spriteFrame = 0, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f)) 
	{
		UNUSED(leftTop);
		UNUSED(leftBottom);
		UNUSED(rightBottom);
		UNUSED(rightTop);
		UNUSED(sprite);
		UNUSED(spriteFrame);
		UNUSED(lineColor);
	}

	virtual int GenerateVertexBuffer(const std::vector<ModelVertex>& vertexes, unsigned int* bufferId) 
	{
		UNUSED(vertexes);
		UNUSED(bufferId);

		return 0;
	}

	virtual int GenerateIndexBuffer(const std::vector<unsigned int>& indexes, unsigned int* bufferId) 
	{
		UNUSED(indexes);
		UNUSED(bufferId);

		return 0;
	}

	virtual void DrawVBO(unsigned int vboId, unsigned int iboId, unsigned int bufferSize, unsigned int mode, bool hasValidIBO = true) 
	{
		UNUSED(vboId);
		UNUSED(iboId);
		UNUSED(bufferSize);	
		UNUSED(mode);
		UNUSED(hasValidIBO);
	}

	virtual void DeleteBuffers(unsigned int* bufferID) 
	{
		UNUSED(bufferID);
	}


	virtual void EnableDepthBuffer()  { }
	virtual void DisableDepthBuffer() { }

	virtual int LoadShader(const char* shaderFilePath, ShaderType type)
	{
		UNUSED(shaderFilePath);
		UNUSED(type);

		return -1;
	}

	virtual void BindAllTextures(SpriteSheet* diffuseTexture, SpriteSheet* normalTexture, SpriteSheet* specularTexture, SpriteSheet* emmisiveTexture)
	{
		UNUSED(diffuseTexture);
		UNUSED(normalTexture);
		UNUSED(specularTexture);
		UNUSED(emmisiveTexture);
	}

	virtual void DrawSolidSphere(float radius, unsigned int numRings, unsigned int sectors, const Vector3DF& center)
	{
		UNUSED(radius);
		UNUSED(numRings);
		UNUSED(sectors);
		UNUSED(center);
	}

};

#endif