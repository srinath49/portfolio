#include "Engine\Rendering\SpriteSheet.hpp"
#include "Engine\Rendering\Texture.hpp"
#include "Engine\Core\Time.hpp"
#include "Engine\Rendering\SriOGLRenderer.hpp"

//---------------------------------------------------------------------------------------------------
SpriteSheet::SpriteSheet(void) :
	m_tilesWide(1),
	m_tilesTall(1),
	m_oneOverTilesWide(1.f),
	m_oneOverTilesTall(1.f),
	m_animSecondsPerFrame(0.5f),
	m_totalFrames(1),
	m_currentFrame(0),
	m_isLastFrame(false)
{
	m_myRenderer = new SriOGLRenderer();
}


//---------------------------------------------------------------------------------------------------
SpriteSheet::SpriteSheet(const std::string& textureFileName, const Vector2DI& tileDimensions) :
	m_tilesWide(tileDimensions.x),
	m_tilesTall(tileDimensions.y),
	m_oneOverTilesWide(1.f/m_tilesWide),
	m_oneOverTilesTall(1.f/m_tilesTall),
	m_animSecondsPerFrame(0.5f),
	m_totalFrames(m_tilesWide*m_tilesTall),
	m_currentFrame(0),
	m_isLastFrame(false)
{
	m_texture = Texture::CreateOrGetTexture(textureFileName);
	m_textureFilePath = textureFileName;
	m_time = Time::GetAbsoluteTimeSeconds();
	
	//m_minTexCoords = GetMinTextureCoordsForFrame(m_currentFrame);
	//m_maxTexCoords = GetMaxTextureCoordsForFrame();

}


//---------------------------------------------------------------------------------------------------
SpriteSheet::~SpriteSheet(void)
{

}


//---------------------------------------------------------------------------------------------------
Vector2DF SpriteSheet::GetMinTextureCoordsForFrame(const int frameNumber)
{
	Vector2DF returnVector;
	returnVector.x = ((float)(frameNumber%m_tilesWide))*m_oneOverTilesWide;
	returnVector.y = ((float)(frameNumber/m_tilesWide))*m_oneOverTilesTall;

	return returnVector;
}


//---------------------------------------------------------------------------------------------------
Vector2DF SpriteSheet::GetMaxTextureCoordsForFrame()
{
	Vector2DF returnVector;

	returnVector.x = m_minTexCoords.x + m_oneOverTilesWide;
	returnVector.y = m_minTexCoords.y + m_oneOverTilesTall;

	return returnVector;
}


//---------------------------------------------------------------------------------------------------
Vector2DF SpriteSheet::GetMaxTextureCoordsFromMinCoords(const Vector2DF& minTexCoords)
{
	Vector2DF returnVector;

	returnVector.x = minTexCoords.x + m_oneOverTilesWide;
	returnVector.y = minTexCoords.y + m_oneOverTilesTall;

	return returnVector;
}


//---------------------------------------------------------------------------------------------------
Texture* SpriteSheet::GetCurrentTexture()
{
	return m_texture;
}


//---------------------------------------------------------------------------------------------------
void SpriteSheet::Update()
{
	if(m_isLastFrame)
	{
		m_currentFrame = 0;
		m_isLastFrame = false;
		return;
	}
	double currentTime = Time::GetAbsoluteTimeSeconds();
	if(currentTime - m_time >= m_animSecondsPerFrame)
	{
		m_currentFrame++;
		if(m_currentFrame >= m_totalFrames)
		{
			m_isLastFrame = true;
		}
		m_time = currentTime;
	}
}


//---------------------------------------------------------------------------------------------------
void SpriteSheet::Draw(float drawRadius)
{
	m_minTexCoords = GetMinTextureCoordsForFrame(m_currentFrame);
	m_maxTexCoords = GetMaxTextureCoordsForFrame();
	
	m_myRenderer->DrawTexturedSquare(Vector3DF(-drawRadius, -drawRadius, 0.f), 2*drawRadius, this, m_currentFrame);
}


//---------------------------------------------------------------------------------------------------
void SpriteSheet::DrawFrame(int frameToDraw, float drawRadius)
{
	m_currentFrame = frameToDraw;
	Draw(drawRadius);
}

void SpriteSheet::DrawWithTextureCoords()
{

}
