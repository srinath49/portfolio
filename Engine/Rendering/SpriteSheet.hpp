#pragma once
#include <string>
#include "Engine\Math\SriMath.hpp"

class Texture;
class Renderer;

//---------------------------------------------------------------------------------------------------
class SpriteSheet
{
public:
	SpriteSheet(void);
	SpriteSheet(const std::string& textureFileName, const Vector2DI& tileDimensions = Vector2DI(1, 1));
	~SpriteSheet(void);

	Vector2DF GetMinTextureCoordsForFrame(const int frameNumber);
	Vector2DF GetMaxTextureCoordsForFrame();
	Vector2DF GetMaxTextureCoordsFromMinCoords(const Vector2DF& minTexCoords);
	Texture* GetCurrentTexture();

	void Update();
	void Draw(float drawRadius);
	void DrawFrame(int frameToDraw, float drawRadius);
	void DrawWithTextureCoords();

public:
	Vector2DF m_minTexCoords;
	Vector2DF m_maxTexCoords;
	int m_tilesWide;
	int m_tilesTall;
	int m_totalFrames;
	Texture* m_texture;
	float m_animSecondsPerFrame;
	int m_currentFrame;
	double m_time;
	bool m_isLastFrame;
	float m_oneOverTilesWide;
	float m_oneOverTilesTall;

	Renderer* m_myRenderer;
	std::string m_textureFilePath;
};

