#include "SriOGLRenderer.hpp"
//#include "MemoryManager/Memory/MemoryOverride.hpp"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

//---------------------------------------------------------------------------------------------------
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "Engine/Rendering/glext.h"
#include <stdio.h>
#include <iostream>
//---------------------------------------------------------------------------------------------------
#include "Engine/Core/RgbaColors.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/Rendering/SpriteSheet.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/SriShaderManager.hpp"
#include "Engine/Rendering/OpenGlExtentions.hpp"
#include "Engine/FileIO/FileSystem.hpp"

//---------------------------------------------------------------------------------------------------
Vector3DF g_defaultColor = Vector3DF(1.f, 1.f, 1.f);
float g_defaultLineWidth = 1.f;
float g_defaultPointSize = 1.f;
HGLRC g_openGLRenderingContext = nullptr;
extern HWND g_hWnd;
extern HDC g_displayDeviceContext;
extern const char* APP_NAME;
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
SriOGLRenderer::SriOGLRenderer(void)
{
	m_shaderManager = new SriShaderManager();
	m_matrixStack.push_back(m_identityMatrix);
	m_currentShaderProgramId = -1;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
SriOGLRenderer::~SriOGLRenderer(void)
{
	delete m_shaderManager;
	std::vector<Matrix4x4F> emptyStack;
	m_matrixStack.clear();
	m_matrixStack.swap(emptyStack);
	m_matrixStack.shrink_to_fit();
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::Initialize(void* platformHandle)
{
	UNUSED(platformHandle);
	g_openGLRenderingContext = wglCreateContext( g_displayDeviceContext );
	wglMakeCurrent( g_displayDeviceContext, g_openGLRenderingContext );
	//glOrtho( 0.0 , 16 , 0.0 , 9 , 0 , 1 );
	glLineWidth(5.f);

	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	InitializeAdvancedOpenGLFunctions();
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::InitializeAdvancedOpenGLFunctions()
{
	glGenBuffers				= (PFNGLGENBUFFERSPROC)					wglGetProcAddress("glGenBuffers" );
	glBindBuffer				= (PFNGLBINDBUFFERPROC)					wglGetProcAddress("glBindBuffer" );
	glBufferData				= (PFNGLBUFFERDATAPROC)					wglGetProcAddress("glBufferData" );
	glGenerateMipmap			= (PFNGLGENERATEMIPMAPPROC)				wglGetProcAddress("glGenerateMipmap" );
	glDeleteBuffers				= (PFNGLDELETEBUFFERSPROC)				wglGetProcAddress("glDeleteBuffers" );
	glCreateShader				= (PFNGLCREATESHADERPROC)				wglGetProcAddress("glCreateShader");
	glShaderSource				= (PFNGLSHADERSOURCEPROC)				wglGetProcAddress("glShaderSource");
	glCompileShader				= (PFNGLCOMPILESHADERPROC)				wglGetProcAddress("glCompileShader");
	glGetShaderiv				= (PFNGLGETSHADERIVPROC)				wglGetProcAddress("glGetShaderiv");
	glCreateProgram				= (PFNGLCREATEPROGRAMPROC)				wglGetProcAddress("glCreateProgram");
	glLinkProgram				= (PFNGLLINKPROGRAMPROC)				wglGetProcAddress("glLinkProgram");
	glGetProgramiv				= (PFNGLGETPROGRAMIVPROC)				wglGetProcAddress("glGetProgramiv");
	glUseProgram				= (PFNGLUSEPROGRAMPROC)					wglGetProcAddress("glUseProgram");
	glAttachShader				= (PFNGLATTACHSHADERPROC)				wglGetProcAddress("glAttachShader");
	glGetProgramInfoLog			= (PFNGLGETPROGRAMINFOLOGPROC)			wglGetProcAddress("glGetProgramInfoLog");
	glGetShaderInfoLog			= (PFNGLGETSHADERINFOLOGPROC)			wglGetProcAddress("glGetShaderInfoLog");
	glActiveTexture				= (PFNGLACTIVETEXTUREPROC)				wglGetProcAddress("glActiveTexture");
	glGetUniformLocation		= (PFNGLGETUNIFORMLOCATIONPROC)			wglGetProcAddress("glGetUniformLocation");
	glUniform1f					= (PFNGLUNIFORM1FPROC)					wglGetProcAddress("glUniform1f");
	glUniform1i					= (PFNGLUNIFORM1IPROC)					wglGetProcAddress("glUniform1i");
	glEnableVertexAttribArray	= (PFNGLENABLEVERTEXATTRIBARRAYPROC) 	wglGetProcAddress("glEnableVertexAttribArray");
	glDisableVertexAttribArray	= (PFNGLDISABLEVERTEXATTRIBARRAYPROC)	wglGetProcAddress("glDisableVertexAttribArray");
	glVertexAttribPointer		= (PFNGLVERTEXATTRIBPOINTERPROC)		wglGetProcAddress("glVertexAttribPointer");
	glGetAttribLocation			= (PFNGLGETATTRIBLOCATIONPROC)			wglGetProcAddress("glGetAttribLocation");
	glUniformMatrix4fv			= (PFNGLUNIFORMMATRIX4FVPROC)			wglGetProcAddress("glUniformMatrix4fv");
	glBindAttribLocation		= (PFNGLBINDATTRIBLOCATIONPROC )		wglGetProcAddress("glBindAttribLocation");
	glUniform3f					= (PFNGLUNIFORM3FPROC)					wglGetProcAddress("glUniform3f");
	glUniform4f					= (PFNGLUNIFORM4FPROC)					wglGetProcAddress("glUniform4f");
	glUniform3fv				= (PFNGLUNIFORM3FVPROC)					wglGetProcAddress("glUniform3fv");
	glUniform4fv				= (PFNGLUNIFORM4FVPROC)					wglGetProcAddress("glUniform4fv");
	glUniform1fv				= (PFNGLUNIFORM1FVPROC)					wglGetProcAddress("glUniform1fv");
	glGenFramebuffers			= (PFNGLGENFRAMEBUFFERSPROC)            wglGetProcAddress("glGenFramebuffers");
	glBindFramebuffer			= (PFNGLBINDFRAMEBUFFERPROC)            wglGetProcAddress("glBindFramebuffer");
	glFramebufferTexture2D		= (PFNGLFRAMEBUFFERTEXTURE2DPROC)       wglGetProcAddress("glFramebufferTexture2D");
	glCheckFramebufferStatus	= (PFNGLCHECKFRAMEBUFFERSTATUSPROC)     wglGetProcAddress("glCheckFramebufferStatus");
	glDeleteFramebuffers		= (PFNGLDELETEFRAMEBUFFERSPROC)			wglGetProcAddress("glDeleteFramebuffers");	
	glDrawBuffers				= (PFNGLDRAWBUFFERSPROC)				wglGetProcAddress("glDrawBuffers");
	glBufferSubData				= (PFNGLBUFFERSUBDATAPROC)				wglGetProcAddress("glBufferSubData");
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::Clear(const RgbaColors& clearColor, unsigned int clearDepth)
{
	glClearColor( clearColor.r, clearColor.g, clearColor.b, clearColor.a );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearDepth(clearDepth);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::PushMatrix()
{
	m_matrixStack.push_back(m_matrixStack.back());
	//glPushMatrix();
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::PopMatrix()
{
	m_matrixStack.pop_back();
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::LoadIdentityMatrix()
{
	m_matrixStack.back().MakeIdentity();
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::LoadOrtho(float left, float right, float bottom, float top, float nearClippingPlane /*= 0.0*/, float farClippingPlane /*= 1.0*/)
{
	LoadIdentityMatrix();
	
	m_matrixStack.back().m_matrixElements[0]  = 2 / (right - left);
	m_matrixStack.back().m_matrixElements[5]  = 2 / (top - bottom);
	m_matrixStack.back().m_matrixElements[10] = -2 / (farClippingPlane- nearClippingPlane);
	m_matrixStack.back().m_matrixElements[12] = -(right + left) / (right - left);
	m_matrixStack.back().m_matrixElements[13] = -(top + bottom) / (top - bottom);
	m_matrixStack.back().m_matrixElements[14] = -(farClippingPlane + nearClippingPlane) / (farClippingPlane - nearClippingPlane);

}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::UnloadOrtho()
{

}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::SetUpPerspectiveProjection(float fieldOfViewY, float aspect, float zNearClipping, float zFarClipping)
{
	LoadIdentityMatrix();
	float tangent = tan(DegreesToRadians(fieldOfViewY*0.5f)); 
	float height = zNearClipping * tangent; 
	float width = height * aspect;          
	glEnable(GL_DEPTH_TEST);

	SetFrustrum(-width, width, -height, height, zNearClipping, zFarClipping);
	
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::SetFrustrum(float left, float right, float bottom, float top, float zNearClipping, float zFarClipping)
{
	m_matrixStack.back().m_matrixElements[0]  = 2 * zNearClipping / (right - left);
	m_matrixStack.back().m_matrixElements[5]  = 2 * zNearClipping / (top - bottom);
	m_matrixStack.back().m_matrixElements[8]  = (right + left) / (right - left);
	m_matrixStack.back().m_matrixElements[9]  = (top + bottom) / (top - bottom);
	m_matrixStack.back().m_matrixElements[10] = -(zFarClipping + zNearClipping) / (zFarClipping - zNearClipping);
	m_matrixStack.back().m_matrixElements[11] = -1;
	m_matrixStack.back().m_matrixElements[14] = -(2 * zFarClipping * zNearClipping) / (zFarClipping - zNearClipping);
	m_matrixStack.back().m_matrixElements[15] = 0.f;	
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::Rotate(float angle, float rotationOnX, float rotationOnY, float rotationOnZ)
{
	m_matrixStack.back().rotate(angle, rotationOnX, rotationOnY, rotationOnZ);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::Translate(float translationOnX, float translationOnY, float translationOnZ)
{
	m_matrixStack.back().Translate(translationOnX, translationOnY, translationOnZ);

}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawPoint(const Vector3DF& point, const RgbaColors& pointColor /*= RGBA(1.f, 1.f, 1.f, 1.f)*/, float pointSize /*= 1.f*/)
{
	VertexData data;
	data.m_color = pointColor;
	data.m_position = point;

	std::vector<VertexData> vertexes;
	vertexes.push_back(data);

	glPointSize(pointSize);
	DrawVertexArray(vertexes, GL_POINTS);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawPoints(const std::vector<Vector3DF>& points, const RgbaColors& pointColor /*= RGBA(1.f, 1.f, 1.f, 1.f)*/, float pointSize /*= 1.f*/)
{
	std::vector<VertexData> vertexes;
	for (int index =0; index < static_cast<int>(points.size()); index++)
	{
		VertexData data;
		data.m_color = pointColor;
		data.m_position = points[index];
		vertexes.push_back(data);
	}

	glPointSize(pointSize);
	DrawVertexArray(vertexes, GL_POINTS);

}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawLine(const Vector3DF& startPoint, const Vector3DF& endPoint, const RgbaColors& lineColor /*= Vector3DF(1.f, 1.f, 1.f)*/, float lineWidth /*= 1.f*/)
{
	VertexData data1;
	VertexData data2;

	std::vector<VertexData> vertexes;

	data1.m_color = lineColor;
	data1.m_position = startPoint;

	data2.m_color = lineColor;
	data2.m_position = endPoint;

	vertexes.push_back(data1);
	vertexes.push_back(data2);

	glLineWidth(lineWidth);
	DrawVertexArray(vertexes, GL_LINES);

}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawLines(const std::vector<Vector3DF>& positions, const RgbaColors& lineColor /*= RGBA(1.f, 1.f, 1.f, 1.f)*/, float lineWidth /*= 1.f*/)
{
	std::vector<VertexData> vertexes;
	for (int index = 0; index < static_cast<int>(positions.size()); index++)
	{
		VertexData data;
		data.m_color = lineColor;
		data.m_position = positions[index];
		vertexes.push_back(data);
	}

	glLineWidth(lineWidth);
	DrawVertexArray(vertexes, GL_LINES);

}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawLineSegment(const Vector3DF& startPoint, const Vector3DF& endPoint, const RgbaColors& lineStartColor, const RgbaColors& lineEndColor, float lineWidth /*= 1.f*/)
{
	VertexData data1;
	VertexData data2;

	std::vector<VertexData> vertexes;

	data1.m_color = lineStartColor;
	data1.m_position = startPoint;

	data2.m_color = lineEndColor;
	data2.m_position = endPoint;

	vertexes.push_back(data1);
	vertexes.push_back(data2);

	glLineWidth(lineWidth);
	DrawVertexArray(vertexes, GL_LINES);
	
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawSquareOutline(const Vector3DF& topLeft, const Vector3DF& bottomleft, const Vector3DF& bottomRight, const Vector3DF& topRight, const RgbaColors& lineColor /*= RGBA(1.f, 1.f, 1.f, 1.f)*/, float lineWidth /*= 1.f*/)
{
	VertexData data1;
	VertexData data2;
	VertexData data3;
	VertexData data4;

	std::vector<VertexData> vertexes;

	data1.m_color = lineColor;
	data1.m_position = topLeft;

	data2.m_color = lineColor;
	data2.m_position = bottomleft;

	data3.m_color = lineColor;
	data3.m_position = bottomRight;

	data4.m_color = lineColor;
	data4.m_position = topRight;

	vertexes.push_back(data1);
	vertexes.push_back(data2);
	vertexes.push_back(data3);
	vertexes.push_back(data4);

	glLineWidth(lineWidth);
	DrawVertexArray(vertexes, GL_LINE_LOOP);
	
}

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawTexturedQuad(const Vector3DF& leftTop, const Vector3DF& leftBottom, const Vector3DF& rightBottom, const Vector3DF& rightTop, const RgbaColors& lineColor /*= RGBA(1.f, 1.f, 1.f, 1.f)*/, SpriteSheet* sprite /*= nullptr*/, int spriteFrame /*= 0*/, const TBN& tbn /*= TBN()*/ )
{
	Vector2DF minTextureCoords; 
	Vector2DF maxTextureCoords; 
	if(sprite)
	{
		minTextureCoords = sprite->GetMinTextureCoordsForFrame(spriteFrame);
		maxTextureCoords = sprite->GetMaxTextureCoordsFromMinCoords(minTextureCoords);
	}

	VertexData data1;
	VertexData data2;
	VertexData data3;
	VertexData data4;

	std::vector<VertexData> vertexes;

	data1.m_color		= lineColor;
	data1.m_position	= leftTop;
	data1.m_texCoords	= Vector2DF(minTextureCoords.x, minTextureCoords.y);
	data1.m_tangent		= tbn.m_tangent;
	data1.m_bitangent	= tbn.m_bitangent;
	data1.m_normal		= tbn.m_normal;
	vertexes.push_back(data1);

	data2.m_color = lineColor;
	data2.m_position = leftBottom;
	data2.m_texCoords = Vector2DF(minTextureCoords.x, maxTextureCoords.y);
	data2.m_tangent		= tbn.m_tangent;
	data2.m_bitangent	= tbn.m_bitangent;
	data2.m_normal		= tbn.m_normal;
	vertexes.push_back(data2);

	data3.m_color = lineColor;
	data3.m_position = rightBottom;
	data3.m_texCoords = Vector2DF(maxTextureCoords.x, maxTextureCoords.y);
	data3.m_tangent		= tbn.m_tangent;
	data3.m_bitangent	= tbn.m_bitangent;
	data3.m_normal		= tbn.m_normal;
	vertexes.push_back(data3);

	data4.m_color = lineColor;
	data4.m_position = rightTop;
	data4.m_texCoords = Vector2DF(maxTextureCoords.x, minTextureCoords.y);
	data4.m_tangent		= tbn.m_tangent;
	data4.m_bitangent	= tbn.m_bitangent;
	data4.m_normal		= tbn.m_normal;
	vertexes.push_back(data4);

	DrawVertexArray(vertexes, GL_QUADS);

}

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawQuad(const Vector3DF& leftTop, const Vector3DF& leftBottom, const Vector3DF& rightBottom, const Vector3DF& rightTop, const RgbaColors& lineColor /*= RGBA(1.f, 1.f, 1.f, 1.f)*/, const Vector2DF& minCoords /*= Vector2DF(0.f, 0.f)*/, const Vector2DF& maxCoords /*= Vector2DF(1.f, 1.f)*/)
{
	Vector2DF minTextureCoords = minCoords; 
	Vector2DF maxTextureCoords = maxCoords; 

	VertexData data1;
	VertexData data2;
	VertexData data3;
	VertexData data4;

	std::vector<VertexData> vertexes;
	TBN tbn;

	data1.m_color = lineColor;
	data1.m_position = leftTop;
	data1.m_texCoords = Vector2DF(minTextureCoords.x, maxTextureCoords.y);
	data1.m_tangent		= tbn.m_tangent;
	data1.m_bitangent	= tbn.m_bitangent;
	data1.m_normal		= tbn.m_normal;
	vertexes.push_back(data1);

	data2.m_color = lineColor;
	data2.m_position = leftBottom;
	data2.m_texCoords = Vector2DF(minTextureCoords.x, minTextureCoords.y);
	data2.m_tangent		= tbn.m_tangent;
	data2.m_bitangent	= tbn.m_bitangent;
	data2.m_normal		= tbn.m_normal;
	vertexes.push_back(data2);

	data3.m_color = lineColor;
	data3.m_position = rightBottom;
	data3.m_texCoords = Vector2DF(maxTextureCoords.x, minTextureCoords.y);
	data3.m_tangent		= tbn.m_tangent;
	data3.m_bitangent	= tbn.m_bitangent;
	data3.m_normal		= tbn.m_normal;
	vertexes.push_back(data3);

	data4.m_color = lineColor;
	data4.m_position = rightTop;
	data4.m_texCoords = Vector2DF(maxTextureCoords.x, maxTextureCoords.y);
	data4.m_tangent		= tbn.m_tangent;
	data4.m_bitangent	= tbn.m_bitangent;
	data4.m_normal		= tbn.m_normal;
	vertexes.push_back(data4);

	DrawVertexArray(vertexes, GL_QUADS);

}

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawTexturedSquare(const Vector3DF& startPoint, float lengthOfEachSide, SpriteSheet* sprite, int spriteFrame, const RgbaColors& lineColor)
{
	Vector2DF minTextureCoords; 
	Vector2DF maxTextureCoords; 
	if(sprite)
	{
		minTextureCoords = sprite->GetMinTextureCoordsForFrame(spriteFrame);
		maxTextureCoords = sprite->GetMaxTextureCoordsFromMinCoords(minTextureCoords);
	}
	
	VertexData data1;
	VertexData data2;
	VertexData data3;
	VertexData data4;

	std::vector<VertexData> vertexes;

	data1.m_color = lineColor;
	data1.m_position = Vector3DF(startPoint.x, startPoint.y + lengthOfEachSide, lengthOfEachSide);
	data1.m_texCoords = Vector2DF(minTextureCoords.x, minTextureCoords.y);
	vertexes.push_back(data1);

	data2.m_color = lineColor;
	data2.m_position = Vector3DF(startPoint.x, startPoint.y + lengthOfEachSide, 0.f);
	data2.m_texCoords = Vector2DF(minTextureCoords.x, maxTextureCoords.y);;
	vertexes.push_back(data2);

	data3.m_color = lineColor;
	data3.m_position = Vector3DF(startPoint.x, startPoint.y, 0.f);
	data3.m_texCoords = Vector2DF(maxTextureCoords.x, maxTextureCoords.y);
	vertexes.push_back(data3);

	data4.m_color = lineColor;
	data4.m_position = Vector3DF(startPoint.x, startPoint.y, lengthOfEachSide);
	data4.m_texCoords = Vector2DF(maxTextureCoords.x, minTextureCoords.y);
	vertexes.push_back(data4);

	DrawVertexArray(vertexes, GL_QUADS);

}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawTexturedRect2D(const Vector2DF& leftTop, const Vector2DF& leftBottom, const Vector2DF& rightBottom, const Vector2DF& rightTop, SpriteSheet* sprite /*= nullptr*/, int spriteFrame /*= 0*/, const RgbaColors& lineColor /*= RGBA(1.f, 1.f, 1.f, 1.f)*/) 
{
	Vector2DF minTextureCoords; 
	Vector2DF maxTextureCoords; 
	
	if(sprite)
	{
		minTextureCoords = sprite->GetMinTextureCoordsForFrame(spriteFrame);
		maxTextureCoords = sprite->GetMaxTextureCoordsFromMinCoords(minTextureCoords);
	}

	VertexData data1;
	VertexData data2;
	VertexData data3;
	VertexData data4;

	std::vector<VertexData> vertexes;

	data1.m_color = lineColor;
	data1.m_position = Vector3DF(leftTop.x, leftTop.y, 0.f);
	data1.m_texCoords = Vector2DF(minTextureCoords.x, minTextureCoords.y);
	vertexes.push_back(data1);

	data2.m_color = lineColor;
	data2.m_position = Vector3DF(leftBottom.x, leftBottom.y , 0.f);
	data2.m_texCoords = Vector2DF(minTextureCoords.x, maxTextureCoords.y);
	vertexes.push_back(data2);

	data3.m_color = lineColor;
	data3.m_position = Vector3DF(rightBottom.x, rightBottom.y, 0.f);
	data3.m_texCoords = Vector2DF(maxTextureCoords.x, maxTextureCoords.y);
	vertexes.push_back(data3);

	data4.m_color = lineColor;
	data4.m_position = Vector3DF(rightTop.x, rightTop.y, 0.f);
	data4.m_texCoords = Vector2DF(maxTextureCoords.x, minTextureCoords.y);
	vertexes.push_back(data4);

	DrawVertexArray(vertexes, GL_QUADS);

}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::GenerateTexture(unsigned int& sizeX, unsigned int& sizeY, unsigned int& textureID, const std::string& imageFilePath)
{
	int numComponents = 0; // Filled in for us to indicate how many color/alpha components the image had (e.g. 3=RGB, 4=RGBA)
	int numComponentsRequested = 0; // don't care; we support 3 (RGB) or 4 (RGBA)
	
	unsigned char* imageData = LoadSTBIFileToBuffer(imageFilePath, sizeX, sizeY, numComponents, numComponentsRequested);

	// Enable texturing
	glEnable( GL_TEXTURE_2D );

	// Tell OpenGL that our pixel data is single-byte aligned
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	// Ask OpenGL for an unused texName (ID number) to use for this texture
	glGenTextures( 1, (unsigned int*) &textureID );

	// Tell OpenGL to bind (set) this as the currently active texture
	glBindTexture( GL_TEXTURE_2D, textureID );

	// Set texture clamp vs. wrap (repeat)
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, /*GL_CLAMP*/GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, /*GL_CLAMP*/GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...

	// Set magnification (texel > pixel) and minification (texel < pixel) filters
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_NEAREST ); // one of: GL_NEAREST, GL_LINEAR
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_NEAREST ); // one of: GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_LINEAR

	unsigned int bufferFormat = GL_RGBA; // the format our source pixel data is currently in; any of: GL_RGB, GL_RGBA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, ...
	if( numComponents == 3 )
		bufferFormat = GL_RGB;

	// Todo: What happens if numComponents is neither 3 nor 4?

	unsigned int internalFormat = bufferFormat; // the format we want the texture to me on the card; allows us to translate into a different texture format as we upload to OpenGL

	glTexImage2D(			// Upload this pixel data to our new OpenGL texture
		GL_TEXTURE_2D,		// Creating this as a 2d texture
		0,					// Which mipmap level to use as the "root" (0.f = the highest-quality, full-res image), if mipmaps are enabled
		internalFormat,		// Type of texel format we want OpenGL to use for this texture internally on the video card
		sizeX,			// Texel-width of image; for maximum compatibility, use 2^N + 2^B, where N is some integer in the range [3,10], and B is the border thickness [0,1]
		sizeY,			// Texel-height of image; for maximum compatibility, use 2^M + 2^B, where M is some integer in the range [3,10], and B is the border thickness [0,1]
		0,					// Border size, in texels (must be 0.f or 1)
		bufferFormat,		// Pixel format describing the composition of the pixel data in buffer
		GL_UNSIGNED_BYTE,	// Pixel color components are unsigned bytes (one byte per color/alpha channel)
		imageData );		// Location of the actual pixel data bytes/buffer

	stbi_image_free( imageData );
}


//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::GenerateDefaultDiffuseTexture(unsigned int& sizeX, unsigned int& sizeY, unsigned int& textureID)
{
	int numComponents = 3;

	unsigned char imageData[3] = {255, 255, 255};
	sizeX = 1;
	sizeY = 1;
	// Enable texturing
	glEnable( GL_TEXTURE_2D );

	// Tell OpenGL that our pixel data is single-byte aligned
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	// Ask OpenGL for an unused texName (ID number) to use for this texture
	glGenTextures( 1, (unsigned int*) &textureID );

	// Tell OpenGL to bind (set) this as the currently active texture
	glBindTexture( GL_TEXTURE_2D, textureID );

	// Set texture clamp vs. wrap (repeat)
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, /*GL_CLAMP/*/GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, /*GL_CLAMP/*/GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...

	// Set magnification (texel > pixel) and minification (texel < pixel) filters
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_NEAREST ); // one of: GL_NEAREST, GL_LINEAR
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_NEAREST ); // one of: GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_LINEAR

	unsigned int bufferFormat = GL_RGBA; // the format our source pixel data is currently in; any of: GL_RGB, GL_RGBA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, ...
	if( numComponents == 3 )
		bufferFormat = GL_RGB;

	// Todo: What happens if numComponents is neither 3 nor 4?

	unsigned int internalFormat = bufferFormat; // the format we want the texture to me on the card; allows us to translate into a different texture format as we upload to OpenGL

	glTexImage2D(			// Upload this pixel data to our new OpenGL texture
		GL_TEXTURE_2D,		// Creating this as a 2d texture
		0,					// Which mipmap level to use as the "root" (0.f = the highest-quality, full-res image), if mipmaps are enabled
		internalFormat,		// Type of texel format we want OpenGL to use for this texture internally on the video card
		sizeX,			// Texel-width of image; for maximum compatibility, use 2^N + 2^B, where N is some integer in the range [3,10], and B is the border thickness [0,1]
		sizeY,			// Texel-height of image; for maximum compatibility, use 2^M + 2^B, where M is some integer in the range [3,10], and B is the border thickness [0,1]
		0,					// Border size, in texels (must be 0.f or 1)
		bufferFormat,		// Pixel format describing the composition of the pixel data in buffer
		GL_UNSIGNED_BYTE,	// Pixel color components are unsigned bytes (one byte per color/alpha channel)
		imageData );		// Location of the actual pixel data bytes/buffer

}

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::GenerateDefaultSpecularTexture(unsigned int& sizeX, unsigned int& sizeY, unsigned int& textureID)
{
	int numComponents = 3;

	unsigned char imageData[3] = {127, 127, 0};
	sizeX = 1;
	sizeY = 1;
	// Enable texturing
	glEnable( GL_TEXTURE_2D );

	// Tell OpenGL that our pixel data is single-byte aligned
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	// Ask OpenGL for an unused texName (ID number) to use for this texture
	glGenTextures( 1, (unsigned int*) &textureID );

	// Tell OpenGL to bind (set) this as the currently active texture
	glBindTexture( GL_TEXTURE_2D, textureID );

	// Set texture clamp vs. wrap (repeat)
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, /*GL_CLAMP/*/GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, /*GL_CLAMP/*/GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...

	// Set magnification (texel > pixel) and minification (texel < pixel) filters
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_NEAREST ); // one of: GL_NEAREST, GL_LINEAR
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_NEAREST ); // one of: GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_LINEAR

	unsigned int bufferFormat = GL_RGBA; // the format our source pixel data is currently in; any of: GL_RGB, GL_RGBA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, ...
	if( numComponents == 3 )
		bufferFormat = GL_RGB;

	// Todo: What happens if numComponents is neither 3 nor 4?

	unsigned int internalFormat = bufferFormat; // the format we want the texture to me on the card; allows us to translate into a different texture format as we upload to OpenGL

	glTexImage2D(			// Upload this pixel data to our new OpenGL texture
		GL_TEXTURE_2D,		// Creating this as a 2d texture
		0,					// Which mipmap level to use as the "root" (0.f = the highest-quality, full-res image), if mipmaps are enabled
		internalFormat,		// Type of texel format we want OpenGL to use for this texture internally on the video card
		sizeX,			// Texel-width of image; for maximum compatibility, use 2^N + 2^B, where N is some integer in the range [3,10], and B is the border thickness [0,1]
		sizeY,			// Texel-height of image; for maximum compatibility, use 2^M + 2^B, where M is some integer in the range [3,10], and B is the border thickness [0,1]
		0,					// Border size, in texels (must be 0.f or 1)
		bufferFormat,		// Pixel format describing the composition of the pixel data in buffer
		GL_UNSIGNED_BYTE,	// Pixel color components are unsigned bytes (one byte per color/alpha channel)
		imageData );		// Location of the actual pixel data bytes/buffer

}

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::GenerateDefaultNormalTexture(unsigned int& sizeX, unsigned int& sizeY, unsigned int& textureID)
{
	int numComponents = 3;

	unsigned char imageData[3] = {128, 128, 255};
	sizeX = 1;
	sizeY = 1;
	// Enable texturing
	glEnable( GL_TEXTURE_2D );

	// Tell OpenGL that our pixel data is single-byte aligned
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	// Ask OpenGL for an unused texName (ID number) to use for this texture
	glGenTextures( 1, (unsigned int*) &textureID );

	// Tell OpenGL to bind (set) this as the currently active texture
	glBindTexture( GL_TEXTURE_2D, textureID );

	// Set texture clamp vs. wrap (repeat)
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, /*GL_CLAMP/*/GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, /*GL_CLAMP/*/GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...

	// Set magnification (texel > pixel) and minification (texel < pixel) filters
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_NEAREST ); // one of: GL_NEAREST, GL_LINEAR
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_NEAREST ); // one of: GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_LINEAR

	unsigned int bufferFormat = GL_RGBA; // the format our source pixel data is currently in; any of: GL_RGB, GL_RGBA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, ...
	if( numComponents == 3 )
		bufferFormat = GL_RGB;

	// Todo: What happens if numComponents is neither 3 nor 4?

	unsigned int internalFormat = bufferFormat; // the format we want the texture to me on the card; allows us to translate into a different texture format as we upload to OpenGL

	glTexImage2D(			// Upload this pixel data to our new OpenGL texture
		GL_TEXTURE_2D,		// Creating this as a 2d texture
		0,					// Which mipmap level to use as the "root" (0.f = the highest-quality, full-res image), if mipmaps are enabled
		internalFormat,		// Type of texel format we want OpenGL to use for this texture internally on the video card
		sizeX,			// Texel-width of image; for maximum compatibility, use 2^N + 2^B, where N is some integer in the range [3,10], and B is the border thickness [0,1]
		sizeY,			// Texel-height of image; for maximum compatibility, use 2^M + 2^B, where M is some integer in the range [3,10], and B is the border thickness [0,1]
		0,					// Border size, in texels (must be 0.f or 1)
		bufferFormat,		// Pixel format describing the composition of the pixel data in buffer
		GL_UNSIGNED_BYTE,	// Pixel color components are unsigned bytes (one byte per color/alpha channel)
		imageData );		// Location of the actual pixel data bytes/buffer

}

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawAxis(float xAxisLength /*= 1.f*/, float lineWidth /*= 1.f*/)
{
	std::vector<VertexData> vertexes;

	static VertexData startPoint;
	static VertexData endPoint;

	startPoint.m_position = Vector3DF(0.f, 0.f, 0.f);
	endPoint.m_position = Vector3DF(xAxisLength, 0.f, 0.f);
	RgbaColors::FloatsToUnsignedBytes(startPoint.m_color, 1.f, 0.f, 0.f);
	RgbaColors::FloatsToUnsignedBytes(endPoint.m_color,   1.f, 0.f, 0.f);
	vertexes.push_back(startPoint);
	vertexes.push_back(endPoint);
	
	startPoint.m_position = Vector3DF(0.0, 0.f, 0.f);
	endPoint.m_position = Vector3DF(0.f, xAxisLength, 0.f);
	RgbaColors::FloatsToUnsignedBytes(startPoint.m_color, 0.f, 1.f, 0.f);
	RgbaColors::FloatsToUnsignedBytes(endPoint.m_color,   0.f, 1.f, 0.f);
	vertexes.push_back(startPoint);
	vertexes.push_back(endPoint);

	endPoint.m_position = Vector3DF(0.f, 0.f, xAxisLength);
	RgbaColors::FloatsToUnsignedBytes(startPoint.m_color, 0.f, 0.f, 1.f);
	RgbaColors::FloatsToUnsignedBytes(endPoint.m_color,   0.f, 0.f, 1.f);
	vertexes.push_back(startPoint);
	vertexes.push_back(endPoint);

	EnableDepthBuffer();
	glLineWidth(lineWidth);
	DrawVertexArray(vertexes, GL_LINES);
	vertexes.clear();
	//DisableDepthBuffer();
	//glLineWidth(lineWidth * 0.5f);
	//DrawVertexArray(vertexes, GL_LINES);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
int SriOGLRenderer::GenerateVertexBuffer(const std::vector<ModelVertex>& vertexes, unsigned int* bufferId)
{
	if( *bufferId == 0 )
	{
		glGenBuffers( 1, bufferId ); 
	}

	glBindBuffer( GL_ARRAY_BUFFER, *bufferId );
	glBufferData( GL_ARRAY_BUFFER, sizeof( ModelVertex ) * vertexes.size(), vertexes.data(), GL_STATIC_DRAW );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	
	return (int)vertexes.size();
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
int SriOGLRenderer::GenerateIndexBuffer(const std::vector<unsigned int>& indexes, unsigned int* bufferId) 
{
	if( *bufferId == 0 )
	{
		glGenBuffers( 1, bufferId ); 
	}

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, *bufferId );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int ) * indexes.size(), indexes.data(), GL_STATIC_DRAW );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	return (int)indexes.size();
}

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawVBO(unsigned int vboId, unsigned int iboId, unsigned int bufferSize, unsigned int mode, bool hasValidIBO /*= true*/)
{
	glBindBuffer( GL_ARRAY_BUFFER, vboId );
	if (hasValidIBO)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboId);

		glEnableVertexAttribArray(SSAL_VERTEXES);
		glEnableVertexAttribArray(SSAL_COLOR);
		glEnableVertexAttribArray(SSAL_TEXCOORDS);
		glEnableVertexAttribArray(SSAL_TANGENTS);
		glEnableVertexAttribArray(SSAL_BITANGENTS);
		glEnableVertexAttribArray(SSAL_NORMALS);

		glVertexAttribPointer(SSAL_VERTEXES, 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof(   ModelVertex, m_position));	
		glVertexAttribPointer(SSAL_COLOR,	 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof( ModelVertex ), (const GLvoid*) offsetof(   ModelVertex, m_color));
		glVertexAttribPointer(SSAL_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ),  (const GLvoid*) offsetof( ModelVertex, m_texCoords) );	
		glVertexAttribPointer(SSAL_TANGENTS,   3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_tangent) );
		glVertexAttribPointer(SSAL_BITANGENTS, 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_bitangent) );
		glVertexAttribPointer(SSAL_NORMALS,	 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ),   (const GLvoid*) offsetof( ModelVertex, m_normal) );

		glDrawElements(mode, bufferSize, GL_UNSIGNED_INT, 0);

		glDisableVertexAttribArray(SSAL_VERTEXES);
		glDisableVertexAttribArray(SSAL_COLOR);
		glDisableVertexAttribArray(SSAL_TEXCOORDS);
		glDisableVertexAttribArray(SSAL_TANGENTS);
		glDisableVertexAttribArray(SSAL_BITANGENTS);
		glDisableVertexAttribArray(SSAL_NORMALS);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	else
	{
		glEnableVertexAttribArray(SSAL_VERTEXES);
		glEnableVertexAttribArray(SSAL_COLOR);
		glEnableVertexAttribArray(SSAL_TEXCOORDS);
		glEnableVertexAttribArray(SSAL_TANGENTS);
		glEnableVertexAttribArray(SSAL_BITANGENTS);
		glEnableVertexAttribArray(SSAL_NORMALS);

		glVertexAttribPointer(SSAL_VERTEXES, 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof(   ModelVertex, m_position));	
		glVertexAttribPointer(SSAL_COLOR,	 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof( ModelVertex ), (const GLvoid*) offsetof(   ModelVertex, m_color));
		glVertexAttribPointer(SSAL_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ),  (const GLvoid*) offsetof( ModelVertex, m_texCoords) );	
		glVertexAttribPointer(SSAL_TANGENTS,   3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_tangent) );
		glVertexAttribPointer(SSAL_BITANGENTS, 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_bitangent) );
		glVertexAttribPointer(SSAL_NORMALS,	 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ),   (const GLvoid*) offsetof( ModelVertex, m_normal) );

		glDrawArrays( mode, 0, bufferSize );

		glDisableVertexAttribArray(SSAL_VERTEXES);
		glDisableVertexAttribArray(SSAL_COLOR);
		glDisableVertexAttribArray(SSAL_TEXCOORDS);
		glDisableVertexAttribArray(SSAL_TANGENTS);
		glDisableVertexAttribArray(SSAL_BITANGENTS);
		glDisableVertexAttribArray(SSAL_NORMALS);

	}
	
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DeleteBuffers(unsigned int* bufferID)
{
	glDeleteBuffers(1, bufferID);
}
//---------------------------------------------------------------------------------------------------

void SriOGLRenderer::EnableDepthBuffer()
{
	glEnable(GL_DEPTH_TEST);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DisableDepthBuffer()
{
	glDisable(GL_DEPTH_TEST);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::SetColor(const RgbaColors& colorToSet)
{
	glColor3f(colorToSet.r, colorToSet.g, colorToSet.b);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::SetLineWidth(float width)
{
	glLineWidth(width);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
int SriOGLRenderer::LoadShader(const char* shaderFilePath, ShaderType type)
{
	return m_shaderManager->LoadShaderFromFileAndCompile(shaderFilePath, type);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawTexturedCube(const Vector3DF& cubeCenter, float lengthOfEachSide, const RgbaColors& color /*= RGBA(1.f, 1.f, 1.f, 1.f)*/, SpriteSheet* diffuseTexture /*= nullptr*/, bool drawDebugOutline /*= false */)
{
	Vector3DF worldMin;
	Vector3DF worldMax;

	//float halfLen = lengthOfEachSide * 0.5f;
	
	worldMin.x = cubeCenter.x/* - halfLen*/;
	worldMin.y = cubeCenter.y/* - halfLen*/;
	worldMin.z = cubeCenter.z/* - halfLen*/;
	
	//worldMax.x = cubeCenter.x + halfLen;
	//worldMax.y = cubeCenter.y + halfLen;
	//worldMax.z = cubeCenter.z + halfLen;
	worldMax.x = cubeCenter.x + lengthOfEachSide;
	worldMax.y = cubeCenter.y + lengthOfEachSide;
	worldMax.z = cubeCenter.z + lengthOfEachSide;

	std::vector<Vector3DF> vertexes;
	std::vector<TBN> tbns;
	TBN tbn;
	// South Facing
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMin.z));
	vertexes.push_back(worldMin);
	vertexes.push_back(Vector3DF(worldMin.x, worldMin.y, worldMax.z));
	tbn.m_tangent	= Vector3DF(0.f, -1.f, 0.f);
	tbn.m_bitangent = Vector3DF(0.f, 0.f, -1.f);
	tbn.m_normal	= Vector3DF(-1.f, 0.f, 0.f);
	tbns.push_back(tbn);


	//East Facing
	vertexes.push_back(Vector3DF(worldMin.x, worldMin.y, worldMax.z));
	vertexes.push_back(worldMin);
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMax.z));
	tbn.m_tangent	= Vector3DF(1.f, 0.f, 0.f);
	tbn.m_bitangent = Vector3DF(0.f, 0.f, -1.f);
	tbn.m_normal	= Vector3DF(0.f, -1.f, 0.f);
	tbns.push_back(tbn);


	//North Facing
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMax.y, worldMin.z));
	vertexes.push_back(worldMax);
	tbn.m_tangent	= Vector3DF(0.f, 1.f, 0.f);
	tbn.m_bitangent = Vector3DF(0.f, 0.f, -1.f);
	tbn.m_normal	= Vector3DF(1.f, 0.f, 0.f);
	tbns.push_back(tbn);


	//West Facing
	vertexes.push_back(worldMax);
	vertexes.push_back(Vector3DF(worldMax.x, worldMax.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMax.z));
	tbn.m_tangent	= Vector3DF(-1.f, 0.f, 0.f);
	tbn.m_bitangent = Vector3DF(0.f, 0.f, -1.f);
	tbn.m_normal	= Vector3DF(0.f, 1.f, 0.f);
	tbns.push_back(tbn);

	//Top Facing
	vertexes.push_back(worldMax);
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMin.x, worldMin.y, worldMax.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMax.z));
	tbn.m_tangent	= Vector3DF(0.f, -1.f, 0.f);
	tbn.m_bitangent = Vector3DF(-1.f, 0.f, 0.f);
	tbn.m_normal	= Vector3DF(0.f, 0.f, 1.f);
	tbns.push_back(tbn);


	//Bottom Facing
	vertexes.push_back(Vector3DF(worldMin.x, worldMax.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMax.y, worldMin.z));
	vertexes.push_back(Vector3DF(worldMax.x, worldMin.y, worldMin.z));
	vertexes.push_back(worldMin);
	tbn.m_tangent	= Vector3DF(0.f, -1.f, 0.f);
	tbn.m_bitangent = Vector3DF(1.f, 0.f, 0.f);
	tbn.m_normal	= Vector3DF(0.f, 0.f, -1.f);
	tbns.push_back(tbn);

	int tbnIndex = 0;
	for (int index = 0; index < (int)vertexes.size(); index +=4)
	{
		EnableDepthBuffer();
		g_myRenderer->DrawTexturedQuad(vertexes[index], vertexes[index+1], vertexes[index+2], vertexes[index+3], color, diffuseTexture, 0, tbns[tbnIndex]);
		tbnIndex++;
	}
	if(drawDebugOutline)
	{
		//glUseProgram(0);
		for (int index = 0; index < (int)vertexes.size(); index +=4)
		{
			g_myRenderer->DrawSquareOutline(vertexes[index], vertexes[index+1], vertexes[index+2], vertexes[index+3], RgbaColors(1.f, 0.f, 0.f), 5.f);
		}
	}
}

//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::BindAllTextures(SpriteSheet* diffuseTexture, SpriteSheet* normalTexture, SpriteSheet* specularTexture, SpriteSheet* emmisiveTexture)
{
	if(emmisiveTexture)
	{
		glActiveTexture(GL_TEXTURE3);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, emmisiveTexture->m_texture->m_openglTextureID);
	}

	if(specularTexture)
	{
		glActiveTexture(GL_TEXTURE2);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, specularTexture->m_texture->m_openglTextureID);
	}

	if(normalTexture)
	{
		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, normalTexture->m_texture->m_openglTextureID);
	}

	if(diffuseTexture)
	{
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, diffuseTexture->m_texture->m_openglTextureID);
	}
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::ReportShaderLinkError(int m_programId, const char* vertexShaderPath, const char* fragmentShaderPath)
{
	int infoLen;
	glGetProgramiv(m_programId, GL_INFO_LOG_LENGTH, &infoLen);
	char* infoLog = new char[infoLen +1];

	glGetProgramInfoLog(m_programId, infoLen, NULL, infoLog);
	std::string infoLogString = "\n";
	infoLogString += std::string(infoLog);

	std::string lineNumber;
	std::string errorCode;
	std::string errorText;
	std::string openglVersion((const char*)glGetString(GL_VERSION));
	std::string glslVersion((const char*)glGetString(GL_SHADING_LANGUAGE_VERSION));

	ParseOpenGLErrorMessage(infoLog, lineNumber, errorCode, errorText);

	char vertexShaderFullPath[_MAX_PATH];
	_fullpath(vertexShaderFullPath, vertexShaderPath, _MAX_PATH);
	std::string vertexFullPathString(vertexShaderFullPath);

	char fragmentShaderFullPath[_MAX_PATH];
	_fullpath(fragmentShaderFullPath, fragmentShaderPath, _MAX_PATH);
	std::string fragmentFullPathString(fragmentShaderFullPath);

	std::string outputErrorMessage = "OpenGL Error: Link Error in Shader Program\n\n";
	char programIdChar = (char)(((int)'0')+m_programId);
	outputErrorMessage += "Shader Program ID : ";
	outputErrorMessage += programIdChar;
	outputErrorMessage += "\n\nError occurred while trying to Link the following files:\n";
	outputErrorMessage += "1. ";
	outputErrorMessage += vertexShaderPath;
	outputErrorMessage += "\n2. ";
	outputErrorMessage += fragmentShaderPath;
	outputErrorMessage += "\n\nLine Number : ";
	outputErrorMessage += lineNumber;
	outputErrorMessage += "\n\nERROR: ";
	outputErrorMessage += errorText;
	outputErrorMessage += "\n\nOpenGL Version : ";
	outputErrorMessage += std::string(openglVersion);
	outputErrorMessage += "\nGLSL Version : ";
	outputErrorMessage += std::string(glslVersion);
	outputErrorMessage += "\n\nVertex Shader File Location :\n";
	outputErrorMessage += std::string(vertexFullPathString);
	outputErrorMessage += "\n\nFragment Shader File Location :\n";
	outputErrorMessage += std::string(fragmentFullPathString);
	outputErrorMessage += "\n\nRaw Error Log\n\n";
	outputErrorMessage += infoLogString;
	outputErrorMessage += "\n\nThe Application Will Now Close";

	LPCSTR msg = outputErrorMessage.c_str();
	LPCSTR caption = "OpenGL GLSL Link Error";
	infoLogString = "\n";
	infoLogString += fragmentShaderPath;
	infoLogString += " (";
	infoLogString += lineNumber;
	infoLogString += ") : ";
	infoLogString += errorCode;
	infoLogString += errorText;
	infoLogString += "\nOpenGL Version : ";
	infoLogString += openglVersion;
	infoLogString += "\nGLSL Version : ";
	infoLogString += glslVersion;
	infoLogString += "\n\n";
	LPCSTR debugMsg = infoLogString.c_str();
	OutputDebugStringA(debugMsg);
	MessageBoxA(NULL, msg, caption, MB_OK);
	exit(0);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::ReportShaderCompileError(int shaderId, const char* shaderPath)
{
	int infoLen;
	glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLen);
	char* infoLog = new char[infoLen +1];

	glGetShaderInfoLog(shaderId, infoLen, NULL, infoLog);
	std::string infoLogString = std::string(infoLog);

	std::string openglVersion((const char*)glGetString(GL_VERSION));
	std::string glslVersion((const char*)glGetString(GL_SHADING_LANGUAGE_VERSION));

	std::string lineNumber;
	std::string errorCode;
	std::string errorText;
	ParseOpenGLErrorMessage(infoLog, lineNumber, errorCode, errorText);

	char shaderFullPath[_MAX_PATH];
	_fullpath(shaderFullPath, shaderPath, _MAX_PATH);
	std::string fullPathString(shaderFullPath);

	char shaderIdChar = (char)(((int)'0')+shaderId);

	std::string outputErrorMessage = "OpenGL Error: Compile Error in GLSL Shader : ";
	outputErrorMessage += shaderPath;
	outputErrorMessage += ", Line Number : ";
	outputErrorMessage += lineNumber;
	outputErrorMessage += "\n\nERROR: ";
	outputErrorMessage += errorText;
	outputErrorMessage += "\nShader Program ID : ";
	outputErrorMessage += shaderIdChar;
	outputErrorMessage += "\n\nOpenGL Version : ";
	outputErrorMessage += std::string(openglVersion);
	outputErrorMessage += "\nGLSL Version : ";
	outputErrorMessage += std::string(glslVersion);
	outputErrorMessage += "\n\nFile Location :\n";
	outputErrorMessage += std::string(fullPathString);
	outputErrorMessage += "\n\nRaw Error Log\n\n";
	outputErrorMessage += infoLogString;
	outputErrorMessage += "\n\nThe Application Will Now Close";

	LPCSTR msg = outputErrorMessage.c_str();
	LPCSTR caption = "OpenGL GLSL Compile Error";
	infoLogString = "\n";
	infoLogString += shaderPath;
	infoLogString += " (";
	infoLogString += lineNumber;
	infoLogString += ") : ";
	infoLogString += errorCode;
	infoLogString += errorText;
	infoLogString += "\nOpenGL Version : ";
	infoLogString += openglVersion;
	infoLogString += "\nGLSL Version : ";
	infoLogString += glslVersion;
	infoLogString += "\n\n";
	LPCSTR debugMsg = infoLogString.c_str();
	OutputDebugStringA(debugMsg);
	MessageBoxA(NULL, msg, caption, MB_OK);
	exit(0);
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::ParseOpenGLErrorMessage(const char* openglErrMsg, std::string& lineNumber, std::string& errorCode, std::string& errorText)
{
	if(openglErrMsg)
	{
		int lineNumStartIndex = 10000;
		bool lineNumAcquired = false;
		lineNumber = "";

		int errorCodeStartIndex = 10000;
		bool errorCodeStarted = false;
		bool errorCodeAcquired = false;
		errorCode = "";

		errorText = "";

		for (int index = 0; index < (int)std::strlen(openglErrMsg); index++)
		{
			if (!lineNumAcquired && openglErrMsg[index] == '(')
			{
				lineNumStartIndex = index + 1;
			}

			if(!lineNumAcquired && index >= lineNumStartIndex)
			{
				if(openglErrMsg[index] == ')')
				{
					lineNumAcquired = true;
					continue;
				}
				lineNumber += openglErrMsg[index];
			}

			if(openglErrMsg[index] == ':' )
			{
				if(!errorCodeStarted)
				{
					errorCodeStartIndex = index + 1;
					errorCodeStarted = true;
				}
				else
				{
					errorCodeAcquired = true;
				}
			}

			if (!errorCodeAcquired && index >= errorCodeStartIndex)
			{
				errorCode += openglErrMsg[index];
			}

			if(lineNumAcquired && errorCodeAcquired)
			{
				errorText += openglErrMsg[index];
			}
		}
	}
}

// The Following Code is Taken and slightly modified from a discussion on stackoverflow.com
// http://stackoverflow.com/questions/5988686/creating-a-3d-sphere-in-opengl-using-visual-c
// Author of the original post/code goes by the forum name datenwolf
//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DrawSolidSphere(float radius, unsigned int numRings, unsigned int sectors, const Vector3DF& center)
{
	UNUSED(center);
	float const R = 1.f/(float)(numRings-1);
	float const S = 1.f/(float)(sectors-1);
	int r, s;
	
	std::vector<GLfloat> vertices;
	std::vector<GLfloat> normals;
	std::vector<GLfloat> texcoords;
	std::vector<GLushort> indices;

	vertices.resize(numRings * sectors * 3);
	normals.resize(numRings * sectors * 3);
	texcoords.resize(numRings * sectors * 2);
	std::vector<GLfloat>::iterator v = vertices.begin();
	std::vector<GLfloat>::iterator n = normals.begin();
	std::vector<GLfloat>::iterator t = texcoords.begin();
	for(r = 0; r < (int)numRings; r++) 
	{
		for(s = 0; s < (int)sectors; s++) 
		{
			float const y = sin( -PI_OVER_TWO + PI * r * R );
			float const x = cos(TWO_PI * s * S) * sin( PI * r * R );
			float const z = sin(TWO_PI * s * S) * sin( PI * r * R );

			*t++ = r*R;
			*t++ = s*S;

			*v++ = z * radius;
			*v++ = y * radius;
			*v++ = x * radius;

			*n++ = z;
			*n++ = y;
			*n++ = x;		
		}
	}

	indices.resize(numRings * sectors * 4);
	std::vector<GLushort>::iterator i = indices.begin();
	for(r = 0; r < (short)numRings-1; r++) for(s = 0; s < (short)sectors-1; s++) {
		*i++ = (unsigned short)(r * sectors + s);
		*i++ = (unsigned short)(r * sectors + (s+1));
		*i++ = (unsigned short)((r+1) * sectors + (s+1));
		*i++ = (unsigned short)((r+1) * sectors + s);
	}

	//glMatrixMode(GL_MODELVIEW);
	//PushMatrix();
//	glTranslatef(center.x,center.y,center.z);

	glEnableVertexAttribArray(SSAL_VERTEXES);
	glEnableVertexAttribArray(SSAL_TEXCOORDS);
	glEnableVertexAttribArray(SSAL_NORMALS);


	glVertexAttribPointer(SSAL_VERTEXES, 3, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertices[0] );		
	glVertexAttribPointer(SSAL_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &texcoords[0] );	
	glVertexAttribPointer(SSAL_NORMALS,   3, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &normals[0] );

	glDrawElements(GL_QUADS, indices.size(), GL_UNSIGNED_SHORT, &indices[0]);

	//glDrawArrays( GL_QUADS, 0, vertices.size() );
	glDisableVertexAttribArray(SSAL_VERTEXES);
	glDisableVertexAttribArray(SSAL_TEXCOORDS);
	glDisableVertexAttribArray(SSAL_NORMALS);


	
	//PopMatrix();
}

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::RenderCharacter(const Vector2DF& startPoint, float width, float height, const Vector2DF& min , const Vector2DF& max, SpriteSheet* sprite /*= nullptr*/, const RgbaColors& lineColor /*= RGBA(1.f, 1.f, 1.f, 1.f)*/)
{
	Vector2DF minTextureCoords; 
	Vector2DF maxTextureCoords; 

	minTextureCoords = min;
	maxTextureCoords = max;
	VertexData data1;
	VertexData data2;
	VertexData data3;
	VertexData data4;

	std::vector<VertexData> vertexes;



	data1.m_color = lineColor;
	data1.m_position = Vector3DF(startPoint.x, startPoint.y + height, 0.f);
	data1.m_texCoords = Vector2DF(minTextureCoords.x, minTextureCoords.y);
	vertexes.push_back(data1);

	data2.m_color = lineColor;
	data2.m_position = Vector3DF(startPoint.x, startPoint.y, 0.f);
	data2.m_texCoords = Vector2DF(minTextureCoords.x, maxTextureCoords.y);
	vertexes.push_back(data2);

	data3.m_color = lineColor;
	data3.m_position = Vector3DF(startPoint.x + width, startPoint.y, 0.f);
	data3.m_texCoords = Vector2DF(maxTextureCoords.x, maxTextureCoords.y);
	vertexes.push_back(data3);

	data4.m_color = lineColor;
	data4.m_position = Vector3DF(startPoint.x + width, startPoint.y + height, 0.f);
	data4.m_texCoords = Vector2DF(maxTextureCoords.x, minTextureCoords.y);
	vertexes.push_back(data4);

	
	if(sprite)
	{
		//glBindTexture(GL_TEXTURE_2D, sprite->m_sprite->m_openglTextureID);
		glEnable(GL_TEXTURE_2D);
	}

	//LoadOrtho(0.0, 1600.0, 0.0, 900.0);

	
	
	DrawVertexArray(vertexes, GL_QUADS);

	for (int index = (int)vertexes.size() - 1; index > -1 ; index--)
	{
		//delete vertexes.back();
		vertexes.pop_back();
	}

}

void SriOGLRenderer::DrawVertexArray(const std::vector<VertexData>& vertexes, unsigned int mode)
{
	if (m_currentShaderProgramId == -1)
	{
		return;
	}

	glEnableVertexAttribArray(SSAL_VERTEXES);
	glEnableVertexAttribArray(SSAL_COLOR);
	glEnableVertexAttribArray(SSAL_TEXCOORDS);
	glEnableVertexAttribArray(SSAL_TANGENTS);
	glEnableVertexAttribArray(SSAL_BITANGENTS);
	glEnableVertexAttribArray(SSAL_NORMALS);

	glVertexAttribPointer(SSAL_VERTEXES, 3, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertexes[0].m_position );	
	glVertexAttribPointer(SSAL_COLOR,	 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof( VertexData ), &vertexes[0].m_color );	
	glVertexAttribPointer(SSAL_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertexes[0].m_texCoords );	
	glVertexAttribPointer(SSAL_TANGENTS,   3, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertexes[0].m_tangent );
	glVertexAttribPointer(SSAL_BITANGENTS, 3, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertexes[0].m_bitangent );
	glVertexAttribPointer(SSAL_NORMALS,	 3, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertexes[0].m_normal );

	glDrawArrays( mode, 0, vertexes.size() );

	glDisableVertexAttribArray(SSAL_VERTEXES);
	glDisableVertexAttribArray(SSAL_COLOR);
	glDisableVertexAttribArray(SSAL_TEXCOORDS);
	glDisableVertexAttribArray(SSAL_TANGENTS);
	glDisableVertexAttribArray(SSAL_BITANGENTS);
	glDisableVertexAttribArray(SSAL_NORMALS);

}

void SriOGLRenderer::DrawVertexArray(const std::vector<ModelVertex>& vertexes, unsigned int mode)
{
	if (m_currentShaderProgramId == -1)
	{
		return;
	}

	glEnableVertexAttribArray(SSAL_VERTEXES);
	glEnableVertexAttribArray(SSAL_COLOR);
	glEnableVertexAttribArray(SSAL_TEXCOORDS);
	glEnableVertexAttribArray(SSAL_TANGENTS);
	glEnableVertexAttribArray(SSAL_BITANGENTS);
	glEnableVertexAttribArray(SSAL_NORMALS);

	glVertexAttribPointer(SSAL_VERTEXES, 3, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertexes[0].m_position );	
	glVertexAttribPointer(SSAL_COLOR,	 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof( VertexData ), &vertexes[0].m_color );	
	glVertexAttribPointer(SSAL_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertexes[0].m_texCoords );	
	glVertexAttribPointer(SSAL_TANGENTS,   3, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertexes[0].m_tangent );
	glVertexAttribPointer(SSAL_BITANGENTS, 3, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertexes[0].m_bitangent );
	glVertexAttribPointer(SSAL_NORMALS,	 3, GL_FLOAT, GL_FALSE, sizeof( VertexData ), &vertexes[0].m_normal );

	glDrawArrays( mode, 0, vertexes.size() );

	glDisableVertexAttribArray(SSAL_VERTEXES);
	glDisableVertexAttribArray(SSAL_COLOR);
	glDisableVertexAttribArray(SSAL_TEXCOORDS);
	glDisableVertexAttribArray(SSAL_TANGENTS);
	glDisableVertexAttribArray(SSAL_BITANGENTS);
	glDisableVertexAttribArray(SSAL_NORMALS);
}

void SriOGLRenderer::SetCurrentShaderProgramID(int id)
{
	m_currentShaderProgramId = id;
}

void SriOGLRenderer::DisableAllTextures()
{
	glActiveTexture(GL_TEXTURE3);
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE2);
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE1);
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE0);
	glDisable(GL_TEXTURE_2D);
}

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::SwapDeviceBuffers()
{
	SwapBuffers( g_displayDeviceContext );
}

//---------------------------------------------------------------------------------------------------
void SriOGLRenderer::DeleteTexture(unsigned int& openglTextureID)
{
	glDeleteTextures(1, &openglTextureID);
}

