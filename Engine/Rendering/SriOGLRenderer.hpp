#pragma once

#ifndef MY_OPENGL_RENDERER_H
#define MY_OPENGL_RENDERER_H

#include "Renderer.hpp"
#include "Engine\Core\EngineCommons.hpp"
#include "Engine\Core\ModelVertex.hpp"


class SriShaderManager;

//---------------------------------------------------------------------------------------------------
class SriOGLRenderer : public Renderer
{
public:
	SriOGLRenderer(void);
	~SriOGLRenderer(void);

	virtual void Initialize(void* platformHandle);
	virtual void InitializeAdvancedOpenGLFunctions();
	virtual void Clear(const RgbaColors& clearColor = RgbaColors(), unsigned int clearDepth = 1.f);
	virtual void PushMatrix();
	virtual void PopMatrix();
	virtual void SetLineWidth(float width);
	virtual void Rotate(float angle, float rotationOnX, float rotationOnY, float rotationOnZ);
	virtual void Translate(float translationOnX, float translationOnY, float translationOnZ);
	virtual void SetColor(const RgbaColors& colorToSet);
	virtual void LoadIdentityMatrix();
	virtual void SetUpPerspectiveProjection(float fieldOfViewY, float aspect, float zNearClipping, float zFarClipping);
	virtual void LoadOrtho(float left = 0.f, float right = 1600.f, float bottom = 0.f, float top = 900.f, float nearClippingPlane = 0.0, float farClippingPlane = 1.0);
	virtual void UnloadOrtho();
	virtual void GenerateTexture(unsigned int& sizeX, unsigned int& sizeY, unsigned int& textureID, const std::string& imageFilePath);
	virtual void GenerateDefaultDiffuseTexture(unsigned int& sizeX, unsigned int& sizeY, unsigned int& textureID);
	virtual void GenerateDefaultSpecularTexture(unsigned int& sizeX, unsigned int& sizeY, unsigned int& textureID);
	virtual void GenerateDefaultNormalTexture(unsigned int& sizeX, unsigned int& sizeY, unsigned int& textureID);
	virtual void DrawAxis(float xAxisLength = 1.f, float lineWidth = 1.f);
	virtual void DrawPoint(const Vector3DF& point, const RgbaColors& pointColor = RgbaColors(1.f, 1.f, 1.f, 1.f), float pointSize = 1.f);
	virtual void DrawPoints(const std::vector<Vector3DF>& points, const RgbaColors& pointColor = RgbaColors(1.f, 1.f, 1.f, 1.f), float pointSize = 1.f);
	virtual void DrawLine(const Vector3DF& startPoint, const Vector3DF& endPoint, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f), float lineWidth = 1.f);
	virtual void DrawLineSegment(const Vector3DF& startPoint, const Vector3DF& endPoint, const RgbaColors& lineStartColor, const RgbaColors& lineEndColor, float lineWidth = 1.f);
	virtual void DrawLines(const std::vector<Vector3DF>& positions, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f), float lineWidth = 1.f);
	virtual void DrawSquareOutline(const Vector3DF& topLeft, const Vector3DF& bottomleft, const Vector3DF& bottomRight, const Vector3DF& topRight, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f), float lineWidth = 1.f);
	virtual void DrawTexturedQuad(const Vector3DF& leftTop, const Vector3DF& leftBottom, const Vector3DF& rightBottom, const Vector3DF& rightTop, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f), SpriteSheet* sprite = nullptr, int spriteFrame = 0, const TBN& tbn = TBN());
	virtual void DrawQuad(const Vector3DF& leftTop, const Vector3DF& leftBottom, const Vector3DF& rightBottom, const Vector3DF& rightTop, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f), const Vector2DF& minCoords = Vector2DF(0.f, 0.f), const Vector2DF& maxCoords = Vector2DF(1.f, 1.f));
	virtual void DrawTexturedCube(const Vector3DF& cubeCenter, float lengthOfEachSide, const RgbaColors& color = RgbaColors(1.f, 1.f, 1.f, 1.f), SpriteSheet* diffuseTexture = nullptr, bool drawDebugOutline = false );
	virtual void DrawTexturedSquare(const Vector3DF& startPoint, float lengthOfEachSide, SpriteSheet* sprite = nullptr, int spriteFrame = 0, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f));
	virtual void DrawTexturedRect2D(const Vector2DF& leftTop, const Vector2DF& leftBottom, const Vector2DF& rightBottom, const Vector2DF& rightTop, SpriteSheet* sprite = nullptr, int spriteFrame = 0, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f)) ;
	virtual int GenerateVertexBuffer(const std::vector<ModelVertex>& vertexes, unsigned int* bufferId);
	virtual int SriOGLRenderer::GenerateIndexBuffer(const std::vector<unsigned int>& indexes, unsigned int* bufferId);
	virtual void DrawVBO(unsigned int vboId, unsigned int iboId, unsigned int bufferSize, unsigned int mode, bool hasValidIBO = true);
	virtual void DeleteBuffers(unsigned int* bufferID);
	virtual void EnableDepthBuffer();
	virtual void DisableDepthBuffer();
	virtual int LoadShader(const char* shaderFilePath, ShaderType type);
	virtual void BindAllTextures(SpriteSheet* diffuseTexture, SpriteSheet* normalTexture, SpriteSheet* specularTexture, SpriteSheet* emmisiveTexture);
	virtual void ReportShaderLinkError(int m_programId, const char* vertexShaderPath, const char* fragmentShaderPath);
	virtual void ReportShaderCompileError(int shaderId, const char* shaderPath);
	virtual void ParseOpenGLErrorMessage(const char* openglErrMsg, std::string& lineNumber, std::string& errorCode, std::string& errorText);
	// The following code is borrowed and slightly modified from a discussion on stackoverflow.com
	// http://stackoverflow.com/questions/5988686/creating-a-3d-sphere-in-opengl-using-visual-c
	// Author of the original post/code goes by the forum name datenwolf
	virtual void DrawSolidSphere(float radius, unsigned int numRings, unsigned int sectors, const Vector3DF& center);
	virtual void RenderCharacter(const Vector2DF& startPoint, float width, float height, const Vector2DF& min , const Vector2DF& max, SpriteSheet* sprite = nullptr, const RgbaColors& lineColor = RgbaColors(1.f, 1.f, 1.f, 1.f));
	virtual void DrawVertexArray(const std::vector<VertexData>& vertexes, unsigned int mode);
	virtual void DrawVertexArray(const std::vector<ModelVertex>& vertexes, unsigned int mode);
	virtual void SetFrustrum(float left, float right, float bottom, float top, float zNearClipping, float zFarClipping);
	virtual void SetCurrentShaderProgramID(int id);
	virtual void DisableAllTextures();
	virtual void SwapDeviceBuffers();
void DeleteTexture(unsigned int& openglTextureID);
	SriShaderManager*		m_shaderManager;
	Matrix4x4F				m_identityMatrix;
	std::vector<Matrix4x4F>	m_matrixStack;
	int m_currentShaderProgramId;
};

#endif