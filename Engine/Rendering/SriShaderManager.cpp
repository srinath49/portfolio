#include "SriShaderManager.hpp"

#define UNUSED(x) (void)(x);

std::map< std::string, SriShaderSource* >	SriShaderManager::s_shaderRegistry;
std::map< std::string, SriShaderSource* >::iterator SriShaderManager::shaderInMap;

SriShaderManager::SriShaderManager(void)
{

}


SriShaderManager::~SriShaderManager(void)
{
	std::map<std::string, SriShaderSource*>::iterator shaderRegistryIterator;
	for(shaderRegistryIterator = s_shaderRegistry.begin(); shaderRegistryIterator != s_shaderRegistry.end(); ++shaderRegistryIterator) 
	{
		delete shaderRegistryIterator->second;
	}
}

SriShaderSource* SriShaderManager::CreateOrGetSriShaderObject(const char* shaderFilePath, ShaderType type)
{
	SriShaderSource* returnShader = GetSriShaderByName(shaderFilePath);
	if(returnShader == nullptr)
	{
		returnShader = new SriShaderSource(shaderFilePath, type);
		s_shaderRegistry[shaderFilePath] = returnShader;
	}	
	return returnShader;
}

SriShaderSource* SriShaderManager::GetSriShaderByName(const char* shaderFilePath)
{
	SriShaderSource* returnShaderSource = nullptr;
	
	if (!s_shaderRegistry.empty())
	{
		shaderInMap = s_shaderRegistry.find(shaderFilePath);
		if (shaderInMap != s_shaderRegistry.end())
		{
			returnShaderSource = shaderInMap->second;
		}
	}
	return returnShaderSource;
}


int SriShaderManager::LoadShaderFromFileAndCompile(const char* shaderFilePath, ShaderType type)
{
	SriShaderSource* createdShader = nullptr;
	createdShader = CreateOrGetSriShaderObject(shaderFilePath, type);

	if(createdShader)
	{
		return createdShader->m_shaderObjectId;
	}
	else
	{
		return -1;
	}
}
