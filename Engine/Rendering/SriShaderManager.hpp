#pragma once

#ifndef SRI_SHADER_MANAGER_H
#define SRI_SHADER_MANAGER_H

#include <map>
#include <string>
#include "SriShaderSource.hpp"


class SriShaderManager
{
public:
	SriShaderManager(void);
	~SriShaderManager(void);

	int LoadShaderFromFileAndCompile(const char* shaderFilePath, ShaderType type);
	
private:
	static SriShaderSource* CreateOrGetSriShaderObject(const char* shaderFilePath, ShaderType type);
	static SriShaderSource* GetSriShaderByName(const char* shaderFilePath);

	static std::map< std::string, SriShaderSource* > s_shaderRegistry;
	static std::map< std::string, SriShaderSource* >::iterator shaderInMap;
};

#endif