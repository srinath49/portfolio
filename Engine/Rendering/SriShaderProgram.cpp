#include "Engine\Core\EngineCommons.hpp"
#include "Engine\Rendering\SriShaderProgram.hpp"
#include "Engine\Rendering\SriOGLRenderer.hpp"
#include "Engine\Rendering\OpenGlExtentions.hpp"


//---------------------------------------------------------------------------------------------------
SriShaderProgram::SriShaderProgram(int vertexShaderId, const char* vertexShaderPath, int fragmentShaderId, const char* fragmentShaderPath)
{
	m_programId = glCreateProgram();

	glAttachShader(m_programId, vertexShaderId);
	glAttachShader(m_programId, fragmentShaderId);

	glBindAttribLocation(m_programId, SSAL_VERTEXES,	"a_vertexes");
	glBindAttribLocation(m_programId, SSAL_COLOR,		"a_color");
	glBindAttribLocation(m_programId, SSAL_TEXCOORDS,	"a_texCoords");
	glBindAttribLocation(m_programId, SSAL_TANGENTS,	"a_tangent");
	glBindAttribLocation(m_programId, SSAL_BITANGENTS,	"a_bitangent");
	glBindAttribLocation(m_programId, SSAL_NORMALS,		"a_normals");

	glLinkProgram(m_programId);

	int wasSuccessful;

	glGetProgramiv(m_programId, GL_LINK_STATUS, &wasSuccessful);

	if (!wasSuccessful)
	{
		g_myRenderer->ReportShaderLinkError(m_programId, vertexShaderPath, fragmentShaderPath);
	}
}


SriShaderProgram::~SriShaderProgram(void)
{
}
