#pragma once

#ifndef SRI_SHADER_PROGRAM_H
#define SRI_SHADER_PROGRAM_H

class SriShaderProgram
{
public:
	
	SriShaderProgram(int vertexShaderId, const char* vertexShaderPath, int fragmentShaderId, const char* fragmentShaderPath);
	~SriShaderProgram(void);

public:
	int m_programId;
};

#endif