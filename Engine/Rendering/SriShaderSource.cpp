#include "Engine\Rendering\SriShaderSource.hpp"
#include "Engine\Core\EngineCommons.hpp"
#include <stdio.h>
#include "Engine\Rendering\SriOGLRenderer.hpp"
#include "Engine\Rendering\OpenGlExtentions.hpp"
#include "Engine\FileIO\FileSystem.hpp"

//---------------------------------------------------------------------------------------------------
SriShaderSource::SriShaderSource(const char* filename, ShaderType type)
{
	char* shaderTextBuffer = nullptr;
	FileLoadingMode loadedMode;
	int numBytesRead = LoadFileToNewBuffer(filename, shaderTextBuffer, loadedMode);

	if(numBytesRead > 0)
	{
		m_shaderObjectId = glCreateShader(type);

		glShaderSource(m_shaderObjectId, 1, &shaderTextBuffer, nullptr);

		glCompileShader(m_shaderObjectId);

		int wasSuccessful;
		glGetShaderiv(m_shaderObjectId, GL_COMPILE_STATUS, &wasSuccessful);

		if (!wasSuccessful)
		{
			g_myRenderer->ReportShaderCompileError(m_shaderObjectId, filename);
		}
	}
	if (shaderTextBuffer && loadedMode == FLM_FROM_DISK)
	{
		delete[] shaderTextBuffer;
	}
}


SriShaderSource::~SriShaderSource(void)
{

}

bool SriShaderSource::operator==(const SriShaderSource& rhs)
{
	bool returnVal = true;

	returnVal = (returnVal && (this->m_shaderSource == rhs.m_shaderSource)); 

	returnVal = (returnVal && (this->m_shaderObjectId == rhs.m_shaderObjectId));

	return returnVal;
}

bool SriShaderSource::operator!=(const SriShaderSource& rhs)
{
	bool returnVal = true;

	returnVal = (returnVal || (this->m_shaderSource != rhs.m_shaderSource)); 

	returnVal = (returnVal || (this->m_shaderObjectId != rhs.m_shaderObjectId));

	return returnVal;
}

void SriShaderSource::operator=(const SriShaderSource& rhs)
{
	m_shaderObjectId	= rhs.m_shaderObjectId;
	m_shaderSource		= rhs.m_shaderSource;
	m_shaderType		= rhs.m_shaderType;
	m_compilerLog		= rhs.m_compilerLog;
	m_isCompiled		= rhs.m_isCompiled;
	m_memAllocated		= rhs.m_memAllocated;
}


int SriShaderSource::LoadFileToNewBuffer(const char* shaderFilePath, char*& shaderTextBuffer, FileLoadingMode& loadedMode)
{
	int numChars = 0;
	LoadShaderFileToBuffer(shaderFilePath, shaderTextBuffer, numChars, loadedMode);
	return numChars; // No Error
}


