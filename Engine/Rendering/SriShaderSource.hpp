#pragma once

#ifndef SRI_SHADER_SOURCE_H
#define SRI_SHADER_SOURCE_H

#include <map>
#include "Engine\Core\EngineCommons.hpp"

class SriShaderSource;

//---------------------------------------------------------------------------------------------------
class SriShaderSource
{
public:
	friend class SriShaderManager;

	virtual ~SriShaderSource(void);

	bool operator==(const SriShaderSource& rhs);
	bool operator!=(const SriShaderSource& rhs);

	int LoadFileToNewBuffer(const char* shaderFilePath, char*& shaderTextBuffer, FileLoadingMode& loadedMode);
	bool Compile(void);            
	char* GetCompilerLog(void);    

private:
	SriShaderSource(const char* filename, ShaderType type);
	void operator=(const SriShaderSource& rhs);

protected:

	//ShaderType		m_shaderType;		// The shader type
	int					m_shaderType;
	unsigned int		m_shaderObjectId;	// Shader Object ID
	unsigned char*		m_shaderSource;		// ASCII Source-Code

	char*				m_compilerLog;		// CompilerLog

	bool				m_isCompiled;		// true if compiled
	bool				m_memAllocated;     // true if memory for shader source was allocated
};

#endif