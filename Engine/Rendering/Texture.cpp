//---------------------------------------------------------------------------
#include "Engine\Core\EngineCommons.hpp"
#include "Engine\Rendering\Texture.hpp"
#include "Engine\Rendering\SriOGLRenderer.hpp"

//---------------------------------------------------------------------------
std::map< std::string, Texture* >	Texture::s_textureRegistry;


//---------------------------------------------------------------------------
Texture::Texture(DefaultTextureType textureType) :
	m_openglTextureID( 0 ), 
	m_sizeX( 0 ), 
	m_sizeY( 0 )
{
	switch (textureType)
	{
		case DTT_DIFFUSE:
			g_myRenderer->GenerateDefaultDiffuseTexture(m_sizeX, m_sizeY, m_openglTextureID);
			break;
		case DTT_SPECULAR:
			g_myRenderer->GenerateDefaultSpecularTexture(m_sizeX, m_sizeY, m_openglTextureID);
			break;
		case DTT_NORMAL:
			g_myRenderer->GenerateDefaultNormalTexture(m_sizeX, m_sizeY, m_openglTextureID);
			break;
	}
}

//---------------------------------------------------------------------------
Texture::Texture( const std::string& imageFilePath) :
	m_openglTextureID( 0 ), 
	m_sizeX( 0 ), 
	m_sizeY( 0 )
{
	g_myRenderer->GenerateTexture(m_sizeX, m_sizeY, m_openglTextureID, imageFilePath);
}

//---------------------------------------------------------------------------------------------------
Texture* Texture::CreateDefaultDiffuseTexture()
{
	Texture* defaultDiffuse = GetTextureByName("DefaultDiffuseTexture");
	if(defaultDiffuse == nullptr)
	{
		defaultDiffuse = new Texture(DTT_DIFFUSE);
		s_textureRegistry["DefaultDiffuseTexture"] = defaultDiffuse;
	}
	return defaultDiffuse;
}


//---------------------------------------------------------------------------------------------------
Texture* Texture::CreateDefaultSpecularTexture()
{
	Texture* defaultSpecular = GetTextureByName("DefaultSpecularTexture");
	if(defaultSpecular == nullptr)
	{
		defaultSpecular = new Texture(DTT_SPECULAR);
		s_textureRegistry["DefaultSpecularTexture"] = defaultSpecular;
	}
	return defaultSpecular;
}

//---------------------------------------------------------------------------------------------------
Texture* Texture::CreateDefaultNormalTexture()
{
	Texture* defaultNormal = GetTextureByName("DefaultNormalTexture");
	if(defaultNormal == nullptr)
	{
		defaultNormal = new Texture(DTT_NORMAL);
		s_textureRegistry["DefaultNormalTexture"] = defaultNormal;
	}
	return defaultNormal;
}

//---------------------------------------------------------------------------
// Returns a pointer to the already-loaded texture of a given image file,
//	or nullptr if no such texture/image has been loaded.
//
Texture* Texture::GetTextureByName( const std::string& imageFilePath )
{
	Texture* returnTexture = nullptr;
	static std::map< std::string, Texture* >::const_iterator textureInMap;
	textureInMap = s_textureRegistry.find(imageFilePath);
	if(textureInMap != s_textureRegistry.end())
	{
		returnTexture = textureInMap->second;
	}
	return returnTexture;
}


//---------------------------------------------------------------------------
// Finds the named Texture among the registry of those already loaded; if
//	found, returns that Texture*.  If not, attempts to load that texture,
//	and returns a Texture* just created (or nullptr if unable to load file).
//
Texture* Texture::CreateOrGetTexture( const std::string& imageFilePath )
{
	Texture* returnTexture = GetTextureByName(imageFilePath);
	if(!returnTexture)
	{
		returnTexture = new Texture(imageFilePath);
		s_textureRegistry[imageFilePath] = returnTexture;
	}	
	return returnTexture;
}

Texture::~Texture(void)
{
	std::map<std::string, Texture*>::iterator textureRegistryIterator;
	for(textureRegistryIterator = s_textureRegistry.begin(); textureRegistryIterator != s_textureRegistry.end(); ++textureRegistryIterator) 
	{
		g_myRenderer->DeleteTexture(textureRegistryIterator->second->m_openglTextureID);  
		delete textureRegistryIterator->second;
	}
}
