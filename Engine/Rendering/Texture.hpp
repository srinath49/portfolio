#pragma once
#include <string>
#include <map>


enum DefaultTextureType
{
	DTT_DIFFUSE,
	DTT_SPECULAR,
	DTT_NORMAL
};

//---------------------------------------------------------------------------------------------------
class Texture
{
public:
	~Texture(void);
	static Texture* GetTextureByName( const std::string& imageFilePath );
	static Texture* CreateOrGetTexture( const std::string& imageFilePath );
	static Texture* CreateDefaultDiffuseTexture();
	static Texture* CreateDefaultSpecularTexture();
	static Texture* CreateDefaultNormalTexture();

public:
	unsigned int m_openglTextureID;
	unsigned int m_sizeX;
	unsigned int m_sizeY;
	static std::map< std::string, Texture* >	s_textureRegistry;

private:
	Texture(DefaultTextureType textureType);
	Texture( const std::string& imageFilePath);
};

