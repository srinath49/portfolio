#include "Engine/ZipUtils/ZipUtils.hpp"
#include "Engine/ZipUtils/unzip.h"
#include "Engine/ZipUtils/zip.h"


#include <tchar.h>
#include <atlconv.h>


void LoadFileFromZipToBuffer(unsigned char*& buffer, const std::string& fileName, unsigned int& bufferLength)
{
	HZIP hz = OpenZip(_T("Data.zip"), 0);
	ZIPENTRY ze; 
	int i;

	USES_CONVERSION;
	WCHAR* name = A2T(fileName.c_str());
	
	FindZipItem(hz, name, true, &i, &ze);
	if (hz)
	{
		buffer = new unsigned char[ze.unc_size];
		bufferLength = ze.unc_size;
		UnzipItem(hz, i, buffer, ze.unc_size);
	}
	CloseZip(hz);
}
