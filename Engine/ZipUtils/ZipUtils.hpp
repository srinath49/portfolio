#pragma once

#ifndef ZIP_UTILS_H
#define ZIP_UTILS_H

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <string>


void LoadFileFromZipToBuffer(unsigned char*& buffer, const std::string& fileName, unsigned int& bufferLength);

#endif
