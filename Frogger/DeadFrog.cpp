#include "DeadFrog.hpp"
#include "Time.hpp"

DeadFrog::DeadFrog(const Vector2DF& position, float collisionRadius)
	: Entity(position, collisionRadius, "", "Images/DeathAnim.png", 4, 1)
{
	m_spriteSheet->m_animSecondsPerFrame = 0.3f;
	m_spawnedTime = GetCurrentTimeSeconds();
}


DeadFrog::~DeadFrog(void)
{
}

void DeadFrog::Update()
{
	if(!m_isAlive)
	{
		return;
	}
	if (GetCurrentTimeSeconds() - m_spawnedTime > 1.2)
	{
		m_isAlive = false;
		return;
	}
	m_spriteSheet->Update();
}

void DeadFrog::Render()
{
	if(!m_isAlive)
	{
		return;
	}
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(m_position.x, m_position.y, 0.f);
	m_spriteSheet->Draw(m_collisionRadius);
	glPopMatrix();
}
