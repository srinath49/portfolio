#pragma once
#include "entity.hpp"
class DeadFrog :
	public Entity
{
public:
	DeadFrog(const Vector2DF& position, float collisionRadius);
	~DeadFrog(void);

	void Update();
	void Render();
	double m_spawnedTime;
};

