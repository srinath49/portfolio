#include "EndPointFrog.hpp"
#include "TheGame.hpp"
#include "WorldMap.hpp"
#include "Time.hpp"


EndPointFrog::EndPointFrog(const Vector2DF& position, float collisionRadius, const std::string& tag, const std::string& textureName) 
	: Entity(position, collisionRadius, tag, textureName)
{
	m_pointReached = false;
	m_hundredSprite = new SpriteSheet("Images/100.png", Vector2DI(1,1));
}


EndPointFrog::~EndPointFrog(void)
{
}

void EndPointFrog::Update()
{

}

void EndPointFrog::Render()
{
	if(m_pointReached)
	{
		glEnable(GL_TEXTURE_2D);
		glPushMatrix();
		glTranslatef(m_position.x, m_position.y, 0.f);
		m_spriteSheet->Draw(m_collisionRadius);
		glPopMatrix();
		if(GetCurrentTimeSeconds() - m_reachdTime < 1.5f)
		{
			glPushMatrix();
			glTranslatef(m_position.x + 13.f, m_position.y + 20.f, 0.f);
			m_hundredSprite->Draw(m_collisionRadius);
			glPopMatrix();
		}
	}
}


void EndPointFrog::OnCollision(Entity* collidedWith)
{
	if (collidedWith->m_tag == "PlayerFrog")
	{
		m_pointReached = true;
		m_reachdTime = GetCurrentTimeSeconds();
		m_gameReference->m_world->PlaySound("Sounds/ReachEndSound.wav", 1.f);
	}
}
