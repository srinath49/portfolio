#pragma once
#include "entity.hpp"

class EndPointFrog :
	public Entity
{
public:
	EndPointFrog(const Vector2DF& position, float collisionRadius, const std::string& tag = "EndPoint", const std::string& textureName = "Images/WinFrog.png");
	~EndPointFrog(void);

	void Update();
	void Render();
	void OnCollision(Entity* collidedWith);

public:
	bool m_pointReached;
	double m_reachdTime;
	SpriteSheet* m_hundredSprite;
};

