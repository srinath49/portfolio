#include "Engine.hpp"
#include "Time.hpp"
#include "Vector2DI.hpp"
#include "Vector2DF.hpp"
#include <math.h>
#include "TheGame.hpp"

const float secondsPerFrame = 1.f/45.f;
double g_deltaTime = GetCurrentTimeSeconds();

Engine::Engine(void)
{
	m_frameNumber = 0;
	m_currentFrame = 0;
	m_previousFrame = 0;
	m_frameChanged = true;
	m_pause = false;
	g_deltaTime = GetCurrentTimeSeconds();
}


Engine::~Engine(void)
{
	
}

void Engine::Render(TheGame* game)
{
	// PreRender Code:

	// End Of PreRender!

	// Render Game:
	game->Render();  // The Game Specific Render
	// End of Game Render!

	// PostRender Code:

	// End Of PostRender!
}

void Engine::Update()
{
	//UpdateGame(++m_frameNumber);
	if(GetCurrentTimeSeconds() - g_deltaTime >= secondsPerFrame)
	{
		//game.Update();
		g_deltaTime = GetCurrentTimeSeconds();
	}
}

