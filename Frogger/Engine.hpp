#pragma once

#ifndef _INCLUDED_ENGINE_H_
#define _INCLUDED_ENGINE_H_

#include <string>
#include <list>
#include "GameCommons.hpp"

class Entity;
class Vector2DF;
class Vector2DI;
class WorldMap;
class TheGame;


enum EngineState
{
	RequestForLevelLoad,
	LoadTheLevel,
	ReloadTheLevel,
	LoadTheNextLevel,
	LoadingLevel,
	RunningLevel,
};


class Engine
{

private:
	

protected:
	unsigned long		m_frameNumber;		/** The current frame */
	bool				m_frameChanged;
	unsigned long int	m_previousFrame;
	unsigned long int	m_currentFrame;

	//double				m_deltaTime;

public:
	
	//Default Constructor
	Engine();

	//Destructor
	~Engine();

	void Render(TheGame* game);

	void Update();


	/*************************************VIRTUAL FUNCTIONS******************************************/
	/************************************************************************************************/
	/* The Following are virtual function to be implemented by the sub-class -> "TheGame"
	/************************************************************************************************/

	/*virtual void StartGame() = 0;

	virtual void UpdateGame(unsigned long currentFrameNumber) = 0;

	virtual void RenderGame() = 0;

	virtual void OnPause() = 0;

	virtual void OnResume() = 0;*/
	
	/**********************************END OF VIRTUAL FUNCTIONS***************************************/
	
	/*WorldCoords TileCoordsToWorldCoords(TileCoords tileCoords);
	TileCoords WorldCoordsToTileCoords(WorldCoords worldCoords);
	TileIndex GetTileIndexAt(TileCoords tileCoords);
	TileIndex GetTileIndexAt(WorldCoords worldCoords);

	float GetDistance(Vector2DF position1, Vector2DF position2);
	int GetDistance(Vector2DI position1, Vector2DI position2);
	float GetDistanceSquared(Vector2DF position1, Vector2DF position2);
	int GetDistanceSquared(Vector2DI position1, Vector2DI position2);

	bool IsPointInCircle(Vector2DF point, float radius, Vector2DF center);
	bool IsPointInCircle(Vector2DI point, float radius, Vector2DF center);

	float GetPenetrationDepth(float penetratingPoint, float penetratedPoint);
	int	  GetPenetrationDepth(int penetratingPoint, int penetratedPoint);
*/

public:
	bool		m_pause;
};

#endif // End of _INCLUDE_ENGINE_