#include "Entity.hpp"
#include "Engine.hpp"
#include "Texture.hpp"
#include "std_image.h"
#include "TheGame.hpp"

Entity::Entity(const Vector2DF& position, float collisionRadius, const std::string& tag, const std::string& textureName , int tilesWide, int tilesTall)
	: m_currentRotationDegrees( 0.f )
{
	m_gameReference = TheGame::GetGameInstance();
	m_engineReference = m_gameReference->m_engineReference;

	m_position = position;
	if(textureName != "")
	{
		m_spriteSheet = new SpriteSheet(textureName, Vector2DI(tilesWide,tilesTall));
		m_currentTexture = m_spriteSheet->GetCurrentTexture();
	}
	SetCollisionRadius(collisionRadius);
	m_isAlive = true;

	m_tag = tag;
}

Entity::~Entity(void)
{

}


void Entity::Destroy()
{
	m_isAlive = false;
	OnDestroy();
}

Vector2DF Entity::GetPosition() const
{
	return m_position;
}

Vector2DF Entity::GetLinearVelocity() const
{
	return m_linearVelocity;
}

float Entity::GetAngularVelocity() const
{
	return m_angularVelocity;
}

float Entity::GetCurrentRotation() const
{
	return m_currentRotationDegrees;
}

float Entity::GetCollisionRadius() const
{
	return m_collisionRadius;
}

float Entity::GetCollisionRadiusSquared() const
{
	return m_collisionRadiusSquare;
}

bool Entity::GetIsAlive() const
{
	return m_isAlive;
}


void Entity::SetPosition(const Vector2DF& newPosition)
{
	m_position = newPosition;
}

void Entity::SetLinearVelocity(const Vector2DF& newVelocity)
{
	m_linearVelocity = newVelocity;
}

void Entity::SetAngularVelocity(float newAngularVelocity)
{
	m_angularVelocity = newAngularVelocity;
}

void Entity::SetCurrentRotation(float newCurrentRotation)
{
	m_currentRotationDegrees = newCurrentRotation;
}

void Entity::SetCollisionRadius(float newCollisionRadius)
{
	m_collisionRadius = newCollisionRadius;
	m_collisionRadiusSquare = (m_collisionRadius)*(m_collisionRadius);
}

void Entity::SetIsAlive(bool newAlive)
{
	m_isAlive = newAlive;
}

Vector2DF Entity::GetCenter() const
{
	Vector2DF center;
	center.x = m_position.x - m_collisionRadius;
	center.y = m_position.y - m_collisionRadius;
	return center;
}

