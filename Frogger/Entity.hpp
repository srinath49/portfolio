#pragma once
#ifndef _INCLUDED_ENTITY_
#define _INCLUDED_ENTITY_

#define WIN32_LEAN_AND_MEAN
#include "Vector2DF.hpp"
#include "Vector2DI.hpp"
#include <windows.h>
#include <gl/gl.h>
#include "Texture.hpp"
#include "SpriteSheet.hpp"
#include <string>
#include <list>
#include <vector>

class Engine;
class Texture;
class TheGame;

class Entity
{
public:
	/************************************************************************/
	/*					Constructors and Destructor	    				    */
	/************************************************************************/
	Entity(const Vector2DF& position, float collisionRadius, const std::string& Tag = "", const std::string& textureName = "", int tilesWide = 1, int tilesTall = 1 );
	~Entity(void);
	void Destroy();

	/************************************************************************/
	/*							Virtual Functions	    				    */
	/************************************************************************/
	virtual void Update() = 0;
	virtual void Render() {};
	virtual void OnDestroy() {};
	virtual void OnCollision(Entity* collidedWith){};

	/************************************************************************/
	/*							Getter Functions	    				    */
	/************************************************************************/
	Vector2DF GetPosition() const; 
	Vector2DF GetCenter() const;
	Vector2DF GetLinearVelocity() const;
	float GetAngularVelocity() const; 
	float GetCurrentRotation() const; 
	float GetCollisionRadius() const; 
	float GetCollisionRadiusSquared() const;
	bool GetIsAlive() const; 


	/************************************************************************/
	/*							Setter Functions	    				    */
	/************************************************************************/
	void SetPosition(const Vector2DF& newPosition); 
	void SetLinearVelocity(const Vector2DF& newVelocity); 
	void SetAngularVelocity(float newAngularVelocity); 
	void SetCurrentRotation(float newCurrentRotation); 
	void SetCollisionRadius(float newCollisionRadius); 
	void SetIsAlive(bool newAlive); 

protected:
	Vector2DF				m_position;				
	Vector2DF				m_linearVelocity;
	float					m_angularVelocity;
	float					m_currentRotationDegrees;
	float					m_collisionRadius;	
	float					m_collisionRadiusSquare;
	float					m_drawScaleX;			
	float					m_drawScaleY;	

public:
	Texture*				m_currentTexture;
	SpriteSheet*			m_spriteSheet;
	Engine*					m_engineReference;
	TheGame*				m_gameReference;
	bool					m_isAlive;	
	std::string				m_tag;
	std::vector<Entity*>	m_inCollisionWith;
};

#endif // _INCLUDED_GAME_OBJECT_