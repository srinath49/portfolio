#include "Frog.hpp"
#include "Engine.hpp"
#include "WorldMap.hpp"
#include "TheGame.hpp"
#include "Tile.hpp"
#include "GameCommons.hpp"
#include "Score.hpp"
#include "DeadFrog.hpp"
#include "XBoxController.hpp"

const Vector2DF g_frogStartPosition = Vector2DF((tileWidth*3), tileHalfY);

const int g_frogIdleFrame = 0;
const int g_frogJumpFrame = 1;

const float g_faceUpAngleDegrees	 = 0.f;
const float g_faceDownAngleDegrees   = 180.f;
const float g_faceRightAngleDegrees  = -90.f;
const float g_faceLeftAngleDegrees   = 90.f;

const int g_firstTileX = 1;
const int g_firstTileY = 0;
const int g_lastTileX  = numTilesWide - 2;
const int g_lastTileY  = numTilesHigh - 1;

const float g_frogSpeed	=	tileHeight*0.2f;

Frog::Frog(const Vector2DF& position, float collisionRadius, const std::string& tag, const std::string& textureName, int tilesWide, int tilesTall)
	: Entity(position, collisionRadius, tag, textureName, tilesWide, tilesTall)
{
	
	m_spriteSheet->m_animSecondsPerFrame = 0.05f;
	m_currentFrame		=	g_frogIdleFrame; 


	m_killFrog			=	false;
	m_spawnAnotherFrog	=	false;

	
	m_frogState			=	IDLE;

	m_onObject			=	nullptr;

	m_position = g_frogStartPosition;

	m_numFrogsLeft = 5;

	m_maxY = 0;
	m_currentY = 0;
	
	Vector2DF scorePosition;
	scorePosition.y = 850.f;
	
	if(m_isPlayerOne)
	{
		scorePosition.x = 250.f;
	}
	else
	{
		scorePosition.x = 550.f;
	}
	
	m_score = new Score(scorePosition, 12.f);
	m_gameReference->m_world->m_UIEntities.push_back(m_score);

	m_numEndPointReached = 0;

	m_xBoxController = new XBoxController();
}

Frog::~Frog(void)
{

}

void Frog::Update()
{
	if(m_numFrogsLeft <= 0)
	{
		m_gameReference->GameOver();
		return;
	}
	if(m_numEndPointReached >= 5)
	{
		m_gameReference->GameWon();
		return;
	}
	GetInput();
	ProcessInput();

	if(m_isAlive)
	{
		if(m_killFrog)
		{
			m_isAlive = false;
			m_killFrog = false;
			DeadFrog* deadFrog = new DeadFrog(m_position, m_collisionRadius);
			m_gameReference->m_world->m_Layer2Entities.push_back(deadFrog);
			if (m_numFrogsLeft > 0)
			{
				m_spawnAnotherFrog = true;
			}
			return;
		}
	}
	else
	{
		if(m_spawnAnotherFrog)
		{
			ReSpawnFrog();
			m_spawnAnotherFrog = false;
		}
		return;
	}

	m_spriteSheet->Update();
	
	MoveFrog();

	UpdateTile();

	UpdateOnObject();

	if(IsFrogInWater())
	{
		m_killFrog = true;
		m_gameReference->m_world->PlaySound("Sounds/SplashSound.wav");
	}
}

void Frog::Render()
{
	if(!m_isAlive)
	{
		return;
	}
	glEnable(GL_TEXTURE_2D);

	glPushMatrix();
	glTranslatef(m_position.x, m_position.y, 0.f);
	glRotatef(m_currentRotationDegrees, 0.f, 0.f, 1.f);
	
	switch (m_frogState)
	{
		case IDLE:
			m_spriteSheet->DrawFrame(g_frogIdleFrame, m_collisionRadius);
			break;
		default:
			m_spriteSheet->DrawFrame(g_frogJumpFrame, m_collisionRadius);
			break;
	}
	
	glPopMatrix();
	
}

void Frog::GetInput()
{
	//if(m_isPlayerOne)
	//{
		CheckKeyboardInput();
	//}

	CheckControllerInput();
}

void Frog::CheckControllerInput()
{
	if(m_xBoxController->HasController())
	{
		int buttonPressedValue = (int)m_xBoxController->GetButtonPressedValue();
		int x;
		switch(buttonPressedValue)
		{
			case 1: 
				m_isWKeyPressed = true;
				x= 0;
				break;
			case 2:
				m_isSKeyPressed = true;
				x= 0;
				break;
			case 4:
				m_isAKeyPressed = true;
				x= 0;
				break;
			case 8:
				m_isDKeyPressed = true;
				x= 0;
				break;
			case 16:
				m_gameReference->TogglePause();
				break;
		}
	}
}

void Frog::CheckKeyboardInput()
{
	if(GetAsyncKeyState(VK_DELETE))
	{
		m_killFrog = true;
	}
	else
	{
		//m_killFrog = false;
	}

	if(GetAsyncKeyState(VK_BACK))
	{
		if(!m_isAlive)
		{
			m_spawnAnotherFrog = true;
		}
	}

	if(GetAsyncKeyState(0x41)) // 0x41 = 'A' Key
	{
		m_isAKeyPressed = true;
	}
	else
	{
		m_isAKeyPressed = false;
	}

	if(GetAsyncKeyState(0x44)) // = 'D' Key
	{
		m_isDKeyPressed = true;
	}
	else
	{
		m_isDKeyPressed = false;
	}

	if(GetAsyncKeyState(0x57)) // 0x57 = 'W' Key
	{
		m_isWKeyPressed = true;
	}
	else
	{
		m_isWKeyPressed = false;
	}

	if(GetAsyncKeyState(0x53)) // 0x53 = 'S' Key
	{
		m_isSKeyPressed = true;
	}
	else
	{
		m_isSKeyPressed = false;
	}
}

void Frog::ProcessInput()
{
	if(!m_isAlive)
	{
		if(m_spawnAnotherFrog)
		{
			ReSpawnFrog();
			m_spawnAnotherFrog = false;
		}
		return;
	}
	if(m_isWKeyPressed)
	{
		if(!m_frogMoveKeyHeld && m_frogState == IDLE)
		{

			SetFrogState(MOVING_UP);
			m_gameReference->m_world->PlaySound("Sounds/FrogSound.wav");

			if(m_onObject == nullptr)
			{
				m_targetTile = m_northTile;
				m_targetLocation = m_gameReference->m_world->m_tiles[GetTileIndexAt(m_targetTile)].m_tileCenter;
			}
			else
			{

				float targetYLocation = m_position.y + tileHeight;
				m_targetLocation = Vector2DF(m_position.x, targetYLocation);
			}	

		}
	}

	if(m_isSKeyPressed)
	{
		if(!m_frogMoveKeyHeld && m_frogState == IDLE)
		{

			SetFrogState(MOVING_DOWN);
			m_gameReference->m_world->PlaySound("Sounds/FrogSound.wav");

			if(m_onObject == nullptr)
			{
				m_targetTile = m_southTile;
				m_targetLocation = m_gameReference->m_world->m_tiles[GetTileIndexAt(m_targetTile)].m_tileCenter;
			}
			else
			{

				float targetYLocation = m_position.y - tileHeight;
				m_targetLocation = Vector2DF(m_position.x, targetYLocation);
			}
		}
	}

	if(m_isAKeyPressed)
	{
		if(!m_frogMoveKeyHeld && m_frogState == IDLE)
		{
			SetFrogState(MOVING_LEFT);
			m_gameReference->m_world->PlaySound("Sounds/FrogSound.wav");

			if(m_onObject == nullptr)
			{
				m_targetTile = m_westTile;
				m_targetLocation = m_gameReference->m_world->m_tiles[GetTileIndexAt(m_targetTile)].m_tileCenter;
			}
			else
			{

				float targetXLocation = m_position.x - m_onObject->GetCollisionRadius()*2.f;
				m_targetLocation = Vector2DF(targetXLocation, m_position.y);
			}
		}
	}

	if(m_isDKeyPressed)
	{
		if(!m_frogMoveKeyHeld && m_frogState == IDLE)
		{
	
			SetFrogState(MOVING_RIGHT);
			m_gameReference->m_world->PlaySound("Sounds/FrogSound.wav");

			if(m_onObject == nullptr)
			{
				m_targetTile = m_eastTile;
				m_targetLocation = m_gameReference->m_world->m_tiles[GetTileIndexAt(m_targetTile)].m_tileCenter;
			}
			else
			{

				float targetXLocation = m_position.x + m_onObject->GetCollisionRadius()*2.f;
				m_targetLocation = Vector2DF(targetXLocation, m_position.y);
			}
		}
	}
	
	if(m_isWKeyPressed || m_isAKeyPressed || m_isSKeyPressed || m_isDKeyPressed)
	{
		m_frogMoveKeyHeld = true;
	}
	else
	{
		m_frogMoveKeyHeld = false;
	}
}

void Frog::MoveFrog()
{

	if(m_onObject != nullptr)
	{
		m_position += m_linearVelocity;
	}

	switch (m_frogState)
	{
	case MOVING_UP:
		m_currentRotationDegrees = g_faceUpAngleDegrees; 
		if(m_position.y < m_targetLocation.y)
		{
			m_position.y += g_frogSpeed;
		}
		else
		{
			m_position.y = m_targetLocation.y;  
			m_currentY = WorldCoordsToTileCoords(m_targetLocation).y;
			if(m_currentY > m_maxY)
			{
				m_score->m_scoreTotal += 10;
				m_maxY += 1;
			}
			SetFrogState(IDLE);
		}
		break;
	case MOVING_DOWN:
		m_currentRotationDegrees = g_faceDownAngleDegrees; 

		if(m_position.y > m_targetLocation.y)
		{
			m_position.y -= g_frogSpeed;
		}
		else
		{
			m_position.y = m_targetLocation.y; 
			SetFrogState(IDLE);
		}
		break;
	case MOVING_RIGHT:
		m_currentRotationDegrees = g_faceRightAngleDegrees; 

		if(m_position.x < m_targetLocation.x)
		{
			m_position.x += g_frogSpeed;
		}
		else
		{
			m_position.x = m_targetLocation.x;
			SetFrogState(IDLE);
		}
		break;
	case MOVING_LEFT:
		m_currentRotationDegrees = g_faceLeftAngleDegrees; 

		if(m_position.x > m_targetLocation.x)
		{
			m_position.x -= g_frogSpeed;
		}
		else
		{
			m_position.x = m_targetLocation.x;
			SetFrogState(IDLE);
		}
		break;
	case IDLE:
		break;
	}
}

void Frog::SetFrogState(FrogState newState)
{
	m_frogState = newState;
}

void Frog::UpdateTile()
{
	m_currentTile = WorldCoordsToTileCoords(m_position);
	m_northTile = TileCoords(m_currentTile.x, m_currentTile.y+1);
	m_southTile = TileCoords(m_currentTile.x, m_currentTile.y-1);
	m_eastTile = TileCoords(m_currentTile.x+1, m_currentTile.y);
	m_westTile = TileCoords(m_currentTile.x-1, m_currentTile.y);

	if(m_westTile.x < g_firstTileX)
	{
		m_westTile = m_currentTile;
	}
	
	if(m_southTile.y < g_firstTileY)
	{
		m_southTile = m_currentTile;
	}
	
	if (m_eastTile.x > g_lastTileX)
	{
		m_eastTile = m_currentTile;
	}

	if(m_northTile.y > g_lastTileY)
	{
		m_northTile = m_currentTile;
	}
}

void Frog::OnCollision(Entity* collidedWith)
{
	if(!m_isAlive)
	{
		return;
	}
	if(collidedWith->m_tag == "Bad")
	{
		m_killFrog = true;
		m_gameReference->m_world->PlaySound("Sounds/SquishSound.wav");
		if (m_numFrogsLeft > 0)
		{
			m_spawnAnotherFrog = true;
		}
	}
	else if(collidedWith->m_tag == "Good")
	{
		LandFrogOnObject(collidedWith);
	}
	else if (collidedWith->m_tag == "EndPoint")
	{
		m_score->m_scoreTotal += 100;
		SetFrogState(IDLE);
		m_position = g_frogStartPosition;
		collidedWith->OnCollision(this);
		m_maxY = 0;
		m_currentY = 0;

		int endPointsIndex = 0;
		for(endPointsIndex = 0; endPointsIndex < (int)m_endPointsReached.size(); endPointsIndex++)
		{
			if(collidedWith->GetPosition() == m_endPointsReached[endPointsIndex])
			{
				break;
			}
		}
		if (endPointsIndex >= (int)m_endPointsReached.size())
		{
			m_endPointsReached.push_back(collidedWith->GetPosition());
			m_numEndPointReached++;
		}
	}
}

void Frog::LandFrogOnObject(Entity* objectToLandOn)
{
	m_position.y = objectToLandOn->GetPosition().y;
	m_linearVelocity = objectToLandOn->GetLinearVelocity();
	m_onObject = objectToLandOn;
	//SetFrogState(IDLE);
}

void Frog::ReSpawnFrog()
{
	m_position = g_frogStartPosition;
	m_currentRotationDegrees = g_faceUpAngleDegrees;
	m_maxY = 0;
	m_currentY = 0;
	m_frogState = IDLE;
	if(!m_isAlive)
	{
		m_isAlive = true;
		m_numFrogsLeft--;
		if(m_numFrogsLeft < 0)
		{
			m_numFrogsLeft = 0;
		}
	}
}

void Frog::UpdateOnObject()
{
	if(m_onObject != nullptr)
	{
		int inCollisionIndex;

		for(inCollisionIndex = 0; inCollisionIndex < (int)m_inCollisionWith.size(); inCollisionIndex++)
		{
			if( m_onObject == m_inCollisionWith.at(inCollisionIndex) )
			{
				return;
			}
		}

		if(inCollisionIndex >= (int)m_inCollisionWith.size() && m_inCollisionWith.size() <= 0)
		{
			m_onObject = nullptr;
		}
	}
}

bool Frog::IsFrogInWater()
{
	if(m_currentTile.y > 6 && m_onObject == nullptr && m_frogState == IDLE)
	{
		return true;
	}

	return false;
}

void Frog::CheckStartButton()
{
	if(m_xBoxController->HasController())
	{
		int buttonPressedValue = (int)m_xBoxController->GetButtonPressedValue();
		int x;
		switch(buttonPressedValue)
		{
			case 16:
				m_gameReference->TogglePause();
				break;
		}
	}
}
