#pragma once
#include "entity.hpp"

class Score;
class XBoxController;

typedef Vector2DI TileCoords;

enum FrogState
{
	MOVING_UP,
	MOVING_DOWN,
	MOVING_RIGHT, 
	MOVING_LEFT,
	IDLE,
};

class Frog :
	public Entity
{
public:
	Frog(const Vector2DF& position, float collisionRadius, const std::string& tag = "", const std::string& textureName = "", int tilesWide = 1, int tilesTall = 1);
	~Frog(void);

	void Update();
	void Render();
	void OnCollision(Entity* collidedWith);

	void GetInput();
	void CheckControllerInput();
	void CheckKeyboardInput();
	void ProcessInput();

	void SetFrogState(FrogState MOVEUP);
	void UpdateTile();
	void MoveFrog();
	void LandFrogOnObject(Entity* objectToLandOn);
	void ReSpawnFrog();
	void UpdateOnObject();
	bool IsFrogInWater();
	void CheckStartButton();
public:

	bool m_isPlayerOne;
	bool m_isAKeyPressed;
	bool m_isDKeyPressed;
	bool m_isWKeyPressed;
	bool m_isSKeyPressed;

	bool m_frogMoveKeyHeld;

	bool m_killFrog;
	bool m_spawnAnotherFrog;

	FrogState m_frogState;

	Vector2DF m_targetLocation;
	TileCoords m_targetTile;
	TileCoords m_currentTile;
	TileCoords m_northTile;
	TileCoords m_southTile;
	TileCoords m_eastTile;
	TileCoords m_westTile;
	
	int m_currentFrame;
	
	Entity* m_onObject;
	int m_numFrogsLeft;
	int m_currentY;
	int m_maxY;
	Score* m_score;
	int m_numEndPointReached;

	std::vector<Vector2DF> m_endPointsReached;
	XBoxController* m_xBoxController;
};

