#include "GameCommons.hpp"
#include <math.h>


WorldCoords TileCoordsToWorldCoords(const TileCoords& tileCoords)
{
	WorldCoords returnCoords;
	returnCoords.x = tileCoords.x * tileWidth;
	returnCoords.y = tileCoords.y * tileHeight;
	return returnCoords;
}

TileCoords WorldCoordsToTileCoords(const WorldCoords& worldCoords)
{
	TileCoords returnCoords;
	returnCoords.x = (int)((worldCoords.x + tileHalfX) * oneOverTileWidth);
	returnCoords.y = (int)((worldCoords.y ) * oneOverTileHeight);
	return returnCoords;
}

TileIndex GetTileIndexAt(TileCoords& tileCoords)
{
	TileIndex returnIndex;
	if(tileCoords.y >= numTilesHigh)
	{
		tileCoords.y = 0;
	}
	else if(tileCoords.y < 0)
	{
		tileCoords.y = numTilesHigh-1;
	}
	if(tileCoords.x >= numTilesWide)
	{
		tileCoords.x = 0;
	}
	else if(tileCoords.x < 0)
	{
		tileCoords.x = numTilesWide-1;
	}

	returnIndex = tileCoords.y*numTilesWide + tileCoords.x;

	return returnIndex;
}

TileIndex GetTileIndexAt(const WorldCoords& worldCoords)
{
	TileIndex returnIndex; 
	TileCoords tileCoords = WorldCoordsToTileCoords(worldCoords);

	returnIndex = GetTileIndexAt(tileCoords);

	return returnIndex;
}

float GetDistance(const Vector2DF& position1, const Vector2DF& position2)
{
	float squareRoot = sqrtf( GetDistanceSquared( position1, position2 ) );
	return squareRoot;
}

int GetDistance(const Vector2DI& position1, const Vector2DI& position2)
{
	return (int)sqrtf((float)GetDistanceSquared(position1, position2));
}

float GetDistanceSquared(const Vector2DF& position1, const Vector2DF& position2)
{
	float p1 = position2.x - position1.x;
	float p2 = position2.y - position1.y;

	p1*=p1;
	p2*=p2;

	return (p1+p2);
}

int GetDistanceSquared(const Vector2DI& position1, const Vector2DI& position2)
{
	int p1 = position2.x - position1.x;
	int p2 = position2.y - position1.y;

	p1*=p1;
	p2*=p2;

	return (p1+p2);
}

bool IsPointInCircle(const Vector2DF& point, float radius, const Vector2DF& center)
{
	if (GetDistanceSquared(point, center) <= (radius*radius))
	{
		return true;
	}
	return false;
}

bool IsPointInCircle(const Vector2DI& point, float radius, const Vector2DF& center)
{
	if (GetDistanceSquared(Vector2DF((float)point.x*69, (float)point.y*69), center) <= (radius*radius))
	{
		return true;
	}
	return false;
}

float GetPenetrationDepth(float penetratingPoint, float penetratedPoint)
{
	return penetratingPoint-penetratedPoint;
}

int GetPenetrationDepth(int penetratingPoint, int penetratedPoint)
{
	return penetratingPoint-penetratedPoint;
}