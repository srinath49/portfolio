#pragma once
#include "Vector2DF.hpp"
#include "Vector2DI.hpp"

typedef Vector2DF WorldCoords;
typedef Vector2DI TileCoords;
typedef int TileIndex;

const int numTilesHigh = 13;
const int numTilesWide = 14;
const float tileWidth = 61.5f;
const float tileHeight = 61.5f;
const float oneOverTileWidth = 1/tileWidth;
const float oneOverTileHeight = 1/tileHeight;

const float tileHalfX = tileWidth*0.5f;
const float tileHalfY = tileHeight*0.5f;

WorldCoords TileCoordsToWorldCoords(const TileCoords& tileCoords);


TileCoords WorldCoordsToTileCoords(const WorldCoords& worldCoords);

TileIndex GetTileIndexAt(TileCoords& tileCoords);

TileIndex GetTileIndexAt(const WorldCoords& worldCoords);

float GetDistance(const Vector2DF& position1, const Vector2DF& position2);

int GetDistance(const Vector2DI& position1, const Vector2DI& position2);

float GetDistanceSquared(const Vector2DF& position1, const Vector2DF& position2);

int GetDistanceSquared(const Vector2DI& position1, const Vector2DI& position2);

bool IsPointInCircle(const Vector2DF& point, float radius, const Vector2DF& center);

bool IsPointInCircle(const Vector2DI& point, float radius, const Vector2DF& center);

float GetPenetrationDepth(float penetratingPoint, float penetratedPoint);

int GetPenetrationDepth(int penetratingPoint, int penetratedPoint);
