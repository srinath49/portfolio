#include "Log.hpp"
#include "GameCommons.hpp"
#include "TheGame.hpp"
#include "WorldMap.hpp"

Log::Log(const Vector2DF& position, float collisionRadius, LogSize logSize, const LogType& logType, float moveSpeed, const std::string& textureName, int tilesWide, int tilesTall, bool isLeadLogPiece, const std::string& tag) 
	: MovingEntity(position, collisionRadius, tag, textureName, tilesWide, tilesTall, moveSpeed)
{
	m_numMidLogs = 0;
	std::string middleLogImage;
	std::string endLogImage;
	m_logType = logType;
	if(m_logType == SIMPLE_LOG)
	{
		if(logSize == SMALL_LOG)
		{
			logSize = MEDIUM_LOG;
		}
		middleLogImage = "Images/LogCenter.png";
		endLogImage = "Images/LogLeft.png";
	}
	else if(m_logType == ANIMATED_BUG_LOG)
	{
		if (logSize == BIG_LOG)
		{
			logSize = MEDIUM_LOG;
		}
		middleLogImage = "Images/BugLog.png";
		endLogImage = "Images/BugLog.png";
		tilesTall = 1;
		tilesWide = 3;
		m_animatedEntity = true;
	}
	else
	{
		if (logSize == BIG_LOG)
		{
			logSize = MEDIUM_LOG;
		}
		
		middleLogImage = "Images/BugLog.png";
		endLogImage = "Images/BugLog.png";
		tilesTall = 1;
		tilesWide = 3;
		m_animatedEntity = true;
	}
	m_isLeadLogPiece = isLeadLogPiece;

	if(m_isLeadLogPiece)
	{
		Vector2DF nextPos = Vector2DF(position.x - 2.f*collisionRadius, position.y);
		m_numMidLogs = (int)logSize - 2;
		for(int middleIndex = 1; middleIndex <= m_numMidLogs; middleIndex++)
		{
			Log* logMiddlePiece = new Log(nextPos, collisionRadius, logSize, m_logType, moveSpeed, middleLogImage, tilesWide, tilesTall, false, tag);
			m_gameReference->m_world->m_Layer2Entities.push_back(logMiddlePiece);
			m_gameReference->m_world->m_allColliderEntities.push_back(logMiddlePiece);
			m_logMiddlePieces.push_back(logMiddlePiece);
			nextPos.x -=  2.f*collisionRadius;
		}

		m_logLeftPiece = new Log(nextPos, collisionRadius, logSize, m_logType, moveSpeed, endLogImage, tilesWide, tilesTall, false, tag);
		m_gameReference->m_world->m_Layer2Entities.push_back(m_logLeftPiece);
		m_gameReference->m_world->m_allColliderEntities.push_back(m_logLeftPiece);
	}
}


Log::~Log(void)
{

}

void Log::Update()
{
	__super::Update();
	if(m_isLeadLogPiece && m_logType == SIMPLE_LOG)
	{
		for(int middleIndex = 1; middleIndex <= m_numMidLogs; middleIndex++)
		{

			float actualDist = m_logMiddlePieces[middleIndex - 1]->GetPosition().x - m_position.x;
			float correctDist = middleIndex * m_collisionRadius * 2;
			if (-actualDist < correctDist && actualDist < 0)
			{
				float diffDist = actualDist + correctDist;
				Vector2DF currentPos = m_logMiddlePieces[middleIndex -1]->GetPosition();
				currentPos.x -= diffDist;
				m_logMiddlePieces[middleIndex -1 ]->SetPosition(currentPos);
			}
		}
	}
}
