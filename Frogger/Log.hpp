#pragma once

#include "movingentity.hpp"

enum LogType
{
	SIMPLE_LOG,
	BUG_LOG,
	ANIMATED_BUG_LOG
};

enum LogSize
{
	SMALL_LOG = 2,
	MEDIUM_LOG = 3,
	BIG_LOG = 4,
	HUGE_LOG = 5
};

class Log :
	public MovingEntity
{
public:
	Log(const Vector2DF& position, float collisionRadius, LogSize logSize = MEDIUM_LOG, const LogType& logType = SIMPLE_LOG, float moveSpeed = 1.5f, const std::string& textureName = "Images/LogRight.png", int tilesWide = 1, int tilesTall = 1, bool isLeadLogPiece = true, const std::string& tag = "Good");
	~Log(void);
	
	void Update();

	std::vector<Log *> m_logMiddlePieces;
	Log* m_logLeftPiece;
	int m_numMidLogs;
	bool m_isLeadLogPiece;
	LogType m_logType;
};
