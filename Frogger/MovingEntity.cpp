#include "MovingEntity.hpp"


MovingEntity::MovingEntity(const Vector2DF& position, float collisionRadius, const std::string& tag, const std::string& textureName, int tilesWide, int tilesTall, float moveSpeed)
	: Entity(position, collisionRadius, tag, textureName, tilesWide, tilesTall)
{
	m_linearVelocity.x = moveSpeed;
	m_linearVelocity.y = 0;
	
	m_isEnemy = false;
	m_drawDiameter = m_collisionRadius;
	m_drawRadius = m_drawDiameter * 0.5f;
	m_animatedEntity = false;
}


MovingEntity::~MovingEntity(void)
{

}

void MovingEntity::Update()
{
	m_position += m_linearVelocity;
	WrapObject();
	if(m_animatedEntity)
	{
		m_spriteSheet->Update();
	}
}

void MovingEntity::Render()
{
	glPushMatrix();
	glTranslatef(m_position.x, m_position.y, 0.f);
	glRotatef(m_currentRotationDegrees, 0.f, 0.f, 1.f);
	if(m_spriteSheet->m_sprite->m_openglTextureID ==0)
	{
		glDisable(GL_TEXTURE_2D);
		glBegin(GL_LINES);
		{
			glColor3f(1.f, 0.2f, 0.4f);
			glVertex2f(0.f, m_drawDiameter);
			glVertex2f(0.f, 0.f);

			glVertex2f(0.f, 0.f);
			glVertex2f(m_drawDiameter, 0.f);

			glVertex2f(m_drawDiameter, 0.f);
			glVertex2f(m_drawDiameter, m_drawDiameter);

			glVertex2f(m_drawDiameter, m_drawDiameter);
			glVertex2f(0.f, m_drawDiameter);
			glColor3f(1.f, 1.f, 1.f);
		}
		glEnd();
	}
	else
	{
		glEnable(GL_TEXTURE_2D);
		m_spriteSheet->Draw(m_collisionRadius);
	}
	glPopMatrix();
}


void MovingEntity::WrapObject()
{
	if (m_position.x < -50.f)
	{
		m_position.x = 850.f;
	} 
	else if(m_position.x > 850.f)
	{
		m_position.x = -50.f;
	}
}
