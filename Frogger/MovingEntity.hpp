#pragma once
#include "entity.hpp"


class MovingEntity :
	public Entity
{
public:
	MovingEntity(const Vector2DF& position, float collisionRadius, const std::string& tag = "Bad", const std::string& textureName = "", int tilesWide = 1, int tilesTall = 1, float moveSpeed = 3.f);
	~MovingEntity(void);

	void Update();
	void Render();

	void WrapObject();

public:
	bool m_isEnemy;
	float m_drawDiameter;
	float m_drawRadius;
	bool m_animatedEntity;
};

