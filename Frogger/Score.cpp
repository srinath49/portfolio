#include "Score.hpp"
//#include "SpriteSheet.hpp"

Score::Score(const Vector2DF& position, float collisionRadius, const std::string& tag /*= "UI"*/, const std::string& textureName /*= "Images/Score.png"*/, int tilesWide /*= 10*/, int tilesTall /*= 1*/)
	: Entity(position, collisionRadius, tag, textureName, tilesWide, tilesTall)
{
	m_scoreTotal = 100000;
	m_scoreUnits = 0;
	m_scoreTens = 0;
	m_scoreHundreds = 0;
	m_scoreThousands = 0;
	m_scoreMillions = 0;
	m_oneUpSprite = new SpriteSheet("Images/1_Up.png", Vector2DI(1,1));
}


Score::~Score(void)
{
}

void Score::Update()
{
	int temp = m_scoreTotal;
	m_scoreUnits = temp%10;
	temp /= 10;
	m_scoreTens = temp%10;
	temp /= 10;
	m_scoreHundreds = temp%10;
	temp /= 10;
	m_scoreThousands = temp%10;
	temp /= 10;
	m_scoreMillions = temp%10;

}

void Score::Render()
{
	const float gap = 15.5f;
	glEnable(GL_TEXTURE_2D);
	
	glColor3f(1.f, 0.f, 0.f);
	glPushMatrix();
	glTranslatef(m_position.x, m_position.y, 0.f);
	m_spriteSheet->DrawFrame(m_scoreUnits, m_collisionRadius);
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(m_position.x - m_collisionRadius - gap, m_position.y, 0.f);
	m_spriteSheet->DrawFrame(m_scoreTens, m_collisionRadius);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(m_position.x - 2.f * m_collisionRadius - 2.f * gap, m_position.y, 0.f);
	m_spriteSheet->DrawFrame(m_scoreHundreds, m_collisionRadius);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(m_position.x - 3.f * m_collisionRadius - 3.f * gap, m_position.y, 0.f);
	m_spriteSheet->DrawFrame(m_scoreThousands, m_collisionRadius);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(m_position.x - 4.f * m_collisionRadius - 4.f * gap, m_position.y, 0.f);
	m_spriteSheet->DrawFrame(m_scoreMillions, m_collisionRadius);
	glPopMatrix();

	glColor3f(1.f, 1.f, 1.f);

	glPushMatrix();
	glTranslatef(m_position.x - 3.f * m_collisionRadius - 3.f * gap, m_position.y + 33.f, 0.f);
	m_oneUpSprite->Draw(2.2f * m_collisionRadius);
	glPopMatrix();
}
