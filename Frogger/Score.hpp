#pragma once
#include "entity.hpp"
class Score :
	public Entity
{
public:
	Score(const Vector2DF& position, float collisionRadius, const std::string& tag = "UI", const std::string& textureName = "Images/Numbers.png", int tilesWide = 10, int tilesTall = 1);
	~Score(void);

	int m_scoreTotal;
	int m_scoreUnits;
	int m_scoreTens;
	int m_scoreHundreds;
	int m_scoreThousands;
	int m_scoreMillions;
	SpriteSheet* m_oneUpSprite;
	void Update();
	void Render();
};

