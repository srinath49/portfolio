#include "SpriteSheet.hpp"
#include "Vector2DI.hpp"
#include "Texture.hpp"
#include "Time.hpp"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <gl/gl.h>

SpriteSheet::SpriteSheet(void)
{

}

SpriteSheet::SpriteSheet(const std::string& textureFileName, const Vector2DI& tileDimensions)
{
	m_sprite = Texture::CreateOrGetTexture(textureFileName);
	m_tilesWide = tileDimensions.x;
	m_tilesTall = tileDimensions.y;
	m_oneOverTilesWide = (1.f/m_tilesWide);
	m_oneOverTilesTall = (1.f/m_tilesTall);

	m_animSecondsPerFrame = 0.5f;
	m_time = GetCurrentTimeSeconds();

	m_totalFrames = m_tilesWide*m_tilesTall;
	m_isLastFrame = false;
	m_currentFrame = 0;
	
	m_minTextureCoords = GetMinTextureCoordsForFrame(m_currentFrame);
	m_maxTextureCoords = GetMaxTextureCoordsForFrame();


}

SpriteSheet::~SpriteSheet(void)
{

}


Vector2DF SpriteSheet::GetMinTextureCoordsForFrame(const int frameNumber)
{
	Vector2DF returnVector;
	returnVector.x = ((float)(frameNumber%m_tilesWide))*m_oneOverTilesWide;
	returnVector.y = ((float)(frameNumber/m_tilesWide))*m_oneOverTilesTall;

	return returnVector;
}

Vector2DF SpriteSheet::GetMaxTextureCoordsForFrame()
{
	Vector2DF returnVector;

	returnVector.x = m_minTextureCoords.x + m_oneOverTilesWide;
	returnVector.y = m_minTextureCoords.y + m_oneOverTilesTall;

	return returnVector;
}

Texture* SpriteSheet::GetCurrentTexture()
{
	return m_sprite;
}

void SpriteSheet::Update()
{
	if(m_isLastFrame)
	{
		m_currentFrame = 0;
		m_isLastFrame = false;
		return;
	}

	if(GetCurrentTimeSeconds() - m_time >= m_animSecondsPerFrame)
	{
		m_currentFrame++;
		if(m_currentFrame >= m_totalFrames)
		{
 			m_isLastFrame = true;
		}
		m_time = GetCurrentTimeSeconds();
	}
}

void SpriteSheet::Draw(float drawRadius)
{
	m_minTextureCoords = GetMinTextureCoordsForFrame(m_currentFrame);
	m_maxTextureCoords = GetMaxTextureCoordsForFrame();
	
	
	glBindTexture( GL_TEXTURE_2D, m_sprite->m_openglTextureID );

	glBegin(GL_QUADS);
	{
		//Top left
		glTexCoord2f(m_minTextureCoords.x, m_minTextureCoords.y);
		glVertex2f(-drawRadius, drawRadius);

		//Bottom left
		glTexCoord2f(m_minTextureCoords.x, m_maxTextureCoords.y);
		glVertex2f(-drawRadius, -drawRadius);

		//Bottom right
		glTexCoord2f(m_maxTextureCoords.x, m_maxTextureCoords.y);
		glVertex2f(drawRadius, -drawRadius);

		//Top right
		glTexCoord2f(m_maxTextureCoords.x, m_minTextureCoords.y);
		glVertex2f(drawRadius, drawRadius);

	}
	glEnd();
}

void SpriteSheet::DrawFrame(int frameToDraw, float drawRadius)
{
	m_currentFrame = frameToDraw;
	Draw(drawRadius);
}
