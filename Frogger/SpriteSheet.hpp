#pragma once
#include <string>
#include "Vector2DF.hpp"

class Texture;
class Vector2DI;

class SpriteSheet
{
public:
	SpriteSheet(void);
	SpriteSheet(const std::string& textureFileName, const Vector2DI& tileDimensions);
	~SpriteSheet(void);

	Vector2DF GetMinTextureCoordsForFrame(const int frameNumber);
	Vector2DF GetMaxTextureCoordsForFrame();
	Texture* GetCurrentTexture();

	void Update();
	void Draw(float drawRadius);
	void DrawFrame(int frameToDraw, float drawRadius);

public:
	Vector2DF m_minTextureCoords;
	Vector2DF m_maxTextureCoords;
	int m_tilesWide;
	int m_tilesTall;
	int m_totalFrames;
	Texture* m_sprite;
	float m_animSecondsPerFrame;
	int m_currentFrame;
	double m_time;
	bool m_isLastFrame;
	float m_oneOverTilesWide;
	float m_oneOverTilesTall;
};

