#include "StationaryEntity.hpp"


StationaryEntity::StationaryEntity(const Vector2DF& position, float collisionRadius, const std::string& tag, const std::string& textureName, int tilesWide, int tilesTall)
	: Entity(position, collisionRadius, tag, textureName, tilesWide, tilesTall)
{
	m_isObstacle = true;
	m_drawRadius = m_collisionRadius * 0.5f;
}


StationaryEntity::~StationaryEntity(void)
{

}


void StationaryEntity::Update()
{

}

void StationaryEntity::Render()
{
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(m_position.x, m_position.y, 0.f);
	m_spriteSheet->Draw(m_drawRadius);
	glPopMatrix();
}
