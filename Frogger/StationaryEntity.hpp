#pragma once
#include "entity.hpp"

class StationaryEntity :
	public Entity
{
public:
	StationaryEntity(const Vector2DF& position, float collisionRadius, const std::string& tag = "", const std::string& textureName = "", int tilesWide = 1, int tilesTall = 1);
	~StationaryEntity(void);

	void Update();
	void Render();

	
public:
	bool m_isObstacle;
	float m_drawRadius;
};

