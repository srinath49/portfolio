#pragma once
#include "std_image.h"
#include <string>
//#include "Vector2D.hpp"
#include <map>

class Texture
{
public:
	Texture(void);
	~Texture(void);
	static Texture* GetTextureByName( const std::string& imageFilePath );
	static Texture* CreateOrGetTexture( const std::string& imageFilePath );
	int m_openglTextureID;
	//Vector2D m_size;
	int m_sizeX;
	int m_sizeY;
	static std::map< std::string, Texture* >	Texture::s_textureRegistry;
private:
	
	Texture( const std::string& imageFilePath );
};

