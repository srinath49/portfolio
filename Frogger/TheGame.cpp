#include "TheGame.hpp"
#include "Vector2DF.hpp"
#include "WorldMap.hpp"
#include "Time.hpp"
#include "Frog.hpp"
#include "Entity.hpp"
#include "SpriteSheet.hpp"

TheGame* TheGame::g_theGame;

void TheGame::Start(Engine* engine)
{
	m_pause = false;
	
	m_engineReference = engine;
		
	m_world = new WorldMap();
	m_world->PopulateWorld();
	m_playerFrog = static_cast<Frog*>(m_world->m_PlayerEntities[0]);

	m_startSprite = new SpriteSheet("Images/StartScreen.png", Vector2DI(1,1));

	m_startTime = GetCurrentTimeSeconds();

	m_gameOverSprite = new SpriteSheet("Images/GameOver.png", Vector2DI(1,1));

	m_gameWonSprite = new SpriteSheet("Images/GameWon.png", Vector2DI(1,1));
	
	m_gameOver = false;
	m_gameWon = false;
}

void TheGame::Update()
{
	if(m_pause)
	{
		if(GetCurrentTimeSeconds() - m_pausedTime > 1.f)
		{
			m_playerFrog->CheckStartButton();
		}
		return;
	}

	if (GetCurrentTimeSeconds() - m_startTime < 2.f)
	{
		return;
	}

	if(m_gameOver)
	{
		if(GetCurrentTimeSeconds() - m_gameOverTime > 3.5f)
		{
			exit(0);
		}
		return;
	}

	if(m_gameWon)
	{
		if(GetCurrentTimeSeconds() - m_gameWonTime > 4.f)
		{
			exit(0);
		}
		return;
	}

	m_world->Update();
	
}

void TheGame::Render()
{
	if (GetCurrentTimeSeconds() - m_startTime < 2.f)
	{
		glPushMatrix();
		glTranslatef(400.f, 450.f, 0.f);
		m_startSprite->Draw(300.f);
		glPopMatrix();
		glClearColor( 1.f, 1.f, 1.f, 1.f );
		return;
	}
	m_world->Render();

	if(m_gameOver)
	{
		glPushMatrix();
		glTranslatef(400.f, 400.f, 0.f);
		m_gameOverSprite->Draw(35.f);
		glPopMatrix();
		glClearColor( 1.f, 1.f, 1.f, 1.f );
		//return;
	}

	if (m_gameWon)
	{
		glPushMatrix();
		glTranslatef(400.f, 400.f, 0.f);
		m_gameWonSprite->Draw(40.f);
		glPopMatrix();
		glClearColor( 1.f, 1.f, 1.f, 1.f );
	}

	if (m_pause)
	{
		// Render Pause Menu
	}
}

void TheGame::OnPause()
{
	//m_pause = true;
	m_world->PauseAudio();
	m_pausedTime = GetCurrentTimeSeconds();
	Sleep(10);
}

void TheGame::OnResume()
{
	//m_pause = false;
	m_world->ResumeAudio();
}

void TheGame::KillPlayer(bool killPlayer1)
{
	if(killPlayer1)
	{
		
	}
}


void TheGame::GameOver()
{
	m_gameOver = true;
	m_gameOverTime = GetCurrentTimeSeconds();
}

void TheGame::GameWon()
{
	m_gameWon = true;
	m_gameWonTime = GetCurrentTimeSeconds();
}

void TheGame::Pause()
{
	m_pause = true;
	OnPause();
}

void TheGame::Resume()
{
	m_pause = false;
	OnResume();
}

void TheGame::TogglePause()
{
	if(m_pause)
	{
		Resume();
	}
	else
	{
		Pause();
	}
}
