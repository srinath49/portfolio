#pragma once
#ifndef _INCLUDED_THE_GAME_H_
#define _INCLUDED_THE_GAME_H_

#include <vector>
#include "Engine.hpp"
#include "Vector2DI.hpp"

class Entity;
class Frog;
class SpriteSheet;


class TheGame
{
public:
	~TheGame() { g_theGame = nullptr; }	

	static TheGame* GetGameInstance() 
	{
		if (g_theGame == nullptr)
			g_theGame = new TheGame();
		return g_theGame;
	}

	void Start(Engine* engine);
	void Update() ;
	void Render();

	void Pause();
	void Resume();
	void TogglePause();
	void OnPause();
	void OnResume();

	void KillPlayer(bool killPlayer1);
	void GameOver();
	void GameWon();

private:
	TheGame(){}
	

public:

	static TheGame* g_theGame;
	
	WorldMap*	m_world;

	Engine* m_engineReference;
	bool m_pause;
	SpriteSheet* m_startSprite;
	double m_startTime;
	SpriteSheet* m_gameOverSprite;
	bool m_gameOver;
	double m_gameOverTime;
	SpriteSheet* m_gameWonSprite;
	bool m_gameWon;
	double m_gameWonTime;
	Frog* m_playerFrog;
	double m_pausedTime;
};
#endif //End of Include