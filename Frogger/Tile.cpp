#include "Tile.hpp"
#define WIN32_LEAN_AND_MEAN
#include<Windows.h>
#include <gl/GL.h>
#include "SpriteSheet.hpp"
#include "GameCommons.hpp"

const float tileDrawRadius = tileWidth*0.5f;
const float tileDrawDiameter = tileHeight;

Tile::Tile( const Vector2DF& worldCoords, const std::string& texturename, bool isSolid)
{
	m_isSolid = isSolid;
	m_worldCoords = worldCoords;
	m_tileCoords = WorldCoordsToTileCoords(m_worldCoords);
	m_tileCenter.x = m_worldCoords.x + tileHalfX;
	m_tileCenter.y = m_worldCoords.y + tileHalfY;

	if ( texturename == "" )
	{
		m_tileSprite = nullptr;
	}
	else
	{
		SetTexture( texturename );
	}
}


Tile::~Tile(void)
{

}

void Tile::Update()
{

}

void Tile::Render()
{
	//DrawGrid();
	DrawBackground();
	DrawTexture();
}

TileCoords Tile::WorldCoordsToTileCoords(WorldCoords worldCoords)
{
	TileCoords returnCoords;
	returnCoords.x = (int)(worldCoords.x*(1/tileWidth));
	returnCoords.y = (int)(worldCoords.y*(1/tileHeight));

	return returnCoords;
}

void Tile::SetTexture(const std::string& textureName)
{
	m_tileSprite = new SpriteSheet(textureName, Vector2DI(1,1));
}

void Tile::DrawGrid()
{
	glPushMatrix();
	glDisable(GL_TEXTURE_2D);
	glTranslatef(m_worldCoords.x, m_worldCoords.y, 0.f);

	glBegin(GL_LINES);
	{
		glVertex2f(0.f, tileDrawDiameter);
		glVertex2f(0.f, 0.f);

		glVertex2f(0.f, 0.f);
		glVertex2f(tileDrawDiameter, 0.f);

		glVertex2f(tileDrawDiameter, 0.f);
		glVertex2f(tileDrawDiameter, tileDrawDiameter);

		glVertex2f(tileDrawDiameter, tileDrawDiameter);
		glVertex2f(0.f, tileDrawDiameter);
	}
	glEnd();

	glPopMatrix();
}

void Tile::DrawBackground()
{
	if(m_tileSprite == nullptr)
	{
		glDisable(GL_TEXTURE_2D);
		glPushMatrix();
		
		glTranslatef(m_worldCoords.x, m_worldCoords.y, 0.f);
		//glDisable(GL_TEXTURE_2D);
	
		glBegin(GL_QUADS);	
		{
			if(m_tileCoords.y > 6)
			{
				glColor3f(0.f, 0.f, 0.278f);
			}
			else
			{
				glColor3f(0.f, 0.f, 0.f);
			}
			glDisable(GL_TEXTURE_2D);
			//Top Left
			glTexCoord2f(0.f, 0.f);
			glVertex2f(0.f, tileDrawDiameter);

			//Bottom left
			glTexCoord2f(0.f, 1.f); 
			glVertex2f(0.f, 0.f);

			//Bottom right
			glTexCoord2f(1.f, 1.f);
			glVertex2f(tileDrawDiameter, 0.f);

			//Top right
			glTexCoord2f(1.f, 0.f);
			glVertex2f(tileDrawDiameter, tileDrawDiameter);
			glColor3f(1.f, 1.f, 1.f);
		}
		glEnd();

		glPopMatrix();
	}

}

void Tile::DrawTexture()
{
	if(m_tileSprite != nullptr)
	{
		glPushMatrix();
		glTranslatef(m_tileCenter.x, m_tileCenter.y, 0.f);
		glEnable(GL_TEXTURE_2D);
		m_tileSprite->Draw(tileDrawRadius);
		glPopMatrix();
	}	
	else
	{
		glDisable(GL_TEXTURE_2D);
	}
}
