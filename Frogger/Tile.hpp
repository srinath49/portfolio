#pragma once
#include "Vector2DF.hpp"
#include "Vector2DI.hpp"
#include <string>

typedef Vector2DF WorldCoords;
typedef Vector2DI TileCoords;

class SpriteSheet;

class Tile
{
public:
	Tile(const Vector2DF& worldCoordinates, const std::string& texturename = "", bool isSolid = false);
	~Tile(void);

	void Update();
	void Render();

	TileCoords WorldCoordsToTileCoords(WorldCoords worldCoords);
	void SetTexture(const std::string& textureName);
	void DrawGrid();
	void DrawBackground();
	void DrawTexture();
public:
	bool m_isSolid;
	TileCoords	m_tileCoords;
	WorldCoords m_worldCoords;
	Vector2DF	m_tileCenter;
	SpriteSheet* m_tileSprite;
	std::string m_tileColor;
};

