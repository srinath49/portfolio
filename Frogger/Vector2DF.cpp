#include "Vector2DF.hpp"

Vector2DF::Vector2DF(void)
{
	x = 0.f;
	y = 0.f;
}

Vector2DF::~Vector2DF(void)
{

}

Vector2DF::Vector2DF(float x, float y)
{
	this->x = x;
	this->y = y;
}

Vector2DF Vector2DF::operator+(const Vector2DF& vec2d)
{
	Vector2DF returnVector;
	returnVector.x = x+vec2d.x;
	returnVector.y = y+vec2d.y;
	return returnVector;
}

Vector2DF Vector2DF::operator+(int scalarInt)
{
	Vector2DF returnVector;
	returnVector.x = x+scalarInt;
	returnVector.y = y+scalarInt;
	return returnVector;
}

Vector2DF Vector2DF::operator+(float scalarFloat)
{
	Vector2DF returnVector;
	returnVector.x = x+scalarFloat;
	returnVector.y = y+scalarFloat;
	return returnVector;
}

void Vector2DF::operator+=(const Vector2DF& vec2d)
{
	x += vec2d.x;
	y += vec2d.y;
}

void Vector2DF::operator+=(int scalarInt)
{
	x += scalarInt;
	y += scalarInt;
}

void Vector2DF::operator+=(float scalarFloat)
{
	x += scalarFloat;
	y += scalarFloat;
}

Vector2DF Vector2DF::operator-(const Vector2DF& vec2d)
{
	Vector2DF returnVector;
	returnVector.x = x-vec2d.x;
	returnVector.y = y-vec2d.y;
	return returnVector;
}

Vector2DF Vector2DF::operator-(int scalarInt)
{
	Vector2DF returnVector;
	returnVector.x = x-scalarInt;
	returnVector.y = y-scalarInt;
	return returnVector;
}

Vector2DF Vector2DF::operator-(float scalarFloat)
{
	Vector2DF returnVector;
	returnVector.x = x-scalarFloat;
	returnVector.y = y-scalarFloat;
	return returnVector;
}

void Vector2DF::operator-=(const Vector2DF& vec2d)
{
	x -= vec2d.x;
	y -= vec2d.y;
}

void Vector2DF::operator-=(int scalarInt)
{
	x -= scalarInt;
	y -= scalarInt;
}

void Vector2DF::operator-=(float scalarFloat)
{
	x -= scalarFloat;
	y -= scalarFloat;
}

void Vector2DF::operator=(const Vector2DF& copyVector)
{
	x = copyVector.x;
	y = copyVector.y;
}

bool Vector2DF::operator==(const Vector2DF& compareVector)
{
	if(x == compareVector.x && y == compareVector.y)
	{
		return true;
	}
	return false;
}

bool Vector2DF::operator!=(const Vector2DF& compareVector)
{
	/*if(x == compareVector.x && y == compareVector.y) // Which is faster or better and Why?
	{
		return false;
	}
	return true;*/
	return ! (this==&compareVector);
}

Vector2DF Vector2DF::operator*(float scalarFloat)
{
	return Vector2DF(this->x*scalarFloat, this->y*scalarFloat);
}

Vector2DF Vector2DF::operator*(int scalarInt)
{
	return Vector2DF(this->x*scalarInt, this->y*scalarInt);
}
