#pragma once

class Vector2DF
{
public:
	Vector2DF(void);
	Vector2DF(float x, float y);
	~Vector2DF(void);

	Vector2DF operator+(const Vector2DF& vectorToAdd);
	Vector2DF operator+(float scalarFloat);
	Vector2DF operator+(int scalarInt);
	void operator+=(const Vector2DF& vectorToAdd);
	void operator+=(float scalarFloat);
	void operator+=(int scalarInt);
	Vector2DF operator-(const Vector2DF& vectorToSubtract);
	Vector2DF operator-(float scalarFloat);
	Vector2DF operator-(int scalarInt);
	void operator-=(const Vector2DF& vectorToSubtract);
	void operator-=(float scalarFloat);
	void operator-=(int scalarInt);
	void operator=(const Vector2DF& copyVector);
	bool operator==(const Vector2DF& compareVector);
	bool operator!=(const Vector2DF& compareVector);
	Vector2DF operator*(float scalarFloat);
	Vector2DF operator*(int scalarInt);

public:
	float x;
	float y;
};

