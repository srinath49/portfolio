#include "Vector2DI.hpp"

Vector2DI::Vector2DI(void)
{
	x = 0;
	y = 0;
}

Vector2DI::~Vector2DI(void)
{

}

Vector2DI::Vector2DI(int x, int y)
{
	this->x = x;
	this->y = y;
}

Vector2DI Vector2DI::operator+(const Vector2DI& vec2d)
{
	Vector2DI returnVector;
	returnVector.x = x+vec2d.x;
	returnVector.y = y+vec2d.y;
	return returnVector;
}

Vector2DI Vector2DI::operator+(int scalarInt)
{
	Vector2DI returnVector;
	returnVector.x = x+scalarInt;
	returnVector.y = y+scalarInt;
	return returnVector;
}

Vector2DI Vector2DI::operator+(float scalarFloat)
{
	Vector2DI returnVector;
	returnVector.x = x+(int)scalarFloat;
	returnVector.y = y+(int)scalarFloat;
	return returnVector;
}

void Vector2DI::operator+=(const Vector2DI& vec2d)
{
	x += vec2d.x;
	y += vec2d.y;
}

void Vector2DI::operator+=(int scalarInt)
{
	x += scalarInt;
	y += scalarInt;
}

void Vector2DI::operator+=(float scalarFloat)
{
	x += (int)scalarFloat;
	y += (int)scalarFloat;
}

Vector2DI Vector2DI::operator-(const Vector2DI& vec2d)
{
	Vector2DI returnVector;
	returnVector.x = x-vec2d.x;
	returnVector.y = y-vec2d.y;
	return returnVector;
}

Vector2DI Vector2DI::operator-(int scalarInt)
{
	Vector2DI returnVector;
	returnVector.x = x-scalarInt;
	returnVector.y = y-scalarInt;
	return returnVector;
}

Vector2DI Vector2DI::operator-(float scalarFloat)
{
	Vector2DI returnVector;
	returnVector.x = x-(int)scalarFloat;
	returnVector.y = y-(int)scalarFloat;
	return returnVector;
}

void Vector2DI::operator-=(const Vector2DI& vec2d)
{
	x -= vec2d.x;
	y -= vec2d.y;
}

void Vector2DI::operator-=(int scalarInt)
{
	x -= scalarInt;
	y -= scalarInt;
}

void Vector2DI::operator-=(float scalarFloat)
{
	x -= (int)scalarFloat;
	y -= (int)scalarFloat;
}

void Vector2DI::operator=(const Vector2DI& copyVector)
{
	x = copyVector.x;
	y = copyVector.y;
}

bool Vector2DI::operator==(const Vector2DI& compareVector)
{
	if(x == compareVector.x && y == compareVector.y)
	{
		return true;
	}
	return false;
}

bool Vector2DI::operator!=(const Vector2DI& compareVector)
{
	if(x == compareVector.x && y == compareVector.y)
	{
		return false;
	}
	return true;
}

Vector2DI Vector2DI::operator*(float scalarFloat)
{
	return Vector2DI((int)(x*scalarFloat), (int)(y*scalarFloat));
}

Vector2DI Vector2DI::operator*(int scalarInt)
{
	return Vector2DI(x*scalarInt, y*scalarInt);
}