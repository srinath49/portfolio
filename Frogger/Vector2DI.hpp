#pragma once

class Vector2DI
{
public:
	Vector2DI(void);
	Vector2DI(int x, int y);
	~Vector2DI(void);

	Vector2DI operator+(const Vector2DI& vectorToAdd);
	Vector2DI operator+(float scalarFloat);
	Vector2DI operator+(int scalarInt);
	void operator+=(const Vector2DI& vectorToAdd);
	void operator+=(float scalarFloat);
	void operator+=(int scalarInt);
	Vector2DI operator-(const Vector2DI& vectorToSubtract);
	Vector2DI operator-(float scalarFloat);
	Vector2DI operator-(int scalarInt);
	void operator-=(const Vector2DI& vectorToSubtract);
	void operator-=(float scalarFloat);
	void operator-=(int scalarInt);
	void operator=(const Vector2DI& copyVector);
	bool operator==(const Vector2DI& compareVector);
	bool operator!=(const Vector2DI& compareVector);
	Vector2DI operator*(float scalarFloat);
	Vector2DI operator*(int scalarInt);

public:
	int x;
	int y;
};

