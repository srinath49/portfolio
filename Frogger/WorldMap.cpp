#include "WorldMap.hpp"
#include "Tile.hpp"
#include "MovingEntity.hpp"
#include "StationaryEntity.hpp"
#include "Frog.hpp"
#include "Engine.hpp"
#include "TheGame.hpp"
#include "GameCommons.hpp"
#include "Log.hpp"
#include "Score.hpp"
#include "Audio.hpp"
#include "EndPointFrog.hpp"

const float greenTileRadius = 24.2f;
const float horizontalGreenWth  = 25.f;
const float horizontalGreenHgt  = (tileHeight * 0.333f) - 0.10f;
const float verticalGreenWth  = 25.f;
const float verticalGreenHgt  = (tileHeight * 0.333f) + 2*0.08f;
const float greenTileStartHeight = 748.5f;

WorldMap::WorldMap(void)
{
	m_theGame = TheGame::GetGameInstance();

	m_audioSystem = new AudioSystem();
	LoadSounds();

	PlaySound("Sounds/ReachEndSound.wav", 1.f);

	PlayMusic("Sounds/FroggerMusic.ogg", 1.f);

	m_gameStarted = false;
}

WorldMap::~WorldMap(void)
{

}

void WorldMap::PopulateWorld()
{
	CreateTiles();
	TextureTiles();
	CreateGameEntities();
	CreateObstacles();
}

void WorldMap::Update()
{
	CheckCollisions();

	m_audioSystem->Update();
	for(unsigned int layer1Index = 0; layer1Index < m_Layer1Entities.size(); layer1Index++)
	{
		m_Layer1Entities.at(layer1Index)->Update();
	}

	for(unsigned int layer2Index = 0; layer2Index < m_Layer2Entities.size(); layer2Index++)
	{
		m_Layer2Entities.at(layer2Index)->Update();
	}

	for(unsigned int playerIndex = 0; playerIndex < m_PlayerEntities.size(); playerIndex++)
	{
		if(m_PlayerEntities.at(playerIndex) != nullptr)
		{
			m_PlayerEntities.at(playerIndex)->Update();
		}
	}

	for(unsigned int uiIndex = 0; uiIndex < m_UIEntities.size(); uiIndex++)
	{
		if(m_UIEntities.at(uiIndex) != nullptr)
		{
			m_UIEntities.at(uiIndex)->Update();
		}
	}
}

void WorldMap::Render()
{
	int tileSize = m_tiles.size()-1;

	if(tileSize >= 0)
	{
		for(int tileIndex = 0; tileIndex <= tileSize; tileIndex++)
		{
			m_tiles[tileIndex].Render();
		}
	}

	for(unsigned int layer1Index = 0; layer1Index < m_Layer1Entities.size(); layer1Index++)
	{
		m_Layer1Entities.at(layer1Index)->Render();
	}
	
	for(unsigned int layer2Index = 0; layer2Index < m_Layer2Entities.size(); layer2Index++)
	{
		m_Layer2Entities.at(layer2Index)->Render();
	}
	
	for(unsigned int playerIndex = 0; playerIndex < m_PlayerEntities.size(); playerIndex++)
	{
		if(m_PlayerEntities.at(playerIndex) != nullptr)
		{
			m_PlayerEntities.at(playerIndex)->Render();
		}
	}

	for(unsigned int uiIndex = 0; uiIndex < m_UIEntities.size(); uiIndex++)
	{
		if(m_UIEntities.at(uiIndex) != nullptr)
		{
			m_UIEntities.at(uiIndex)->Render();
		}
	}
}

TileCoords WorldMap::GetTileCoordsFromIndex(int tileIndex)
{
	TileCoords returnCoords;

	returnCoords.x = (int)tileIndex%m_tilesPerRow;
	returnCoords.y = (int)tileIndex/m_tilesPerRow;

	return returnCoords;
}

TileIndex WorldMap::GetTileIndexFromCoords(TileCoords tileCoords)
{
	TileIndex returnIndex;

	returnIndex = tileCoords.x + tileCoords.y*m_tilesPerRow;

	return returnIndex;
}

void WorldMap::CheckCollisions()
{
	for (int firstEntity = 0;  firstEntity < (int) m_allColliderEntities.size(); firstEntity++)
	{
		for(int secondEntity = 0; (int) secondEntity < (int) m_allColliderEntities.size(); secondEntity++)
		{
			if(secondEntity == firstEntity)
			{
				continue;
			}

			// First Check if the entities previously in collision, are still colliding with each other.
			// If not, remove them from the inCollisionWith vector.
			for (int collisionIndex = 0; collisionIndex < (int) m_allColliderEntities[firstEntity]->m_inCollisionWith.size(); collisionIndex++ )
			{
				float collisionRadiiSqr = m_allColliderEntities[firstEntity]->GetCollisionRadiusSquared() + m_allColliderEntities[firstEntity]->m_inCollisionWith.at(collisionIndex)->GetCollisionRadiusSquared();
				float distanceSqrBetweenCollidingEntities = GetDistanceSquared((m_allColliderEntities[firstEntity])->GetCenter(), (m_allColliderEntities[firstEntity])->m_inCollisionWith.at(collisionIndex)->GetCenter());
				if(distanceSqrBetweenCollidingEntities >collisionRadiiSqr) 
				{
					(m_allColliderEntities[firstEntity])->m_inCollisionWith.at(collisionIndex) = (m_allColliderEntities[firstEntity])->m_inCollisionWith.at((m_allColliderEntities[firstEntity])->m_inCollisionWith.size()-1);
					(m_allColliderEntities[firstEntity])->m_inCollisionWith.pop_back();
				}
			}

			// Check For Collisions between entities
			float sumofRadiiSqr = (m_allColliderEntities[firstEntity])->GetCollisionRadiusSquared() + (m_allColliderEntities[secondEntity])->GetCollisionRadiusSquared();
			float distanceBetweenEntitiesSqr = GetDistanceSquared((m_allColliderEntities[firstEntity])->GetCenter(), (m_allColliderEntities[secondEntity])->GetCenter());
			if(distanceBetweenEntitiesSqr <= sumofRadiiSqr) 
			{
				// check if already in collision with this entity.
				int entityIndex;
				for( entityIndex = 0; entityIndex < (int) m_allColliderEntities[firstEntity]->m_inCollisionWith.size(); entityIndex++ )
				{
					if( m_allColliderEntities[secondEntity] == m_allColliderEntities[firstEntity]->m_inCollisionWith.at(entityIndex) )
					{
						break;
					}
				}
				// If not, call the OnCollision() on the firstEntity.
				if( entityIndex >= (int) m_allColliderEntities[firstEntity]->m_inCollisionWith.size() )
				{
					m_allColliderEntities[firstEntity]->OnCollision(m_allColliderEntities[secondEntity]);
					m_allColliderEntities[firstEntity]->m_inCollisionWith.push_back(m_allColliderEntities[secondEntity]);
				}
			}
		}
	}
}

void WorldMap::CreateTiles()
{
	for(int tileYIndex = 0; tileYIndex < numTilesHigh; tileYIndex++)
	{
		for(int tileXIndex = 0; tileXIndex < numTilesWide; tileXIndex++)
		{
			Tile newTile( Vector2DF( tileXIndex*tileWidth - tileHalfX, tileYIndex*tileHeight ) );
			m_tiles.push_back(newTile);
		}
	}
}

void WorldMap::TextureTiles()
{
	// Texture the first Row
	m_tiles[0].SetTexture("Images/MiddleBlock.png");
	m_tiles[1].SetTexture("Images/MiddleBlock.png");
	m_tiles[2].SetTexture("Images/MiddleBlock.png");
	m_tiles[3].SetTexture("Images/MiddleBlock.png");
	m_tiles[4].SetTexture("Images/MiddleBlock.png");
	m_tiles[5].SetTexture("Images/MiddleBlock.png");
	m_tiles[6].SetTexture("Images/MiddleBlock.png");
	m_tiles[7].SetTexture("Images/MiddleBlock.png");
	m_tiles[8].SetTexture("Images/MiddleBlock.png");
	m_tiles[9].SetTexture("Images/MiddleBlock.png");
	m_tiles[10].SetTexture("Images/MiddleBlock.png");
	m_tiles[11].SetTexture("Images/MiddleBlock.png");
	m_tiles[12].SetTexture("Images/MiddleBlock.png");
	m_tiles[13].SetTexture("Images/MiddleBlock.png");

	// Texture the Sixth Row
	m_tiles[84].SetTexture("Images/MiddleBlock.png");
	m_tiles[85].SetTexture("Images/MiddleBlock.png");
	m_tiles[86].SetTexture("Images/MiddleBlock.png");
	m_tiles[87].SetTexture("Images/MiddleBlock.png");
	m_tiles[88].SetTexture("Images/MiddleBlock.png");
	m_tiles[89].SetTexture("Images/MiddleBlock.png");
	m_tiles[90].SetTexture("Images/MiddleBlock.png");
	m_tiles[91].SetTexture("Images/MiddleBlock.png");
	m_tiles[92].SetTexture("Images/MiddleBlock.png");
	m_tiles[93].SetTexture("Images/MiddleBlock.png");
	m_tiles[94].SetTexture("Images/MiddleBlock.png");
	m_tiles[95].SetTexture("Images/MiddleBlock.png");
	m_tiles[96].SetTexture("Images/MiddleBlock.png");
	m_tiles[97].SetTexture("Images/MiddleBlock.png");
}

void WorldMap::CreateGameEntities()
{
	CreateLogs();
	CreateCars();

	m_PlayerEntities.push_back(new Frog(Vector2DF(246.f, 0.f), 18.f, "PlayerFrog", "Images/Frog_Straight1.png", 2, 1));
	//m_allColliderEntities.push_back(m_PlayerEntities[0]);
	m_allColliderEntities.push_back(m_PlayerEntities[0]);

	EndPointFrog* endFrog = new EndPointFrog(Vector2DF(60.f, 760.f), 25.f);
	m_PlayerEntities.push_back(endFrog);
	m_allColliderEntities.push_back(endFrog);

	endFrog = new EndPointFrog(Vector2DF(230.f, 760.f), 25.f);
	m_PlayerEntities.push_back(endFrog);
	m_allColliderEntities.push_back(endFrog);

	endFrog = new EndPointFrog(Vector2DF(400.f, 760.f), 25.f);
	m_PlayerEntities.push_back(endFrog);
	m_allColliderEntities.push_back(endFrog);

	endFrog = new EndPointFrog(Vector2DF(570.f, 760.f), 25.f);
	m_PlayerEntities.push_back(endFrog);
	m_allColliderEntities.push_back(endFrog);

	endFrog = new EndPointFrog(Vector2DF(740.f, 760.f), 25.f);
	m_PlayerEntities.push_back(endFrog);
	m_allColliderEntities.push_back(endFrog);

	//Score* testScore = new Score(Vector2DF(250.f, 850.f), 15.f);
	//m_UIEntities.push_back(testScore);
}

void WorldMap::CreateObstacles()
{
	// Create the Green Tiles at the top 
	
	// Left facing tiles
	CreateGreenBottomLeftTiles();
	CreateGreenMiddleLeftTiles();
	CreateGreenTopLeftTiles();

	// Right Facing Tiles
	CreateGreenTopRightTiles();
	CreateGreenMiddleRightTiles();
	CreateGreenBottomRightTiles();

	//Middle Tiles
	CreateGreenMiddleBottomTiles();
	CreateGreenMiddleMiddleTiles();
	CreateGreenMiddleTopTiles();
}

void WorldMap::CreateGreenBottomLeftTiles()
{
	Vector2DF leftBottomPos = Vector2DF(12.5f,greenTileStartHeight);
	StationaryEntity* leftBottom = new StationaryEntity(leftBottomPos, greenTileRadius, "Bad", "Images/leftBottomGreen.png"); 
	m_Layer2Entities.push_back(leftBottom);
	m_allColliderEntities.push_back(leftBottom);

	leftBottomPos.x += 6 * (greenTileRadius) + greenTileRadius;
	leftBottom = new StationaryEntity(leftBottomPos, greenTileRadius, "Bad", "Images/leftBottomGreen.png"); 
	m_Layer2Entities.push_back(leftBottom);
	m_allColliderEntities.push_back(leftBottom);

	leftBottomPos.x += 6 * (greenTileRadius) + greenTileRadius;
	leftBottom = new StationaryEntity(leftBottomPos, greenTileRadius, "Bad", "Images/leftBottomGreen.png"); 
	m_Layer2Entities.push_back(leftBottom);
	m_allColliderEntities.push_back(leftBottom);

	leftBottomPos.x += 6 * (greenTileRadius) + greenTileRadius;
	leftBottom = new StationaryEntity(leftBottomPos, greenTileRadius, "Bad", "Images/leftBottomGreen.png"); 
	m_Layer2Entities.push_back(leftBottom);
	m_allColliderEntities.push_back(leftBottom);

	leftBottomPos.x += 6 * (greenTileRadius) + greenTileRadius;
	leftBottom = new StationaryEntity(leftBottomPos, greenTileRadius, "Bad", "Images/leftBottomGreen.png"); 
	m_Layer2Entities.push_back(leftBottom);
	m_allColliderEntities.push_back(leftBottom);
}

void WorldMap::CreateGreenMiddleLeftTiles()
{
	Vector2DF middleLeftPos = Vector2DF(12.5f, greenTileStartHeight + greenTileRadius);
	StationaryEntity* middleLeft = new StationaryEntity(middleLeftPos, greenTileRadius, "Bad", "Images/leftMiddleGreen.png"); 
	m_Layer2Entities.push_back(middleLeft);
	m_allColliderEntities.push_back(middleLeft);

	middleLeftPos.x += 6 * (greenTileRadius) + greenTileRadius;
	middleLeft = new StationaryEntity(middleLeftPos, greenTileRadius, "Bad", "Images/leftMiddleGreen.png"); 
	m_Layer2Entities.push_back(middleLeft);
	m_allColliderEntities.push_back(middleLeft);

	middleLeftPos.x += 6 * (greenTileRadius) + greenTileRadius;
	middleLeft = new StationaryEntity(middleLeftPos, greenTileRadius, "Bad", "Images/leftMiddleGreen.png"); 
	m_Layer2Entities.push_back(middleLeft);
	m_allColliderEntities.push_back(middleLeft);

	middleLeftPos.x += 6 * (greenTileRadius) + greenTileRadius;
	middleLeft = new StationaryEntity(middleLeftPos, greenTileRadius, "Bad", "Images/leftMiddleGreen.png"); 
	m_Layer2Entities.push_back(middleLeft);
	m_allColliderEntities.push_back(middleLeft);

	middleLeftPos.x += 6 * (greenTileRadius) + greenTileRadius;
	middleLeft = new StationaryEntity(middleLeftPos, greenTileRadius, "Bad", "Images/leftMiddleGreen.png"); 
	m_Layer2Entities.push_back(middleLeft);
	m_allColliderEntities.push_back(middleLeft);
}

void WorldMap::CreateGreenTopLeftTiles()
{
	Vector2DF leftTopPos = Vector2DF(12.5f, greenTileStartHeight + greenTileRadius + greenTileRadius);
	StationaryEntity* leftTop = new StationaryEntity(leftTopPos, greenTileRadius, "Bad", "Images/leftTopGreen.png"); 
	m_Layer2Entities.push_back(leftTop);
	m_allColliderEntities.push_back(leftTop);

	leftTopPos.x += 6 * (greenTileRadius) + greenTileRadius;
	leftTop = new StationaryEntity(leftTopPos, greenTileRadius, "Bad", "Images/leftTopGreen.png"); 
	m_Layer2Entities.push_back(leftTop);
	m_allColliderEntities.push_back(leftTop);

	leftTopPos.x += 6 * (greenTileRadius) + greenTileRadius;
	leftTop = new StationaryEntity(leftTopPos, greenTileRadius, "Bad", "Images/leftTopGreen.png"); 
	m_Layer2Entities.push_back(leftTop);
	m_allColliderEntities.push_back(leftTop);

	leftTopPos.x += 6 * (greenTileRadius) + greenTileRadius;
	leftTop = new StationaryEntity(leftTopPos, greenTileRadius, "Bad", "Images/leftTopGreen.png"); 
	m_Layer2Entities.push_back(leftTop);
	m_allColliderEntities.push_back(leftTop);

	leftTopPos.x += 6 * (greenTileRadius) + greenTileRadius;
	leftTop = new StationaryEntity(leftTopPos, greenTileRadius, "Bad", "Images/leftTopGreen.png"); 
	m_Layer2Entities.push_back(leftTop);
	m_allColliderEntities.push_back(leftTop);
}

void WorldMap::CreateGreenTopRightTiles()
{
	Vector2DF rightTopPos = Vector2DF(12.5f + 4 * greenTileRadius, greenTileStartHeight + greenTileRadius + greenTileRadius);
	StationaryEntity* rightTop = new StationaryEntity(rightTopPos, greenTileRadius, "Bad", "Images/rightTopGreen.png"); 
	m_Layer2Entities.push_back(rightTop);
	m_allColliderEntities.push_back(rightTop);

	rightTopPos.x += 6 * (greenTileRadius) + greenTileRadius;
	rightTop = new StationaryEntity(rightTopPos, greenTileRadius, "Bad", "Images/rightTopGreen.png"); 
	m_Layer2Entities.push_back(rightTop);
	m_allColliderEntities.push_back(rightTop);

	rightTopPos.x += 6 * (greenTileRadius) + greenTileRadius;
	rightTop = new StationaryEntity(rightTopPos, greenTileRadius, "Bad", "Images/rightTopGreen.png"); 
	m_Layer2Entities.push_back(rightTop);
	m_allColliderEntities.push_back(rightTop);

	rightTopPos.x += 6 * (greenTileRadius) + greenTileRadius;
	rightTop = new StationaryEntity(rightTopPos, greenTileRadius, "Bad", "Images/rightTopGreen.png"); 
	m_Layer2Entities.push_back(rightTop);
	m_allColliderEntities.push_back(rightTop);

	rightTopPos.x += 6 * (greenTileRadius) + greenTileRadius;
	rightTop = new StationaryEntity(rightTopPos, greenTileRadius, "Bad", "Images/rightTopGreen.png"); 
	m_Layer2Entities.push_back(rightTop);
	m_allColliderEntities.push_back(rightTop);

}

void WorldMap::CreateGreenMiddleRightTiles()
{
	Vector2DF middleRightPos = Vector2DF(12.5f + 4 * greenTileRadius, greenTileStartHeight + greenTileRadius);
	StationaryEntity* middleRight = new StationaryEntity(middleRightPos, greenTileRadius, "Bad", "Images/rightMiddleGreen.png"); 
	m_Layer2Entities.push_back(middleRight);
	m_allColliderEntities.push_back(middleRight);

	middleRightPos.x += 6 * (greenTileRadius) + greenTileRadius;
	middleRight = new StationaryEntity(middleRightPos, greenTileRadius, "Bad", "Images/rightMiddleGreen.png"); 
	m_Layer2Entities.push_back(middleRight);
	m_allColliderEntities.push_back(middleRight);

	middleRightPos.x += 6 * (greenTileRadius) + greenTileRadius;
	middleRight = new StationaryEntity(middleRightPos, greenTileRadius, "Bad", "Images/rightMiddleGreen.png"); 
	m_Layer2Entities.push_back(middleRight);
	m_allColliderEntities.push_back(middleRight);

	middleRightPos.x += 6 * (greenTileRadius) + greenTileRadius;
	middleRight = new StationaryEntity(middleRightPos, greenTileRadius, "Bad", "Images/rightMiddleGreen.png"); 
	m_Layer2Entities.push_back(middleRight);
	m_allColliderEntities.push_back(middleRight);

	middleRightPos.x += 6 * (greenTileRadius) + greenTileRadius;
	middleRight = new StationaryEntity(middleRightPos, greenTileRadius, "Bad", "Images/rightMiddleGreen.png"); 
	m_Layer2Entities.push_back(middleRight);
	m_allColliderEntities.push_back(middleRight);
}

void WorldMap::CreateGreenBottomRightTiles()
{
	Vector2DF rightBottomPos = Vector2DF(12.5f + 4 * greenTileRadius, greenTileStartHeight);
	StationaryEntity* rightBottom = new StationaryEntity(rightBottomPos, greenTileRadius, "Bad", "Images/rightBottomGreen.png"); 
	m_Layer2Entities.push_back(rightBottom);
	m_allColliderEntities.push_back(rightBottom);

	rightBottomPos.x += 6 * (greenTileRadius) + greenTileRadius;
	rightBottom = new StationaryEntity(rightBottomPos, greenTileRadius, "Bad", "Images/rightBottomGreen.png"); 
	m_Layer2Entities.push_back(rightBottom);
	m_allColliderEntities.push_back(rightBottom);

	rightBottomPos.x += 6 * (greenTileRadius) + greenTileRadius;
	rightBottom = new StationaryEntity(rightBottomPos, greenTileRadius, "Bad", "Images/rightBottomGreen.png"); 
	m_Layer2Entities.push_back(rightBottom);
	m_allColliderEntities.push_back(rightBottom);

	rightBottomPos.x += 6 * (greenTileRadius) + greenTileRadius;
	rightBottom = new StationaryEntity(rightBottomPos, greenTileRadius, "Bad", "Images/rightBottomGreen.png"); 
	m_Layer2Entities.push_back(rightBottom);
	m_allColliderEntities.push_back(rightBottom);

	rightBottomPos.x += 6 * (greenTileRadius) + greenTileRadius;
	rightBottom = new StationaryEntity(rightBottomPos, greenTileRadius, "Bad", "Images/rightBottomGreen.png"); 
	m_Layer2Entities.push_back(rightBottom);
	m_allColliderEntities.push_back(rightBottom);
}

void WorldMap::CreateGreenMiddleBottomTiles()
{
	Vector2DF middleBottomPos = Vector2DF(12.5f + greenTileRadius, greenTileStartHeight + greenTileRadius + greenTileRadius);
	StationaryEntity* middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);


	middleBottomPos.x += greenTileRadius;
	middleBottomPos.x += greenTileRadius;
	middleBottomPos.y = greenTileStartHeight;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);


	middleBottomPos.x += greenTileRadius;
	middleBottomPos.x += greenTileRadius;
	middleBottomPos.y = greenTileStartHeight + greenTileRadius + greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);


	middleBottomPos.x += greenTileRadius;
	middleBottomPos.x += greenTileRadius;
	middleBottomPos.y = greenTileStartHeight;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);


	middleBottomPos.x += greenTileRadius;
	middleBottomPos.x += greenTileRadius;
	middleBottomPos.y = greenTileStartHeight + greenTileRadius + greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);


	middleBottomPos.x += greenTileRadius;
	middleBottomPos.x += greenTileRadius;
	middleBottomPos.y = greenTileStartHeight;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);


	middleBottomPos.x += greenTileRadius;
	middleBottomPos.x += greenTileRadius;
	middleBottomPos.y = greenTileStartHeight + greenTileRadius + greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);


	middleBottomPos.x += greenTileRadius;
	middleBottomPos.x += greenTileRadius;
	middleBottomPos.y = greenTileStartHeight;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);


	middleBottomPos.x += greenTileRadius;
	middleBottomPos.x += greenTileRadius;
	middleBottomPos.y = greenTileStartHeight + greenTileRadius + greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

	middleBottomPos.x += greenTileRadius;
	middleBottom = new StationaryEntity(middleBottomPos, greenTileRadius, "Bad", "Images/middleBottomGreen.png"); 
	m_Layer2Entities.push_back(middleBottom);
	m_allColliderEntities.push_back(middleBottom);

}

void WorldMap::CreateGreenMiddleMiddleTiles()
{
	Vector2DF middleMiddlePos = Vector2DF(12.5f + 5*greenTileRadius, greenTileStartHeight + greenTileRadius);
	StationaryEntity* middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);

	middleMiddlePos.x += greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += 6*greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += 6*greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += 6*greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += 6*greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);
}

void WorldMap::CreateGreenMiddleTopTiles()
{
	Vector2DF middleMiddlePos = Vector2DF(12.5f + 5*greenTileRadius, greenTileStartHeight + greenTileRadius + greenTileRadius);
	StationaryEntity* middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);

	middleMiddlePos.x += greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += 6*greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += 6*greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += 6*greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += 6*greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);


	middleMiddlePos.x += greenTileRadius;
	middleMiddle = new StationaryEntity(middleMiddlePos, greenTileRadius, "Bad", "Images/middleMiddle.png"); 
	m_Layer2Entities.push_back(middleMiddle);
	m_allColliderEntities.push_back(middleMiddle);
}

void WorldMap::CreateLogs()
{
	Log* testLog = new Log(Vector2DF(12.9f*tileWidth, 8.f*tileHeight + tileHalfY), 23.f, MEDIUM_LOG);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(9.f*tileWidth, 8.f*tileHeight + tileHalfY), 23.f, MEDIUM_LOG);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(5.f*tileWidth, 8.f*tileHeight + tileHalfY), 23.f, MEDIUM_LOG);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(1.2f*tileWidth, 8.f*tileHeight + tileHalfY), 23.f, MEDIUM_LOG);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(14.f*tileWidth, 9.f*tileHeight + tileHalfY), 23.f, HUGE_LOG, SIMPLE_LOG, 2.9f);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(6.f*tileWidth, 9.f*tileHeight + tileHalfY), 23.f, HUGE_LOG, SIMPLE_LOG, 2.9f);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(-2.f*tileWidth, 9.f*tileHeight + tileHalfY), 23.f, HUGE_LOG, SIMPLE_LOG, 2.9f);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(2.f*tileWidth, 7.f*tileHeight + tileHalfY), 23.f, BIG_LOG, BUG_LOG, -3.5f, "Images/BugLog.png", 3, 1);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(6.f*tileWidth, 7.f*tileHeight + tileHalfY), 23.f, BIG_LOG, BUG_LOG, -3.5f, "Images/BugLog.png", 3, 1);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(10.f*tileWidth, 7.f*tileHeight + tileHalfY), 23.f, BIG_LOG, BUG_LOG, -3.5f, "Images/BugLog.png", 3, 1);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);


	testLog = new Log(Vector2DF(2.f*tileWidth, 10.f*tileHeight + tileHalfY), 23.f, SMALL_LOG, BUG_LOG, -3.5f, "Images/BugLog.png", 3, 1);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(5.f*tileWidth, 10.f*tileHeight + tileHalfY), 23.f, SMALL_LOG, BUG_LOG, -3.5f, "Images/BugLog.png", 3, 1);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(8.f*tileWidth, 10.f*tileHeight + tileHalfY), 23.f, SMALL_LOG, BUG_LOG, -3.5f, "Images/BugLog.png", 3, 1);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(11.f*tileWidth, 10.f*tileHeight + tileHalfY), 23.f, SMALL_LOG, BUG_LOG, -3.5f, "Images/BugLog.png", 3, 1);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(7.f*tileWidth, 11.f*tileHeight + tileHalfY), 23.f, BIG_LOG, SIMPLE_LOG, 2.2f);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(2.f*tileWidth, 11.f*tileHeight + tileHalfY), 23.f, BIG_LOG, SIMPLE_LOG, 2.2f);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

	testLog = new Log(Vector2DF(12.f*tileWidth, 11.f*tileHeight + tileHalfY), 23.f, BIG_LOG, SIMPLE_LOG, 2.2f);
	m_Layer2Entities.push_back(testLog);
	m_allColliderEntities.push_back(testLog);

}

void WorldMap::CreateCars()
{
	MovingEntity* yellowCar = new MovingEntity(Vector2DF(3.f*tileWidth, tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(2.f*tileWidth, tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(8.f*tileWidth, tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(7.f*tileWidth, tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(0.f*tileWidth, tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(-2.f*tileWidth, tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(10.f*tileWidth, tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(11.f*tileWidth, tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);


	yellowCar = new MovingEntity(Vector2DF(3.f*tileWidth, 5*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(2.f*tileWidth, 5*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(8.f*tileWidth, 5*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(7.f*tileWidth, 5*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(0.f*tileWidth, 5*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(-2.f*tileWidth, 5*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(10.f*tileWidth, 5*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	yellowCar = new MovingEntity(Vector2DF(11.f*tileWidth, 5*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/YellowCar.png", 1, 1, 1.2f);
	m_Layer2Entities.push_back(yellowCar);
	m_allColliderEntities.push_back(yellowCar);

	MovingEntity* purpleCar = new MovingEntity(Vector2DF(1.f*tileWidth, 2*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/PurpleCar.png", 1, 1, -1.7f);
	m_Layer2Entities.push_back(purpleCar);
	m_allColliderEntities.push_back(purpleCar);

	purpleCar = new MovingEntity(Vector2DF(2.f*tileWidth, 2*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/PurpleCar.png", 1, 1, -1.7f);
	m_Layer2Entities.push_back(purpleCar);
	m_allColliderEntities.push_back(purpleCar);

	purpleCar = new MovingEntity(Vector2DF(6.f*tileWidth, 4*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/PurpleCar.png", 1, 1, -1.4f);
	//purpleCar->SetCurrentRotation(180.f);
	m_Layer2Entities.push_back(purpleCar);
	m_allColliderEntities.push_back(purpleCar);

	purpleCar = new MovingEntity(Vector2DF(7.f*tileWidth, 4*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/PurpleCar.png", 1, 1, -1.4f);
	//purpleCar->SetCurrentRotation(180.f);
	m_Layer2Entities.push_back(purpleCar);
	m_allColliderEntities.push_back(purpleCar);

	purpleCar = new MovingEntity(Vector2DF(6.f*tileWidth, 2*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/PurpleCar.png", 1, 1, -1.7f);
	m_Layer2Entities.push_back(purpleCar);
	m_allColliderEntities.push_back(purpleCar);

	purpleCar = new MovingEntity(Vector2DF(5.f*tileWidth, 2*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/PurpleCar.png", 1, 1, -1.7f);
	m_Layer2Entities.push_back(purpleCar);
	m_allColliderEntities.push_back(purpleCar);

	purpleCar = new MovingEntity(Vector2DF(3.f*tileWidth, 4*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/PurpleCar.png", 1, 1, -1.4f);
	//purpleCar->SetCurrentRotation(180.f);
	m_Layer2Entities.push_back(purpleCar);
	m_allColliderEntities.push_back(purpleCar);

	purpleCar = new MovingEntity(Vector2DF(4.f*tileWidth, 4*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/PurpleCar.png", 1, 1, -1.4f);
	//purpleCar->SetCurrentRotation(180.f);
	m_Layer2Entities.push_back(purpleCar);
	m_allColliderEntities.push_back(purpleCar);

	MovingEntity* whiteCar = new MovingEntity(Vector2DF(1.f*tileWidth, 3*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/WhiteCar.png", 1, 1, 3.5f);
	m_Layer2Entities.push_back(whiteCar);
	m_allColliderEntities.push_back(whiteCar);

	whiteCar = new MovingEntity(Vector2DF(3.f*tileWidth, 3*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/WhiteCar.png", 1, 1, 3.5f);
	m_Layer2Entities.push_back(whiteCar);
	m_allColliderEntities.push_back(whiteCar);

	whiteCar = new MovingEntity(Vector2DF(6.f*tileWidth, 3*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/WhiteCar.png", 1, 1, 3.5f);
	m_Layer2Entities.push_back(whiteCar);
	m_allColliderEntities.push_back(whiteCar);

	whiteCar = new MovingEntity(Vector2DF(8.f*tileWidth, 3*tileHeight + tileHalfY), tileHalfX, "Bad", "Images/WhiteCar.png", 1, 1, 3.5f);
	m_Layer2Entities.push_back(whiteCar);
	m_allColliderEntities.push_back(whiteCar);
}

void WorldMap::PlaySound(const std::string& soundFilePath, float volumeLevel)
{
	int soundID = m_audioSystem->CreateOrGetSound(soundFilePath, false);
	m_audioSystem->PlaySound(soundID, volumeLevel);
}

void WorldMap::LoadSounds()
{
	m_audioSystem->CreateOrGetSound("Sounds/FroggerMusic.ogg", true);
	m_audioSystem->CreateOrGetSound("Sounds/ReachEndSound.wav", false);
	m_audioSystem->CreateOrGetSound("Sounds/FrogSound.wav", false);
	m_audioSystem->CreateOrGetSound("Sounds/SplashSound.wav", false);
}

void WorldMap::PlayMusic(const std::string& musicFilePath, float volumeLevel)
{
	int musicID = m_audioSystem->CreateOrGetSound(musicFilePath, true);
	m_audioSystem->PlayMusic(musicID, volumeLevel);
}

void WorldMap::PauseAudio()
{
	m_audioSystem->PauseAudio();
}

void WorldMap::ResumeAudio()
{
	m_audioSystem->ResumeAudio();
}
