#pragma once
#include "Vector2DF.hpp"
#include "Vector2DI.hpp"
#include <vector>

class Frog;
class Time;
class MovingEntity;
class StationaryEntity;
class Entity;
class Tile;
class TheGame;
class AudioSystem;

typedef Vector2DF WorldCoords;
typedef Vector2DI TileCoords;
typedef int TileIndex;


class WorldMap
{
public:
	WorldMap(void);
	~WorldMap(void);

	void PopulateWorld();
	void Update();
	void Render();
	void CheckCollisions();

	TileCoords GetTileCoordsFromIndex(int tileIndex);
	TileIndex GetTileIndexFromCoords(TileCoords tileCoords);
	
	void CreateTiles();
	void TextureTiles();
	
	void CreateGameEntities();
	void CreateObstacles();
	
	void CreateGreenBottomLeftTiles();
	void CreateGreenMiddleLeftTiles();
	void CreateGreenTopLeftTiles();
	void CreateGreenMiddleBottomTiles();
	void CreateGreenTopRightTiles();
	void CreateGreenMiddleRightTiles();
	void CreateGreenBottomRightTiles();
	void CreateGreenMiddleMiddleTiles();
	void CreateGreenMiddleTopTiles();
	
	void CreateLogs();
	void CreateCars();

	void PlaySound(const std::string& soundFilePath, float volumeLevel = 1.f);
	void PlayMusic(const std::string& musicFilePath, float volumeLevel = 1.f);
	void LoadSounds();
	void PauseAudio();
	void ResumeAudio();
public:
	std::vector<Tile> m_tiles;
	int m_tilesPerRow;

	std::vector<Entity*> m_Layer1Entities;
	std::vector<Entity*> m_Layer2Entities;
	std::vector<Entity*> m_UIEntities;
	std::vector<Entity*> m_PlayerEntities;
	std::vector<Entity*> m_allColliderEntities;

	TheGame* m_theGame;
	AudioSystem* m_audioSystem;
	bool m_gameStarted;
};

