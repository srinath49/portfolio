#include "XBoxController.hpp"
#include <stdio.h>
#include <math.h>

int XBoxController::availableID = 0;
const double PI = 3.14159265;

XBoxController::XBoxController(void)
{
	if(XBoxController::availableID + 1 <= 5)
	{
		m_controllerID = XBoxController::availableID++;
	}
}


XBoxController::~XBoxController(void)
{

}

bool XBoxController::HasController()
{
	memset( &m_xboxControllerState, m_controllerID, sizeof( m_controllerID ) );
	DWORD errorStatus = XInputGetState( m_controllerID, &m_xboxControllerState );

	if( errorStatus == ERROR_SUCCESS )
	{
		return true;	
	}
	else if( errorStatus == ERROR_DEVICE_NOT_CONNECTED )
	{
		printf( "Xbox controller is not connected.\n" );
	}
	else
	{
		printf( "Xbox controller reports unknown error status code %u (0x%08x).\n", errorStatus, errorStatus );
	}
	return false;
}


bool XBoxController::IsStartButtonPressed()
{
	return false;
}

bool XBoxController::IsBackButtonPressed()
{
	return false;
}

bool XBoxController::IsAButtonPressed()
{
	if (m_xboxControllerState.Gamepad.wButtons == 4096)
	{
		return true;
	}
	return false;
}

bool XBoxController::IsBButtonPressed()
{
	return false;
}

bool XBoxController::IsXButtonPressed()
{
	return false;
}

bool XBoxController::IsYButtonPressed()
{
	return false;
}

bool XBoxController::IsLButtonPressed()
{
	return false;
}

bool XBoxController::IsRButtoPressed()
{
	return false;
}

bool XBoxController::IsLeftTriggerPressed()
{
	return false;
}

bool XBoxController::IsRightTriggetPressed()
{
	if (m_xboxControllerState.Gamepad.bRightTrigger != 0)
	{
		return true;
	}
	return false;
}

float XBoxController::GetLStickAngleDegrees()
{
	float x = GetLStickX();
	float y = GetLStickY();

	float angleDegrees = (float)(atan2(y,x)*180.f/PI);
	return angleDegrees;
}

float XBoxController::GetRStickAngleDegrees()
{
	float x = GetRStickX();
	float y = GetRStickY();

	float angleDegrees = (float)(atan2(y,x)*180.f/PI);
	return angleDegrees;
}

float XBoxController::GetLStickThrust()
{
	if(m_xboxControllerState.Gamepad.sThumbLX > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE-1000 || m_xboxControllerState.Gamepad.sThumbLX < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE+1000 || m_xboxControllerState.Gamepad.sThumbLY > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE-1000 || m_xboxControllerState.Gamepad.sThumbLY < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE+1000)
	{
		return 3.f;
		//return m_xboxControllerState.Gamepad.sThumbLX;
	}
	return 0.f;
}

float XBoxController::GetRStickThrust()
{
	if(m_xboxControllerState.Gamepad.sThumbRX > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE-1000 || m_xboxControllerState.Gamepad.sThumbRX < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE+1000 || m_xboxControllerState.Gamepad.sThumbRY > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE-1000 || m_xboxControllerState.Gamepad.sThumbRY < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE+1000)
	{
		return 3.f;
		//return m_xboxControllerState.Gamepad.sThumbRX;
	}
	return 0.f;
}

SHORT XBoxController::GetLStickX()
{
	return m_xboxControllerState.Gamepad.sThumbLX;
}

SHORT XBoxController::GetLStickY()
{
	return m_xboxControllerState.Gamepad.sThumbLY;
}

SHORT XBoxController::GetRStickX()
{
	return m_xboxControllerState.Gamepad.sThumbRX;
}

SHORT XBoxController::GetRStickY()
{
	return m_xboxControllerState.Gamepad.sThumbRY;
}

int XBoxController::GetID()
{
	return m_controllerID;
}

SHORT XBoxController::GetButtonPressedValue()
{
	return m_xboxControllerState.Gamepad.wButtons;
}
