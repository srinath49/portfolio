#pragma once

#ifndef _INCLUDED_XBOX_CONTROLLER_H_
#define _INCLUDED_XBOX_CONTROLLER_H_

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Xinput.h> // include the Xinput API
#pragma comment( lib, "xinput" ) // Link in the xinput.lib static library

class XBoxController
{
public:
	XBoxController(void);
	~XBoxController(void);

	bool HasController();

	bool IsStartButtonPressed();
	bool IsBackButtonPressed();
	
	bool IsAButtonPressed();
	bool IsBButtonPressed();
	bool IsXButtonPressed();
	bool IsYButtonPressed();
	bool IsLButtonPressed();
	bool IsRButtoPressed();

	
	bool IsLeftTriggerPressed();
	bool IsRightTriggetPressed();

	float GetLStickAngleDegrees();
	float GetRStickAngleDegrees();
	float GetLStickThrust();
	float GetRStickThrust();

	SHORT GetLStickX();
	SHORT GetLStickY();
	SHORT GetRStickX();
	SHORT GetRStickY();
	int GetID();

	SHORT GetButtonPressedValue();
public:

	XINPUT_STATE m_xboxControllerState;
	int	m_controllerID;
private:
	static int	availableID;
	double m_LStickX;
	double m_LStickY;
	double m_RStickX;
	double m_RStickY;
};

#endif // _INCLUDED_XBOX_CONTROLLER_H_