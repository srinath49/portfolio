#include "Cooker.hpp"
#include <vector>
#include "Engine\Core\EngineCommons.hpp"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

//---------------------------------------------------------------------------------------------------
#include <Windows.h>
#include <string>
#include "Engine\FileIO\BinaryBufferParser.hpp"
#include "Engine\FileIO\OBJLoader.hpp"
#include "Engine\Core\MyMesh.hpp"
#include <xlocale>
#include <sys\stat.h>
#include <wchar.h>


//---------------------------------------------------------------------------------------------------
void ReadWord(int& currentIndex, char* wordToFill, std::string& commandArgs);


//---------------------------------------------------------------------------------------------------
Cooker::Cooker() :
	m_cookAll(false)
{

}


//---------------------------------------------------------------------------------------------------
Cooker::~Cooker()
{

}

//---------------------------------------------------------------------------------------------------
bool Cooker::ProcessCommandlineArgs(std::string& commandArgs)
{
	bool retVal = false;
	int index = 0;
	char word[200];
	ReadWord(index, word, commandArgs);
	std::string wordReadFromArgs(word);

	if(wordReadFromArgs == "-cook"  || wordReadFromArgs == "-cookall")
	{
		if (wordReadFromArgs == "-cookall")
		{
			m_cookAll = true;
		}

		index++;
		ReadWord(index, word, commandArgs);
		m_platform = std::string(word);
		retVal = true;
	}
	return retVal;
}

//---------------------------------------------------------------------------------------------------
bool Cooker::Run(std::string& rootDir)
{
	if(m_platform == "Win32")
	{
		printf("Cooking files for Win32...\n\n");
	}
	else
	{
		printf("Platform '%s' not recognized, 'Win32' assumed\n", m_platform.c_str());
		printf("Cooking files for Win32...\n\n");
		m_platform = "Win32";
	}
	ProcessFiles(rootDir);

	return true;
}

//---------------------------------------------------------------------------------------------------
void Cooker::ProcessFiles(std::string& rootDir)
{
	std::vector<std::string> files;
	std::vector<std::string> directories;
	rootDir += "*";
	EnumerateFilesInFolder(rootDir, files, directories);

	LPSECURITY_ATTRIBUTES attribs = NULL;
	CreateDirectory(L"../Run_Win32", attribs);
	CreateDirectory(L"../Run_Win32/Data", attribs);
	if (!directories.empty())
	{
		for (std::vector<std::string>::iterator dirIter = directories.begin(); dirIter != directories.end(); dirIter++)
		{
			if( (*dirIter) == "../RawData/")
			{
				continue;
			}
			std::string path("../Run_Win32/Data/");
			path += (*dirIter);
			std::wstring wpath(path.begin(), path.end());
			CreateDirectory(wpath.c_str(), attribs);
		}
		for (std::vector<std::string>::iterator fileIter = files.begin(); fileIter != files.end(); fileIter++)
		{
			std::string sourcePath = "../RawData/" + *fileIter;
			std::string destinationPath = "../Run_Win32/Data/" + *fileIter;
			size_t pos = 0;
			pos = sourcePath.find(".obj");
			bool cookFile = false;
			if (pos > 0 && pos < sourcePath.length())
			{
				cookFile = true;
				pos = destinationPath.find(".obj");
				destinationPath.erase(pos);
				destinationPath += ".C23";
			}
			if (cookFile)
			{
				CookFile(sourcePath, destinationPath, *fileIter);
			}
			else
			{
				FILE* sourceFile;
				fopen_s(&sourceFile, sourcePath.c_str(), "rb");

				if (sourceFile == NULL)
				{
					continue;
				}

				fseek(sourceFile, 0, SEEK_END);
				size_t fsize = ftell(sourceFile);
				rewind(sourceFile);

				unsigned char* fileBuffer = new unsigned char[fsize];

				fread(fileBuffer, sizeof(unsigned char), fsize, sourceFile);
				fclose(sourceFile);

				FILE* destinationFile;
				fopen_s(&destinationFile, destinationPath.c_str(), "wb");
				if (destinationFile == NULL)
				{
					continue;
				}
				fwrite(fileBuffer, sizeof(unsigned char), fsize, destinationFile);
				fclose(destinationFile);
			}
		}
	}
}

//---------------------------------------------------------------------------------------------------
void Cooker::CookFile(std::string& sourcePath, std::string& destinationPath, std::string& printString)
{
	bool shouldCook = true;
	if (!m_cookAll)
	{
		shouldCook = false;
		struct tm clockSource;
		struct tm clockDest;

		struct stat attribSource;
		struct stat attribDest;

		stat(sourcePath.c_str(), &attribSource);
		stat(destinationPath.c_str(), &attribDest);
		
		gmtime_s(&clockSource, &(attribSource.st_mtime));
		gmtime_s(&clockDest, &(attribDest.st_mtime));
		int diff = attribDest.st_mtime - attribSource.st_mtime;
		if(diff < 0)
		{
			shouldCook = true;
		}
	}
	
	if (shouldCook)
	{
		BinaryBufferParser bbp;
		OBJLoader objL;
		MyMesh* mesh = new MyMesh();
		printf("Loading File %s\n", printString.c_str());
		objL.LoadFile(sourcePath);
		printf("Processing File %s\n", printString.c_str());
		objL.ParseVertexDataFromOBJFile(mesh);
		std::string destPrintString = printString;
		size_t pos = 0;
		pos = destPrintString.find(".obj");
		destPrintString.erase(pos);
		destPrintString += ".C23";
		printf("Cooking File From :\n%s\tto\t%s\n\n", printString.c_str(), destPrintString.c_str());
		bbp.BuildBinaryBufferFromVertexData(mesh, destinationPath, true);
	}
}

//---------------------------------------------------------------------------------------------------
void ReadWord(int& currentIndex, char* wordToFill, std::string& commandArgs)
{
	int charIndex = 0;
	const char* buffer = commandArgs.c_str();

	while( currentIndex <= (int)commandArgs.length() && !isspace(buffer[currentIndex]) )
	{
		wordToFill[charIndex] = buffer[currentIndex];
		charIndex++;
		currentIndex++;
	}
	wordToFill[charIndex] = '\0';
}