#pragma once

#ifndef	COOKER_H
#define COOKER_H
//---------------------------------------------------------------------------------------------------
#include <string>

//---------------------------------------------------------------------------------------------------
class Cooker
{
public:
	Cooker();
	~Cooker();

	bool ProcessCommandlineArgs(std::string& commandArgs);
	bool Run(std::string& rootDir);
	void ProcessFiles(std::string& rootDir);
	void CookFile(std::string& sourcePath, std::string& destinationPath, std::string& printString);
	bool m_cookAll;
	std::string m_platform;
};


#endif

/*****************************************************************************************************
Create default textures.

example code for initializing default white texture
unsigned char imageData[3] = {255, 255, 255};
m_whiteTexture = CreateTextureData("PlainWhite", imageData, IntVector2::ONE, 3);
vec4 emmisiveColor = vec4(diffuseTexel.rgb * emmisive strength, 0.0)
DefaultNormal = Lightblue(127, 127, 255);
For spec/gloss/emmisive RGB(0.5, 0.5. 0.0);
Consider having a "MissingTexture" default texture.

Check notebook for calculating missing normals.
*****************************************************************************************************/