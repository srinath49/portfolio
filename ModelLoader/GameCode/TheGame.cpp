#include "TheGame.hpp"
#include "World.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/Core/GraphicsDebugManager.hpp"
#include "Engine/Input/InputSystem.hpp"
#include "Engine/Rendering/SpriteSheet.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/SriOGLRenderer.hpp"
#include "Engine/DebugConsole/DebugConsole.hpp"
#include "Engine/DebugConsole/BitmapFont.hpp"
#include "Engine/Rendering/OpenGlExtentions.hpp"
#include "Engine/Rendering/SriShaderProgram.hpp"
#include "Engine/Rendering/Light.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/FileIO/BinaryBufferParser.hpp"
#include "Engine/DebugConsole/BitmapFont.hpp"
#include "Engine/Core/MyMesh.hpp"
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
TheGame::TheGame(void) :
	m_camera(),
	m_centerCursorPos(800, 450),
	m_timeSinceLastUpdate(0.0),
	m_cameraMovingSpeed(0.5f),
	m_lostFocus(false),
	m_myDebugManager(nullptr),
	m_console(nullptr),
	m_world(nullptr),
	m_inputSystem(nullptr),
	m_previousFrameTimeSeconds(0.0),
	m_isBinaryMode(true),
	m_hasCookedFiles(false),
	m_modelIndex(0)
{	
	m_activeGameState = STARTING_UP;
	ShowCursor(false);
	SetCursorPos( m_centerCursorPos.x, m_centerCursorPos.y );
	FillModelList();	
}

//---------------------------------------------------------------------------------------------------
TheGame::~TheGame(void)
{
	delete m_world;
	delete m_myDebugManager;
	delete m_console;
	
	while (!m_lights.empty())
	{
		delete m_lights.back();
		m_lights.pop_back();
	}
}

//---------------------------------------------------------------------------------------------------
void TheGame::InitilizeGame(void* platformHandle)
{
	m_isGameRunning = true;

	m_camera.m_position = Vector3DF(-2.f, 0.75f, 1.5f);
	m_camera.m_cameraSpeed = m_cameraMovingSpeed;

	CreateInputSystem(platformHandle);
	InitializeRenderer(platformHandle);
	CreateWorld();
	std::vector<std::string> fontTextures;
	fontTextures.push_back("Data/Fonts/Arial_0.png");
	m_bitmapFont = BitmapFont("Data/Fonts/Arial_0.png", "Data/Fonts/Arial.fnt");
	m_bitmapFont.InitializeFonts(m_world->m_basicMaterial->m_shaderProgramId, fontTextures);
	m_activeGameState = RENDERING;
}

//---------------------------------------------------------------------------------------------------
void TheGame::InitializeRenderer(void* platformHandle)
{
	g_myRenderer = new SriOGLRenderer();
	g_myRenderer->Initialize(platformHandle);
}

//---------------------------------------------------------------------------------------------------
void TheGame::CreateInputSystem(void* platformHandle)
{
	m_inputSystem = new InputSystem(platformHandle);
}

//---------------------------------------------------------------------------------------------------
void TheGame::CreateWorld()
{
	m_world = new World(m_camera);
}

//---------------------------------------------------------------------------------------------------
void TheGame::RunGame()
{
	while(m_isGameRunning)
	{
		RunFrame();
	}
}

//---------------------------------------------------------------------------------------------------
void TheGame::RunFrame()
{
	CheckInput();
	Update(DELTASECONDS);
	Render();
	WaitForFrameAndSwapBuffers();
}

//---------------------------------------------------------------------------------------------------
void TheGame::WaitForFrameAndSwapBuffers()
{
	while(GetCurrentTimeSeconds() - m_previousFrameTimeSeconds < DELTASECONDS)
	{
		// Waiting For DeltaSeconds before swapping buffers for consistent frame rate.
	}
	m_previousFrameTimeSeconds = GetCurrentTimeSeconds();

	if(g_myRenderer)
	{
		g_myRenderer->SwapDeviceBuffers();
	}
}

//---------------------------------------------------------------------------------------------------
void TheGame::CheckInput()
{
	switch (m_activeGameState)
	{
		case RENDERING:
			ProcessInput();
			break;
		default:
			break;
	}
}

//---------------------------------------------------------------------------------------------------
void TheGame::Update( float deltaSeconds)
{
	m_inputSystem->Update(DELTASECONDS);
	switch (m_activeGameState)
	{
		case LOAD_C23:
			g_myRenderer->LoadOrtho();
			m_bitmapFont.SetUpMyMaterial(m_camera.m_position);
			m_bitmapFont.DrawString("Loading .C23 file...", RgbaColors(1.0f, 0.0f, 0.0f), 32.f, Vector2DF(500.f, 800.f), false);
			m_activeGameState = LOADING_C23;
			break;
		case LOAD_OBJ:
			g_myRenderer->LoadOrtho();
			m_bitmapFont.SetUpMyMaterial(m_camera.m_position);
			m_bitmapFont.DrawString("Loading .OBJ file...", RgbaColors(1.0f, 0.0f, 0.0f), 32.f, Vector2DF(500.f, 800.f), false);
			m_activeGameState = LOADING_OBJ;
			break;
		case COOK:
			g_myRenderer->LoadOrtho();
			m_bitmapFont.SetUpMyMaterial(m_camera.m_position);
			m_bitmapFont.DrawString("Cooking .OBJ files to .C23...", RgbaColors(1.0f, 0.0f, 0.0f), 32.f, Vector2DF(500.f, 800.f), false);
			m_activeGameState = COOKING_OBJ_TO_C23;
			break;
		case LOADING_OBJ:
			m_world->SetModelToLoad(m_modelsList[m_modelIndex], m_isBinaryMode);
			m_activeGameState = RENDERING;
			break;
		case LOADING_C23:
			m_world->SetModelToLoad(m_modelsList[m_modelIndex], m_isBinaryMode);
			m_activeGameState = RENDERING;
			break;
		case RENDERING:
			UpdateCameraFromMouseAndKeyboard( m_camera, deltaSeconds );	
			m_world->Update(m_camera.m_position, deltaSeconds);
			break;
		case COOKING_OBJ_TO_C23:
			CookOBJToC23();
			m_activeGameState = RENDERING;
			break;
		default:
			break;
	}
}

//---------------------------------------------------------------------------
void TheGame::Render()
{
	switch (m_activeGameState)
	{
		case RENDERING:
			RenderScene();
			break;
		default:
			break;
	}
}

//---------------------------------------------------------------------------------------------------
void TheGame::ProcessInput()
{
	if (m_inputSystem)
	{
		if(m_inputSystem->IsKeyDown(KEY_CODE_ESCAPE))
		{
			m_isGameRunning = false;
		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_STAR))
		{
			m_activeGameState = COOK;
		}
// 		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_B))
// 		{
//  			if(m_isBinaryMode)
//  			{
//  				m_isBinaryMode = false;
//  			}
//  			else
//  			{
//  				m_isBinaryMode = true;
//  				if (!m_hasCookedFiles)
//  				{
//  					m_activeGameState = COOK;
//  				}
//  			}
// 		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_0))
		{
			if (m_isBinaryMode)
			{
				m_activeGameState = LOAD_C23;
			}
			else
			{	
				m_activeGameState = LOAD_OBJ;
			}
			m_modelIndex = 0;
		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_1))
		{
			if (m_isBinaryMode)
			{
				m_activeGameState = LOAD_C23;
			}
			else
			{	
				m_activeGameState = LOAD_OBJ;
			}
			m_modelIndex = 1;
		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_2))
		{
			if (m_isBinaryMode)
			{
				m_activeGameState = LOAD_C23;
			}
			else
			{	
				m_activeGameState = LOAD_OBJ;
			}
			m_modelIndex = 2;
		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_3))
		{
			if (m_isBinaryMode)
			{
				m_activeGameState = LOAD_C23;
			}
			else
			{	
				m_activeGameState = LOAD_OBJ;
			}
			m_modelIndex = 3;
		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_4))
		{
			if (m_isBinaryMode)
			{
				m_activeGameState = LOAD_C23;
			}
			else
			{	
				m_activeGameState = LOAD_OBJ;
			}
			m_modelIndex = 4;
		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_5))
		{
			if (m_isBinaryMode)
			{
				m_activeGameState = LOAD_C23;
			}
			else
			{	
				m_activeGameState = LOAD_OBJ;
			}
			m_modelIndex = 5;
		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_6))
		{
			if (m_isBinaryMode)
			{
				m_activeGameState = LOAD_C23;
			}
			else
			{	
				m_activeGameState = LOAD_OBJ;
			}
			m_modelIndex = 6;
		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_7))
		{
			if (m_isBinaryMode)
			{
				m_activeGameState = LOAD_C23;
			}
			else
			{	
				m_activeGameState = LOAD_OBJ;
			}
			m_modelIndex = 7;
		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_8))
		{
			if (m_isBinaryMode)
			{
				m_activeGameState = LOAD_C23;
			}
			else
			{	
				m_activeGameState = LOAD_OBJ;
			}
			m_modelIndex = 8;
		}
		if (m_inputSystem->HasKeyJustBeenReleased(KEY_CODE_9))
		{
			if (m_isBinaryMode)
			{
				m_activeGameState = LOAD_C23;
			}
			else
			{	
				m_activeGameState = LOAD_OBJ;
			}
			m_modelIndex = 9;
		}
	}	
	m_world->ProcessInput(m_inputSystem);
	m_camera.ProcessInput(m_inputSystem);
}

//---------------------------------------------------------------------------------------------------
void TheGame::RenderScene()
{
	g_myRenderer->Clear(CLEAR_SCREEN_COLOR);
	SetUpPerspectiveProjection();
	ApplyCameraTransform( m_camera );
	m_world->Render();
	m_world->m_basicMaterial->SetupMaterial(m_camera.m_position);
	RenderAxis(500.f);
	std::string mode = "";
	if (m_isBinaryMode)
	{
		mode += "Binary : .C23";
	}
	else
	{
		mode += ".OBJ";
	}
	g_myRenderer->LoadOrtho();
	m_bitmapFont.SetUpMyMaterial(m_camera.m_position);
	m_bitmapFont.DrawString("RenderingMode: " + mode, RgbaColors(1.0f, 0.0f, 0.0f), 32.f, Vector2DF(200.f, 850.f), false);
}

//---------------------------------------------------------------------------------------------------
void TheGame::RenderAxis(float axisLength /*= 50.f*/, float axisWidth /*= 5.f*/)
{
	m_world->m_basicMaterial->SetupMaterial();
	g_myRenderer->DrawAxis(axisLength, axisWidth);
}

//---------------------------------------------------------------------------------------------------
void TheGame::SetUpPerspectiveProjection()
{
	g_myRenderer->SetUpPerspectiveProjection(FOVY, ASPECT, NEAR_CLIPPING_PLANE, FAR_CLIPPING_PLANE);
}

//---------------------------------------------------------------------------------------------------
void TheGame::ApplyCameraTransform( const Camera3D& camera )
{
	// Put us in our preferred coordinate system: +X is east (forward), +Y is north, +Z is up
	g_myRenderer->Rotate( -90.f, 1.f, 0.f, 0.f ); // lean "forward" 90 degrees, to put +Z up (was +Y)
	g_myRenderer->Rotate( 90.f, 0.f, 0.f, 1.f ); // spin "left" 90 degrees, to put +X forward (was +Y)
	
	g_myRenderer->Rotate( -camera.m_orientation.m_rollDegreesAboutX,	1.f, 0.f, 0.f );
	g_myRenderer->Rotate( -camera.m_orientation.m_pitchDegreesAboutY,	0.f, 1.f, 0.f ); 	
	g_myRenderer->Rotate( -camera.m_orientation.m_yawDegreesAboutZ,	0.f, 0.f, 1.f );
	
	g_myRenderer->Translate( -camera.m_position.x, -camera.m_position.y, -camera.m_position.z );
	
}

//---------------------------------------------------------------------------------------------------
Vector2DF TheGame::GetMouseMovementSinceLastChecked()
{
	POINT cursorPos;
	GetCursorPos( &cursorPos );

	if(!m_lostFocus)
		SetCursorPos( m_centerCursorPos.x, m_centerCursorPos.y );
	else
		return Vector2DF(0.0f,0.0f);

	Vector2DI mouseDeltaInts( cursorPos.x - m_centerCursorPos.x, cursorPos.y - m_centerCursorPos.y );
	Vector2DF mouseDeltas( (float) mouseDeltaInts.x, (float) mouseDeltaInts.y );
	return mouseDeltas;
}

//---------------------------------------------------------------------------------------------------
void TheGame::UpdateCameraFromMouseAndKeyboard( Camera3D& camera, float deltaSeconds )
{
	// Update camera orientation (yaw and pitch only) from mouse x,y movement
	const float degreesPerMouseDelta = 0.04f;
	Vector2DF mouseDelta = GetMouseMovementSinceLastChecked();

	camera.m_orientation.m_yawDegreesAboutZ	-= (degreesPerMouseDelta * mouseDelta.x);
	camera.m_orientation.m_pitchDegreesAboutY	+= (degreesPerMouseDelta * mouseDelta.y);

	// Update camera position based on which (if any) movement keys are down
	ClampPitchDegrees( camera );
	float cameraYawRadians = DegreesToRadians( camera.m_orientation.m_yawDegreesAboutZ);
	camera.m_forwardVector = Vector3DF( cos( cameraYawRadians ) , sin( cameraYawRadians), 0.f );
	camera.m_cameraLeftXYVector = Vector3DF( cos( cameraYawRadians + DegreesToRadians(90.f) ), sin( cameraYawRadians + DegreesToRadians(90.f) ), 0.f );
	camera.UpdateCamera(deltaSeconds);

} 

//---------------------------------------------------------------------------------------------------
void TheGame::ClampPitchDegrees(Camera3D& camera)
{
	if( camera.m_orientation.m_pitchDegreesAboutY > 89.0f )
	{
		camera.m_orientation.m_pitchDegreesAboutY = 89.0f;
	}
	else if( camera.m_orientation.m_pitchDegreesAboutY < -89.0f )
	{
		camera.m_orientation.m_pitchDegreesAboutY = -89.0f;
	}
}

//---------------------------------------------------------------------------------------------------
Vector3DF TheGame::GetCameraPosition()
{
	return m_camera.m_position;
}

//---------------------------------------------------------------------------------------------------
void TheGame::SetGameState(GameState newGameState)
{
	m_activeGameState = newGameState;
}

//---------------------------------------------------------------------------------------------------
void TheGame::CookOBJToC23()
{
	return;
	if(!m_hasCookedFiles)
	{
		for (int index = 0; index < (int)m_modelsList.size(); index++)
		{
			std::string modelPath(m_modelsList[index]);
			modelPath += ".C23";

			BinaryBufferParser bbp;
			bbp.LoadBufferFromBinaryFile(modelPath);

			if(bbp.m_fileLoaded)
			{
				continue;
			}
			else
			{
				OBJLoader loader;
				std::string objModelPath(m_modelsList[index]);
				MyMesh* mesh = new MyMesh();
				objModelPath += ".OBJ";
				loader.LoadFile(objModelPath);
				loader.ParseVertexDataFromOBJFile(mesh);

				bbp.BuildBinaryBufferFromVertexData(mesh, objModelPath);
			}
		}
	}
	m_hasCookedFiles = true;
}

//---------------------------------------------------------------------------------------------------
void TheGame::FillModelList()
{
	m_modelsList.push_back("Data/Models/TutorialBox_Phong/Tutorial_Box");
	m_modelsList.push_back("Data/Models/Woman/Woman");
	m_modelsList.push_back("Data/Models/bunny");
	m_modelsList.push_back("Data/Models/Cube_vni_c");
	m_modelsList.push_back("Data/Models/axe01");
	m_modelsList.push_back("Data/Models/palm1");
	m_modelsList.push_back("Data/Models/Predator");
	m_modelsList.push_back("Data/Models/Ship");
	m_modelsList.push_back("Data/Models/simplified");
	m_modelsList.push_back("Data/Models/Tower");
}