#pragma once

#ifndef THEGAME_HPP
#define THEGAME_HPP

#define STBI_HEADER_FILE_ONLY

#include "Engine/Core/std_image.h"
#include "Engine/Core/Camera3D.hpp"
#include "Engine/Core/Time.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/Math/SriMath.hpp"
#include "Engine/DebugConsole/BitmapFont.hpp"

#include <vector>
#include <stdio.h>
// #include <Xinput.h>					// include the Xinput API
// #pragma comment( lib, "xinput" )	// Link in the xinput.lib static library
#pragma comment( lib, "glu32" ) // Link in the OpenGL32.lib static library


class Renderer;
class World;
class GraphicsDebugManager;
class DebugConsole;
class Light;
class Material;
class FrameBuffer;
class InputSystem;

//---------------------------------------------------------------------------------------------------
enum GameState
{
	STARTING_UP,
	LOAD_OBJ,
	LOAD_C23,
	COOK,
	LOADING_OBJ,
	LOADING_C23,
	COOKING_OBJ_TO_C23,
	SHUTTING_DOWN,
	QUITING,
	RENDERING
};

//---------------------------------------------------------------------------------------------------
class TheGame
{
public:
	TheGame(void);
	TheGame(const TheGame& gameToCopy);
	~TheGame(void);


	void CreateInputSystem(void* platformHandle);
	void InitilizeGame(void* platformHandle);
	void InitializeRenderer(void* platformHandle);
	void WaitForFrameAndSwapBuffers();
	void CreateWorld();

	void RunGame();
	void RunFrame();

	void Update( float deltaSeconds );

	void Render();
	void RenderToFrameBuffers();
	void RenderToScreen();
	void RenderScene();
	void RenderAxis(float axisLength = 50.f, float axisWidth = 5.f);

	void SetUpPerspectiveProjection();
	void ApplyCameraTransform(const Camera3D& camera);
	void UpdateCameraFromMouseAndKeyboard( Camera3D& camera, float deltaSeconds );
	Vector2DF GetMouseMovementSinceLastChecked();
	void ClampPitchDegrees(Camera3D& camera);
	float ConvertDegreesToRadians(const float yawDegrees);
	void CheckInput();
	Vector3DF GetCameraPosition();
	void ProcessInput();

	GameState GetGameState() const { return m_activeGameState; }
	void SetGameState(GameState newGameState);
	void CookOBJToC23();
	void FillModelList();

private:
	Camera3D m_camera;
	double m_timeSinceLastUpdate;
	Vector2DI m_centerCursorPos;
	float m_cameraMovingSpeed;
	bool m_lostFocus;
	World* m_world;
	Renderer* m_myRenderer;
	GraphicsDebugManager* m_myDebugManager;
	bool m_isEffectOn;

	GameState m_activeGameState;
	InputSystem* m_inputSystem;

	double m_previousFrameTimeSeconds;

public:
	DebugConsole* m_console;
	std::vector<Light*>	m_lights;
	std::vector<FrameBuffer*> m_frameBufferObjects;
	std::vector<std::string> m_modelsList;
	bool m_isGameRunning;
	bool m_isBinaryMode;
	bool m_hasCookedFiles;
	int	m_modelIndex;
	BitmapFont m_bitmapFont;
};

#endif
