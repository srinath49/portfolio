#include "World.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/Core/Camera3D.hpp"
#include "Engine/Core/Time.hpp"
#include "Engine/Input/InputSystem.hpp"
#include "Engine/Rendering/SpriteSheet.hpp"
#include "Engine/Rendering/Texture.hpp"
#include "Engine/Rendering/SriOGLRenderer.hpp"
#include "Engine/Rendering/SriShaderProgram.hpp"
#include "Engine/Rendering/OpenGlExtentions.hpp"
#include "Engine/Rendering/Material.hpp"
#include "Engine/FileIO/OBJLoader.hpp"
#include "Engine/FileIO/BinaryBufferParser.hpp"
#include "Engine/Rendering/MyModel.hpp"
#include "Engine/Core/MyMesh.hpp"

#include <string>



//---------------------------------------------------------------------------------------------------
World::World(Camera3D& camera) :
	m_camera(camera),
	m_buffer(nullptr),
	m_model(nullptr),
	m_currentTexureIndex(0)
{
	m_myRenderer = new SriOGLRenderer();
	
	int vertexId   = m_myRenderer->LoadShader("Data/Shaders/BasicVertexShaderWithSimpleLight.srishader.glsl", ST_VERTEX_SHADER);
	int fragmentId = m_myRenderer->LoadShader("Data/Shaders/BasicFragmentShaderWithSimpleLight.srishader.glsl", ST_FRAGMENT_SHADER);

	m_complexProgram = new SriShaderProgram(vertexId, "Data/Shaders/BasicVertexShaderWithSimpleLight.srishader.glsl", fragmentId, "Data/Shaders/BasicFragmentShaderWithSimpleLight.srishader.glsl");

	vertexId   = m_myRenderer->LoadShader("Data/Shaders/BasicVertexShader.srishader.glsl", ST_VERTEX_SHADER);
	fragmentId = m_myRenderer->LoadShader("Data/Shaders/BasicFragmentShader.srishader.glsl", ST_FRAGMENT_SHADER);

	m_program = new SriShaderProgram(vertexId, "Data/Shaders/BasicVertexShader.srishader.glsl", fragmentId, "Data/Shaders/BasicFragmentShader.srishader.glsl");
	
	vertexId   = m_myRenderer->LoadShader("Data/Shaders/PhongVertexShader.srishader.glsl", ST_VERTEX_SHADER);
	fragmentId = m_myRenderer->LoadShader("Data/Shaders/PhongFragmentShader.srishader.glsl", ST_FRAGMENT_SHADER);

	m_phongProgram = new SriShaderProgram(vertexId, "Data/Shaders/PhongVertexShader.srishader.glsl", fragmentId, "Data/Shaders/PhongFragmentShader.srishader.glsl");
	
	//m_textures.push_back("Data/Images/EmptyPixel.png");
	m_material = new Material(m_phongProgram->m_programId, m_textures);
	//m_material = new Material(m_complexProgram->m_programId, m_textures);
	//m_objFilePath = "Data/Models/Woman.obj";

	m_model = new MyModel();
	m_model->SetMaterial(m_material);

	m_basicMaterial = new Material(m_program->m_programId, m_textures);
	
	//m_textures.push_back("Data/Images/TestImage.png");
	
}

//---------------------------------------------------------------------------------------------------
World::~World(void)
{
	delete m_myRenderer;
}

//---------------------------------------------------------------------------------------------------
void World::Update(const Vector3DF& cameraPosition, float deltaSeconds)
{
	UNUSED(deltaSeconds);
	UNUSED(cameraPosition);
}

//---------------------------------------------------------------------------------------------------
void World::Render()
{
	DrawModel(m_model);
}

//---------------------------------------------------------------------------------------------------
void World::ProcessInput(InputSystem* inputSystem)
{
	m_material->ProcessInput(inputSystem);
	m_basicMaterial->ProcessInput(inputSystem);
	if (inputSystem && inputSystem->HasKeyJustBeenReleased(KEY_CODE_T))
	{
		if (m_currentTexureIndex == 0)
		{
			m_currentTexureIndex = 1;
		}
		else
		{
			m_currentTexureIndex = 0;
		}
	}
}

//---------------------------------------------------------------------------------------------------
void World::DrawModel(MyModel* modelToDraw)
{
	if (modelToDraw)
	{
		m_model->SetUpMyMaterial(m_camera.m_position);
		m_model->Render(GL_TRIANGLES);
	}
}

//---------------------------------------------------------------------------------------------------
void World::SetModelToLoad(const std::string& modelToLoad, bool isBinaryMode)
{
	std::string fileToLoad(modelToLoad);
	m_model->LoadModel(fileToLoad, isBinaryMode);
}
