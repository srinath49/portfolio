#pragma once

#ifndef WORLD_HPP
#define WORLD_HPP
//---------------------------------------------------------------------------------------------------

#include <map>
#include "Engine/Math/SriMath.hpp"
#include "Engine/Core/EngineCommons.hpp"
#include "Engine/Core/ModelVertex.hpp"

#include "Engine/FileIO/OBJLoader.hpp"
#include <set>
#include <vector>


//---------------------------------------------------------------------------------------------------
class Vector3DF;
class Vector2DI;
class Vector3DI;
class Camera3D;
class SpriteSheet;
class Renderer;
class AudioSystem;
class SriShaderProgram;
class Material;
class InputSystem;
class MyModel;

//---------------------------------------------------------------------------------------------------
class World
{
public:
	enum ShaderMode
	{
		SHADER_OFF,
		SHADER_BASIC,
		SHADER_COMPLEX
	};

private:
	World& operator= (const World& rhs);

public:
	World(Camera3D& camera);
	~World(void);

	void Render();
	void Update(const Vector3DF& cameraPosition, float deltaSeconds);
	void ProcessInput(InputSystem* inputSystem);
	void DrawModel(MyModel* modelToDraw);
	void SetModelToLoad(const std::string& modelToLoad, bool isBinaryMode);

public:
	Camera3D&					m_camera;	
	Renderer*					m_myRenderer;
	SriShaderProgram*			m_program;
	SriShaderProgram*			m_complexProgram;
	Material*					m_material;
	Material*					m_basicMaterial;
	std::vector<ModelVertex>	m_modelVertexes;
	std::vector<Vector3DF>		m_vertexPositions;
	unsigned char*				m_buffer;
	int							m_dataSize;
	std::string					m_objFilePath;
	MyModel*					m_model;
	OBJLoader					m_objLoader;
	std::vector<std::string>	m_textures;
	int							m_currentTexureIndex;
	SriShaderProgram* m_phongProgram;
};

#endif