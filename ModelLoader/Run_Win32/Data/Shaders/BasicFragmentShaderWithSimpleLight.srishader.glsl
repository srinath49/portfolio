#version 130

//---------------------------------------------------------------------------------------------------
in vec2 v_texCoords;
in vec3 v_color;
in vec3 v_vertex;
in vec3 v_tangent;
in vec3 v_bitangent;
in vec3 v_normal;

uniform sampler2D u_diffuseTexture;
uniform vec3 u_cameraPosition;

out vec4 o_fragColor;

//---------------------------------------------------------------------------------------------------
mat3 BuildMatrixFromTBN(vec3 tangent, vec3 bitangent, vec3 normal);
mat3 g_tangentToWorldMatrix;

vec4 g_diffuseColor;

float GetLightValue();

//---------------------------------------------------------------------------------------------------
void main (void)  
{
	g_tangentToWorldMatrix = BuildMatrixFromTBN(v_tangent, v_bitangent, v_normal);
	g_diffuseColor = texture(u_diffuseTexture, v_texCoords);   

	float lightValue = GetLightValue();
	vec4 currentColor;

	currentColor = g_diffuseColor;
	currentColor.rgb = clamp(v_color.rgb * g_diffuseColor.rgb, 0.0, 1.0);
	currentColor.rgb = vec3(currentColor.rgb * lightValue);
	o_fragColor = currentColor;
}

//---------------------------------------------------------------------------------------------------
float GetLightValue()
{
	vec3 normal = normalize(v_normal);

	vec3 vertexToCamera = normalize(u_cameraPosition - v_vertex);

	float lightValue = dot(vertexToCamera, normal);

	lightValue = clamp(lightValue, 0.0, 1.0);

	return lightValue;
}

//---------------------------------------------------------------------------------------------------
mat3 BuildMatrixFromTBN(vec3 tangent, vec3 bitangent, vec3 normal)
{
	tangent = normalize(tangent);
	bitangent = normalize(bitangent);
	normal = normalize(v_normal);
	mat3 matrixToBuild = mat3(tangent, bitangent, normal);
	return matrixToBuild;
}