#version 130

in vec3 a_vertexes;
in vec3 a_color;
in vec2 a_texCoords;
in vec3 a_tangent;
in vec3 a_bitangent;
in vec3 a_normals;

uniform float u_time;
uniform int u_effect;
uniform int u_debugInt;
uniform mat4 u_tangentToWorldMatrix;
uniform mat4 u_worldToClipMatrix;

out vec2 v_texCoords;
out vec3 v_color;
out vec3 v_vertex;
out vec3 v_tangent;
out vec3 v_bitangent;
out vec3 v_normal;


//---------------------------------------------------------------------------------------------------
void main(void)
{
	v_vertex = a_vertexes;
	v_color = a_color;
	v_texCoords = a_texCoords;
	v_tangent = a_tangent;
	v_bitangent = a_bitangent;
	v_normal = a_normals;

	gl_Position = u_worldToClipMatrix * (u_tangentToWorldMatrix * vec4(v_vertex, 1.0));
}