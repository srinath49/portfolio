#version 130

//---------------------------------------------------------------------------------------------------
in vec2 v_texCoords;
in vec3 v_colors;
in vec3 v_vertexes;
in vec3 v_tangent;
in vec3 v_bitangent;
in vec3 v_normal;

//---------------------------------------------------------------------------------------------------
const int MAX_LIGHTS = 16;
const float g_lightScale =  1.0;
const float PI = 3.14159265;
const float TWO_PI = 2.0 * PI;
//---------------------------------------------------------------------------------------------------
uniform sampler2D u_diffuseTexture;
uniform sampler2D u_normalTexture;
uniform sampler2D u_specularTexture;
uniform sampler2D u_emissiveTexture;
//---------------------------------------------------------------------------------------------------
uniform float u_time;
uniform float u_effectStartTime;
uniform float u_fogMinDistance;
uniform float u_fogMaxDistance;
uniform vec4  u_fogColorAndIntensity;
uniform vec3  u_cameraPosition;
uniform int   u_debugInt;
//---------------------------------------------------------------------------------------------------
uniform vec3  u_lightWorldPositions[MAX_LIGHTS];
uniform vec4  u_lightColors[MAX_LIGHTS];
uniform float u_lightInnerRadii[MAX_LIGHTS];
uniform float u_lightOuterRadii[MAX_LIGHTS];
uniform float u_lightInnerPenumbraDots[MAX_LIGHTS];
uniform float u_lightOuterPenumbraDots[MAX_LIGHTS];
uniform float u_lightAmbiences[MAX_LIGHTS];
uniform vec3  u_lightForwardVectors[MAX_LIGHTS];
uniform int   u_numLightsToProcess;
//---------------------------------------------------------------------------------------------------
out vec4 o_fragColor;

//---------------------------------------------------------------------------------------------------
vec3 ConvertRGBtoXYZ(vec3 rgb);

vec3 ConvertXYZtoRGB(vec3 xyz);

void SampleTextures();

vec3 GetNormalFromNormalMap(vec4 sampledNormalMapColor, mat3 objectToWorld);

vec3 ComputeTotalLightFromAllLights(out vec3 totalAmbiance);

float ComputeFogColorPercent();

float GetFogDensity();

vec3 GetSpecularColorToAdd();

mat3 BuildMatrixFromTBN(vec3 tangent, vec3 bitangent, vec3 normal);

float SmoothStep( float value );

float ComputePerlinNoiseValueAtPosition2D( vec2 position, float perlinNoiseGridCellSize, float amplitude, float persistance );

void ApplySpecialEffect();

//---------------------------------------------------------------------------------------------------
vec4 g_diffuseColor; 
vec4 g_normalColor;  
vec4 g_specularColor;
vec4 g_emissiveColor;
vec3 g_normalFromNormalMap;
mat3 g_tangentToWorldMatrix;

vec4 g_currentColor;

float effectStartTime = 0.0;
float effectLife = 10.0;
//---------------------------------------------------------------------------------------------------
void main (void)  
{
	SampleTextures();
	g_tangentToWorldMatrix = BuildMatrixFromTBN(v_tangent, v_bitangent, v_normal);
	g_normalFromNormalMap = GetNormalFromNormalMap(g_normalColor, g_tangentToWorldMatrix);
	
	vec3 totalAmbiance = vec3(0.0, 0.0, 0.0);
	vec3 totalLight = ComputeTotalLightFromAllLights(totalAmbiance);
	float fogColorPercent    = ComputeFogColorPercent();	
	float objectColorPercent = 1.0 - fogColorPercent;

	g_currentColor = vec4(vec3(totalLight * v_colors * g_diffuseColor.rgb), g_diffuseColor.a);

	g_currentColor = vec4(g_currentColor.rgb + g_emissiveColor.rgb, g_currentColor.a);

	g_currentColor = vec4(g_currentColor.rgb * objectColorPercent + u_fogColorAndIntensity.rgb * fogColorPercent, g_currentColor.a);
	
	if(u_debugInt == 6  || u_debugInt == 9)
	{
		ApplySpecialEffect();
	}

	o_fragColor = vec4(g_currentColor.rgb, g_currentColor.a);

}

//-----------------------------------------------------------------------------------------------
void ApplySpecialEffect()
{
	vec3 fragmentPosition = v_vertexes;
	float effectStartTime = u_effectStartTime;
	float timeEffectLasts = 0.25;
	float effectLifeSpan = effectStartTime + 0.25;
	float effectCoefficient = u_time;
	float effectAge = u_time;
	float effectSpeed = 3.5;
	float fragmentValueToDiscard = 0.7;
	vec4 effectColorToAdd = vec4(0.25, 0.30, 0.85, 0.0); // Should be a uniform

	vec2 perlinNoiseSamplePosition = v_vertexes.xy + vec2( 3.0 *effectCoefficient, 0.5 * effectCoefficient );
	float perlinNoiseValue = ComputePerlinNoiseValueAtPosition2D( fragmentPosition.xy, 2.0, 0.5, 0.5 );
	effectCoefficient =  60.0 * perlinNoiseValue;

	float sineCosineValues = sin( sin(u_time) - fragmentPosition.x ) + sin( 0.5 * fragmentPosition.z ) + cos( 1.5 * fragmentPosition.y );
	sineCosineValues -= cos( sin(u_time) + 0.5 * effectCoefficient + sineCosineValues );
	sineCosineValues += sin( -0.9 * fragmentPosition.x ) + cos( 1.7 * fragmentPosition.z + cos(u_time)) + sin( 0.9 * fragmentPosition.x );
	sineCosineValues *= cos( -1.2 * effectCoefficient + sineCosineValues );
	
	float howMuchEffectToApply = sineCosineValues * (1.0 - sineCosineValues) + sineCosineValues;
	howMuchEffectToApply *= howMuchEffectToApply;
	

	if( effectAge > effectLifeSpan )
	{
		howMuchEffectToApply *= 1.0 / (effectSpeed  * (effectAge - effectLifeSpan) ) ;
		effectColorToAdd = vec4(effectColorToAdd * howMuchEffectToApply);
		float otherEffectToApply = 1.0 - howMuchEffectToApply;
		effectColorToAdd += vec4(effectColorToAdd * otherEffectToApply);
		g_currentColor += vec4( effectColorToAdd);
	}
	else
	{
		if(howMuchEffectToApply >= fragmentValueToDiscard)
		{
			discard;
		}
	}
 }

//---------------------------------------------------------------------------------------------------
vec2 GetPseudoRandomNoiseUnitVector2D( int xPosition, int yPosition )
{
	const float ONE_OVER_MAX_POSITIVE_INT = (1.f / 2147483648.f);
	const float SCALE_FACTOR = ONE_OVER_MAX_POSITIVE_INT * TWO_PI;
	int position = xPosition + (yPosition * 57);
	int bits = (position << 13) ^ position;
	int pseudoRandomPositiveInt = (bits * ((bits * bits * 15731) + 789221) + 1376312589) & 0x7fffffff;
	float pseudoRandomFloatZeroToTwoPi = SCALE_FACTOR * pseudoRandomPositiveInt;

	// TODO: Rewrite this to use the randomized int to look up Vector2 from a cos/sin table; vectors don't need to be super-precise,
	//	and we certainly don't want to pay full price for cos/sin if this is merely going to be used for, say, Perlin noise gradiants.
	//	Note however that cos/sin are typically pretty fast on GPUs so this can probably stand as-is in shader code.
	return vec2( cos( pseudoRandomFloatZeroToTwoPi ), sin( pseudoRandomFloatZeroToTwoPi ) );
}

 //---------------------------------------------------------------------------
float ComputePerlinNoiseValueAtPosition2D( vec2 position, float perlinNoiseGridCellSize, float amplitude, float persistance )
{
	float totalPerlinNoise = 0.0;
	float invCurrentGridCellSize = 1.0 / perlinNoiseGridCellSize;
	const int PERLIN_OCTAVES = 5;
	for( int octave = 0; octave < PERLIN_OCTAVES; ++ octave )
	{
		vec2 perlinPosition = position * invCurrentGridCellSize;
		vec2 perlinPositionFloor = vec2( floor( perlinPosition.x ), floor( perlinPosition.y ) );
		ivec2 perlinCell = ivec2( int( perlinPositionFloor.x ), int( perlinPositionFloor.y ) );
		vec2 perlinPositionUV = perlinPosition - perlinPositionFloor;
		vec2 perlinPositionAntiUV = vec2( perlinPositionUV.x - 1.0, perlinPositionUV.y - 1.0 );
		float eastWeight = SmoothStep( perlinPositionUV.x );
		float northWeight = SmoothStep( perlinPositionUV.y );
		float westWeight = 1.0 - eastWeight;
		float southWeight = 1.0 - northWeight;

		vec2 southwestNoiseGradient = GetPseudoRandomNoiseUnitVector2D( perlinCell.x, perlinCell.y );
		vec2 southeastNoiseGradient = GetPseudoRandomNoiseUnitVector2D( perlinCell.x + 1, perlinCell.y );
		vec2 northeastNoiseGradient = GetPseudoRandomNoiseUnitVector2D( perlinCell.x + 1, perlinCell.y + 1 );
		vec2 northwestNoiseGradient = GetPseudoRandomNoiseUnitVector2D( perlinCell.x, perlinCell.y + 1 );

		float southwestDot = dot( southwestNoiseGradient, perlinPositionUV );
		float southeastDot = dot( southeastNoiseGradient, vec2( perlinPositionAntiUV.x, perlinPositionUV.y ) );
		float northeastDot = dot( northeastNoiseGradient, perlinPositionAntiUV );
		float northwestDot = dot( northwestNoiseGradient, vec2( perlinPositionUV.x, perlinPositionAntiUV.y ) );

		float southBlend = (eastWeight * southeastDot) + (westWeight * southwestDot);
		float northBlend = (eastWeight * northeastDot) + (westWeight * northwestDot);
		float fourWayBlend = (southWeight * southBlend) + (northWeight * northBlend);
		float perlinNoiseAtThisOctave = amplitude * fourWayBlend;
		totalPerlinNoise += perlinNoiseAtThisOctave;
		amplitude *= persistance;
		invCurrentGridCellSize *= 2.0;
	}

	return totalPerlinNoise;
}

//---------------------------------------------------------------------------------------------------
vec3 ConvertRGBtoXYZ(vec3 rgb)
{
	vec3 returnXYZ = vec3(2.0 * rgb) - 1.0; 
	return returnXYZ;
}

//---------------------------------------------------------------------------------------------------
vec3 ConvertXYZtoRGB(vec3 xyz)
{
	vec3 returnRGB = vec3(xyz + 1) * 0.5;
	return returnRGB;
}

//---------------------------------------------------------------------------------------------------
void SampleTextures()
{
	g_diffuseColor  = texture(u_diffuseTexture, v_texCoords);   
	g_normalColor   = texture(u_normalTexture, v_texCoords);
	g_specularColor = texture(u_specularTexture, v_texCoords);
	g_emissiveColor = texture(u_emissiveTexture, v_texCoords);
}

//---------------------------------------------------------------------------------------------------
vec3 GetNormalFromNormalMap(vec4 sampledNormalMapColor, mat3 objectToWorld)
{
	vec3 normalFromMap = ConvertRGBtoXYZ(sampledNormalMapColor.rgb);
	normalFromMap = objectToWorld * normalFromMap;
	return normalFromMap;
}

//---------------------------------------------------------------------------------------------------
vec3 ComputeTotalLightFromAllLights(out vec3 totalAmbiance)
{
	float lightFromNormalMap = 0.0;
	vec3  totalLight = vec3(0.0,0.0,0.0);
	float penumbraBrightness = 0.0;
	float attenuationFallOff = 0.0;
	float ambience = 0.0;
	float penumbraDot = 0.0;
	float returnTest = 0.0;
	float attenuationBrightness  = 0.0;

	for(int lightIndex = 0; lightIndex < u_numLightsToProcess; lightIndex++ )
	{
		vec3  forwardVector = normalize(u_lightForwardVectors[lightIndex]);
		vec3  lightColor = u_lightColors[lightIndex].rgb;
		float lightToPixelDistance = distance(v_vertexes, u_lightWorldPositions[lightIndex]);
		vec3  lightToPixelDirection = normalize(v_vertexes -  u_lightWorldPositions[lightIndex]);

		vec3 pixelToLightDirection = normalize(u_lightWorldPositions[lightIndex] - v_vertexes);
		lightFromNormalMap = dot(pixelToLightDirection, g_normalFromNormalMap);
		lightFromNormalMap = clamp(lightFromNormalMap, 0.0, 1.0);

		float outerRadius = u_lightOuterRadii[lightIndex];
		float innerRadius = u_lightInnerRadii[lightIndex];
		
		attenuationFallOff = smoothstep( -outerRadius, -innerRadius, -lightToPixelDistance );

		float innerPenumbraDot = u_lightInnerPenumbraDots[lightIndex];
		float outerPenumbraDot = u_lightOuterPenumbraDots[lightIndex];

		penumbraDot = dot(forwardVector, pixelToLightDirection);
		totalAmbiance += u_lightAmbiences[lightIndex] *u_lightColors[lightIndex].rgb ;
		penumbraBrightness = smoothstep(outerPenumbraDot, innerPenumbraDot, penumbraDot );
		
		totalLight = totalLight + (lightFromNormalMap * attenuationFallOff * penumbraBrightness * u_lightColors[lightIndex].rgb * u_lightColors[lightIndex].a);
	}
	//return vec3(penumbraBrightness, 0.0, 0.0);
	return totalLight;
}

//---------------------------------------------------------------------------
float SmoothStep( float value )
{
	float valueSquared = (value * value);
	float smoothValue = (3.0 * valueSquared) - (2.0 * value * valueSquared);
	return smoothValue;
}

//---------------------------------------------------------------------------------------------------
float ComputeFogColorPercent()
{
	vec3 cameraToPixelVector = u_cameraPosition - v_vertexes;
	float cameraToPixelDistance = distance(u_cameraPosition, v_vertexes);
	float fogFraction = (cameraToPixelDistance - u_fogMinDistance)/(u_fogMaxDistance - u_fogMinDistance);
	fogFraction = clamp(fogFraction, 0.0, 1.0);
	float fogColorPercent = fogFraction * u_fogColorAndIntensity.w;
	fogColorPercent = min (fogColorPercent, u_fogColorAndIntensity.w);
	fogColorPercent = clamp(fogColorPercent, 0.0, 1.0);
	return fogColorPercent;
}

//---------------------------------------------------------------------------------------------------
float GetFogDensity()
{
	float distanceToCamera = distance(u_cameraPosition, v_vertexes);
	float density = min( (distanceToCamera - u_fogMinDistance) / (u_fogMaxDistance - u_fogMinDistance), u_fogColorAndIntensity.w );
	return density;
}


//---------------------------------------------------------------------------------------------------
vec3 GetSpecularColorToAdd()
{
	float specularity = g_specularColor.r;
	float glossiness = g_specularColor.b;
	const float MAX_SPECULAR_POWER = 32.0;
	vec3 lightColor = u_lightColors[0].rgb;

	vec3 cameraToVertexDirection = normalize(v_vertexes - u_cameraPosition);
	vec3 idealDirectionToLight = reflect(g_normalColor.rgb, cameraToVertexDirection);
	
	vec3 directionToLight = normalize(vec3(-2.0, 0.5, 2.0) - v_vertexes);
	float specularIntensity = clamp(dot(directionToLight, idealDirectionToLight), 0.0, 1.0);
	
	specularIntensity = pow( specularIntensity, 1.0 + (MAX_SPECULAR_POWER * glossiness) );
	specularIntensity *= specularity;
	specularIntensity = clamp (specularIntensity, 0.0, 0.1);
	vec3 specularColor = lightColor * specularIntensity;
	return specularColor;
}


//---------------------------------------------------------------------------------------------------
mat3 BuildMatrixFromTBN(vec3 tangent, vec3 bitangent, vec3 normal)
{
	tangent = normalize(tangent);
	bitangent = normalize(bitangent);
	normal = normalize(normal);
	mat3 matrixToBuild = mat3(tangent, bitangent, normal);
	return matrixToBuild;
}