#version 130

in vec3 a_vertexes;
in vec3 a_color;
in vec2 a_texCoords;
in vec3 a_tangent;
in vec3 a_bitangent;
in vec3 a_normals;

uniform float u_time;
uniform int u_effect;
uniform int u_debugInt;
uniform mat4 u_objectToClipMatrix;

out vec2 v_texCoords;
out vec3 v_colors;
out vec3 v_vertexes;
out vec3 v_tangent;
out vec3 v_bitangent;
out vec3 v_normal;


//---------------------------------------------------------------------------------------------------
void main(void)
{
	v_vertexes = a_vertexes;
	v_colors = a_color;
	v_texCoords = a_texCoords;
	v_tangent = a_tangent;
	v_bitangent = a_bitangent;
	v_normal = a_normals;
	mat4 a_objectToClipMatrix = u_objectToClipMatrix;
	gl_Position = a_objectToClipMatrix * vec4(v_vertexes, 1.0);
}

