#version 130

//---------------------------------------------------------------------------------------------------
in vec2 v_texCoords;
in vec3 v_color;
in vec3 v_vertex;
in vec3 v_tangent;
in vec3 v_bitangent;
in vec3 v_normal;

//---------------------------------------------------------------------------------------------------
uniform sampler2D u_diffuseTexture;
uniform sampler2D u_normalTexture;
uniform sampler2D u_specularTexture;
uniform sampler2D u_emissiveTexture;
uniform vec3 u_cameraPosition;
uniform mat4 u_tangentToWorldMatrix;
uniform mat4 u_worldToClipMatrix;

out vec4 o_fragColor;

//---------------------------------------------------------------------------------------------------
mat3 BuildMatrixFromTBN();
float GetLightValue();
void SampleTextures();
vec3 GetNormalFromNormalMap(vec4 sampledNormalMapColor, mat3 tbnMatrix);
vec3 ConvertRGBtoXYZ(vec3 rgb);
vec3 ConvertXYZtoRGB(vec3 xyz);
float ComputeSpecAndGloss();
//---------------------------------------------------------------------------------------------------
vec4 g_diffuseColor; 
vec4 g_normalColor;  
vec4 g_specularColor;
vec4 g_emissiveColor;
vec3 g_normalFromNormalMap;
vec4 g_currentColor;
vec3 g_tangent;
vec3 g_bitangent;
vec3 g_normal;
mat3 g_tbnMatrix;
//---------------------------------------------------------------------------------------------------
void main (void)  
{
	SampleTextures();
	
	g_tbnMatrix = BuildMatrixFromTBN();
	g_normalFromNormalMap = GetNormalFromNormalMap(g_normalColor, g_tbnMatrix);

	float lightValue = clamp(GetLightValue(), 0.0	, 1.0);
	float emissiveStrength = g_specularColor.b;
	g_emissiveColor = vec4(g_diffuseColor.rgb * emissiveStrength, 0.0);

	float specGloss = ComputeSpecAndGloss();

	g_currentColor = g_diffuseColor;
	g_currentColor.rgb = clamp(v_color.rgb * g_diffuseColor.rgb, 0.0, 1.0);
	g_currentColor.rgb = clamp(g_currentColor.rgb * lightValue, 0.0, 1.0);
	
	o_fragColor = g_currentColor;
	//o_fragColor = vec4(g_normalFromNormalMap.rgb, 1.0); 
}

//---------------------------------------------------------------------------------------------------
float GetLightValue()
{
	vec3 normal = normalize(g_normalFromNormalMap);

	vec3 lightPos = u_cameraPosition;
	vec3 vertexToLight = normalize(lightPos - v_vertex);
	//vertexToLight = normalize(v_vertex - lightPos);
	float lightValue = dot(vertexToLight, g_normalFromNormalMap);

	lightValue = clamp(lightValue, 0.05, 1.0);

	return lightValue;
}

//---------------------------------------------------------------------------------------------------
mat3 BuildMatrixFromTBN()
{
	g_tangent = normalize(v_tangent);
	g_bitangent = normalize(v_bitangent);
	g_normal = normalize(v_normal);
	mat3 matrixToBuild = mat3(g_tangent, g_bitangent, g_normal);
	return matrixToBuild;
}


//---------------------------------------------------------------------------------------------------
vec3 GetNormalFromNormalMap(vec4 sampledNormalMapColor, mat3 tbnMatrix)
{
	vec3 normalFromMap = ConvertRGBtoXYZ(sampledNormalMapColor.rgb);
	normalFromMap = tbnMatrix * normalFromMap;
	return normalFromMap;
}

//---------------------------------------------------------------------------------------------------
vec3 ConvertRGBtoXYZ(vec3 rgb)
{
	vec3 returnXYZ = vec3(2.0 * rgb) - 1.0; 
	return returnXYZ;
}

//---------------------------------------------------------------------------------------------------
vec3 ConvertXYZtoRGB(vec3 xyz)
{
	vec3 returnRGB = vec3(xyz + 1) * 0.5;
	return returnRGB;
}


//---------------------------------------------------------------------------------------------------
void SampleTextures()
{
	g_diffuseColor  = texture(u_diffuseTexture,  v_texCoords);   
	g_normalColor   = texture(u_normalTexture,	 v_texCoords);
	g_specularColor = texture(u_specularTexture, v_texCoords);
}

//---------------------------------------------------------------------------------------------------
float ComputeSpecAndGloss()
{
	vec3 cameraToVertex		= normalize(vec3(u_cameraPosition - v_vertex));
	vec3 idealLightVector	= reflect(cameraToVertex, g_normal);
	vec3 lightPos			= vec3(50.0, 25.0, 100.0);
	vec3 lightToVertex		= normalize(v_vertex - lightPos);

	float actualLight	= clamp(dot(idealLightVector, lightToVertex), 0.0, 1.0);
	float reflectivity	= g_specularColor.r;
	float glossiness	= g_specularColor.g;

	float specularStrength = 1.0 + (glossiness * 64);
	
	float returnSpecularValue =  reflectivity * pow(actualLight, specularStrength);

	return returnSpecularValue;
}